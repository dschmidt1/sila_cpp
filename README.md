[![pipeline status](https://gitlab.com/SiLA2/sila_cpp/badges/master/pipeline.svg)](https://gitlab.com/SiLA2/sila_cpp/pipelines)
[![coverage report](https://gitlab.com/SiLA2/sila_cpp/badges/master/coverage.svg)](https://gitlab.com/SiLA2/sila_cpp/-/commits/master)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)

# SiLA C++
SiLA 2 C++ reference implementation.

|                |                                                        |
|----------------|--------------------------------------------------------|
| SiLA Homepage  | [https://sila-standard.com](https://sila-standard.com) |
| Chat group     | [Join the group on Slack](https://join.slack.com/t/sila-standard/shared_invite/enQtNDI0ODcxMDg5NzkzLTBhOTU3N2I0NTc4NDcyMjg2ZDIwZDc1Yjg4N2FmYjZkMzljZDAyZjAwNTc5OTVjYjIwZWJjYjA0YTY0NTFiNDA)|
| Maintainer     | Florian Meinicke ([florian.meinicke@cetoni.de](mailto:florian.meinicke@cetoni.de)) of [CETONI](https://www.cetoni.com) |

## Status
**Important**:
This code, in its current form, is mainly to give active SiLA 2 WG Members and other interested parties a reference point.
It might not comply with the latest version of the Standard, and much of its content might change in the future.

For more general information about the standard, we refer to the [sila_base](https://gitlab.com/SiLA2/sila_base) repository.

All active development is done through (feature) branches.
These can be generally expected to be unstable and contain unfinished code.  
The code in the [`master`](https://gitlab.com/SiLA2/sila_cpp/-/tree/master) branch of the repository can be considered to be the latest stable development version of the library.  
For the most recent version that is guaranteed to be stable check out the latest [tagged commit](https://gitlab.com/SiLA2/sila_cpp/-/tags) or visit the [releases page](https://gitlab.com/SiLA2/sila_cpp/-/releases) and get the code from there.

## Getting started
### Cloning
Clone the repository with its submodules either with HTTPs or SSH (choose the appropriate link in the clone button)
```shell
$ git clone --recursive https://gitlab.com/SiLA2/sila_cpp.git
```
To update the submodules run
```shell
$ git submodule update --recursive
```

### Building with CMake
`sila_cpp` uses CMake as its build system.
At least CMake version 3.13 is required to build `sila_cpp`.

#### Prerequisites
You'll need the following utilities and packages installed on your system in order to successfully build and run `sila_cpp`:

##### Linux:
- Qt5 Core (at least the development package (e.g. `qtbase5-dev` on Ubuntu) or using the [online installer])
- Avahi development libraries (e.g. `libavahi-client-dev` and `libavahi-common-dev` on Ubuntu)
- OpenSSL development libraries (e.g. `libssl-dev` on Ubuntu) and binaries (e.g. `openssl` on Ubuntu)
- gRPC v1.31.0 (be sure to use the system OpenSSL libraries or check [BUILDING.md](BUILDING.md) if you don't know how)

##### Windows:
- Qt5 Core (using the [online installer])
- [Apple Bonjour Print Services](https://support.apple.com/kb/DL999)
- OpenSSL sources and binaries (use Qt's [online installer] / Maintenance Tool in the section *Developer and Designer Tools*)
- gRPC v1.31.0 (be sure to use the aforementioned OpenSSL libraries or check [BUILDING.md](BUILDING.md) if you don't know how)

> #### Note:  
> **It is necessary that gRPC and sila_cpp have been built using the same build type since mixing static and shared libs can cause errors!**

Be sure to have everything in a place where CMake can find these libraries (e.g. by adding the directories to your `PATH` environment variable)

#### Building `sila_cpp`
The classic CMake build approach also applies here:
```shell
$ mkdir build && cd build
$ cmake ..
$ cmake --build .
```

The following options can be controlled either via the CMake GUI / `ccmake` or by setting `-D` options when invoking `cmake`:

| Option                     | Default | Description |
|----------------------------|---------|-------------|
| `BUILD_SHARED_LIBS`        | `ON`    | Build `sila_cpp` as a shared library |
| `SILA_CPP_BUILD_EXAMPLES`  | `ON`    | Build the executables in the `examples` directory |
| `SILA_CPP_BUILD_TESTS`     | `OFF`   | Build the Catch2 tests in the `test` directory |
| `SILA_CPP_ENABLE_COVERAGE` | `OFF`   | Enable test coverage reporting for gcc/clang |
| `SILA_CPP_USE_CONAN`       | `OFF`   | Use the Conan package manager for third party dependencies (see [Building with Conan section](#building-with-conan) for more info) |

#### Installing
To install `sila_cpp` locally you can simply run
```shell
$ cmake --install .
```
after building the library.

On Linux systems this requires `sudo` as the default install prefix is `/usr/local`.
To control the installation directory you can set the `CMAKE_INSTALL_PREFIX` variable during configuration to a different path:
```shell
$ cmake -DCMAKE_INSTALL_PREFIX=<path-to-desired-install-dir> .. 
```

#### Examples - Try it out
The [`HelloSiLA2`](examples/HelloSiLA2) example project is a good starting point if you want to familiarize yourself with SiLA 2 in C++.
Refer to the [README.md](examples/HelloSiLA2/README.md) there and also check out our [Wiki](https://gitlab.com/SiLA2/sila_cpp/-/wikis/home) for detailed information about how to use the `sila_cpp` library.

#### Testing
The tests in the `test` subdirectory use the Catch2 framework.
Currently Catch2 is always installed using the Conan package manager, so be sure to install Conan first, if you want to build the tests.
All other dependencies (gRPC, protobuf, Qt) don't work with Conan yet (see below).

### Building with Conan
> #### Note:
> **Conan is not yet supported!**  
> There are some difficulties with the gRPC recipe and some of its dependencies which makes it impossible to provide a uniform, cross-plattform way of building and distributing `sila_cpp` with Conan.
> The following is just a way of how it might work in the future.  
> For now we only fully support building `sila_cpp` with CMake.
> See [Building with CMake](#building-with-cmake) and [BUILDING.md](BUILDING.md) for more information

<details>
<summary> See how it might work in the future
</summary>

#### Add remote
We use the [Conan](https://www.conan.io) package manager to distribute this library as well as managing its dependencies (mainly gRPC and Qt).
To integrate `sila_cpp` into your own project you first need to add a new remote:
<!-- TODO: add actual remote -->
```cmd
> conan remote add sila2 "https://api.bintray/conan/sila2/sila_cpp"
```

#### Basic setup
Then you can simply install the `sila_cpp` conan package by running the following in your terminal:
```
> conan install sila_cpp/latest@sila2/stable
```

#### Project setup
If you handle multiple dependencies in your project, it is better to add a `conanfile.txt`
```
[requires]
sila_cpp/latest@sila_cpp/stable

[generators]
cmake
```

Complete the installation of requirements for your project by running:
```cmd
> mkdir build & cd build & conan install ..
```

If you are using CMake as your build system you can also include the `conan install` step into your `CMakeLists.txt` file.
Have a look into the [examples/HelloSiLA2](examples/HelloSiLA2) directory to see how you might do this in your project, as well.

#### Package options
| Option                   | Default | Possible Values     |
|--------------------------|---------|:-------------------:|
| `shared`                 | `True`  | [`True`, `False`]   |
| `with_qt`                | `True`  | [`True`, `False`]   |
| `qt_install_prefix_path` |         | ANY                 |

Since Qt is quite a big dependency and it might happen that you've already got Qt development packages installed on your system, you don't necessarily want to download and build it.
The `sila_cpp` conan package provides the option `with_qt` which you can simply set to `False`, if you don't want to install Qt from Conan.
However, in order to find the correct Qt installation you need to provide the path to Qt's install directory by setting the `qt_install_prefix_path` option.
For example:
```cmd
> conan install sila_cpp/latest@sila2/stable -o sila_cpp:with_qt=False -o sila_cpp:qt_install_prefix_path=C:/Qt/5.x.x/mingw73_64
```
or
```
[requires]
sila_cpp/latest@sila_cpp/stable

[options]
sila_cpp: with_qt=False
sila_cpp: qt_install_prefix_path=C:/Qt/5.x.x/mingw73_64
```
</details>


## License
This code is licensed under the [MIT License](License)

## Contributing
To get involved, read our [contributing docs](CONTRIBUTING.md).

Check out the [milestones](https://gitlab.com/SiLA2/sila_cpp/-/milestones) to find issues that you can help working on.
Feel free to also [create new issues](https://gitlab.com/SiLA2/sila_cpp/-/issues/new) if there is something missing from the library.

[online installer]: https://www.qt.io/download-qt-installer

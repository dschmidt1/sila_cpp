/**
 ** This file is part of the sila_cpp project.
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file    test_error_handling.cpp
/// \authors Florian Meinicke
/// \date    29.01.2020
/// \brief   Unit tests for the error handling implementation
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/framework/error_handling/ExecutionError.h>
#include <sila_cpp/framework/error_handling/FrameworkError.h>
#include <sila_cpp/framework/error_handling/ValidationError.h>
#include <sila_cpp/internal/Base64.h>

#include <catch2/catch.hpp>

//=============================================================================
SCENARIO("SiLA Validation Error", "[error_handling][validation_error]")
{
    const auto ParameterName = "Parameter";
    const auto DefaultMessage =
        "A validation error occurred during parameter evaluation.";
    auto Message = GENERATE(as<std::string>{}, "",
                            "This is a custom error message for a validation "
                            "error");

    GIVEN("A Validation Error with the parameter '"
          << ParameterName << "' and " << (Message.empty() ? "no" : "a")
          << " custom message")
    {
        auto DefaultValidationError =
            SiLA2::CValidationError(ParameterName, Message);

        WHEN("this error is raised using the gRPC framework")
        {
            const auto GrpcErrorMessage = DefaultValidationError.raise();

            THEN("the resulting grpc::Status contains the status code ABORTED")
            {
                REQUIRE(GrpcErrorMessage.error_code()
                        == grpc::StatusCode::ABORTED);
            }
            AND_THEN(
                "a SiLAError protobuf message is serialized into the error "
                "details")
            {
                auto SiLAErrorMessage = sila2::org::silastandard::SiLAError();
                auto ErrorMessage =
                    new sila2::org::silastandard::ValidationError();
                ErrorMessage->set_message(Message.empty() ? DefaultMessage :
                                                            Message);
                ErrorMessage->set_parameter(ParameterName);
                SiLAErrorMessage.set_allocated_validationerror(ErrorMessage);

                REQUIRE(SiLA2::internal::base64Decode(
                            GrpcErrorMessage.error_message())
                        == SiLAErrorMessage.SerializeAsString());
            }
        }
    }
}

//=============================================================================
SCENARIO("SiLA Defined Execution Error",
         "[error_handling][defined_execution_error]")
{
    const auto IdentifierName = "Identifier";
    const auto DefaultMessage =
        "A defined execution error occurred during command execution or property "
        "access.";
    auto Message = GENERATE(as<std::string>{}, "",
                            "This is a custom error message for a defined "
                            "execution error");

    GIVEN("A Defined Execution Error with the identifier '"
          << IdentifierName << "' and " << (Message.empty() ? "no" : "a")
          << " custom message")
    {
        auto DefaultDefinedExecutionError =
            SiLA2::CDefinedExecutionError(IdentifierName, Message);

        WHEN("this error is raised using the gRPC framework")
        {
            const auto GrpcErrorMessage = DefaultDefinedExecutionError.raise();

            THEN(
                "the serialized SiLAError protobuf message contains the custom "
                "message")
            {
                auto SiLAErrorMessage = sila2::org::silastandard::SiLAError();
                auto ErrorMessage =
                    new sila2::org::silastandard::DefinedExecutionError();
                ErrorMessage->set_message(Message.empty() ? DefaultMessage :
                                                            Message);
                ErrorMessage->set_erroridentifier(IdentifierName);
                SiLAErrorMessage.set_allocated_definedexecutionerror(
                    ErrorMessage);

                REQUIRE(SiLA2::internal::base64Decode(
                            GrpcErrorMessage.error_message())
                        == SiLAErrorMessage.SerializeAsString());
            }
        }
    }
}

//=============================================================================
SCENARIO("SiLA Undefined Execution Error",
         "[error_handling][undefined_execution_error]")
{
    const auto DefaultMessage =
        "An undefined execution error occurred during command execution or "
        "property access.";
    auto Message = GENERATE(as<std::string>{}, "",
                            "This is a custom error message for an undefined "
                            "execution error");

    GIVEN("A Undefined Execution Error with " << (Message.empty() ? "no" : "a")
                                              << " custom message")
    {
        auto DefaultUndefinedExecutionError =
            SiLA2::CUndefinedExecutionError(Message);

        WHEN("this error is raised using the gRPC framework")
        {
            const auto GrpcErrorMessage = DefaultUndefinedExecutionError.raise();

            THEN(
                "the serialized SiLAError protobuf message contains the custom "
                "message")
            {
                auto SiLAErrorMessage = sila2::org::silastandard::SiLAError();
                auto ErrorMessage =
                    new sila2::org::silastandard::UndefinedExecutionError();
                ErrorMessage->set_message(Message.empty() ? DefaultMessage :
                                                            Message);
                SiLAErrorMessage.set_allocated_undefinedexecutionerror(
                    ErrorMessage);

                REQUIRE(SiLA2::internal::base64Decode(
                            GrpcErrorMessage.error_message())
                        == SiLAErrorMessage.SerializeAsString());
            }
        }
    }
}

//=============================================================================
SCENARIO("SiLA Framework Error", "[error_handling][framework_error]")
{
    using ErrorType = SiLA2::CFrameworkError::ErrorType;
    const auto Type = GENERATE(ErrorType::COMMAND_EXECUTION_NOT_ACCEPTED,
                               ErrorType::INVALID_COMMAND_EXECUTION_UUID,
                               ErrorType::COMMAND_EXECUTION_NOT_FINISHED,
                               ErrorType::INVALID_METADATA,
                               ErrorType::NO_METADATA_ALLOWED);
    const auto DefaultMessage =
        "A framework error occurred while accessing the SiLA server.";
    auto Message = GENERATE(as<std::string>{}, "",
                            "This is a custom error message for a framework "
                            "error");

    GIVEN("A Framework Error with the error type "
          << SiLA2::CFrameworkError::errorTypeName(Type) << " and "
          << (Message.empty() ? "no" : "a") << " custom message")
    {
        auto DefaultFrameworkError = SiLA2::CFrameworkError(Type, Message);

        WHEN("this error is raised using the gRPC framework")
        {
            const auto GrpcErrorMessage = DefaultFrameworkError.raise();

            THEN("the resulting grpc::Status contains the status code ABORTED")
            {
                REQUIRE(GrpcErrorMessage.error_code()
                        == grpc::StatusCode::ABORTED);
            }
            AND_THEN(
                "a SiLAError protobuf message is serialized into the error "
                "details")
            {
                auto SiLAErrorMessage = sila2::org::silastandard::SiLAError();
                auto ErrorMessage =
                    new sila2::org::silastandard::FrameworkError();
                ErrorMessage->set_message(Message.empty() ? DefaultMessage :
                                                            Message);
                ErrorMessage->set_errortype(
                    static_cast<sila2::org::silastandard::FrameworkError_ErrorType>(
                        Type));
                SiLAErrorMessage.set_allocated_frameworkerror(ErrorMessage);

                REQUIRE(SiLA2::internal::base64Decode(
                            GrpcErrorMessage.error_message())
                        == SiLAErrorMessage.SerializeAsString());
            }
        }
    }
}

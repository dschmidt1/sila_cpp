/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   InteroperabilityTestServer.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   2020-08-26
/// \brief  Declaration of the InteroperabilityTestServer class
//============================================================================
#ifndef INTEROPERABILITYTESTSERVER_H
#define INTEROPERABILITYTESTSERVER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/FeatureID.h>
#include <sila_cpp/common/ServerInformation.h>
#include <sila_cpp/server/SiLAServer.h>

// include SiLA features
#include "DataTypeProvider/DataTypeProviderImpl.h"

/**
 * @brief A service with all standard test features that can be used to test
 * sila_cpp against the other reference implementations
 */
class InteroperabilityTestServer : public SiLA2::CSiLAServer
{
public:
    /**
     * @brief C'tor
     */
    explicit InteroperabilityTestServer(
        const SiLA2::CServerInformation& ServerInfo = {},
        const SiLA2::CServerAddress& Address = {});
};

#endif  // INTEROPERABILITYTESTSERVER_H

/**
 ** This file is part of the sila_cpp project.
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file    InteroperabilityTestClient-bin.cpp
/// \authors Florian Meinicke
/// \date    2020-08-24
/// \brief   Standalone InteroperabilityTest client application
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/ServerAddress.h>

#include "InteroperabilityTestClient.h"

#include <QCommandLineParser>
#include <QCoreApplication>

/**
 * @brief Looking for command line arguments
 *
 * @return CServerAddress The address of the SiLA server to connect with that was
 * set on the command line
 */
SiLA2::CServerAddress parseCommandLine()
{
    QCommandLineParser Parser;
    Parser.setApplicationDescription("A SiLA2 client: InteroperabilityTest");
    Parser.addHelpOption();
    Parser.addVersionOption();
    Parser.addOptions({{{"a", "server-ip-address"},
                        "The IP address of the SiLA server to connect with",
                        "ip-address",
                        "127.0.0.1"},
                       {{"p", "server-port"},
                        "The port on which the SiLA server should run",
                        "port",
                        "50052"}});

    if (!Parser.parse(QCoreApplication::arguments()))
    {
        qCritical() << "Could not read server information from command line:"
                    << Parser.errorText();
        qWarning() << "Assuming default values for all command line options!";
        return {};
    }

    if (Parser.isSet("help"))
    {
        Parser.showHelp();
    }

    if (Parser.isSet("version"))
    {
        Parser.showVersion();
    }

    return {Parser.value("server-ip-address"), Parser.value("server-port")};
}

//============================================================================
int main(int argc, char* argv[])
{
    QCoreApplication App{argc, argv};
    QCoreApplication::setApplicationName("InteroperabilityTestClient");
    QCoreApplication::setApplicationVersion("0.0.1");

    // Create and start the client
    auto SiLAClient = InteroperabilityTestClient{parseCommandLine()};

    // Commands
    SiLAClient.EchoStringValue();
    SiLAClient.EchoIntegerValue();
    SiLAClient.EchoRealValue();
    SiLAClient.EchoBooleanValue();
    SiLAClient.EchoBinaryValue();
    SiLAClient.EchoDateValue();
    SiLAClient.EchoTimeValue();
    SiLAClient.EchoTimeStampValue();
    SiLAClient.EchoStringList();
    SiLAClient.EchoIntegerList();
    SiLAClient.EchoStructureValues();
    SiLAClient.EchoMultipleMixedValues();
    SiLAClient.GetMultipleMixedValues();

    // Properties
    SiLAClient.Get_StringValue();
    SiLAClient.Get_IntegerValue();
    SiLAClient.Get_RealValue();
    SiLAClient.Get_BooleanValue();
    SiLAClient.Get_BinaryValue();
    SiLAClient.Get_DateValue();
    SiLAClient.Get_TimeValue();
    SiLAClient.Get_TimeStampValue();
    SiLAClient.Get_StringList();
    SiLAClient.Get_IntegerList();
    SiLAClient.Get_StructureValues();

    return 0;
}

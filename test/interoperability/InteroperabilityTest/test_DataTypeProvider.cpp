/**
 ** This file is part of the sila_cpp project.
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file    test_DataTypeProvider.cpp
/// \authors Florian Meinicke
/// \date    2020-08-24
/// \brief   Unit tests for the DataTypeProvider Feature of the interoperability
///          server
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/ServerAddress.h>
#include <sila_cpp/common/logging.h>

#include "InteroperabilityTestClient.h"
#include "test_globals.h"

#include <vector>

using namespace std;
using namespace SiLA2;
using namespace sila2::org::silastandard::test::datatypeprovider::v1;

/**
 * @brief Helpers to output vectors
 */
std::ostream& operator<<(std::ostream& os,
                         const std::vector<SiLA2::CString>& rhs);
std::ostream& operator<<(std::ostream& os,
                         const std::vector<SiLA2::CInteger>& rhs);

#include <catch2/catch.hpp>

//============================================================================
std::ostream& operator<<(std::ostream& os, const std::vector<SiLA2::CString>& rhs)
{
    os << "std::vector{";
    for (const auto& el : rhs)
    {
        os << "\"" << el.value() << "\", ";
    }
    os << "\b\b} ";
    return os;
}

//============================================================================
std::ostream& operator<<(std::ostream& os,
                         const std::vector<SiLA2::CInteger>& rhs)
{
    os << "std::vector{";
    for (const auto& el : rhs)
    {
        os << el.value() << ", ";
    }
    os << "\b\b} ";
    return os;
}

//=============================================================================
SCENARIO("DataTypeProvider Echo Commands Test", "[echo]")
{
    GIVEN(
        "A SiLA InteroperabilityTest client instance connected to the SiLA "
        "Server at "
        << ServerIP << ":" << ServerPort)
    {
        static auto TestClient =
            InteroperabilityTestClient{{ServerIP, ServerPort}};

        static const auto StringValue = CString{"test string - %$&öäü"};
        static const auto IntegerValue = CInteger{42};
        static const auto RealValue = CReal{3.1415926};
        static const auto BooleanValue = CBoolean{true};
        static const auto BinaryValue = CBinary{"0xff"};
        static const auto DateValue = CDate{22, 6, 2020};
        static const auto TimeValue = CTime{11, 55, 42};
        static const auto TimestampValue = CTimestamp{};
        static const auto StructureValues = AllDatatypeStructure{
            StringValue, IntegerValue, RealValue, BooleanValue,
            BinaryValue, DateValue,    TimeValue, TimestampValue};

        AND_GIVEN("the string value \"" << StringValue.value() << "\"")
        {
            WHEN("the Command EchoStringValue is called")
            {
                const auto ReceivedString =
                    TestClient.EchoStringValue(StringValue).receivedstringvalue();

                THEN("it returns the same value")
                {
                    REQUIRE(ReceivedString.value() == StringValue.value());
                }
            }
        }
        AND_GIVEN("the integer value " << IntegerValue.value())
        {
            WHEN("the Command EchoIntegerValue is called")
            {
                const auto ReceivedInteger =
                    TestClient.EchoIntegerValue(IntegerValue)
                        .receivedintegervalue();
                THEN("it returns the same value")
                {
                    REQUIRE(ReceivedInteger.value() == IntegerValue.value());
                }
            }
        }
        AND_GIVEN("the real value " << RealValue.value())
        {
            WHEN("the Command EchoRealValue is called")
            {
                const auto ReceivedReal =
                    TestClient.EchoRealValue(RealValue).receivedrealvalue();
                THEN("it returns the same value")
                {
                    REQUIRE(ReceivedReal.value() == RealValue.value());
                }
            }
        }
        AND_GIVEN("the boolean value " << BooleanValue.value())
        {
            WHEN("the Command EchoBooleanValue is called")
            {
                const auto ReceivedBoolean =
                    TestClient.EchoBooleanValue(BooleanValue)
                        .receivedbooleanvalue();
                THEN("it returns the same value")
                {
                    REQUIRE(ReceivedBoolean.value() == BooleanValue.value());
                }
            }
        }
        AND_GIVEN("the binary value " << BinaryValue.value())
        {
            WHEN("the Command EchoBinaryValue is called")
            {
                const auto ReceivedBinary =
                    TestClient.EchoBinaryValue(BinaryValue).receivedbinaryvalue();
                THEN("it returns the same value")
                {
                    REQUIRE(ReceivedBinary.value() == BinaryValue.value());
                }
            }
        }
        AND_GIVEN("the date value " << DateValue.day() << "-" << DateValue.month()
                                    << "-" << DateValue.year())
        {
            WHEN("the Command EchoDateValue is called")
            {
                const auto ReceivedDate =
                    TestClient.EchoDateValue(DateValue).receiveddatevalue();
                THEN("it returns the same value")
                {
                    REQUIRE(ReceivedDate.day() == DateValue.day());
                    REQUIRE(ReceivedDate.month() == DateValue.month());
                    REQUIRE(ReceivedDate.year() == DateValue.year());
                }
            }
        }
        AND_GIVEN("the time value " << TimeValue.hour() << ":"
                                    << TimeValue.minute() << ":"
                                    << TimeValue.second())
        {
            WHEN("the Command EchoTimeValue is called")
            {
                const auto ReceivedTime =
                    TestClient.EchoTimeValue(TimeValue).receivedtimevalue();
                THEN("it returns the same value")
                {
                    REQUIRE(ReceivedTime.hour() == TimeValue.hour());
                    REQUIRE(ReceivedTime.minute() == TimeValue.minute());
                    REQUIRE(ReceivedTime.second() == TimeValue.second());
                }
            }
        }
        AND_GIVEN("the timestamp value "
                  << TimestampValue.day() << "-" << TimestampValue.month() << "-"
                  << TimestampValue.year() << " " << TimestampValue.hour() << ":"
                  << TimestampValue.minute() << ":" << TimestampValue.second())
        {
            WHEN("the Command EchoTimestampValue is called")
            {
                const auto ReceivedTimestamp =
                    TestClient.EchoTimeStampValue(TimestampValue).receivedvalue();
                THEN("it returns the same value")
                {
                    REQUIRE(ReceivedTimestamp.day() == TimestampValue.day());
                    REQUIRE(ReceivedTimestamp.month() == TimestampValue.month());
                    REQUIRE(ReceivedTimestamp.year() == TimestampValue.year());
                    REQUIRE(ReceivedTimestamp.hour() == TimestampValue.hour());
                    REQUIRE(ReceivedTimestamp.minute()
                            == TimestampValue.minute());
                    REQUIRE(ReceivedTimestamp.second()
                            == TimestampValue.second());
                }
            }
        }
        /*
         * Disabled due to IMO missing response from EchoStructureValues Command
        AND_GIVEN("all of the previous values in a structure")
        {
            WHEN("the Command EchoStructureValues is called")
            {
                const auto ReceivedValues =
                    TestClient.EchoStructureValues(StructureValues)
                        .receivedvalues()
                        .alldatatypestructure();
                const auto& ReceivedStringValue =
                    ReceivedValues.stringstructureelement();
                const auto& ReceivedIntegerValue =
                    ReceivedValues.integerstructureelement();
                const auto& ReceivedRealValue =
                    ReceivedValues.realstructureelement();
                const auto& ReceivedBooleanValue =
                    ReceivedValues.booleanstructureelement();
                const auto& ReceivedBinaryValue =
                    ReceivedValues.binarystructureelement();
                const auto& ReceivedDateValue =
                    ReceivedValues.datestructureelement();
                const auto& ReceivedTimeValue =
                    ReceivedValues.timestructureelement();
                const auto& ReceivedTimestampValue =
                    ReceivedValues.timestampstructureelement();

                THEN("it returns the same String value")
                {
                    REQUIRE(ReceivedString.value() == StringValue.value());
                }
                THEN("it returns the same Integer value")
                {
                    REQUIRE(ReceivedInteger.value() == IntegerValue.value());
                }
                THEN("it returns the same Real value")
                {
                    REQUIRE(ReceivedReal.value() == RealValue.value());
                }
                THEN("it returns the same Boolean value")
                {
                    REQUIRE(ReceivedBoolean.value() == BooleanValue.value());
                }
                THEN("it returns the same Date value")
                {
                    REQUIRE(ReceivedDate.day() == DateValue.day());
                    REQUIRE(ReceivedDate.month() == DateValue.month());
                    REQUIRE(ReceivedDate.year() == DateValue.year());
                }
                THEN("it returns the same Time value")
                {
                    REQUIRE(ReceivedTime.hour() == TimeValue.hour());
                    REQUIRE(ReceivedTime.minute() == TimeValue.minute());
                    REQUIRE(ReceivedTime.second() == TimeValue.second());
                }

                THEN("it returns the same Timestamp value")
                {
                    REQUIRE(ReceivedTimestamp.day() == TimestampValue.day());
                    REQUIRE(ReceivedTimestamp.month() == TimestampValue.month());
                    REQUIRE(ReceivedTimestamp.year() == TimestampValue.year());
                    REQUIRE(ReceivedTimestamp.hour() == TimestampValue.hour());
                    REQUIRE(ReceivedTimestamp.minute()
                            == TimestampValue.minute());
                    REQUIRE(ReceivedTimestamp.second()
                            == TimestampValue.second());
                }
            }
        }
        */
        /*
         * Disabled due to IMO wrong return type for EchoMultipleMixedValues
         * Command
        AND_GIVEN("all of the previous values")
        {
            WHEN("the Command EchoMultipleMixedValues is called")
            {
                const auto ReceivedValues = TestClient.EchoMultipleMixedValues(
                    StringValue, IntegerValue, RealValue, BooleanValue, DateValue,
                    TimeValue, TimestampValue);
                const auto ReceivedString = ReceivedValues.stringvalue();
                const auto ReceivedInteger = ReceivedValues.integervalue();
                const auto ReceivedReal = ReceivedValues.realvalue();
                const auto ReceivedBoolean = ReceivedValues.booleanvalue();
                const auto ReceivedDate = ReceivedValues.datevalue();
                const auto ReceivedTime = ReceivedValues.timevalue();
                const auto ReceivedTimestamp = ReceivedValues.timestampvalue();

                THEN("it returns the same String value")
                {
                    REQUIRE(ReceivedString.value() == StringValue.value());
                }
                THEN("it returns the same Integer value")
                {
                    REQUIRE(ReceivedInteger.value() == IntegerValue.value());
                }
                THEN("it returns the same Real value")
                {
                    REQUIRE(ReceivedReal.value() == RealValue.value());
                }
                THEN("it returns the same Boolean value")
                {
                    REQUIRE(ReceivedBoolean.value() == BooleanValue.value());
                }
                THEN("it returns the same Date value")
                {
                    REQUIRE(ReceivedDate.day() == DateValue.day());
                    REQUIRE(ReceivedDate.month() == DateValue.month());
                    REQUIRE(ReceivedDate.year() == DateValue.year());
                }
                THEN("it returns the same Time value")
                {
                    REQUIRE(ReceivedTime.hour() == TimeValue.hour());
                    REQUIRE(ReceivedTime.minute() == TimeValue.minute());
                    REQUIRE(ReceivedTime.second() == TimeValue.second());
                }

                THEN("it returns the same Timestamp value")
                {
                    REQUIRE(ReceivedTimestamp.day() == TimestampValue.day());
                    REQUIRE(ReceivedTimestamp.month() == TimestampValue.month());
                    REQUIRE(ReceivedTimestamp.year() == TimestampValue.year());
                    REQUIRE(ReceivedTimestamp.hour() == TimestampValue.hour());
                    REQUIRE(ReceivedTimestamp.minute()
                            == TimestampValue.minute());
                    REQUIRE(ReceivedTimestamp.second()
                            == TimestampValue.second());
                }
            }
        }
        */
    }
}

//=============================================================================
SCENARIO("DataTypeProvider Properties Test", "[datatypes]")
{
    GIVEN(
        "A SiLA InteroperabilityTest client instance connected to the SiLA "
        "Server at "
        << ServerIP << ":" << ServerPort)
    {
        static auto TestClient =
            InteroperabilityTestClient{{ServerIP, ServerPort}};

        static const auto ExpectedStringValue =
            CString{"SiLA2_Test_String_Value"};
        static const auto ExpectedIntegerValue = CInteger{42};
        static const auto ExpectedRealValue = CReal{3.1415926};
        static const auto ExpectedBooleanValue = CBoolean{true};
        static const auto ExpectedBinaryValue =
            CBinary{"SiLA2_Test_String_Value"};
        static const auto ExpectedDateValue = CDate{24, 8, 2019};
        static const auto ExpectedTimeValue = CTime{12, 34, 56};
        static const auto ExpectedTimestampValue =
            CTimestamp{12, 34, 56, 24, 8, 2018};
        static const auto ExpectedStringList =
            vector<CString>{{"SiLA2"}, {"is"}, {"great"}};
        static const auto ExpectedIntegerList = vector<CInteger>{1, 2, 3};

        WHEN("we request the Property StringValue from the server")
        {
            const auto StringValue = TestClient.Get_StringValue().stringvalue();

            THEN("we expect the value to be " << ExpectedStringValue.value())
            {
                REQUIRE(StringValue.value() == ExpectedStringValue.value());
            }
        }
        WHEN("we request the Property IntegerValue from the server")
        {
            const auto IntegerValue =
                TestClient.Get_IntegerValue().integervalue();

            THEN("we expect the value to be " << ExpectedIntegerValue.value())
            {
                REQUIRE(IntegerValue.value() == ExpectedIntegerValue.value());
            }
        }
        WHEN("we request the Property RealValue from the server")
        {
            const auto RealValue = TestClient.Get_RealValue().realvalue();

            THEN("we expect the value to be " << ExpectedRealValue.value())
            {
                REQUIRE(RealValue.value() == ExpectedRealValue.value());
            }
        }
        WHEN("we request the Property BooleanValue from the server")
        {
            const auto BooleanValue =
                TestClient.Get_BooleanValue().booleanvalue();

            THEN("we expect the value to be " << ExpectedBooleanValue.value())
            {
                REQUIRE(BooleanValue.value() == ExpectedBooleanValue.value());
            }
        }
        WHEN("we request the Property BinaryValue from the server")
        {
            const auto BinaryValue = TestClient.Get_BinaryValue().binaryvalue();

            THEN("we expect the value to be " << ExpectedBinaryValue.value())
            {
                REQUIRE(BinaryValue.value() == ExpectedBinaryValue.value());
            }
        }
        WHEN("we request the Property DateValue from the server")
        {
            const auto DateValue = TestClient.Get_DateValue().datevalue();

            THEN("we expect the value to be " << ExpectedDateValue.day() << "-"
                                              << ExpectedDateValue.month() << "-"
                                              << ExpectedDateValue.year())
            {
                REQUIRE(DateValue.day() == ExpectedDateValue.day());
                REQUIRE(DateValue.month() == ExpectedDateValue.month());
                REQUIRE(DateValue.year() == ExpectedDateValue.year());
            }
        }
        WHEN("we request the Property TimeValue from the server")
        {
            const auto TimeValue = TestClient.Get_TimeValue().timevalue();

            THEN("we expect the value to be " << ExpectedTimeValue.hour() << ":"
                                              << ExpectedTimeValue.minute() << ":"
                                              << ExpectedTimeValue.second())
            {
                REQUIRE(TimeValue.hour() == ExpectedTimeValue.hour());
                REQUIRE(TimeValue.minute() == ExpectedTimeValue.minute());
                REQUIRE(TimeValue.second() == ExpectedTimeValue.second());
            }
        }
        WHEN("we request the Property TimestampValue from the server")
        {
            const auto TimestampValue =
                TestClient.Get_TimeStampValue().timestampvalue();

            THEN("we expect the value to be "
                 << ExpectedTimestampValue.day() << "-"
                 << ExpectedTimestampValue.month() << "-"
                 << ExpectedTimestampValue.year() << " "
                 << ExpectedTimestampValue.hour() << ":"
                 << ExpectedTimestampValue.minute() << ":"
                 << ExpectedTimestampValue.second())
            {
                REQUIRE(TimestampValue.day() == ExpectedTimestampValue.day());
                REQUIRE(TimestampValue.month() == ExpectedTimestampValue.month());
                REQUIRE(TimestampValue.year() == ExpectedTimestampValue.year());
                REQUIRE(TimestampValue.hour() == ExpectedTimestampValue.hour());
                REQUIRE(TimestampValue.minute()
                        == ExpectedTimestampValue.minute());
                REQUIRE(TimestampValue.second()
                        == ExpectedTimestampValue.second());
            }
        }
        WHEN("we request the Property StringList from the server")
        {
            const auto StringList = TestClient.Get_StringList().stringlist();
            const auto ExpectedListLength = ExpectedStringList.size();

            THEN("we expect the length of the list to be " << ExpectedListLength)
            {
                REQUIRE(StringList.size() == ExpectedListLength);
                AND_THEN("we expect the list to be " << ExpectedStringList)
                {
                    for (size_t i = 0; i < ExpectedListLength; ++i)
                    {
                        REQUIRE(StringList.at(i).value()
                                == ExpectedStringList.at(i).value());
                    }
                }
            }
        }
        WHEN("we request the Property IntegerList from the server")
        {
            const auto IntegerList = TestClient.Get_IntegerList().integerlist();
            const auto ExpectedListLength = ExpectedIntegerList.size();

            THEN("we expect the length of the list to be " << ExpectedListLength)
            {
                REQUIRE(IntegerList.size() == ExpectedListLength);
                AND_THEN("we expect the list to be " << ExpectedIntegerList)
                {
                    for (size_t i = 0; i < ExpectedListLength; ++i)
                    {
                        REQUIRE(IntegerList.at(i).value()
                                == ExpectedIntegerList.at(i).value());
                    }
                }
            }
        }
        WHEN("we request the Property StructureValues from the server")
        {
            const auto StructureValues = TestClient.Get_StructureValues()
                                             .structurevalues()
                                             .alldatatypestructure();
            const auto& StringValue = StructureValues.stringstructureelement();
            const auto& IntegerValue = StructureValues.integerstructureelement();
            const auto& RealValue = StructureValues.realstructureelement();
            const auto& BooleanValue = StructureValues.booleanstructureelement();
            const auto& BinaryValue = StructureValues.binarystructureelement();
            const auto& DateValue = StructureValues.datestructureelement();
            const auto& TimeValue = StructureValues.timestructureelement();
            const auto& TimestampValue =
                StructureValues.timestampstructureelement();

            THEN("we expect the String value to be "
                 << ExpectedStringValue.value())
            {
                REQUIRE(StringValue.value() == ExpectedStringValue.value());
            }
            THEN("we expect the Integer value to be "
                 << ExpectedIntegerValue.value())
            {
                REQUIRE(IntegerValue.value() == ExpectedIntegerValue.value());
            }
            THEN("we expect the Real value to be " << ExpectedRealValue.value())
            {
                REQUIRE(RealValue.value() == ExpectedRealValue.value());
            }
            THEN("we expect the Boolean value to be "
                 << ExpectedBooleanValue.value())
            {
                REQUIRE(BooleanValue.value() == ExpectedBooleanValue.value());
            }
            THEN("we expect the Binary value to be "
                 << ExpectedBinaryValue.value())
            {
                REQUIRE(BinaryValue.value() == ExpectedBinaryValue.value());
            }
            THEN("we expect the Date value to be "
                 << ExpectedDateValue.day() << "-" << ExpectedDateValue.month()
                 << "-" << ExpectedDateValue.year())
            {
                REQUIRE(DateValue.day() == ExpectedDateValue.day());
                REQUIRE(DateValue.month() == ExpectedDateValue.month());
                REQUIRE(DateValue.year() == ExpectedDateValue.year());
            }
            THEN("we expect the Time value to be "
                 << ExpectedTimeValue.hour() << ":" << ExpectedTimeValue.minute()
                 << ":" << ExpectedTimeValue.second())
            {
                REQUIRE(TimeValue.hour() == ExpectedTimeValue.hour());
                REQUIRE(TimeValue.minute() == ExpectedTimeValue.minute());
                REQUIRE(TimeValue.second() == ExpectedTimeValue.second());
            }
            THEN("we expect the Timestamp value to be "
                 << ExpectedTimestampValue.day() << "-"
                 << ExpectedTimestampValue.month() << "-"
                 << ExpectedTimestampValue.year() << " "
                 << ExpectedTimestampValue.hour() << ":"
                 << ExpectedTimestampValue.minute() << ":"
                 << ExpectedTimestampValue.second())
            {
                REQUIRE(TimestampValue.day() == ExpectedTimestampValue.day());
                REQUIRE(TimestampValue.month() == ExpectedTimestampValue.month());
                REQUIRE(TimestampValue.year() == ExpectedTimestampValue.year());
                REQUIRE(TimestampValue.hour() == ExpectedTimestampValue.hour());
                REQUIRE(TimestampValue.minute()
                        == ExpectedTimestampValue.minute());
                REQUIRE(TimestampValue.second()
                        == ExpectedTimestampValue.second());
            }
        }
    }
}

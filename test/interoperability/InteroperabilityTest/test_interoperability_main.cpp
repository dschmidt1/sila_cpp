/**
 ** This file is part of the sila_cpp project.
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file    test_DataTypeProvider.cpp
/// \authors Florian Meinicke
/// \date    2020-08-26
/// \brief   Standalone unit test application for the interoperability tests
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/ServerAddress.h>

#include "InteroperabilityTestServer.h"
#include "test_globals.h"

#include <QElapsedTimer>

#define CATCH_CONFIG_RUNNER
#include <catch2/catch.hpp>

// variables we want to be able to set from the command line
std::string ServerPort = "50051";
std::string ServerIP = "localhost";
std::string ServerType = "sila_cpp";
std::string ReportFilename = "DataTypeProvider_report.csv";

int main(int argc, char* argv[])
{
    auto CatchSession = Catch::Session{};

    // Build a new parser on top of Catch's
    using namespace Catch::clara;
    auto CommandLine =
        CatchSession.cli()
        | Opt(ServerPort, "port")["-p"]["--port"](
            "Port of the SiLA Server to address (default: " + ServerPort + ")")
        | Opt(ServerIP, "ip")["-i"]["--ip"](
            "IP of the SiLA Server to address (default: " + ServerIP + ")")
        | Opt(ServerType, "type")["--type"](
            "Type of the SiLA Server to address (default: " + ServerType + ")")
        | Opt(ReportFilename, "name")["-r"]["--report-filename"](
            "Name of the CSV file for the test result output (default: "
            + ReportFilename + ")");

    // Pass the new composite back to Catch
    CatchSession.cli(CommandLine);

    // Let Catch parse the command line
    const auto Error = CatchSession.applyCommandLine(argc, argv);
    if (Error)
    {
        return Error;
    }

    // The server to which the clients below can connect to
    auto TestServer = InteroperabilityTestServer{
        {"InteroperabilityTest", "TestServer"}, {"[::]", ServerPort}};
    TestServer.run(false);

    return CatchSession.run(argc, argv);
}

class CSVReporter : public Catch::StreamingReporterBase<CSVReporter>
{
public:
    explicit CSVReporter(const Catch::ReporterConfig& config)
        : Catch::StreamingReporterBase<CSVReporter>{config}
    {
        ///
        /// Generate the header of the results file - this will be moved to test
        /// manager script, once everything is running
        ///
        stream << "Server; Client; TestName; NumFailures; Failures; "
                  "TestDuration\n";
        m_Timer.start();
    }

    ~CSVReporter() override;

    static std::string getDescription()
    {
        return "Reports test results in CSV format for the SiLA 2 "
               "interoperability tests";
    }

    void assertionStarting(const Catch::AssertionInfo&) override {}

    bool assertionEnded(const Catch::AssertionStats&) override { return true; }

    void testCaseEnded(const Catch::TestCaseStats& Stats) override
    {
        // Name without the 'Scenario:' prefix
        const auto TestName =
            Stats.testInfo.tagsAsString() + Stats.testInfo.name.substr(9);
        const auto NumFails = Stats.totals.assertions.failed;
        // clang-format off
        stream << ServerType   << "; "
               << "sila_cpp"   << "; "
               << TestName     << "; "
               << NumFails     << "; "
               << "<failures>" << "; "
               << std::to_string(m_Timer.restart()) << '\n';
        // clang-format on
        StreamingReporterBase::testCaseEnded(Stats);
    }

private:
    QElapsedTimer m_Timer{};
};

#ifdef CATCH_IMPL
CSVReporter::~CSVReporter() = default;
#endif

CATCH_REGISTER_REPORTER("csv_interop", CSVReporter)

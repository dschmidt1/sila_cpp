/**
 ** This file is part of the sila_cpp project.
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file    test_error_handling.cpp
/// \authors Florian Meinicke
/// \date    17.01.2020
/// \brief   Unit tests for the data types implementation
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/data_types.h>

#include <catch2/catch.hpp>
#include <limits>

//=============================================================================
SCENARIO("Construct a SiLA String from a c-string", "[data_types][string]")
{
    GIVEN("A C-string value")
    {
        auto val = "Hello World";

        WHEN("a SiLA2::CString is constructed from the value")
        {
            auto String = SiLA2::CString{val};
            auto SiLAString = sila2::org::silastandard::String{};
            SiLAString.set_value(val);

            THEN("this object has the same value as the SiLAFramework String")
            {
                REQUIRE(String.value() == SiLAString.value());
            }
            AND_THEN("the underlying protobuf messages are equal")
            {
                REQUIRE(String.toProtoMessage().SerializeAsString()
                        == SiLAString.SerializeAsString());
            }
        }
    }
}

SCENARIO("Construct a SiLA String from a std::string", "[data_types][string]")
{
    GIVEN("A C++ std::string value")
    {
        auto val = std::string{"Hello World"};

        WHEN("a SiLA2::CString is constructed from the value")
        {
            auto String = SiLA2::CString{val};
            auto SiLAString = sila2::org::silastandard::String{};
            SiLAString.set_value(val);

            THEN("this object has the same value as the SiLAFramework String")
            {
                REQUIRE(String.value() == SiLAString.value());
            }
            AND_THEN("the underlying protobuf messages are equal")
            {
                REQUIRE(String.toProtoMessage().SerializeAsString()
                        == SiLAString.SerializeAsString());
            }
        }
    }
}

TEMPLATE_TEST_CASE("Modify a SiLA String", "[data_types][string]", std::string,
                   QString)
{
    GIVEN("A SiLA2::CString value")
    {
        auto String = SiLA2::CString{"Hello"};

        AND_GIVEN("a value to append to the string")
        {
            const auto World = TestType{" World"};

            WHEN("this value is appended to the string")
            {
                String.append(World);

                THEN(
                    "the resulting string contains the concatenated version of "
                    "these strings")
                {
                    REQUIRE(String.value() == "Hello World");
                }
            }
        }
    }
}

//=============================================================================
SCENARIO("Construct and modify a SiLA Integer from an int64_t",
         "[data_types][int]")
{
    GIVEN("An integer value")
    {
        int64_t val = 42;

        WHEN("a SiLA2::CInteger is constructed from the value")
        {
            auto Integer = SiLA2::CInteger{val};
            auto SiLAInteger = sila2::org::silastandard::Integer{};
            SiLAInteger.set_value(val);

            THEN("this object has the same value as the SiLAFramework Integer")
            {
                REQUIRE(Integer.value() == SiLAInteger.value());
            }
            AND_THEN("the underlying protobuf messages are equal")
            {
                REQUIRE(Integer.toProtoMessage().SerializeAsString()
                        == SiLAInteger.SerializeAsString());
            }
            // Modification
            AND_WHEN("the value is increased with the postfix ++")
            {
                const auto PreviousInteger = Integer;
                THEN("the returned value is the same")
                {
                    REQUIRE(Integer++ == PreviousInteger);
                    AND_THEN("the increased value is 1 more than before")
                    {
                        REQUIRE(Integer == PreviousInteger + 1);
                    }
                }
            }
            AND_WHEN("the value is increased with the prefix ++")
            {
                const auto PreviousInteger = Integer;
                THEN("the returned value is 1 more than before")
                {
                    REQUIRE(++Integer == PreviousInteger + 1);
                    AND_THEN("the increased value is 1 more than before")
                    {
                        REQUIRE(Integer == PreviousInteger + 1);
                    }
                }
            }
            AND_WHEN("the value is decreased with the postfix --")
            {
                const auto PreviousInteger = Integer;
                THEN("the returned value is the same")
                {
                    REQUIRE(Integer-- == PreviousInteger);
                    AND_THEN("the decreased value is 1 less than before")
                    {
                        REQUIRE(Integer == PreviousInteger - 1);
                    }
                }
            }
            AND_WHEN("the value is decreased with the prefix --")
            {
                const auto PreviousInteger = Integer;
                THEN("the returned value is 1 less than before")
                {
                    REQUIRE(--Integer == PreviousInteger - 1);
                    AND_THEN("the decreased value is 1 less than before")
                    {
                        REQUIRE(Integer == PreviousInteger - 1);
                    }
                }
            }
        }
    }
}

//=============================================================================
SCENARIO("Construct a SiLA Real from a double", "[data_types][real]")
{
    GIVEN("A double value")
    {
        double val = 3.1415926;

        WHEN("a SiLA2::CReal is constructed from the value")
        {
            auto Real = SiLA2::CReal{val};
            auto SiLAReal = sila2::org::silastandard::Real{};
            SiLAReal.set_value(val);

            THEN("this object has the same value as the SiLAFramework Real")
            {
                REQUIRE(Real.value() == SiLAReal.value());
            }
            AND_THEN("the underlying protobuf messages are equal")
            {
                REQUIRE(Real.toProtoMessage().SerializeAsString()
                        == SiLAReal.SerializeAsString());
            }
        }
    }
}

//=============================================================================
SCENARIO("Construct a SiLA Boolean from a bool", "[data_types][bool]")
{
    GIVEN("A boolean value")
    {
        auto val = true;

        WHEN("a SiLA2::CBoolean is constructed from the value")
        {
            auto Boolean = SiLA2::CBoolean{val};
            auto SiLABoolean = sila2::org::silastandard::Boolean{};
            SiLABoolean.set_value(val);

            THEN("this object has the same value as the SiLAFramework Boolean")
            {
                REQUIRE(Boolean.value() == SiLABoolean.value());
            }
            AND_THEN("the underlying protobuf messages are equal")
            {
                REQUIRE(Boolean.toProtoMessage().SerializeAsString()
                        == SiLABoolean.SerializeAsString());
            }
        }
    }
}

//=============================================================================
// SCENARIO("Construct a SiLA Binary from a string", "[data_types][binary]")
//{
//    GIVEN("A string value")
//    {
//        auto val = std::string{"binary"};
//
//        WHEN("a SiLA2::CBinary is constructed from the value")
//        {
//            auto Binary = SiLA2::CBinary{val};
//            auto SiLABinary = sila2::org::silastandard::Binary{};
//            SiLABinary.set_value(val);
//
//            THEN("this object has the same value as the SiLAFramework Binary")
//            {
//                REQUIRE(Binary.value() == SiLABinary.value());
//            }
//            AND_THEN("the underlying protobuf messages are equal")
//            {
//                REQUIRE(
//                    sila2::org::silastandard::Binary{Binary}.SerializeAsString()
//                    == SiLABinary.SerializeAsString());
//            }
//        }
//    }
//}

//=============================================================================
SCENARIO("Construct and modify a SiLA Timezone", "[data_types][timezone]")
{
    GIVEN("Two integer values for hours and minutes")
    {
        int32_t hours = GENERATE(take(10, random(-12, 12)));
        uint32_t minutes = GENERATE(take(10, random(0, 59)));

        WHEN("a SiLA2::CTimezone is constructed from " << hours << ":" << minutes)
        {
            auto Timezone = SiLA2::CTimezone{hours, minutes};
            auto SiLATimezone = sila2::org::silastandard::Timezone{};
            SiLATimezone.set_hours(hours);
            SiLATimezone.set_minutes(minutes);

            THEN(
                "this object has the same values as the SiLAFramework "
                "Timezone")
            {
                REQUIRE(Timezone.hours() == SiLATimezone.hours());
                REQUIRE(Timezone.minutes() == SiLATimezone.minutes());
            }
            AND_THEN("the underlying protobuf messages are equal")
            {
                REQUIRE(Timezone.toProtoMessage().SerializeAsString()
                        == SiLATimezone.SerializeAsString());
            }
            // Modification
            AND_WHEN("the hours are increased by 1")
            {
                if (hours <= 11)
                {
                    AND_WHEN("this results in a valid timezone")
                    {
                        REQUIRE_NOTHROW(Timezone.setHours(Timezone.hours() + 1));
                        THEN("the hours are now " << hours + 1)
                        {
                            REQUIRE(Timezone.hours() == hours + 1);
                        }
                    }
                }
                else
                {
                    AND_WHEN("this would result in an invalid date")
                    {
                        REQUIRE_THROWS_AS(Timezone.setHours(Timezone.hours() + 1),
                                          SiLA2::CInvalidTimezone);
                        THEN("the hours stay the same")
                        {
                            REQUIRE(Timezone.hours() == hours);
                        }
                    }
                }
            }
            AND_WHEN("the minutes are decreased by 10")
            {
                if (minutes >= 10)
                {
                    AND_WHEN("this results in a valid timezone")
                    {
                        REQUIRE_NOTHROW(
                            Timezone.setMinutes(Timezone.minutes() - 10));
                        THEN("the minutes are now " << minutes - 10)
                        {
                            REQUIRE(Timezone.minutes() == minutes - 10);
                        }
                    }
                }
                else
                {
                    AND_WHEN("this would result in an invalid timezone")
                    {
                        REQUIRE_THROWS_AS(
                            Timezone.setMinutes(Timezone.minutes() - 10),
                            SiLA2::CInvalidTimezone);
                        THEN("the minutes stay the same")
                        {
                            REQUIRE(Timezone.minutes() == minutes);
                        }
                    }
                }
            }
        }
    }
    // Wrong input
    GIVEN("these values represent an invalid timezone")
    {
        int32_t hours = GENERATE(take(
            10, filter([](auto h) { return abs(h) > 12; }, random(-14, 14))));
        uint32_t minutes =
            GENERATE(take(10, random(0u, std::numeric_limits<uint32_t>::max())));
        WHEN("a SiLA2::CTimezone is constructed from " << hours << ":" << minutes)
        {
            THEN("a SiLA2::CInvalidTimezone is thrown")
            {
                REQUIRE_THROWS_AS(SiLA2::CTimezone(hours, minutes),
                                  SiLA2::CInvalidTimezone);
            }
        }
    }
}

//=============================================================================
SCENARIO("Construct and modify a SiLA Date", "[data_types][date]")
{
    GIVEN("A Timezone and integer values for day, month and year")
    {
        // only save dates
        uint32_t day = GENERATE(take(10, random(0, 28)));
        uint32_t month = GENERATE(take(10, random(1, 12)));
        uint32_t year = GENERATE(take(10, random(1, 9999)));
        auto tz = SiLA2::CTimezone{1, 0};

        WHEN("a SiLA2::CDate is constructed from the values")
        {
            auto Date = SiLA2::CDate{day, month, year, tz};
            auto SiLADate = sila2::org::silastandard::Date{};
            SiLADate.set_day(day);
            SiLADate.set_month(month);
            SiLADate.set_year(year);
            SiLADate.set_allocated_timezone(tz.toProtoMessagePtr());

            THEN("this object has the same values as the SiLAFramework Date")
            {
                REQUIRE(Date.day() == SiLADate.day());
                REQUIRE(Date.month() == SiLADate.month());
                REQUIRE(Date.year() == SiLADate.year());
                REQUIRE(Date.timezone().hours() == SiLADate.timezone().hours());
                REQUIRE(Date.timezone().minutes()
                        == SiLADate.timezone().minutes());
            }
            AND_THEN("the underlying protobuf messages are equal")
            {
                REQUIRE(Date.toProtoMessage().SerializeAsString()
                        == SiLADate.SerializeAsString());
            }
            // Modification
            AND_WHEN("the day is decreased by 6")
            {
                if (day >= 7)
                {
                    AND_WHEN("this results in a valid date")
                    {
                        REQUIRE_NOTHROW(Date.setDay(Date.day() - 6));
                        THEN("the day is now " << day - 6)
                        {
                            REQUIRE(Date.day() == day - 6);
                        }
                    }
                }
                else
                {
                    AND_WHEN("this would result in an invalid date")
                    {
                        REQUIRE_THROWS_AS(Date.setDay(Date.day() - 6),
                                          SiLA2::CInvalidDate);
                        THEN("the day stays the same")
                        {
                            REQUIRE(Date.day() == day);
                        }
                    }
                }
            }
            AND_WHEN("the month is increased by 3")
            {
                if (month <= 9)
                {
                    AND_WHEN("this results in a valid date")
                    {
                        REQUIRE_NOTHROW(Date.setMonth(Date.month() + 3));
                        THEN("the month is now " << month + 3)
                        {
                            REQUIRE(Date.month() == month + 3);
                        }
                    }
                }
                else
                {
                    AND_WHEN("this would result in an invalid date")
                    {
                        REQUIRE_THROWS_AS(Date.setMonth(Date.month() + 3),
                                          SiLA2::CInvalidDate);
                        THEN("the month stays the same")
                        {
                            REQUIRE(Date.month() == month);
                        }
                    }
                }
            }
            AND_WHEN("the year is decreased by 21")
            {
                if (year >= 22)
                {
                    AND_WHEN("this results in a valid date")
                    {
                        REQUIRE_NOTHROW(Date.setYear(Date.year() - 21));
                        THEN("the year is now " << year - 21)
                        {
                            REQUIRE(Date.year() == year - 21);
                        }
                    }
                }
                else
                {
                    AND_WHEN("this would result in an invalid date")
                    {
                        REQUIRE_THROWS_AS(Date.setYear(Date.year() - 21),
                                          SiLA2::CInvalidDate);
                        THEN("the year stays the same")
                        {
                            REQUIRE(Date.year() == year);
                        }
                    }
                }
            }
            AND_WHEN("the timezone is set to {6, 30}")
            {
                const auto NewTZ = SiLA2::CTimezone{6, 30};
                Date.setTimezone(NewTZ);
                THEN("the timezone is now SiLA2::CTimezone{6, 30}")
                {
                    REQUIRE(Date.timezone() == NewTZ);
                }
            }
        }
    }
    // Wrong input
    GIVEN("these values represent an invalid date")
    {
        auto vals = GENERATE(std::array{29, 2, 2021},    // no leap year
                             std::array{31, 4, 1678},    // April has only 30 days
                             std::array{32, 3, 6815},    // March has only 31 days
                             std::array{31, 6, 10000});  // year to high

        WHEN("a SiLA2::CDate is constructed from the values")
        {
            THEN("a SiLA2::CInvalidDate is thrown")
            {
                REQUIRE_THROWS_AS(
                    SiLA2::CDate(vals.at(0), vals.at(1), vals.at(2)),
                    SiLA2::CInvalidDate);
            }
        }
    }
}

//=============================================================================
SCENARIO("Construct and modify a SiLA Time", "[data_types][time]")
{
    GIVEN("A Timezone and integer values for second, minute and hour")
    {
        // only save times
        uint32_t hour = GENERATE(take(10, random(0, 23)));
        uint32_t minute = GENERATE(take(10, random(0, 59)));
        uint32_t second = GENERATE(take(10, random(0, 59)));

        auto tz = SiLA2::CTimezone{-4, 0};

        WHEN("a SiLA2::CTime is constructed from the values")
        {
            auto Time = SiLA2::CTime{hour, minute, second, tz};
            auto SiLATime = sila2::org::silastandard::Time{};
            SiLATime.set_hour(hour);
            SiLATime.set_minute(minute);
            SiLATime.set_second(second);
            SiLATime.set_allocated_timezone(tz.toProtoMessagePtr());

            THEN("this object has the same values as the SiLAFramework Time")
            {
                REQUIRE(Time.hour() == SiLATime.hour());
                REQUIRE(Time.minute() == SiLATime.minute());
                REQUIRE(Time.second() == SiLATime.second());
                REQUIRE(Time.timezone().hours() == SiLATime.timezone().hours());
                REQUIRE(Time.timezone().minutes()
                        == SiLATime.timezone().minutes());
            }
            AND_THEN("the underlying protobuf messages are equal")
            {
                REQUIRE(Time.toProtoMessage().SerializeAsString()
                        == SiLATime.SerializeAsString());
            }
            // Modification
            AND_WHEN("the hour is decreased by 5")
            {
                if (hour >= 5)
                {
                    AND_WHEN("this results in a valid time")
                    {
                        REQUIRE_NOTHROW(Time.setHour(Time.hour() - 5));
                        THEN("the hour is now " << hour - 5)
                        {
                            REQUIRE(Time.hour() == hour - 5);
                        }
                    }
                }
                else
                {
                    AND_WHEN("this would result in an invalid time")
                    {
                        REQUIRE_THROWS_AS(Time.setHour(Time.hour() - 5),
                                          SiLA2::CInvalidTime);
                        THEN("the hour stays the same")
                        {
                            REQUIRE(Time.hour() == hour);
                        }
                    }
                }
            }
            AND_WHEN("the minute is increased by 38")
            {
                if (minute <= 21)
                {
                    AND_WHEN("this results in a valid time")
                    {
                        REQUIRE_NOTHROW(Time.setMinute(Time.minute() + 38));
                        THEN("the minute is now " << minute + 38)
                        {
                            REQUIRE(Time.minute() == minute + 38);
                        }
                    }
                }
                else
                {
                    AND_WHEN("this would result in an invalid time")
                    {
                        REQUIRE_THROWS_AS(Time.setMinute(Time.minute() + 38),
                                          SiLA2::CInvalidTime);
                        THEN("the minute stays the same")
                        {
                            REQUIRE(Time.minute() == minute);
                        }
                    }
                }
            }
            AND_WHEN("the second is decreased by 44")
            {
                if (second >= 44)
                {
                    AND_WHEN("this results in a valid time")
                    {
                        REQUIRE_NOTHROW(Time.setSecond(Time.second() - 44));
                        THEN("the second is now " << second - 44)
                        {
                            REQUIRE(Time.second() == second - 44);
                        }
                    }
                }
                else
                {
                    AND_WHEN("this would result in an invalid time")
                    {
                        REQUIRE_THROWS_AS(Time.setSecond(Time.second() - 44),
                                          SiLA2::CInvalidTime);
                        THEN("the second stays the same")
                        {
                            REQUIRE(Time.second() == second);
                        }
                    }
                }
            }
            AND_WHEN("the timezone is set to {7, 0}")
            {
                const auto NewTZ = SiLA2::CTimezone{7, 0};
                Time.setTimezone(NewTZ);
                THEN("the timezone is now SiLA2::CTimezone{7, 0}")
                {
                    REQUIRE(Time.timezone() == NewTZ);
                }
            }
        }
    }
    // Wrong input
    GIVEN("these values represent an invalid time")
    {
        uint32_t hours =
            GENERATE(take(10, random(24u, std::numeric_limits<uint32_t>::max())));
        uint32_t minutes =
            GENERATE(take(10, random(60u, std::numeric_limits<uint32_t>::max())));
        uint32_t seconds =
            GENERATE(take(10, random(60u, std::numeric_limits<uint32_t>::max())));
        WHEN("a SiLA2::CTime is constructed from " << hours << ":" << minutes
                                                   << ":" << seconds)
        {
            THEN("a SiLA2::CInvalidTime is thrown")
            {
                REQUIRE_THROWS_AS(SiLA2::CTime(hours, minutes, seconds),
                                  SiLA2::CInvalidTime);
            }
        }
    }
}

//=============================================================================
SCENARIO("Construct and modify a SiLA Timestamp", "[data_types][timestamp]")
{
    GIVEN(
        "A Timezone and integer values for day, month, year, second, minute "
        "and "
        "hour")
    {
        // only save timestamps
        uint32_t hour = GENERATE(take(5, random(0, 23)));
        uint32_t minute = GENERATE(take(5, random(0, 59)));
        uint32_t second = GENERATE(take(5, random(0, 59)));
        uint32_t day = GENERATE(take(5, random(1, 28)));
        uint32_t month = GENERATE(take(5, random(1, 12)));
        uint32_t year = GENERATE(take(5, random(1, 9999)));
        auto tz = SiLA2::CTimezone{-7, 30};

        WHEN("a SiLA2::CTimestamp is constructed from the values")
        {
            auto Timestamp =
                SiLA2::CTimestamp{hour, minute, second, day, month, year, tz};
            auto SiLATimestamp = sila2::org::silastandard::Timestamp{};
            SiLATimestamp.set_hour(hour);
            SiLATimestamp.set_minute(minute);
            SiLATimestamp.set_second(second);
            SiLATimestamp.set_day(day);
            SiLATimestamp.set_month(month);
            SiLATimestamp.set_year(year);
            SiLATimestamp.set_allocated_timezone(tz.toProtoMessagePtr());

            THEN(
                "this object has the same values as the SiLAFramework "
                "Timestamp")
            {
                REQUIRE(Timestamp.hour() == SiLATimestamp.hour());
                REQUIRE(Timestamp.minute() == SiLATimestamp.minute());
                REQUIRE(Timestamp.second() == SiLATimestamp.second());
                REQUIRE(Timestamp.day() == SiLATimestamp.day());
                REQUIRE(Timestamp.month() == SiLATimestamp.month());
                REQUIRE(Timestamp.year() == SiLATimestamp.year());
                REQUIRE(Timestamp.timezone().hours()
                        == SiLATimestamp.timezone().hours());
                REQUIRE(Timestamp.timezone().minutes()
                        == SiLATimestamp.timezone().minutes());
            }
            AND_THEN("the underlying protobuf messages are equal")
            {
                REQUIRE(Timestamp.toProtoMessage().SerializeAsString()
                        == SiLATimestamp.SerializeAsString());
            }
            // Modification
            AND_WHEN("the hour is decreased by 5")
            {
                if (hour >= 5)
                {
                    AND_WHEN("this results in a valid timestamp")
                    {
                        REQUIRE_NOTHROW(Timestamp.setHour(Timestamp.hour() - 5));
                        THEN("the hour is now " << hour - 5)
                        {
                            REQUIRE(Timestamp.hour() == hour - 5);
                        }
                    }
                }
                else
                {
                    AND_WHEN("this would result in an invalid timestamp")
                    {
                        REQUIRE_THROWS_AS(Timestamp.setHour(Timestamp.hour() - 5),
                                          SiLA2::CInvalidTimestamp);
                        THEN("the hour stays the same")
                        {
                            REQUIRE(Timestamp.hour() == hour);
                        }
                    }
                }
            }
            AND_WHEN("the minute is increased by 38")
            {
                if (minute <= 21)
                {
                    AND_WHEN("this results in a valid timestamp")
                    {
                        REQUIRE_NOTHROW(
                            Timestamp.setMinute(Timestamp.minute() + 38));
                        THEN("the minute is now " << minute + 38)
                        {
                            REQUIRE(Timestamp.minute() == minute + 38);
                        }
                    }
                }
                else
                {
                    AND_WHEN("this would result in an invalid timestamp")
                    {
                        REQUIRE_THROWS_AS(
                            Timestamp.setMinute(Timestamp.minute() + 38),
                            SiLA2::CInvalidTimestamp);
                        THEN("the minute stays the same")
                        {
                            REQUIRE(Timestamp.minute() == minute);
                        }
                    }
                }
            }
            AND_WHEN("the second is decreased by 44")
            {
                if (second >= 44)
                {
                    AND_WHEN("this results in a valid timestamp")
                    {
                        REQUIRE_NOTHROW(
                            Timestamp.setSecond(Timestamp.second() - 44));
                        THEN("the second is now " << second - 44)
                        {
                            REQUIRE(Timestamp.second() == second - 44);
                        }
                    }
                }
                else
                {
                    AND_WHEN("this would result in an invalid timestamp")
                    {
                        REQUIRE_THROWS_AS(
                            Timestamp.setSecond(Timestamp.second() - 44),
                            SiLA2::CInvalidTimestamp);
                        THEN("the second stays the same")
                        {
                            REQUIRE(Timestamp.second() == second);
                        }
                    }
                }
            }
            AND_WHEN("the day is decreased by 6")
            {
                if (day >= 7)
                {
                    AND_WHEN("this results in a valid timestamp")
                    {
                        REQUIRE_NOTHROW(Timestamp.setDay(Timestamp.day() - 6));
                        THEN("the day is now " << day - 6)
                        {
                            REQUIRE(Timestamp.day() == day - 6);
                        }
                    }
                }
                else
                {
                    AND_WHEN("this would result in an invalid timestamp")
                    {
                        REQUIRE_THROWS_AS(Timestamp.setDay(Timestamp.day() - 6),
                                          SiLA2::CInvalidTimestamp);
                        THEN("the day stays the same")
                        {
                            REQUIRE(Timestamp.day() == day);
                        }
                    }
                }
            }
            AND_WHEN("the month is increased by 3")
            {
                if (month <= 9)
                {
                    AND_WHEN("this results in a valid timestamp")
                    {
                        REQUIRE_NOTHROW(
                            Timestamp.setMonth(Timestamp.month() + 3));
                        THEN("the month is now " << month + 3)
                        {
                            REQUIRE(Timestamp.month() == month + 3);
                        }
                    }
                }
                else
                {
                    AND_WHEN("this would result in an invalid timestamp")
                    {
                        REQUIRE_THROWS_AS(
                            Timestamp.setMonth(Timestamp.month() + 3),
                            SiLA2::CInvalidTimestamp);
                        THEN("the month stays the same")
                        {
                            REQUIRE(Timestamp.month() == month);
                        }
                    }
                }
            }
            AND_WHEN("the year is decreased by 21")
            {
                if (year >= 22)
                {
                    AND_WHEN("this results in a valid timestamp")
                    {
                        REQUIRE_NOTHROW(Timestamp.setYear(Timestamp.year() - 21));
                        THEN("the year is now " << year - 21)
                        {
                            REQUIRE(Timestamp.year() == year - 21);
                        }
                    }
                }
                else
                {
                    AND_WHEN("this would result in an invalid timestamp")
                    {
                        REQUIRE_THROWS_AS(
                            Timestamp.setYear(Timestamp.year() - 21),
                            SiLA2::CInvalidTimestamp);
                        THEN("the year stays the same")
                        {
                            REQUIRE(Timestamp.year() == year);
                        }
                    }
                }
            }
            AND_WHEN("the timezone is set to {-3, 30}")
            {
                const auto NewTZ = SiLA2::CTimezone{-3, 30};
                Timestamp.setTimezone(NewTZ);
                THEN("the timezone is now SiLA2::CTimezone{-3, 30}")
                {
                    REQUIRE(Timestamp.timezone() == NewTZ);
                }
            }
        }
    }
    // Wrong input
    GIVEN("these values represent an invalid timestamp")
    {
        uint32_t hours =
            GENERATE(take(10, random(24u, std::numeric_limits<uint32_t>::max())));
        uint32_t minutes =
            GENERATE(take(10, random(60u, std::numeric_limits<uint32_t>::max())));
        uint32_t seconds =
            GENERATE(take(10, random(60u, std::numeric_limits<uint32_t>::max())));
        auto vals = GENERATE(std::array{29, 2, 2021},    // no leap year
                             std::array{31, 4, 1678},    // April has only 30 days
                             std::array{32, 3, 6815},    // March has only 31 days
                             std::array{31, 6, 10000});  // year to high
        WHEN("a SiLA2::CTimestamp is constructed from these values")
        {
            THEN("a SiLA2::CInvalidTimestamp is thrown")
            {
                REQUIRE_THROWS_AS(
                    SiLA2::CTimestamp(hours, minutes, seconds, vals.at(0),
                                      vals.at(1), vals.at(2)),
                    SiLA2::CInvalidTimestamp);
            }
        }
    }
}

//=============================================================================
// SCENARIO("Construct a SiLA AnyType from a ", "[data_types][int]")
// {
//     GIVEN("A integer value")
//     {
//         int64_t val = 42;

//         WHEN("a SiLA2::CAnyType is constructed from the value")
//         {
//             auto AnyType = SiLA2::CAnyType{val};
//             auto SiLAAnyType = sila2::org::silastandard::AnyType{};
//             SiLAAnyType.set_value(val);

//             THEN("this object has the same value as the SiLAFramework
//             AnyType")
//             {
//                 REQUIRE(AnyType.value() == SiLAAnyType.value());
//             }
//             AND_THEN("the underlying protobuf messages are equal")
//             {
//                 REQUIRE(sila2::org::silastandard::AnyType{AnyType}.SerializeAsString()
//                 == SiLAAnyType.SerializeAsString());
//             }
//         }
//     }
// }

//=============================================================================
SCENARIO("Construct a Command Execution UUID from a QUuid", "[data_types][uuid]")
{
    GIVEN("A QUuid value")
    {
        auto val = QUuid::createUuid();

        WHEN("a SiLA2::CCommandExecutionUUID is constructed from the value")
        {
            auto UUID = SiLA2::CCommandExecutionUUID{val};
            auto SiLAUUID = sila2::org::silastandard::CommandExecutionUUID{};
            SiLAUUID.set_value(
                val.toString(QUuid::WithoutBraces).toLocal8Bit().constData());

            THEN("this object has the same value as the SiLAFramework UUID")
            {
                REQUIRE(UUID.toStdString() == SiLAUUID.value());
            }
            AND_THEN("the underlying protobuf messages are equal")
            {
                REQUIRE(UUID.toProtoMessage().SerializeAsString()
                        == SiLAUUID.SerializeAsString());
            }
        }
    }
}

//=============================================================================
SCENARIO("Construct a Command Execution UUID from a string", "[data_types][uuid]")
{
    GIVEN("A string value")
    {
        auto val = "f81d4fae-7dec-11d0-a765-00a0c91e6bf6";

        WHEN("a SiLA2::CCommandExecutionUUID is constructed from the value")
        {
            auto UUID = SiLA2::CCommandExecutionUUID{val};
            auto SiLAUUID = sila2::org::silastandard::CommandExecutionUUID{};
            SiLAUUID.set_value(val);

            THEN("this object has the same value as the SiLAFramework UUID")
            {
                REQUIRE(UUID.toStdString() == SiLAUUID.value());
            }
            AND_THEN("the underlying protobuf messages are equal")
            {
                REQUIRE(UUID.toProtoMessage().SerializeAsString()
                        == SiLAUUID.SerializeAsString());
            }
        }
    }
}

//=============================================================================
SCENARIO("Construct a Command Execution UUID from a string in the wrong format",
         "[data_types][uuid]")
{
    GIVEN("A wrongly formatted string value")
    {
        auto val = "This is never ever a valid UUID";

        WHEN("a SiLA2::CCommandExecutionUUID is constructed from the value")
        {
            auto UUID = SiLA2::CCommandExecutionUUID{val};

            THEN("this object is a null-UUID") { REQUIRE(UUID.value().isNull()); }
        }
    }
}

//=============================================================================
SCENARIO("Construct a random Command Execution UUID", "[data_types][uuid]")
{
    WHEN("A SiLA2::CCommandExecutionUUID is constructed without a value")
    {
        auto UUID = SiLA2::CCommandExecutionUUID{};

        THEN("this object contains a random UUID")
        {
            REQUIRE_FALSE(UUID.value().isNull());
        }
    }
}

//=============================================================================
SCENARIO("Construct and modify a SiLA Duration", "[data_types][duration]")
{
    GIVEN("Integer values for seconds and nanoseconds")
    {
        uint64_t seconds = 14;
        uint32_t nanos = 687400000;

        WHEN("a SiLA2::CDuration is constructed from the values")
        {
            auto Duration = SiLA2::CDuration{seconds, nanos};
            auto SiLADuration = sila2::org::silastandard::Duration{};
            SiLADuration.set_seconds(seconds);
            SiLADuration.set_nanos(nanos);

            THEN(
                "this object has the same values as the SiLAFramework "
                "Duration")
            {
                REQUIRE(Duration.seconds() == SiLADuration.seconds());
                REQUIRE(Duration.nanos() == SiLADuration.nanos());
            }
            AND_THEN("the underlying protobuf messages are equal")
            {
                REQUIRE(Duration.toProtoMessage().SerializeAsString()
                        == SiLADuration.SerializeAsString());
            }
            AND_WHEN("the seconds are set to 0")
            {
                Duration.setSeconds(0);
                THEN("the seconds are 0") { REQUIRE(Duration.seconds() == 0); }
            }
            AND_WHEN("the nanos are set to 0")
            {
                Duration.setNanos(0);
                THEN("the nanos are 0") { REQUIRE(Duration.nanos() == 0); }
            }
            AND_WHEN("seconds and nanos are set to 0")
            {
                Duration.setValue({0, 0});
                THEN("the Duration is null") { REQUIRE(Duration.isNull()); }
            }
            AND_WHEN("the Duration is converted to std::chrono::milliseconds")
            {
                const auto MilliDuration = Duration.toMilliSeconds();
                const auto ExpectedMilli =
                    std::chrono::milliseconds{seconds * 1000 + nanos / 1000000};
                THEN(
                    "this value is equivalent to the sum of the seconds and "
                    "the "
                    "nanos in milliseconds")
                {
                    REQUIRE(MilliDuration.count() == ExpectedMilli.count());
                }
            }
        }
    }
}

//=============================================================================
SCENARIO("Construct an Execution Info", "[data_types][execution_info]")
{
    GIVEN(
        "A command status, an optional progress, remaining time and updated "
        "lifetime of execution")
    {
        auto Status = GENERATE(SiLA2::CommandStatus::WAITING,
                               SiLA2::CommandStatus::RUNNING,
                               SiLA2::CommandStatus::FINISHED_SUCCESSFULLY,
                               SiLA2::CommandStatus::FINISHED_WITH_ERROR);
        auto Progress = GENERATE(SiLA2::CReal{0.23}, SiLA2::CReal{});
        auto RemainingTime =
            GENERATE(SiLA2::CDuration{14, 631}, SiLA2::CDuration{});
        auto UpdatedLifetime =
            GENERATE(SiLA2::CDuration{42, 287}, SiLA2::CDuration{});

        WHEN("a SiLA2::CExecutionInfo is constructed from the values")
        {
            auto ExecInfo = SiLA2::CExecutionInfo{Status, Progress, RemainingTime,
                                                  UpdatedLifetime};
            auto SiLAExecInfo = sila2::org::silastandard::ExecutionInfo{};
            using sila2::org::silastandard::ExecutionInfo_CommandStatus;
            SiLAExecInfo.set_commandstatus(
                static_cast<ExecutionInfo_CommandStatus>(Status));
            SiLAExecInfo.set_allocated_progressinfo(Progress.toProtoMessagePtr());
            SiLAExecInfo.set_allocated_estimatedremainingtime(
                RemainingTime.toProtoMessagePtr());
            SiLAExecInfo.set_allocated_updatedlifetimeofexecution(
                UpdatedLifetime.toProtoMessagePtr());

            THEN(
                "this object has the same values as the SiLAFramework "
                "Execution "
                "Info")
            {
                REQUIRE(ExecInfo.commandStatus() == SiLAExecInfo.commandstatus());
                REQUIRE(SiLAExecInfo.has_progressinfo());
                REQUIRE(ExecInfo.progressInfo().value()
                        == SiLAExecInfo.progressinfo().value());
                REQUIRE(SiLAExecInfo.has_estimatedremainingtime());
                REQUIRE(ExecInfo.estimatedRemainingTime().seconds()
                        == SiLAExecInfo.estimatedremainingtime().seconds());
                REQUIRE(ExecInfo.estimatedRemainingTime().nanos()
                        == SiLAExecInfo.estimatedremainingtime().nanos());
                REQUIRE(SiLAExecInfo.has_updatedlifetimeofexecution());
                REQUIRE(ExecInfo.updatedLifetimeOfExecution().seconds()
                        == SiLAExecInfo.updatedlifetimeofexecution().seconds());
                REQUIRE(ExecInfo.updatedLifetimeOfExecution().nanos()
                        == SiLAExecInfo.updatedlifetimeofexecution().nanos());
                AND_THEN("the underlying protobuf messages are equal")
                {
                    REQUIRE(ExecInfo.toProtoMessage().SerializeAsString()
                            == SiLAExecInfo.SerializeAsString());
                }
            }
        }
    }
}

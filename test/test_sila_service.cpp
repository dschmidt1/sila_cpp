/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   test_sila_service.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   27.08.2020
/// \brief  Unit tests for the SiLA Service Feature implementation
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/SiLAClient.h>
#include <sila_cpp/common/ServerAddress.h>

#include "interoperability/InteroperabilityTest/test_globals.h"

using namespace SiLA2;
using namespace std::string_literals;

std::ostream& operator<<(std::ostream& os, const CString& rhs)
{
    return os << rhs.value();
}

#include <catch2/catch.hpp>

SCENARIO("SiLA Service", "[sila_service][features]")
{
    GIVEN(
        "A SiLA Client instance connected to the InteroperabilityTest server at "
        << ServerIP << ":" << ServerPort)
    {
        static auto TestClient = CSiLAClient{{ServerIP, ServerPort}};
        const auto ExpectedName = SiLA2::CString{"InteroperabilityTest"};

        WHEN("the Server Name Property is requested")
        {
            const auto ServerName = TestClient.Get_ServerName();

            THEN("it has the value \"" << ExpectedName << "\"")
            {
                REQUIRE(ServerName == ExpectedName);
            }
        }
        AND_GIVEN("a new name for the Server")
        {
            const auto NewName =
                GENERATE(as<SiLA2::CString>{},
                         "This is a very long name that definitely exceeds the "
                         "character limit of two hundred and fifty-five (255) "
                         "characters and thus should throw a SiLA 2 Validation "
                         "Error - why would anyone use a name that is this long? "
                         "- I can't even come up with any more nonsense",
                         "", "New Server Name");
            WHEN("the Server Name Property is changed to this new name")
            {
                TestClient.SetServerName(NewName);
                const auto ServerName = TestClient.Get_ServerName();

                if (NewName.value().size() > 255)
                {
                    AND_WHEN("then name is longer than the constraint allows")
                    {
                        THEN("the Server throws a Validation Error")
                        {
                            AND_THEN("the Server Name Property remains unchanged")
                            {
                                REQUIRE(ServerName == ExpectedName);
                            }
                        }
                    }
                }
                else if (NewName.value().empty())
                {
                    AND_WHEN("the name is empty")
                    {
                        THEN("the Server throws a Validation Error")
                        {
                            AND_THEN("the Server Name Property remains unchanged")
                            {
                                REQUIRE(ServerName == ExpectedName);
                            }
                        }
                    }
                }
                else
                {
                    AND_WHEN("then name passes the parameter constraint")
                    {
                        THEN("it has the value \"" << NewName << "\"")
                        {
                            REQUIRE(ServerName == NewName);
                        }
                    }
                }
            }
        }
        WHEN("the Server Type Property is requested")
        {
            const auto ExpectedType = SiLA2::CString{"TestServer"};
            const auto ServerType = TestClient.Get_ServerType();

            THEN("it has the value \"" << ExpectedType << "\"")
            {
                REQUIRE(ServerType == ExpectedType);
            }
        }
        WHEN("the Server VendorURL Property is requested")
        {
            const auto ExpectedVendorURL = SiLA2::CString{"www.example.com"};
            const auto ServerVendorURL = TestClient.Get_ServerVendorURL();

            THEN("it has the value \"" << ExpectedVendorURL << "\"")
            {
                REQUIRE(ServerVendorURL == ExpectedVendorURL);
            }
        }
    }
}
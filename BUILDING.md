# Building sila_cpp and gRPC
## Linux
Before you begin you need to make sure you have at least Qt5 Core installed.
On Ubuntu you can simply install the `qtbase5-dev` package and you're good to go.  
You will also need OpenSSL's development libraries installed.
This can be achieved by installing e.g. `libssl-dev` on Ubuntu.

### gRPC
Follow the instructions at https://grpc.io/docs/quickstart/cpp/.
The version v1.31.0 works perfectly with this guide. 
**v1.32.0 has proven to cause build errors with the current version of sila_cpp so be sure to choose a version prior to that one.**  
The only thing you'll have to change is the SSL provider.
We're not going to use boringssl which comes with gRPC but rather the aforementioned OpenSSL libs.
This is because `sila_cpp` needs OpenSSL as well.
If we were to build gRPC with boringssl but `sila_cpp` with OpenSSL then we would run into all kinds of weird issues.
So, to prevent that we need to tell gRPC not to use boringssl.  
If you strictly follow the guide apart from that little change, you will build the static version of gRPC.
But you can also build a shared version by simply setting the `BUILD_SHARED_LIBS` variable to `ON`:
```shell
$ cmake ../.. \
    -DgRPC_INSTALL=ON \
    -DgRPC_BUILD_TESTS=OFF \
    -DgRPC_SSL_PROVIDER=package \
    -DCMAKE_INSTALL_PREFIX=$MY_INSTALL_DIR \
    -DBUILD_SHARED_LIBS=ON
```
At the end you should have a working gRPC installation in `$MY_INSTALL_DIR`.

### sila_cpp
Then you can `cd` into the directory of sila_cpp, create a build folder and run CMake:
```shell
$ cd path/to/sila_cpp
$ mkdir build && cd build
$ cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_PREFIX_PATH=$MY_INSTALL_DIR
$ cmake --build .
```
If you want to install sila_cpp afterwards (i.e. with `cmake --install .`) but want to customize the installation directory you need to change the value of the `CMAKE_INSTALL_PREFIX` variable during configuration:
```shell
$ cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_PREFIX_PATH=%MY_INSTALL_DIR% -DCMAKE_INSTALL_PREFIX=<path-to-sila_cpp-install-dir>
```

Like above, you can control whether sila_cpp should be built as static or shared library by changing the value of the `BUILD_SHARED_LIBS` variable.
But in contrast to gRPC, sila_cpp will create a shared library by default.  
**Be sure, however, to select the same build type for both gRPC and sila_cpp as mixing static and shared libs can cause errors!**

> ##### Note:
> You only need to specify the `CMAKE_PREFIX_PATH` if you don't already have `$MY_INSTALL_DIR` in your `PATH`.  
> But remember to definitely add `$MY_INSTALL_DIR` to `PATH` when you built the shared version of gRPC!
> **You might also need to prepend `LD_LIBRARY_PATH=$MY_INSTALL_DIR/lib` to the `cmake --build .` line.**
> **Otherwise other applications can't find the shared library files and won't run properly.**
> 
> You can also permanently add `$MY_INSTALL_DIR/lib` to the search paths for the dynamic linker for not having to set `LD_LIBRARY_PATH` every time.
> Simply create a new file in `/etc/ld.so.conf.d` (called `grpc.conf` for example) and add the full path to the `lib` directory.
> Then run `ldconfig` with root privileges to re-generate the linker cache.
> You can check with `ldconfig -v` if the directory and all its containing libraries have been correctly added.
> ```shell
> $ echo "$MY_LIBRARY_DIR/lib" | sudo tee /etc/ld.so.conf.d/grpc.conf
> $ sudo ldconfig
> $ sudo ldconfig -v | less
> ```

## Windows
Similarly, you need to have Qt5 and OpenSSL installed.
The easiest way is using the [online-installer](https://www.qt.io/download-qt-installer).
I installed the MinGW 7.3 32- and 64-bit compiler toolchains and the corresponding pre-built Qt components.
You also need to add the following two directories to your `PATH`.

For 32-bit:
```cmd
set PATH=%PATH%;C:\Qt\5.13.2\mingw73_32\bin;C:\Qt\Tools\mingw730_32\bin
```
And for 64-bit:
```cmd
set PATH=%PATH%;C:\Qt\5.13.2\mingw73_64\bin;C:\Qt\Tools\mingw730_64\bin
```

Further, you need to install the OpenSSL sources and binaries from Qt's online installer.
These are alongside the compiler toolchains in the *Developer and Designer Tools* section.

### gRPC
> ##### Note:
> The following has been tested using gRPC v1.31.0.  
> **v1.32.0 has proven to cause build errors with the current version of sila_cpp so be sure to choose a version prior to that one.**

Clone the repository (substitute the correct release tag) and download all submodules for gRPC's dependencies:
```cmd
> git clone -b RELEASE_TAG_HERE https://github.com/grpc/grpc
> cd grpc
> git submodule update --init
```

#### as static library
Building gRPC with MinGW on Windows as a static library is similarly easy to building on Linux.

You can build gRPC with
```cmd
> md .build & cd .build
> set MY_INSTALL_DIR=C:\grpc-install
> cmake .. -G"MinGW Makefiles" -DCMAKE_INSTALL_PREFIX=%MY_INSTALL_DIR% -DgRPC_SSL_PROVIDER=package
> cmake --build .
> cmake --install .
```
You can, of course, adjust the path of `MY_INSTALL_PREFIX` to whatever you prefer.

**Note the `-DgRPC_SSL_PROVIDER=package` option, though!**
**It is important to add this since we don't want gRPC to use boringssl but OpenSSL.**

#### as shared library
> ##### Note:
> Instead of manually applying the following fixes you can also use the provided patch in the `.grpc-patches` directory via `git apply`.
> It contains all the necessary changes for building gRPC as shared library with MinGW.

For building a shared library of gRPC you need to make a few adjustments:

- In `third_party/abseil_cpp/absl/string/CMakeLists.txt` you need to change
<!-- This was fixed on 2020-04-22 by PR and was released as a patch release so it should soon make its way into gRPC -->
  ```cmake
  absl_cc_library(
    NAME
      cord
    HDRS
      # ...
    DEPS
      absl::strings_internal
      # ...
    PUBLIC
  )
  ```
  to
  ```cmake
  absl_cc_library(
    NAME
      cord
    HDRS
      # ...
    DEPS
      absl::strings
      # ...
    PUBLIC
  )
  ```
- Lastly, before running CMake you need to add `.build/third_party/protobuf` to your `PATH`.
  Otherwise `protoc` won't find `libprotoc`/`libprotobuf` during build and will exit with the following error
  ```
  --grpc_out: protoc-gen-grpc: Plugin failed with status code 3221225781.
  ```
Now you're ready to build gRPC with
```cmd
> md .build & cd .build
> set PATH=%cd%\third_party\protobuf
> set MY_INSTALL_DIR=C:\grpc-install
> cmake .. -G"MinGW Makefiles" -DCMAKE_INSTALL_PREFIX=%MY_INSTALL_DIR% -DgRPC_SSL_PROVIDER=package -DBUILD_SHARED_LIBS=ON
> cmake --build .
> cmake --install .
```

If you get an error from Abseil saying
```
ABSL: compiling absl requires a gtest CMake target in your project
```
you need to simply turn off the Abseil tests by adding `-DBUILD_TESTING=OFF` to the configure options for CMake:
```cmd
> cmake .. -G"MinGW Makefiles" -DCMAKE_INSTALL_PREFIX=%MY_INSTALL_DIR% -DgRPC_SSL_PROVIDER=package -DBUILD_SHARED_LIBS=ON -DBUILD_TESTING=OFF
> cmake --build .
```

### sila_cpp
Building sila_cpp is very similar to the procedure on Linux.
`cd` into the directory of sila_cpp, create a build folder and run CMake:
```cmd
> cd path\to\sila_cpp
> md build & cd build
> cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_PREFIX_PATH=%MY_INSTALL_DIR%
> cmake --build .
```
If you want to install sila_cpp afterwards (i.e. with `cmake --install .`) but want to customize the installation directory you need to change the value of the `CMAKE_INSTALL_PREFIX` variable during configuration:
```cmd
> cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_PREFIX_PATH=%MY_INSTALL_DIR% -DCMAKE_INSTALL_PREFIX=<path-to-sila_cpp-install-dir>
```

Like above with gRPC, you can control whether sila_cpp should be built as static or shared library by changing the value of the `BUILD_SHARED_LIBS` variable.
But in contrast to gRPC, sila_cpp will create a shared library by default.
**Be sure, however, to select the same build type for both gRPC and sila_cpp as mixing static and shared libs can cause errors!**

> ##### Note:
> You only need to specify the `CMAKE_PREFIX_PATH` if you don't already have `%MY_INSTALL_DIR%` in your `PATH`.  
> But remember to definitely add `%MY_INSTALL_DIR%` to `PATH` when you built the shared version of gRPC!
> Otherwise other applications can't find the DLLs and won't run properly.

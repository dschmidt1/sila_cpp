#include <iostream>
#include <sila_cpp/common/ServerInformation.h>
#include <QtDebug>

int main() {
    SiLA2::CServerInformation Info("Name", "Type", "Description");
    qDebug() << Info.serverName();
    return 0;
}

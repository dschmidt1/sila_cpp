<!--- Provide a general summary of your changes in the Title above -->

## Description
<!--- Describe your changes in detail -->


## What is the current behavior?
<!-- You can also link to an open issue here -->


## What is the new/improved behavior?


## Does this MR introduce any breaking changes?
<!-- What changes might users need to make in their application due to this MR? -->


## Other information
<!-- Anything else that is important -->


## Checklist:
<!--- Go over all the following points, and put an `x` in all the boxes that apply. -->
<!--- If you're unsure about any of these, don't hesitate to ask. We're here to help! -->
- [ ] The commit messages follow our guidelines
- [ ] If necessary, tests have been added
- [ ] Docs have been added / updated
- [ ] [CHANGELOG.md](CHANGELOG.md) has been updated in the **Added**/**Changed** section

/assign @FMeinicke

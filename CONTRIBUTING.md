# Contributing to sila_cpp
We love your input!
We want to make contributing to this project as easy and transparent as possible, whether it's:

- Reporting a bug
- Discussing the current state of the code
- Submitting a fix
- Proposing new features
- Becoming a maintainer

## We Develop with GitLab
We use GitLab to host code, to track issues and feature requests, as well as accept Merge Requests.

## Report bugs using GitLab's [issues](https://gitlab.com/SiLA2/sila_cpp/-/issues)
We use GitLab issues to track public bugs.
Report a bug by [opening a new issue](https://gitlab.com/SiLA2/sila_cpp/-/issues/new); it's that easy!

### Write bug reports with detail, background, and sample code
Choose the provided 'Bug' template when creating a new issue to report a bug.

**Great Bug Reports** tend to have:

- A quick summary and/or background
- Steps to reproduce
  - Be specific!
  - Give sample code if you can.
- What you expected would happen
- What actually happens
- Notes (possibly including why you think this might be happening, or stuff you tried that didn't work)

People *love* thorough bug reports.
I'm not even kidding.

## We Use [Github Flow](https://guides.github.com/introduction/flow/index.html) adapted for GitLab, So All Code Changes Happen Through Merge Requests
Merge Requests are the best way to propose changes to the codebase.
We actively welcome your Merge Requests:

1. If you have at least 'Developer' access to the repo, create a new branch from `master`.  
   If not, either request access (see [below](#join-the-gitlab-sila-2-developer-group)) or simply fork the repo.
2. If you've added code that should be tested, add tests.
3. If you've changed APIs, update the documentation.
4. Ensure the tests pass.
5. For any larger/breaking changes or bug fixes in the library add an entry to the [CHANGELOG.md](CHANGELOG.md) in the corresponding section (see [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) for reference).
6. Issue that Merge Request!

It's best you create your Merge Request as early as possible and [mark it as 'Draft'](https://docs.gitlab.com/ee/user/project/merge_requests/work_in_progress_merge_requests.html).
That way we and others know what you're working on and there won't be someone else working on the same problem as you.  
Feel free to ask for help and feedback; use the Merge Request form to directly discuss topics regarding your code.

Once you're done with your work, remove the 'Draft' prefix to indicate your Merge Request is ready for a merge.

## Use a Consistent Coding Style
Please maintain a consistent coding style and follow the style of existing code.
Before submitting any new changes be sure to format your code with the [ClangFormat](https://clang.llvm.org/docs/ClangFormat.html) tool.
Use the provided `.clang-format` file in the repository root.

Some of the most important style guidelines:
- Use 4 spaces rather than tabs
- Use upper CamelCase for class names and identifiers; lower camelCase for method names
- Class names should start with a capital `C`
- Member variables of a class should have the prefix `m_` (e.g. `m_SomeData`)
- Use modern C++17 wherever and whenever possible

## Write good commit messages
Write commit messages using the present tense, i.e. *"Add some awesome feature"* or *"Change/Fix ..."*.

Summarize the changes in around 50 characters or less in the first line of your commit (the 'subject line').
Add a more detailed explanation, if necessary, starting from the third line and wrap it to about 72 characters.  
See ['How to Write a Git Commit Message'](https://chris.beams.io/posts/git-commit/#seven-rules) by Chris Beams for more.

Also, if possible, maintain a clean commit history by using `git rebase origin/master` and `git pull --rebase`.
If it's not possible, clearly indicate a merge by using `git merge --no-ff` and fix potential conflicts in the merge commit.

## Join Us
If any help is needed during your effort to contribute on this project, please don't hesitate to join our community:

### Join the SiLA 2 Slack Community
Sign up to the [slack](https://join.slack.com/t/sila-standard/shared_invite/enQtNDI0ODcxMDg5NzkzLTc4YjdkNzgxYjM5NDIyMzAyNTJjMjE1ZWI5MzY0M2Y2NmY3ZGQ2NTI3YzJiMmIzNTFmZmJkMWI3ZTMyMTk5NGY) community and join the `#sila_cpp` channel.

### Join the GitLab SiLA 2 Developer group
Request 'Developer' access to the [SiLA2 Group](https://gitlab.com/SiLA2) via the `#development` slack channel.

## License
By contributing, you agree that your contributions will be understood to be under the same [MIT License](http://choosealicense.com/licenses/mit/) that covers the project.
Feel free to contact the maintainers if that's a concern.

## References
This document was adapted from [briandk's Contributing .md template](https://gist.github.com/briandk/3d2e8b3ec8daf5a27a62)

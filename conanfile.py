from conans import ConanFile, CMake, tools
from conans.errors import ConanException
import os

class SilacppConan(ConanFile):
    name = "sila_cpp"
    version = "0.0.1"
    license = "MIT"
    author = "Florian Meinicke <florian.meinicke@cetoni.de>"
    url = "https://gitlab.com/SiLA2/sila_cpp"
    description = "Official SiLA 2 C++ reference implementation"
    topics = ("SiLA 2", "Lab Automation", "Development")
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [True, False],
        "with_qt": [True, False],
        "qt_install_prefix_path": "ANY"
    }
    default_options = {
        "shared": True,
        "with_qt": True,
        "qt_install_prefix_path": None
    }
    requires = "grpc/1.25.0@inexorgame/stable"
    generators = "cmake"
    exports_sources = "src/*", "third_party/*", "cmake/*"

    _qt_install_prefix_path = ""

    def configure(self):
        if self.options.with_qt:
            self.options["qt"].GUI = False
            self.options["qt"].widgets = False
        else:
            if not self.options.qt_install_prefix_path:
                raise ConanException("You need to set `qt_install_prefix_path` to point to your Qt installation directory!")
            self._qt_install_prefix_path = str(self.options.qt_install_prefix_path)

    def requirements(self):
        if self.options.with_qt:
            self.requires("qt/5.13.2@bincrafters/stable")
            self.requires("openssl/1.0.2t", override=True) # override with openssl version from gRPC

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_PREFIX_PATH"] = self._qt_install_prefix_path
        cmake.definitions["USE_CONAN"] = "ON"
        cmake.configure(source_folder="src")
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()


    def package_info(self):
        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
        self.cpp_info.libs = ["sila_cpp"]
        if self.settings.os == "Windows":
            self.cpp_info.defines += ["_WIN32_WINNT=0x600"] # required by gRPC
        if not self.options.with_qt:
            self.cpp_info.cxxflags += ["-L" + os.path.join(self._qt_install_prefix_path, "lib")]
            self.cpp_info.cxxflags += ["-I" + os.path.join(self._qt_install_prefix_path, "include")]
            self.output.info("Setting CMAKE_PREFIX_PATH to Qt's install prefix path")
            self.env_info.CMAKE_PREFIX_PATH = self._qt_install_prefix_path
            self.output.info("Appending PATH env var with Qt's install prefix path")
            self.env_info.PATH.append(os.path.join(self._qt_install_prefix_path, "bin"))

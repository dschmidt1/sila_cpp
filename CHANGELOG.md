# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!--
Types of changes

    `Added` for new features.
    `Changed` for changes in existing functionality.
    `Deprecated` for soon-to-be removed features.
    `Removed` for now removed features.
    `Fixed` for any bug fixes.
    `Security` in case of vulnerabilities.
-->

## [Unreleased]

## [v0.2.0]
### Added
- !3: Basic interoperability tests following the current proposal in sila_interoperability!1
- `SiLA2::CServerAddress` can now be constructed from `std::string`s in addition to the existing `QString` c'tor
- `SiLA2::CServer` will now automatically shut the internal gRPC server down on destruction
- The logging level can be restricted even more by setting it to "FATAL"
- #26: Any SiLA server application can now be shut down properly with Ctrl-C (resp. `SIGINT` on Unix or `CTRL_C_EVENT` on Windows) from the command line
- #33: The Lifetime of Execution of an Observable Command is now properly taken into account
- #12: SiLA Servers now advertise themselves in the network using ZeroConf (SiLA Server Discovery)
- !5/#14: All communication between server and client is now encrypted (we only fall back to insecure communication as the very last resort)
- #35: You can force the server to only use encrypted communication (it'll refuse to start when this can't be achieved)

### Changed
- `HelloSiLA2` now stores the FDL files in a .qrc file
- Naming in the `HelloSiLA2` example is now more C++-like (i.e. uses CamelCase for the application names rather than suffixes with underscores)
- Use gRPC version 1.31.0 (makes building gRPC easier for MinGW)
- Use Protobuf version 3.12.2 (this breaks the Data Types implementation; see next point)
- #32: Change implementation of Data Types (don't use inheritance from the Protobuf generated classes)
- Update `sila_base` (you need to run `git submodule update` after pulling the new version of `sila_cpp`)
- `SiLA2::CSiLAClient` now has a much easier to work with interface as it uses SiLA convenience Data Types as parameters and return values instead of the ugly Protobuf Message types
- Time-/date-related data types now check if they're given a valid time/date
- SiLA servers and clients now have a command line parser with some default options that can be extended by subclasses

### Removed
- `CCommandConfirmation` data type class since it shouldn't be used by users (in the context of #32)

### Fixed
- #29: CMake tag version string parsing
- Almost all data types now have a default constructor
- AsyncRPCHandler: There won't be segmentation fault any more when an RPC gets cancelled while it's running in another thread.
- #30: Server crash when port is not available
- #34: An Observable Command that finished immediately after it was started won't cause the client hang any more

## [v0.1.0]
### Added
- Basic library structure (`src`, `examples`, `test`) and CMake project setup
- Implementation of the following parts of the SiLA 2 standard
  - SiLA Server and Client creation through dedicated base classes
  - Implementation of mandatory `SiLAService` Feature as part of the Server
  - SiLA Feature implementation through dedicated base class
  - Convenient creation of Unobservable Commands and Properties
  - Convenient creation of Observable Commands and Properties
  - Mapping of SiLA Error Handling on C++ errors
  - Convenience classes for SiLA Data Types
- Simple `HelloSiLA2` example to showcase the usage of the library
  - Implementation of `GreetingProvider` Feature showing how to use Unobservable Commands/Properties
  - Implementation of `TemperatureController` Feature showing how to use Observable Commands/Properties


[Unreleased]: https://gitlab.com/SiLA2/sila_cpp/-/tree/master
[v0.2.0]: https://gitlab.com/SiLA2/sila_cpp/-/compare/v0.1.0...v0.2.0
[v0.1.0]: https://gitlab.com/SiLA2/sila_cpp/-/releases/v0.1.0

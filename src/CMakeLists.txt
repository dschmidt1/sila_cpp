cmake_minimum_required(VERSION 3.13)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_VERBOSE_MAKEFILE ON)

if(SILA_CPP_USE_CONAN)
    message(STATUS "Using conan package manager for dependencies")
    include(../cmake/Conan.cmake)
    run_conan()
endif()

if(NOT MSVC)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17")
endif()
if(WIN32)
    # Tell gRPC we're using at least Windows Vista
    add_definitions(-D_WIN32_WINNT=0x600)
endif()

# contains helper function to generate C++ protobuf code from .proto files
include(../cmake/sila_cppConfig.cmake)
include(../cmake/Protobuf.cmake)

set(HEADER_PATH include/sila_cpp)
set(SOURCE_PATH lib)
set(STD_FEATURES_DIR framework/features/org/silastandard/core)

# This step might not seem necessary since the generated files are contained in
# the library. But it ensures that when SiLAFramework.proto changed in sila_base,
# the generated files will be regenerated and therefore kept up to date.
# Maybe this step will be replaced by a CI tool in the future but for now this
# seems to be the best solution for an up-to-date SiLAFramework.
protobuf_generate_cpp(PROTO_SRCS PROTO_HDRS
    ${CMAKE_CURRENT_SOURCE_DIR}/lib/sila_base/protobuf/SiLAFramework.proto
)

set(PROTOBUF_IMPORT_DIRS ${CMAKE_SOURCE_DIR}/src/lib/sila_base/protobuf)
set(DEST_PREFIX ${SOURCE_PATH}/${STD_FEATURES_DIR})
protoc_generate_cpp(FEATURE_PROTO_SRCS FEATURE_PROTO_IMPLS
    ${CMAKE_CURRENT_SOURCE_DIR}/${DEST_PREFIX}/meta/SiLAService.proto
)

set(SOURCES
    ${SOURCE_PATH}/client/SiLAClient.cpp

    ${SOURCE_PATH}/server/InterruptSignalHandler.cpp
    ${SOURCE_PATH}/server/SiLAServer.cpp

    ${SOURCE_PATH}/common/logging.cpp
    ${SOURCE_PATH}/common/ServerInformation.cpp
    ${SOURCE_PATH}/common/ServerAddress.cpp
    ${SOURCE_PATH}/common/FeatureID.cpp
    ${SOURCE_PATH}/common/SSLCredentials.cpp
    ${SOURCE_PATH}/common/SelfSignedCertificateHelper.cpp
    ${SOURCE_PATH}/common/Status.cpp
    ${SOURCE_PATH}/internal/Base64.cpp
    ${SOURCE_PATH}/internal/HostInfo.cpp
    ${SOURCE_PATH}/framework/data_types/CommandExecutionUUID.cpp

    ${SOURCE_PATH}/framework/data_types/ExecutionInfo.cpp
    ${SOURCE_PATH}/framework/data_types/SiLAAnyType.cpp
    ${SOURCE_PATH}/framework/data_types/SiLABinary.cpp
    ${SOURCE_PATH}/framework/data_types/SiLABoolean.cpp
    ${SOURCE_PATH}/framework/data_types/SiLADate.cpp
    ${SOURCE_PATH}/framework/data_types/SiLADuration.cpp
    ${SOURCE_PATH}/framework/data_types/SiLAInteger.cpp
    ${SOURCE_PATH}/framework/data_types/SiLAReal.cpp
    ${SOURCE_PATH}/framework/data_types/SiLAString.cpp
    ${SOURCE_PATH}/framework/data_types/SiLATime.cpp
    ${SOURCE_PATH}/framework/data_types/SiLATimestamp.cpp
    ${SOURCE_PATH}/framework/data_types/SiLATimezone.cpp

    ${SOURCE_PATH}/framework/error_handling/SiLAError.cpp
    ${SOURCE_PATH}/framework/error_handling/ValidationError.cpp
    ${SOURCE_PATH}/framework/error_handling/ExecutionError.cpp
    ${SOURCE_PATH}/framework/error_handling/FrameworkError.cpp
    ${SOURCE_PATH}/framework/error_handling/ClientError.cpp

    ${PROTO_SRCS}
    ${FEATURE_PROTO_SRCS}

    ${SOURCE_PATH}/${STD_FEATURES_DIR}/SiLAService/SiLAService.cpp
)
set(HEADERS
    ${HEADER_PATH}/global.h
    ${HEADER_PATH}/data_types.h

    ${HEADER_PATH}/internal/type_traits.h

    ${HEADER_PATH}/client/SiLAClient.h

    ${SOURCE_PATH}/server/InterruptSignalHandler.h
    ${HEADER_PATH}/server/SiLAServer.h
    ${HEADER_PATH}/server/SiLAFeature.h
    ${HEADER_PATH}/server/command/ICommandManager.h
    ${HEADER_PATH}/server/command/ICommandWrapper.h
    ${HEADER_PATH}/server/command/IObservableCommandManager.h
    ${HEADER_PATH}/server/command/IObservableCommandWrapper.h
    ${HEADER_PATH}/server/command/ObservableCommand.h
    ${HEADER_PATH}/server/command/ObservableCommandManager.h
    ${HEADER_PATH}/server/command/ObservableCommandWrapper.h
    ${HEADER_PATH}/server/command/UnobservableCommand.h
    ${HEADER_PATH}/server/command/UnobservableCommandManager.h
    ${HEADER_PATH}/server/command/UnobservableCommandWrapper.h
    ${HEADER_PATH}/server/property/IPropertyWrapper.h
    ${HEADER_PATH}/server/property/ObservablePropertyWrapper.h
    ${HEADER_PATH}/server/property/UnobservablePropertyWrapper.h
    ${HEADER_PATH}/server/property/PropertyObserver.h
    ${HEADER_PATH}/server/rpc/AsyncRPC.h
    ${HEADER_PATH}/server/rpc/AsyncRPCHandler.h
    ${HEADER_PATH}/server/rpc/GetRPC.h
    ${HEADER_PATH}/server/rpc/CommandRPC.h
    ${HEADER_PATH}/server/rpc/CommandInitRPC.h
    ${HEADER_PATH}/server/rpc/CommandInfoRPC.h
    ${HEADER_PATH}/server/rpc/CommandResultRPC.h
    ${HEADER_PATH}/server/rpc/IntermediateResultRPC.h
    ${HEADER_PATH}/server/rpc/SubscribeRPC.h
    ${HEADER_PATH}/server/rpc/definitions.h

    ${HEADER_PATH}/common/logging.h
    ${HEADER_PATH}/common/ServerInformation.h
    ${HEADER_PATH}/common/ServerAddress.h
    ${HEADER_PATH}/common/FeatureID.h
    ${HEADER_PATH}/common/SSLCredentials.h
    ${SOURCE_PATH}/common/SelfSignedCertificateHelper.h
    ${HEADER_PATH}/common/Status.h
    ${HEADER_PATH}/internal/Base64.h
    ${HEADER_PATH}/internal/HostInfo.h

    ${HEADER_PATH}/framework/data_types/CommandExecutionUUID.h
    ${HEADER_PATH}/framework/data_types/DataType.h
    ${HEADER_PATH}/framework/data_types/ExecutionInfo.h
    ${HEADER_PATH}/framework/data_types/SiLAAnyType.h
    ${HEADER_PATH}/framework/data_types/SiLABinary.h
    ${HEADER_PATH}/framework/data_types/SiLABoolean.h
    ${HEADER_PATH}/framework/data_types/SiLADate.h
    ${HEADER_PATH}/framework/data_types/SiLADuration.h
    ${HEADER_PATH}/framework/data_types/SiLAInteger.h
    ${HEADER_PATH}/framework/data_types/SiLAReal.h
    ${HEADER_PATH}/framework/data_types/SiLAString.h
    ${HEADER_PATH}/framework/data_types/SiLATime.h
    ${HEADER_PATH}/framework/data_types/SiLATimestamp.h
    ${HEADER_PATH}/framework/data_types/SiLATimezone.h
    ${HEADER_PATH}/framework/data_types/utils.h

    ${HEADER_PATH}/framework/error_handling/SiLAError.h
    ${SOURCE_PATH}/framework/error_handling/SiLAError_p.h
    ${HEADER_PATH}/framework/error_handling/ValidationError.h
    ${HEADER_PATH}/framework/error_handling/ExecutionError.h
    ${HEADER_PATH}/framework/error_handling/FrameworkError.h
    ${HEADER_PATH}/framework/error_handling/ClientError.h

    ${SOURCE_PATH}/${STD_FEATURES_DIR}/SiLAService/SiLAService.h

    ${PROTO_HDRS}
)

# library definition
add_library(${PROJECT_NAME}
    ${SOURCES} ${HEADERS}
    sila_cpp.qrc
)
target_include_directories(${PROJECT_NAME} SYSTEM PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/include>
)
target_include_directories(${PROJECT_NAME} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/${SOURCE_PATH}/${STD_FEATURES_DIR}>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/../third_party>
    $<INSTALL_INTERFACE:include>
)

if(${BUILD_SHARED_LIBS})
    message(STATUS "Building shared sila_cpp library")
    target_compile_definitions(${PROJECT_NAME} PRIVATE SILA_CPP_LIBRARY)
else()
    message(STATUS "Building static sila_cpp library")
    target_compile_definitions(${PROJECT_NAME} PUBLIC SILA_CPP_STATIC)
endif()

target_link_libraries(${PROJECT_NAME} PUBLIC
    protobuf::libprotobuf
    gRPC::grpc++
    Qt5::Core Qt5::Network
)
target_link_libraries(${PROJECT_NAME} PRIVATE QtZeroConf)

# Install
if(NOT DEFINED CMAKE_INSTALL_LIBDIR)
    message(STATUS "Manually setting CMAKE_INSTALL_LIBDIR to `lib'")
    set(CMAKE_INSTALL_LIBDIR lib)
endif()
set(INSTALL_CMAKEDIR "lib/cmake/${PROJECT_NAME}" CACHE STRING "Installation directory for cmake config files")

# create version file for CMake
include(CMakePackageConfigHelpers)
write_basic_package_version_file(
    "sila_cppConfigVersion.cmake"
    VERSION ${VERSION_SHORT}
    COMPATIBILITY SameMajorVersion
)

# install targets
# sila_cpp
install(TARGETS ${PROJECT_NAME}
    EXPORT "${PROJECT_NAME}Targets"
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
)
# QtZeroConf
install(TARGETS QtZeroConf
    EXPORT "${PROJECT_NAME}Targets"
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    PUBLIC_HEADER DESTINATION include/QtZeroConf
)

# install header files
install(DIRECTORY ${HEADER_PATH} DESTINATION include)
# install generated proto header files
install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${HEADER_PATH} DESTINATION include)
# install protobuf SiLA framework files
install(DIRECTORY ${PROTOBUF_IMPORT_DIRS} DESTINATION include/${PROJECT_NAME}/framework)
# install core SiLA features implementation header files
install(DIRECTORY ${SOURCE_PATH}/${STD_FEATURES_DIR}/ DESTINATION include/${PROJECT_NAME}/${STD_FEATURES_DIR}
    FILES_MATCHING PATTERN "*.h"
)
# install polymorphic_value
install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/../third_party/polymorphic_value DESTINATION include)
# install cmake files
install(FILES
    ../cmake/sila_cppConfig.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/sila_cppConfigVersion.cmake
    ../cmake/fix-include.cmake
    DESTINATION ${INSTALL_CMAKEDIR}
)
install(EXPORT "${PROJECT_NAME}Targets"
    DESTINATION ${INSTALL_CMAKEDIR}
    NAMESPACE sila2::
)
# install license
install(FILES
    "${CMAKE_SOURCE_DIR}/LICENSE"
    DESTINATION license
    COMPONENT license
)

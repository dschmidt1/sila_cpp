/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   IPropertyWrapper.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   14.07.2020
/// \brief  Definition of the IPropertyWrapper class
//============================================================================
#ifndef IPROPERTYWRAPPER_H
#define IPROPERTYWRAPPER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/internal/type_traits.h>

#include "PropertyObserver.h"

#include <QList>
#include <QMutex>

#include <google/protobuf/descriptor.h>

#include <string_view>

namespace SiLA2
{
/**
 * @brief The IPropertyWrapper class template is the base class for all SiLA 2
 * Properties
 *
 * @tparam T The underlying type of the Property
 * @tparam ResponsesT The type of the Property Response
 */
template<typename T, typename ResponsesT>
class IPropertyWrapper
{
    using PropertyObserver = CPropertyObserver<ResponsesT>;

public:
    /**
     * @brief C'tor
     *
     * @param Value The initial value of the Property
     * @param Name The name of the Property. This is for debugging/logging
     * purposes only.
     */
    explicit IPropertyWrapper(const T& Value, std::string_view Name = "");

    /**
     * @brief Add @a Observer to the list of observers for this Property. The new
     * observer will get the current value of this Property immediately.
     * Subsequently, all observers will be notified about changes in the value of
     * the Property.
     *
     * @param Observer A pointer to a CPropertyObserver derived instance that
     * should be added to the list of observers.
     */
    void addObserver(PropertyObserver* Observer);

    /**
     * @brief Remove @a Observer from the list of observers for this Property.
     *
     * @param Observer A pointer to a CPropertyObserver derived instance that
     * should be removed from the list of observers.
     */
    void removeObserver(PropertyObserver* Observer);

    /**
     * @brief Set the value of the Property to @a val. Notifies all currently
     * subscribed clients about the change
     *
     * @param val The new value to set
     */
    void setValue(const T& val);

    /**
     * @brief Get the current value of the Property.
     *
     * @return The current value of the Property
     */
    [[nodiscard]] T value() const;

    /**
     * @brief Append the given value @a val to the Property. This is only
     * available for Properties whose underlying type is a container (i.e.
     * Properties that have SiLA List Type).
     * @note Currently only STL-containers that provide the @c push_back()
     * function are supported. Also, @a val needs to be of the exact type of the
     * container's underlying type (e.g. when you have
     * @c std::vector<SiLA2::CString> as underlying type, you can only call this
     * function with a value of type @c SiLA2::CString).
     *
     * @tparam U The contained type of the Properties underlying container type
     *
     * @param val The value to append
     */
    template<typename U>
    void append(const U& val);

    /**
     * @brief Get the Name of the Property or an empty string if no Name was set
     * in the c'tor
     *
     * @return The Name of the Property
     */
    [[nodiscard]] std::string name() const;

    /**
     * @brief Add operator
     *
     * @tparam U The underlying type of the Property or something convertible to
     * the underlying type
     * @param rhs The value to add
     */
    template<typename U = T>
    typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                                  || internal::has_add_operator_v<T, U>,
                              IPropertyWrapper>
    operator+(const U& rhs) const;

    /**
     * @brief Subtract operator
     *
     * @tparam U The underlying type of the Property or something convertible to
     * the underlying type
     * @param rhs The value to subtract
     */
    template<typename U = T>
    typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                                  || internal::has_subtract_operator_v<T, U>,
                              IPropertyWrapper>
    operator-(const U& rhs) const;

    /**
     * @brief Multiply operator
     *
     * @tparam U The underlying type of the Property or something convertible to
     * the underlying type
     * @param rhs The value to multiply with
     */
    template<typename U = T>
    typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                                  || internal::has_multiply_operator_v<T, U>,
                              IPropertyWrapper>
    operator*(const U& rhs) const;

    /**
     * @brief Divide operator
     *
     * @tparam U The underlying type of the Property or something convertible to
     * the underlying type
     * @param rhs The value to divide by
     */
    template<typename U = T>
    typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                                  || internal::has_divide_operator_v<T, U>,
                              IPropertyWrapper>
    operator/(const U& rhs) const;

    /**
     * @brief Modulo operator
     *
     * @tparam U The underlying type of the Property or something convertible to
     * the underlying type
     * @param rhs The value to modulo by
     */
    template<typename U = T>
    typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                                  || internal::has_modulo_operator_v<T, U>,
                              IPropertyWrapper>
    operator%(const U& rhs) const;

    /**
     * @brief Add and assignment operator
     *
     * @tparam U The underlying type of the Property or something convertible to
     * the underlying type
     * @param rhs The value to add
     */
    template<typename U = T>
    typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                                  || internal::has_add_assignment_operator_v<T, U>,
                              IPropertyWrapper&>
    operator+=(const U& rhs);

    /**
     * @brief Subtract and assignment operator
     *
     * @tparam U The underlying type of the Property or something convertible to
     * the underlying type
     * @param rhs The value to subtract
     */
    template<typename U = T>
    typename std::enable_if_t<
        (std::is_pod_v<T> && std::is_pod_v<U>)
            || internal::has_subtract_assignment_operator_v<T, U>,
        IPropertyWrapper&>
    operator-=(const U& rhs);

    /**
     * @brief Multiply and assignment operator
     *
     * @tparam U The underlying type of the Property or something convertible to
     * the underlying type
     * @param rhs The value to multiply with
     */
    template<typename U = T>
    typename std::enable_if_t<
        (std::is_pod_v<T> && std::is_pod_v<U>)
            || internal::has_multiply_assignment_operator_v<T, U>,
        IPropertyWrapper&>
    operator*=(const U& rhs);

    /**
     * @brief Divide and assignment operator
     *
     * @tparam U The underlying type of the Property or something convertible to
     * the underlying type
     * @param rhs The value to divide by
     */
    template<typename U = T>
    typename std::enable_if_t<
        (std::is_pod_v<T> && std::is_pod_v<U>)
            || internal::has_divide_assignment_operator_v<T, U>,
        IPropertyWrapper&>
    operator/=(const U& rhs);

    /**
     * @brief Modulo and assignment operator
     *
     * @tparam U The underlying type of the Property or something convertible to
     * the underlying type
     * @param rhs The value to modulo by
     */
    template<typename U = T>
    typename std::enable_if_t<
        (std::is_pod_v<T> && std::is_pod_v<U>)
            || internal::has_modulo_assignment_operator_v<T, U>,
        IPropertyWrapper&>
    operator%=(const U& rhs);

    /**
     * @brief Equality comparison operator
     *
     * @tparam U The underlying type of the Property or something convertible to
     * the underlying type
     * @param rhs The value to compare
     *
     * @returns true, if the internal value is equal to the value of @a rhs
     * @returns false, otherwise
     */
    template<typename U = T>
    typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                                  || internal::has_equal_operator_v<T, U>,
                              bool>
    operator==(const U& rhs) const;

    /**
     * @brief Inequality comparison operator
     *
     * @tparam U The underlying type of the Property or something convertible to
     * the underlying type
     * @param rhs The value to compare
     *
     * @returns true, if the internal value is unequal to the value of @a rhs
     * @returns false, otherwise
     */
    template<typename U = T>
    typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                                  || internal::has_unequal_operator_v<T, U>,
                              bool>
    operator!=(const U& rhs) const;

    /**
     * @brief Less than comparison operator
     *
     * @tparam U The underlying type of the Property or something convertible to
     * the underlying type
     * @param rhs The value to compare
     *
     * @returns true, if the internal value is less than to the value of @a rhs
     * @returns false, otherwise
     */
    template<typename U = T>
    typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                                  || internal::has_less_operator_v<T, U>,
                              bool>
    operator<(const U& rhs) const;

    /**
     * @brief Greater than comparison operator
     *
     * @tparam U The underlying type of the Property or something convertible to
     * the underlying type
     * @param rhs The value to compare
     *
     * @returns true, if the internal value is greater than to the value of @a rhs
     * @returns false, otherwise
     */
    template<typename U = T>
    typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                                  || internal::has_greater_operator_v<T, U>,
                              bool>
    operator>(const U& rhs) const;

    /**
     * @brief Less than or equal comparison operator
     *
     * @tparam U The underlying type of the Property or something convertible to
     * the underlying type
     * @param rhs The value to compare
     *
     * @returns true, if the internal value is less than or equal to the value of
     * @a rhs
     * @returns false, otherwise
     */
    template<typename U = T>
    typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                                  || internal::has_less_equal_operator_v<T, U>,
                              bool>
    operator<=(const U& rhs) const;

    /**
     * @brief Greater than or equal comparison operator
     *
     * @tparam U The underlying type of the Property or something convertible to
     * the underlying type
     * @param rhs The value to compare
     *
     * @returns true, if the internal value is greater than or equal to the value
     * of @a rhs
     * @returns false, otherwise
     */
    template<typename U = T>
    typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                                  || internal::has_greater_equal_operator_v<T, U>,
                              bool>
    operator>=(const U& rhs) const;

private:
    /**
     * @brief Construct a response from @a m_Value
     *
     * @return The @a m_Value of the Property wrapped inside the @a Response type
     */
    [[nodiscard]] ResponsesT makeResponse() const;

    T m_Value{};               ///< Value of the underlying (actual Property) type
    const std::string m_Name;  ///< Name of the Property (for debugging)
    QMutex m_Mutex{};          ///< Mutex to protect setter access to @a m_Value
    QList<PropertyObserver*>
        m_Observers;  ///< Objects that observe Property value changes
};

//=============================================================================
template<typename T, typename ResponsesT>
IPropertyWrapper<T, ResponsesT>::IPropertyWrapper(const T& Value,
                                                  std::string_view Name)
    : m_Value{Value}, m_Name{Name}
{}

//=============================================================================
template<typename T, typename ResponsesT>
void IPropertyWrapper<T, ResponsesT>::addObserver(PropertyObserver* Observer)
{
    m_Observers.append(Observer);
    Observer->setValue(makeResponse());
}

//=============================================================================
template<typename T, typename ResponsesT>
void IPropertyWrapper<T, ResponsesT>::removeObserver(PropertyObserver* Observer)
{
    m_Observers.removeOne(Observer);
}

//=============================================================================
template<typename T, typename ResponsesT>
void IPropertyWrapper<T, ResponsesT>::setValue(const T& val)
{
    QMutexLocker Lock{&m_Mutex};
    m_Value = val;
    const auto Response = makeResponse();
    foreach (auto RPC, m_Observers)
    {
        RPC->setValue(Response);
    }
}

//=============================================================================
template<typename T, typename ResponsesT>
T IPropertyWrapper<T, ResponsesT>::value() const
{
    return m_Value;
}

//=============================================================================
template<typename T, typename ResponsesT>
std::string IPropertyWrapper<T, ResponsesT>::name() const
{
    return m_Name;
}

//=============================================================================
template<typename T, typename ResponsesT>
template<typename U>
void IPropertyWrapper<T, ResponsesT>::append(const U& val)
{
    static_assert(internal::is_container_v<T>,
                  "IPropertyWrapper::append() called for a Property whose "
                  "underlying type is not a container!");
    m_Value.push_back(val);
}

//=============================================================================
template<typename T, typename ResponsesT>
ResponsesT IPropertyWrapper<T, ResponsesT>::makeResponse() const
{
    ResponsesT Result;
    auto Reflection = Result.GetReflection();
    auto Field = Result.GetDescriptor()->field(0);
    if constexpr (internal::is_container_v<T>)
    {
        if (!Field->is_repeated())
        {
            qCCritical(sila_cpp)
                << "Cannot construct Protobuf message for Property that "
                   "has a container type as underlying type but is not "
                   "represented by a repeated Protobuf Message";
            return Result;
        }

        for_each(std::begin(m_Value), std::end(m_Value), [&](const auto& el) {
            using ElemT = std::remove_cv_t<decltype(el)>;
            Reflection->AddAllocatedMessage(&Result, Field, ElemT{el}.toProtoMessagePtr());
        });
    }
    else
    {
        Reflection->SetAllocatedMessage(&Result, T{m_Value}.toProtoMessagePtr(), Field);
    }
    return Result;
}

///===========================================================================
///                               Debug operator
///===========================================================================
template<typename T, typename ResponsesT>
QDebug operator<<(QDebug dbg, const IPropertyWrapper<T, ResponsesT>& rhs)
{
    return dbg << rhs.value();
}

///===========================================================================
///   Operator overloads to make a Property behave like its underlying type
///===========================================================================

//============================================================================
template<typename T, typename ResponsesT>
template<typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_add_operator_v<T, U>,
                          IPropertyWrapper<T, ResponsesT>>
IPropertyWrapper<T, ResponsesT>::operator+(const U& rhs) const
{
    auto res = *this;
    res.m_Value = m_Value + rhs;
    return res;
}

//============================================================================
template<typename T, typename ResponsesT>
template<typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_subtract_operator_v<T, U>,
                          IPropertyWrapper<T, ResponsesT>>
IPropertyWrapper<T, ResponsesT>::operator-(const U& rhs) const
{
    auto res = *this;
    res.m_Value = m_Value - rhs;
    return res;
}

//============================================================================
template<typename T, typename ResponsesT>
template<typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_multiply_operator_v<T, U>,
                          IPropertyWrapper<T, ResponsesT>>
IPropertyWrapper<T, ResponsesT>::operator*(const U& rhs) const
{
    auto res = *this;
    res.m_Value = m_Value * rhs;
    return res;
}

//============================================================================
template<typename T, typename ResponsesT>
template<typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_divide_operator_v<T, U>,
                          IPropertyWrapper<T, ResponsesT>>
IPropertyWrapper<T, ResponsesT>::operator/(const U& rhs) const
{
    auto res = *this;
    res.m_Value = m_Value / rhs;
    return res;
}

//============================================================================
template<typename T, typename ResponsesT>
template<typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_modulo_operator_v<T, U>,
                          IPropertyWrapper<T, ResponsesT>>
IPropertyWrapper<T, ResponsesT>::operator%(const U& rhs) const
{
    auto res = *this;
    res.m_Value = m_Value % rhs;
    return res;
}

//============================================================================
template<typename T, typename ResponsesT>
template<typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_add_assignment_operator_v<T, U>,
                          IPropertyWrapper<T, ResponsesT>&>
IPropertyWrapper<T, ResponsesT>::operator+=(const U& rhs)
{
    setValue(m_Value + rhs);
    return *this;
}

//============================================================================
template<typename T, typename ResponsesT>
template<typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_subtract_assignment_operator_v<T, U>,
                          IPropertyWrapper<T, ResponsesT>&>
IPropertyWrapper<T, ResponsesT>::operator-=(const U& rhs)
{
    setValue(m_Value - rhs);
    return *this;
}

//============================================================================
template<typename T, typename ResponsesT>
template<typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_multiply_assignment_operator_v<T, U>,
                          IPropertyWrapper<T, ResponsesT>&>
IPropertyWrapper<T, ResponsesT>::operator*=(const U& rhs)
{
    setValue(m_Value * rhs);
    return *this;
}

//============================================================================
template<typename T, typename ResponsesT>
template<typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_divide_assignment_operator_v<T, U>,
                          IPropertyWrapper<T, ResponsesT>&>
IPropertyWrapper<T, ResponsesT>::operator/=(const U& rhs)
{
    setValue(m_Value / rhs);
    return *this;
}

//============================================================================
template<typename T, typename ResponsesT>
template<typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_modulo_assignment_operator_v<T, U>,
                          IPropertyWrapper<T, ResponsesT>&>
IPropertyWrapper<T, ResponsesT>::operator%=(const U& rhs)
{
    setValue(m_Value % rhs);
    return *this;
}

//============================================================================
template<typename T, typename ResponsesT>
template<typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_equal_operator_v<T, U>,
                          bool>
IPropertyWrapper<T, ResponsesT>::operator==(const U& rhs) const
{
    return m_Value == rhs;
}

//============================================================================
template<typename T, typename ResponsesT>
template<typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_unequal_operator_v<T, U>,
                          bool>
IPropertyWrapper<T, ResponsesT>::operator!=(const U& rhs) const
{
    return m_Value != rhs;
}

//============================================================================
template<typename T, typename ResponsesT>
template<typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_less_operator_v<T, U>,
                          bool>
IPropertyWrapper<T, ResponsesT>::operator<(const U& rhs) const
{
    return m_Value < rhs;
}

//============================================================================
template<typename T, typename ResponsesT>
template<typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_greater_operator_v<T, U>,
                          bool>
IPropertyWrapper<T, ResponsesT>::operator>(const U& rhs) const
{
    return m_Value > rhs;
}

//============================================================================
template<typename T, typename ResponsesT>
template<typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_less_equal_operator_v<T, U>,
                          bool>
IPropertyWrapper<T, ResponsesT>::operator<=(const U& rhs) const
{
    return m_Value <= rhs;
}

//============================================================================
template<typename T, typename ResponsesT>
template<typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_greater_equal_operator_v<T, U>,
                          bool>
IPropertyWrapper<T, ResponsesT>::operator>=(const U& rhs) const
{
    return m_Value >= rhs;
}

///===========================================================================
///                Non-member operator overloads for symmetry
///===========================================================================
template<typename T, typename ResponsesT, typename U = T>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_add_operator_v<U, T>,
                          U>
operator+(const U& lhs, const IPropertyWrapper<T, ResponsesT>& rhs)
{
    return lhs + rhs.value();
}

//============================================================================
template<typename T, typename ResponsesT, typename U = T>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_subtract_operator_v<U, T>,
                          U>
operator-(const U& lhs, const IPropertyWrapper<T, ResponsesT>& rhs)
{
    return lhs - rhs.value();
}

//============================================================================
template<typename T, typename ResponsesT, typename U = T>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_multiply_operator_v<U, T>,
                          U>
operator*(const U& lhs, const IPropertyWrapper<T, ResponsesT>& rhs)
{
    return lhs * rhs.value();
}

//============================================================================
template<typename T, typename ResponsesT, typename U = T>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_divide_operator_v<U, T>,
                          U>
operator/(const U& lhs, const IPropertyWrapper<T, ResponsesT>& rhs)
{
    return lhs / rhs.value();
}

//============================================================================
template<typename T, typename ResponsesT, typename U = T>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_modulo_operator_v<U, T>,
                          U>
operator%(const U& lhs, const IPropertyWrapper<T, ResponsesT>& rhs)
{
    return lhs % rhs.value();
}

//============================================================================
template<typename T, typename ResponsesT, typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_equal_operator_v<T, U>,
                          bool>
operator==(const U& lhs, const IPropertyWrapper<T, ResponsesT>& rhs)
{
    return rhs == lhs;
}

//============================================================================
template<typename T, typename ResponsesT, typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_unequal_operator_v<T, U>,
                          bool>
operator!=(const U& lhs, const IPropertyWrapper<T, ResponsesT>& rhs)
{
    return rhs != lhs;
}

//============================================================================
template<typename T, typename ResponsesT, typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_less_operator_v<T, U>,
                          bool>
operator<(const U& lhs, const IPropertyWrapper<T, ResponsesT>& rhs)
{
    return rhs > lhs;
}

//============================================================================
template<typename T, typename ResponsesT, typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_greater_operator_v<T, U>,
                          bool>
operator>(const U& lhs, const IPropertyWrapper<T, ResponsesT>& rhs)
{
    return rhs < lhs;
}

//============================================================================
template<typename T, typename ResponsesT, typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_less_equal_operator_v<T, U>,
                          bool>
operator<=(const U& lhs, const IPropertyWrapper<T, ResponsesT>& rhs)
{
    return rhs >= lhs;
}

//============================================================================
template<typename T, typename ResponsesT, typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_greater_equal_operator_v<T, U>,
                          bool>
operator>=(const U& lhs, const IPropertyWrapper<T, ResponsesT>& rhs)
{
    return rhs <= lhs;
}
}  // namespace SiLA2

#endif  // IPROPERTYWRAPPER_H

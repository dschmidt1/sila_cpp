/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ICommandWrapper.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   13.07.2020
/// \brief  Definition of the ICommandWrapper class
//============================================================================
#ifndef ICOMMANDWRAPPER_H
#define ICOMMANDWRAPPER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/Status.h>
#include <sila_cpp/framework/error_handling/ExecutionError.h>

#include "ICommandManager.h"

namespace SiLA2
{
/**
 * @brief The ICommandWrapper class is the base class for every SiLA 2 Command
 * containing common functionality like accessors to the Command Parameters and
 * the Command Result
 *
 * @tparam CommandWrapperT The class that inherits from this class template
 */
template<typename CommandWrapperT>
class ICommandWrapper
{
protected:
    using CommandManager = ICommandManager<CommandWrapperT>;
    using ExecutorF = typename CommandManager::ExecutorF;
    using ParametersT = typename CommandManager::Parameters;
    using ResponsesT = typename CommandManager::Responses;

public:
    /**
     * @brief C'tor
     *
     * @param Manager A pointer to the Command Manager that manages this Command
     */
    explicit ICommandWrapper(CommandManager* Manager);

    /**
     * @brief D'tor
     */
    virtual ~ICommandWrapper() = default;

    /**
     * @brief Get the Command's parameters
     *
     * @return The Command's parameters
     */
    [[nodiscard]] ParametersT parameters() const;

    /**
     * @brief Set the Command's Parameters to @a Params
     *
     * @param Params The Command Parameters received from the client
     */
    void setParameters(const ParametersT& Params);

    /**
     * @brief Get the current status of the Command Execution. This can be called
     * anytime during Command Execution. If an Error occurred, this function will
     * return the Error. If no Error occurred so far, this returns @a CStatus::OK.
     * Hence this function can be used to get both, Errors from Parameter
     * Validation and Execution Errors.
     *
     * @return The current status of the Execution
     */
    [[nodiscard]] CStatus status() const;

    /**
     * @brief Retrieve the Command result
     *
     * @return The Command Responses
     */
    [[nodiscard]] ResponsesT result() const;

    /**
     * @brief Get the Executor function
     *
     * @return The Executor
     */
    [[nodiscard]] const ExecutorF executor() const;

    /**
     * @brief Start the Command Execution.
     */
    virtual void execute() = 0;

    /**
     * @brief Request interruption of the Command Execution. Long running Commands
     * should regularly call @a isInterruptionRequested() and terminate themselves
     * in a defined manner.
     */
    virtual void requestInterruption() = 0;

    /**
     * @brief Check if the Command Execution should be interrupted, because it was
     * requested e.g. by another call to the same Command. Regularly poll this
     * function to make your Command Execution cleanly interruptable. Take care
     * not to call it too often, to keep the overhead low. Not reacting on the
     * interruption request will not terminate the Execution forcefully. In this
     * case, not polling this function for long running Commands will make the
     * Command Execution non-interruptable.
     *
     * @return true, if the Execution should be interrupted, false otherwise
     */
    [[nodiscard]] virtual bool isInterruptionRequested() const = 0;

protected:
    ResponsesT m_Result{};  ///< The Command result
    CStatus m_Status{};     ///< Status fo the Execution (contains possible Error)

private:
    ParametersT m_Parameters;         ///< The Command parameters
    const CommandManager* m_Manager;  ///< The Command's Manager
};

//============================================================================
template<typename CommandWrapperT>
ICommandWrapper<CommandWrapperT>::ICommandWrapper(CommandManager* Manager)
    : m_Manager{Manager}
{}

//============================================================================
template<typename CommandWrapperT>
typename ICommandWrapper<CommandWrapperT>::ParametersT
ICommandWrapper<CommandWrapperT>::parameters() const
{
    return m_Parameters;
}

//============================================================================
template<typename CommandWrapperT>
void ICommandWrapper<CommandWrapperT>::setParameters(const ParametersT& Params)
{
    m_Parameters = Params;
}

//============================================================================
template<typename CommandWrapperT>
typename ICommandWrapper<CommandWrapperT>::ResponsesT
ICommandWrapper<CommandWrapperT>::result() const
{
    return m_Result;
}

//============================================================================
template<typename CommandWrapperT>
CStatus ICommandWrapper<CommandWrapperT>::status() const
{
    return m_Status;
}

//============================================================================
template<typename CommandWrapperT>
const typename ICommandWrapper<CommandWrapperT>::ExecutorF
ICommandWrapper<CommandWrapperT>::executor() const
{
    return m_Manager->executor();
}
}  // namespace SiLA2

#endif  // ICOMMANDWRAPPER_H

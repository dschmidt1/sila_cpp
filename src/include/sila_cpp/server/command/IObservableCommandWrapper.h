/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   IObservableCommandWrapper.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   23.06.2020
/// \brief  Definition of the IObservableCommandWrapper class
//============================================================================
#ifndef IOBSERVABLECOMMANDWRAPPER_H
#define IOBSERVABLECOMMANDWRAPPER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/framework/data_types/CommandExecutionUUID.h>
#include <sila_cpp/framework/data_types/ExecutionInfo.h>

#include <QObject>

namespace SiLA2
{
/**
 * @brief The IObservableCommandWrapper class is the non-templated base class of a
 * SiLA 2 Observable Command Wrapper. It derives from QObject and thus provides
 * useful Qt features (like signals/slots) to work with the templated derived
 * class CObservableCommandWrapper.
 */
class SILA_CPP_EXPORT IObservableCommandWrapper : public QObject
{
    Q_OBJECT

public:
    /**
     * @brief C'tor
     */
    IObservableCommandWrapper()
    {
        // To use the following types in queued connections they needs to be
        // registered to QMetaType; but Q_DECLARE_METATYPE isn't enough since the
        // names are resolved at run time
        qRegisterMetaType<CExecutionInfo>("const CExecutionInfo&");
        qRegisterMetaType<CommandStatus>("CommandStatus");
        qRegisterMetaType<CDuration>("const CDuration&");
        qRegisterMetaType<CCommandExecutionUUID>("const CCommandExecutionUUID&");
    }

signals:
    /**
     * @brief Indicates that there is new Execution Info to be sent to the Client.
     *
     * @param ExecutionInfo The Execution Info to send
     */
    void executionInfoReady(const CExecutionInfo& ExecutionInfo) const;

    /**
     * @brief Indicates that there is a new Intermediate Result to be sent to the
     * client. Use the @a CObservableCommandWrapper::intermediateResult method to
     * obtain the Result.
     * @note Emitting this signal when the Command doesn't have Intermediate
     * Responses has no effect.
     */
    void intermediateResultReady() const;

    /**
     * @brief Indicates that the Execution has finished with @a Status.
     *
     * @param ExecutionInfo The Execution Info with the Status of the finished
     * Execution (should be either
     * @c CExecutionInfo::CommandStatus::FINISHED_SUCCESSFULLY or
     * @c CExecutionInfo::CommandStatus::FINISHED_WITH_ERROR)
     */
    void executionFinished(const CExecutionInfo& ExecutionInfo) const;

    /**
     * @brief This signal is used to indicate a change in the Lifetime of
     * Execution in an Observable Command. It's used to send the updated lifetime
     * from the ExecutorThread to the thread that started the Executor.
     *
     * @param UpdatedLifetime The new lifetime of the Command
     */
    void lifetimeUpdated(const CDuration& UpdatedLifetime) const;

    /**
     * @brief This signal is emitted whenever the Command's Lifetime of Execution
     * has expired and thus all it's associated resources can be deleted.
     *
     * @param UUID The UUID that identifies the Command Wrapper for this execution
     */
    void lifetimeExpired(const CCommandExecutionUUID& UUID) const;
};
}  // namespace SiLA2
#endif  // IOBSERVABLECOMMANDWRAPPER_H

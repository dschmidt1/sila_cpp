/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ObservableCommandWrapper.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   23.06.2020
/// \brief  Definition of the CObservableCommandWrapper class
//============================================================================
#ifndef OBSERVABLECOMMANDWRAPPER_H
#define OBSERVABLECOMMANDWRAPPER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/Status.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/data_types/CommandExecutionUUID.h>
#include <sila_cpp/framework/data_types/SiLADuration.h>
#include <sila_cpp/framework/error_handling/ExecutionError.h>
#include <sila_cpp/internal/type_traits.h>

#include "ICommandWrapper.h"
#include "IObservableCommandWrapper.h"

#include <QEventLoop>
#include <QThread>
#include <QTimer>

#include <utility>

namespace SiLA2
{
/**
 * @brief The CObservableCommandWrapper class represents one Command Execution of
 * an Observable Command.
 *
 * @tparam ParametersT The type of the Command Parameters
 * @tparam ResponsesT The type of the Command Responses
 * @tparam IntermediateResponsesT The type of the Intermediate Responses (not
 * actually a pack, but rather an optional type parameter, we can't use a void
 * default because we have a member variable of this type)
 */
template<typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
class CObservableCommandWrapper :
    public IObservableCommandWrapper,
    public ICommandWrapper<CObservableCommandWrapper<ParametersT, ResponsesT,
                                                     IntermediateResponsesT...>>
{
    using Base =
        ICommandWrapper<CObservableCommandWrapper<ParametersT, ResponsesT,
                                                  IntermediateResponsesT...>>;
    using CommandManager = typename Base::CommandManager;

public:
    /**
     * @brief C'tor
     *
     * @param UUID The Command's UUID for this execution
     * @param Lifetime The initial Lifetime of this Command (specifies the
     * Duration for which the UUID of a Command Execution is valid). If
     * Lifetime.isNull() is true, no Lifetime will be used. The Lifetime can be
     * updated when calling @a CObservableCommandWrapper::setExecutionInfo().
     * @param Manager A pointer to the Command Manager that manages this Command
     *
     * @sa CObservableCommandWrapper::setExecutionInfo()
     */
    CObservableCommandWrapper(CCommandExecutionUUID UUID, CDuration Lifetime,
                              CommandManager* Manager);

    /**
     * @brief Get the current Command Execution UUID
     *
     * @return The current UUID
     */
    [[nodiscard]] CCommandExecutionUUID uuid() const;

    /**
     * @override
     * @brief Starts the @a m_ExecutorThread.
     */
    void execute() override;

    /**
     * @override
     * @brief Requests interruption of the @a m_ExecutorThread. Poll the @a
     * isInterruptionRequested() function if you want to be notified about
     * interruption of the Command Execution.
     * @sa ICommandWrapper::isInterruptionRequested()
     */
    void requestInterruption() override;

    /**
     * @override
     * @brief Check if the @a m_ExecutorThread has been requested to be
     * interrupted
     *
     * @return true, if the Thread should be interrupted, false otherwise
     */
    [[nodiscard]] bool isInterruptionRequested() const override;

    /**
     * @brief Whether the Command Execution has been started (i.e. whether the
     * Validation of Parameters was successful and the first Execution info has
     * been set).
     *
     * @returns true, if the Execution has been started
     * @returns false, otherwise
     */
    [[nodiscard]] bool started() const;

    /**
     * @brief Whether the Command Execution has finished (i.e. whether the
     * Executor Thread has finished).
     *
     * @returns true, if the Executor Thread has finished
     * @returns false, otherwise
     */
    [[nodiscard]] bool finished() const;

    /**
     * @brief Request resending of the last Execution Info
     */
    void requestLastExecutionInfo() const;

    /**
     * @brief Set the Command's Execution Info. This will emit the corresponding
     * signal with the updated Execution Info.
     *
     * @param Progress The progress of the Command in percent (from 0.0 to 1.0)
     * @param RemainingTime The estimated remaining time of the Command (optional)
     * @param UpdatedLifetime The updated Lifetime of Execution of the Command
     * (optional, only used when an initial Lifetime has been specified in the
     * c'tor of @a CObservableCommandManager). Can be left empty if the Lifetime
     * shouldn't be updated.
     *
     * @note When this is called for the first time, it means that Parameter
     * Validation was successful and that the actual Execution of the Command
     * Logic has been started.
     *
     * @sa CObservableCommandManager::CObservableCommandManager()
     */
    void setExecutionInfo(const CReal& Progress,
                          const CDuration& RemainingTime = {},
                          const CDuration& UpdatedLifetime = {});

    /**
     * @brief Set the Intermediate Result. This will emit the corresponding
     * signal. Use the @a intermediateResult() method to get the Result.
     * @note This function is only available if Intermediate Responses have been
     * specified in the FDL.
     *
     * @param Result The Intermediate Result
     */
    template<typename T = internal::first_type_t<IntermediateResponsesT...>,
             typename = std::enable_if_t<!std::is_void_v<T>>>
    void setIntermediateResult(const IntermediateResponsesT&... Result);

    /**
     * @brief Get the current Intermediate Result
     * @note This function is only available if Intermediate Responses have been
     * specified in the FDL.
     *
     * @return IntermediateResponses The current Intermediate Result
     */
    template<typename T = internal::first_type_t<IntermediateResponsesT...>,
             typename = std::enable_if_t<!std::is_void_v<T>>>
    auto intermediateResult() const;

private:
    /**
     * @brief Set the Command's execution info. This will emit the
     * corresponding signal  with the updated Execution Info.
     *
     * @note This overload is for internal use only since the user doesn't
     * need to specify the status himself
     *
     * @param Status The current status of the command
     * @param Progress The progress of the Command in percent (from 0.0 to 1.0)
     * @param RemainingTime The estimated remaining time of the Command
     * @param UpdatedLifetime The updated Lifetime of Execution of the Command
     */
    void setExecutionInfo(CommandStatus Status, const CReal& Progress,
                          const CDuration& RemainingTime,
                          const CDuration& UpdatedLifetime);

    const CCommandExecutionUUID m_UUID{};  ///< UUID of this Command
    CDuration m_Lifetime{};                ///< Lifetime of Execution
    QTimer m_LifetimeTimer{};  ///< Timer to fire on Lifetime expiration
    std::tuple<IntermediateResponsesT...>
        m_IntermediateResult;     ///< Current Intermediate Result
    CExecutionInfo m_LastInfo{};  ///< Last execution info from the Command

    class CExecutorThread;
    friend class CExecutorThread;
    CExecutorThread m_ExecutorThread;  ///< Thread that executes the actual command
    bool m_ExecutionStarted{};  ///< Whether Validation was successful and Execution has started

    /**
     * @brief The CExecutorThread class is a thread that executes the Executor
     * function and provides an accessor for the Execution Result.
     */
    class CExecutorThread : public QThread
    {
    public:
        /**
         * @brief C'tor
         *
         * @param parent The Observable Command Wrapper instance that this thread
         * belongs to
         */
        explicit CExecutorThread(CObservableCommandWrapper* parent);

    protected:
        void run() override;

    private:
        CObservableCommandWrapper*
            m_CommandWrapper;  ///< The Command that this thread belongs to
    };
};

///============================================================================
///                  CObservableCommandWrapper implementation
///============================================================================
template<typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
CObservableCommandWrapper<ParametersT, ResponsesT, IntermediateResponsesT...>::
    CObservableCommandWrapper(CCommandExecutionUUID UUID, CDuration Lifetime,
                              CommandManager* Manager)
    : Base{Manager},
      m_UUID{std::move(UUID)},
      m_Lifetime{std::move(Lifetime)},
      m_LifetimeTimer{this},
      m_ExecutorThread{this}
{
    // NOTE: This slot (lambda)  will be executed in the signalling thread
    connect(&m_ExecutorThread, &CExecutorThread::finished, [this]() {
        // If the Command Execution finishes immediately after it was started,
        // the Command Info RPC might not have connected to our signals yet.
        // Give it some time to do so. Otherwise the client won't be notified
        // properly if the connections aren't established.
        //        QThread::msleep(75); // TODO Remove
        qCDebug(sila_cpp) << "Executor thread finished";

        const auto Status = this->m_Status.ok() ?
                                CommandStatus::FINISHED_SUCCESSFULLY :
                                CommandStatus::FINISHED_WITH_ERROR;

        if (m_Lifetime.isNull())
        {
            emit executionFinished({Status, 1, 0});
        }
        else
        {
            emit executionFinished({Status, 1, 0, m_Lifetime});
        }
    });

    // save the last Execution Info for the Command Info RPC in case it needs to
    // explicitly request the Info (this is the case when the Command Execution
    // finishes right away; the original signal only goes to Command Init RPC
    // which is why the Command Info RPC would be stuck waiting for more Info)
    connect(this, &IObservableCommandWrapper::executionFinished, this,
            [this](const auto& Info) { m_LastInfo = Info; });

    // signal to our Manager that our lifetime has expired
    m_LifetimeTimer.setSingleShot(true);
    m_LifetimeTimer.start(m_Lifetime.toMilliSeconds());
    connect(&m_LifetimeTimer, &QTimer::timeout, this,
            [this]() { emit lifetimeExpired(m_UUID); });

    connect(
        this, &IObservableCommandWrapper::lifetimeUpdated, this,
        [this](const CDuration& UpdatedLifetime) {
            // restart the timer with the added duration from the
            // UpdatedLifetime
            const auto RemainingTime = m_LifetimeTimer.remainingTimeAsDuration()
                                       + UpdatedLifetime.toMilliSeconds()
                                       - m_Lifetime.toMilliSeconds();
            m_LifetimeTimer.start(RemainingTime);
        },
        Qt::BlockingQueuedConnection);
}

//============================================================================
template<typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
CCommandExecutionUUID CObservableCommandWrapper<
    ParametersT, ResponsesT, IntermediateResponsesT...>::uuid() const
{
    return m_UUID;
}

//============================================================================
template<typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
void CObservableCommandWrapper<ParametersT, ResponsesT,
                               IntermediateResponsesT...>::execute()
{
    m_ExecutorThread.start();
}

//============================================================================
template<typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
void CObservableCommandWrapper<ParametersT, ResponsesT,
                               IntermediateResponsesT...>::requestInterruption()
{
    m_ExecutorThread.requestInterruption();
}

//============================================================================
template<typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
bool CObservableCommandWrapper<
    ParametersT, ResponsesT, IntermediateResponsesT...>::isInterruptionRequested()
    const
{
    return m_ExecutorThread.isInterruptionRequested();
}

//============================================================================
template<typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
bool CObservableCommandWrapper<ParametersT, ResponsesT,
                               IntermediateResponsesT...>::started() const
{
    return m_ExecutionStarted;
}

//============================================================================
template<typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
bool CObservableCommandWrapper<ParametersT, ResponsesT,
                               IntermediateResponsesT...>::finished() const
{
    return m_ExecutorThread.isFinished();
}

//============================================================================
template<typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
void CObservableCommandWrapper<
    ParametersT, ResponsesT, IntermediateResponsesT...>::requestLastExecutionInfo()
    const
{
    emit executionFinished(m_LastInfo);
}

//============================================================================
template<typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
void CObservableCommandWrapper<
    ParametersT, ResponsesT,
    IntermediateResponsesT...>::setExecutionInfo(const CReal& Progress,
                                                 const CDuration& RemainingTime,
                                                 const CDuration& UpdatedLifetime)
{
    setExecutionInfo(CommandStatus::RUNNING, Progress, RemainingTime,
                     UpdatedLifetime);
}

//============================================================================
template<typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
template<typename T, typename>
void CObservableCommandWrapper<ParametersT, ResponsesT, IntermediateResponsesT...>::
    setIntermediateResult(const IntermediateResponsesT&... Result)
{
    m_IntermediateResult = std::make_tuple(Result...);
    emit intermediateResultReady();
}

//============================================================================
template<typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
template<typename T, typename>
auto CObservableCommandWrapper<ParametersT, ResponsesT,
                               IntermediateResponsesT...>::intermediateResult()
    const
{
    return std::get<0>(m_IntermediateResult);
}

//============================================================================
template<typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
void CObservableCommandWrapper<
    ParametersT, ResponsesT,
    IntermediateResponsesT...>::setExecutionInfo(CommandStatus Status,
                                                 const CReal& Progress,
                                                 const CDuration& RemainingTime,
                                                 const CDuration& UpdatedLifetime)
{
    m_ExecutionStarted = true;

    if (m_Lifetime.isNull())
    {
        emit executionInfoReady({Status, Progress, RemainingTime});
        return;
    }

    if (UpdatedLifetime > m_Lifetime)
    {
        emit lifetimeUpdated(UpdatedLifetime);
        m_Lifetime = UpdatedLifetime;
    }
    else if (!UpdatedLifetime.isNull() && UpdatedLifetime != m_Lifetime)
    {
        qCWarning(sila_cpp)
            << "Updated Lifetime of Execution is less than the previous "
               "Lifetime specified! Won't update the Lifetime.\nPrevious "
               "Lifetime:"
            << m_Lifetime << "\nUpdated Lifetime:" << UpdatedLifetime;
    }
    emit executionInfoReady({Status, Progress, RemainingTime, m_Lifetime});
}

///============================================================================
///                       CExecutorThread implementation
///============================================================================
template<typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
CObservableCommandWrapper<ParametersT, ResponsesT, IntermediateResponsesT...>::
    CExecutorThread::CExecutorThread(CObservableCommandWrapper* parent)
    : QThread{parent}, m_CommandWrapper{parent}
{}

//============================================================================
template<typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
void CObservableCommandWrapper<ParametersT, ResponsesT,
                               IntermediateResponsesT...>::CExecutorThread::run()
{
    qCDebug(sila_cpp) << "Started new executor thread";
    try
    {
        m_CommandWrapper->m_Result =
            m_CommandWrapper->executor()(m_CommandWrapper);
    }
    catch (const CSiLAError& e)
    {
        m_CommandWrapper->m_Status = CStatus::fromSiLAError(e);
    }
}
}  // namespace SiLA2

#endif  // OBSERVABLECOMMANDWRAPPER_H

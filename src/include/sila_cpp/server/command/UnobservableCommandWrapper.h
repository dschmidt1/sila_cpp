/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   UnobservableCommandWrapper.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   14.07.2020
/// \brief  Declaration of the CUnobservableCommandWrapper class
//============================================================================
#ifndef UNOBSERVABLECOMMANDWRAPPER_H
#define UNOBSERVABLECOMMANDWRAPPER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include "ICommandWrapper.h"

#include <string_view>

namespace SiLA2
{
/**
 * @brief The CUnobservableCommandWrapper class represents one Command Execution
 * of an Unobservable Command.
 *
 * @tparam ParametersT The type of the Command Parameters
 * @tparam ResponsesT The type of the Command Responses
 */
template<typename ParametersT, typename ResponsesT>
class CUnobservableCommandWrapper :
    public ICommandWrapper<CUnobservableCommandWrapper<ParametersT, ResponsesT>>
{
    using Base =
        ICommandWrapper<CUnobservableCommandWrapper<ParametersT, ResponsesT>>;
    using CommandManager = typename Base::CommandManager;

public:
    /**
     * @brief C'tor
     *
     * @param Manager A pointer to the Command Manager that manages this Command
     * @param Name The name of the Command. This is for debugging/logging
     * purposes only.
     */
    explicit CUnobservableCommandWrapper(CommandManager* Manager,
                                         std::string_view Name = "");

    /**
     * @override
     * @brief Starts the Command Execution by calling the Executor function.
     */
    void execute() override;

    /**
     * @override
     * @brief Requests interruption of the Executor function execution. Poll the
     * @a isInterruptionRequested() function if you want to be notified about
     * interruption of the Command Execution.
     * @sa ICommandWrapper::isInterruptionRequested()
     */
    void requestInterruption() override;

    /**
     * @override
     * @brief Check if the execution of the Executor Function should interrupted
     *
     * @return true, if the function should interrupted, false otherwise
     */
    [[nodiscard]] bool isInterruptionRequested() const override;

    /**
     * @brief Get the Name of the Command or an empty string if no Name was set
     * in the c'tor
     *
     * @return The Name of the Command
     */
    [[nodiscard]] std::string name() const;

private:
    bool m_Interrupted{false};   ///< Whether the Execution has been interrupted
    const std::string m_Name{};  ///< The Name of the Command (for debugging)
};

//============================================================================
template<typename ParametersT, typename ResponsesT>
CUnobservableCommandWrapper<ParametersT, ResponsesT>::CUnobservableCommandWrapper(
    CommandManager* Manager, std::string_view Name)
    : Base{Manager}, m_Name{Name}
{}

//============================================================================
template<typename ParametersT, typename ResponsesT>
void CUnobservableCommandWrapper<ParametersT, ResponsesT>::execute()
{
    // reset everything for the next run of the execution
    m_Interrupted = false;
    this->m_Status = CStatus::OK;
    this->m_Result = ResponsesT{};

    try
    {
        this->m_Result = this->executor()(this);
    }
    catch (const CSiLAError& e)
    {
        this->m_Status = CStatus::fromSiLAError(e);
    }
}

//============================================================================
template<typename ParametersT, typename ResponsesT>
void CUnobservableCommandWrapper<ParametersT, ResponsesT>::requestInterruption()
{
    m_Interrupted = true;
}

//============================================================================
template<typename ParametersT, typename ResponsesT>
bool CUnobservableCommandWrapper<ParametersT,
                                 ResponsesT>::isInterruptionRequested() const
{
    return m_Interrupted;
}

//============================================================================
template<typename ParametersT, typename ResponsesT>
std::string CUnobservableCommandWrapper<ParametersT, ResponsesT>::name() const
{
    return m_Name;
}

}  // namespace SiLA2

#endif  // UNOBSERVABLECOMMANDWRAPPER_H

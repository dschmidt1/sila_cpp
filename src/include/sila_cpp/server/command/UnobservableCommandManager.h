/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   UnobservableCommandManager.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   13.07.2020
/// \brief  Definition of the CUnobservableCommandManager class
//============================================================================
#ifndef UNOBSERVABLECOMMANDMANAGER_H
#define UNOBSERVABLECOMMANDMANAGER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/FeatureID.h>
#include <sila_cpp/server/rpc/AsyncRPCHandler.h>
#include <sila_cpp/server/rpc/CommandRPC.h>
#include <sila_cpp/server/rpc/definitions.h>

#include "ICommandManager.h"
#include "UnobservableCommandWrapper.h"

#include <QObject>

#include <string>

namespace SiLA2
{
/**
 * @brief The CUnobservableCommandManager class is a class template that allows to
 * implement SiLA 2 Unobservable Commands.
 *
 * @tparam RequestCommand Member function pointer of the `Request<Command>`
 *
 * @details As a user you just need to create a CUnobservableCommandManager
 * instance providing the necessary function for the Unobservable Command as
 * template parameter and give it a pointer to your Feature implementation in the
 * c'tor. Optionally, you can provide the Name of the Command (only used for
 * debugging, so it doesn't need to be Fully Qualified). Additionally, you
 * need to provide a function that contains the actual logic of the Command (the
 * Executor). The rest is handled by this class. For example
 * @code
 * class MySiLAFeatureImpl : public SiLA2::CSiLAFeature<...>
 * {
 *  using MyUnobservableCommand = SiLA2::CUnobservableCommandManager<
 *      &MySiLAFeatureImpl::RequestMyUnobservableCommand>;
 *  using MyUnobservableCommandWrapper =
 *      SiLA2::CUnobservableCommandWrapper<MyUnobservableCommand_Parameters,
 *                                       MyUnobservableCommand_Responses>;
 * public:
 *     MySiLAFeatureImpl()
 *         : m_MyUnobservableCommand{this, "MyUnobservableCommand"}
 *     {
 *         m_MyUnobservableCommand.setExecutor(
 *             this, &MySiLAFeatureImpl::MyUnobservableCommandExecutor);
 *     }
 *
 *     MyUnobservableCommand_Responses MyUnobservableCommandExecutor(
 *         MyUnobservableCommandWrapper* Command)
 *     {
 *     }
 * private:
 *     MyUnobservableCommand m_MyUnobservableCommand;
 * };
 * @endcode
 *
 * If your Executor contains relatively little code and you don't want to use a
 * dedicated member function, you can also pass it as anonymous lambda to the
 * Command Manager.
 *
 * @note The SiLA 2 standard requires validation of the Command Parameters before
 * starting your actual implementation of the Command. If any of the Parameters
 * don't meet your requirements, you need to throw a corresponding Validation
 * Error. Also, if your Command Execution fails, you can throw a corresponding
 * Execution Error to notify the client that something went wrong.
 *
 * See the CGreetingProviderImpl class in the HelloSiLA2 example for an
 * example on how to implement Unobservable Commands.
 */
template<auto RequestCommand>
class CUnobservableCommandManager;

/**
 * @brief Template specialisation of the CUnobservableCommandManager class
 *
 * @tparam RequestServiceT The class that contains the `Request<Command>` member
 * function
 * @tparam ParametersT The type of the Command Parameters
 * @tparam ResponsesT The type of the Command Responses
 * @tparam RC `Request<Command>` member function pointer
 */
template<typename RequestServiceT, typename ParametersT, typename ResponsesT,
         RequestServiceTemplate>
class CUnobservableCommandManager<RC> :
    public ICommandManager<CUnobservableCommandWrapper<ParametersT, ResponsesT>>
{
    using UnobservableCommandWrapper =
        CUnobservableCommandWrapper<ParametersT, ResponsesT>;

public:
    /**
     * @brief C'tor
     *
     * @tparam SiLAFeatureT User defined SiLA Feature Implementation
     *
     * @param Feature The SiLA Feature that contains this Unobservable Command
     * @param Name The name of the Command. This is for debugging/logging
     * purposes only.
     */
    template<typename SiLAFeatureT>
    explicit CUnobservableCommandManager(SiLAFeatureT* Feature,
                                         std::string_view Name = "");

private:
    /**
     * @brief Start accepting requests for this Unobservable Command
     */
    void serve();

    CAsyncRPCHandler<UnobservableCommandWrapper, RequestServiceT,
                     CCommandRPC<RequestServiceT, ParametersT, ResponsesT, RC>>
        m_CommandHandler;      ///< RPC handler for the Command RPC
    const std::string m_Name;  ///< Name of the Command (for debugging)
};

//============================================================================
template<typename RequestServiceT, typename ParametersT, typename ResponsesT,
         RequestServiceTemplate>
template<typename SiLAFeatureT>
CUnobservableCommandManager<RC>::CUnobservableCommandManager(
    SiLAFeatureT* Feature, std::string_view Name)
    : m_CommandHandler{new UnobservableCommandWrapper{this, Name}, Feature,
                       Feature->server()->addCompletionQueue()}
{
    QObject::connect(Feature->server(), &CSiLAServer::started,
                     [this]() { serve(); });
}

//============================================================================
template<typename RequestServiceT, typename ParametersT, typename ResponsesT,
         RequestServiceTemplate>
void CUnobservableCommandManager<RC>::serve()
{
    if (!this->executor())
    {
        qCCritical(sila_cpp)
            << "No executor function set for Unobservable Command" << m_Name
            << "\nThis Command will not be available for clients to request!";
        return;
    }

    m_CommandHandler.start();
}
}  // namespace SiLA2

#endif  // UNOBSERVABLECOMMANDMANAGER_H

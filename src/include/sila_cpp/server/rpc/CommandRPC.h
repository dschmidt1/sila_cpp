/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   CommandRPC.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   13.07.2020
/// \brief  Declaration of the CCommandRPC class
//============================================================================
#ifndef COMMANDRPC_H
#define COMMANDRPC_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/server/command/UnobservableCommandWrapper.h>

#include "AsyncRPC.h"
#include "definitions.h"

#include <grpcpp/grpcpp.h>
#include <grpcpp/impl/codegen/async_stream.h>
#include <grpcpp/impl/codegen/async_unary_call.h>

#include <utility>

namespace SiLA2
{
/**
 * @brief The CCommandRPC class encapsulates the logic of the Command RPC for
 * Unobservable Commands
 *
 * @tparam RequestServiceT The gRPC Service that contains the `Request<Command>`
 * member function
 * @tparam ParametersT The type of the Command Parameters
 * @tparam ResponsesT The type of the Command Responses
 * @tparam RC Signature of the `Request<Command>` member function pointer
 */
template<typename RequestServiceT, typename ParametersT, typename ResponsesT,
         RequestServiceTemplate>
class CCommandRPC :
    public CAsyncRPC<RequestServiceT, grpc::ServerAsyncResponseWriter, ResponsesT>
{
    using AsyncRPCBase =
        CAsyncRPC<RequestServiceT, grpc::ServerAsyncResponseWriter, ResponsesT>;
    using UnobservableCommandWrapper =
        CUnobservableCommandWrapper<ParametersT, ResponsesT>;

public:
    /**
     * @brief C'tor
     *
     * @param Service The gRPC Async Service class that contains the
     * `Request<CommandName>` member function
     * @param CompletionQueue The gRPC server completion queue that should be
     * used for this RPC
     * @param Command The Command Wrapper that this RPC belongs to
     */
    CCommandRPC(RequestServiceT* Service,
                std::shared_ptr<grpc::ServerCompletionQueue> CompletionQueue,
                UnobservableCommandWrapper* Command);

    /**
     * @override
     * @brief Get the name of this RPC (e.g. InitRPC, InfoRPC, or ResultRPC)
     * This is only for nicer debugging.
     *
     * @return The RPC name as a string
     */
    [[nodiscard]] const char* name() const override;

    /**
     * @override
     * @brief What should happen in the @a CREATE state of the RPC.
     */
    void onCreate() override;

    /**
     * @override
     * @brief What should happen in the @a PROCESS state of the RPC.
     *
     * @return A pair of the Status of the RPC execution and its result
     */
    std::pair<const grpc::Status, const ResponsesT> onProcess() override;

    /**
     * @override
     * @brief Create a new CCommandRPC instance
     */
    void create() const override;

private:
    UnobservableCommandWrapper*
        m_Command;  ///< The Command that this RPC belongs to
    ParametersT m_Request{};
};

//=============================================================================
template<typename RequestServiceT, typename ParametersT, typename ResponsesT,
         RequestServiceTemplate>
CCommandRPC<RequestServiceT, ParametersT, ResponsesT, RC>::CCommandRPC(
    RequestServiceT* Service,
    std::shared_ptr<grpc::ServerCompletionQueue> CompletionQueue,
    UnobservableCommandWrapper* Command)
    : AsyncRPCBase{Service, CompletionQueue}, m_Command{Command}
{
    // Invoke the serving logic right away
    this->serve();
}

//============================================================================
template<typename RequestServiceT, typename ParametersT, typename ResponsesT,
         RequestServiceTemplate>
const char* CCommandRPC<RequestServiceT, ParametersT, ResponsesT, RC>::name() const
{
    return "Command RPC";
}

//============================================================================
template<typename RequestServiceT, typename ParametersT, typename ResponsesT,
         RequestServiceTemplate>
void CCommandRPC<RequestServiceT, ParametersT, ResponsesT, RC>::onCreate()
{
    (this->m_Service->*RC)(&this->m_Context, &m_Request, &this->m_Responder,
                           this->m_CompletionQueue.get(),
                           this->m_CompletionQueue.get(),
                           new CAsyncRPCTag{this, TagType::REQUEST});
}

//============================================================================
template<typename RequestServiceT, typename ParametersT, typename ResponsesT,
         RequestServiceTemplate>
std::pair<const grpc::Status, const ResponsesT>
CCommandRPC<RequestServiceT, ParametersT, ResponsesT, RC>::onProcess()
{
    qCInfo(sila_cpp)
        << "--- Unobservable Command" << m_Command->name() << "called";
    m_Command->setParameters(m_Request);
    m_Command->execute();

    const auto Response = m_Command->result();
    qCDebug(sila_cpp) << "Command result" << Response;

    return {grpc::Status{m_Command->status()}, Response};
}

//============================================================================
template<typename RequestServiceT, typename ParametersT, typename ResponsesT,
         RequestServiceTemplate>
void CCommandRPC<RequestServiceT, ParametersT, ResponsesT, RC>::create() const
{
    new CCommandRPC{this->m_Service, this->m_CompletionQueue, m_Command};
}
}  // namespace SiLA2
#endif  // COMMANDRPC_H

/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   GetRPC.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   15.07.2020
/// \brief  Declaration of the CGetRPC class
//============================================================================
#ifndef GETRPC_H
#define GETRPC_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/server/property/IPropertyWrapper.h>
#include <sila_cpp/server/property/PropertyObserver.h>

#include "AsyncRPC.h"
#include "definitions.h"

namespace SiLA2
{
/**
 * @brief The CGetRPC class encapsulates the logic of the Get Property RPC
 *
 * @tparam GetServiceT The gRPC Service that contains the `RequestGet_<Property>`
 * member function
 * @tparam ParametersT The type of the Property Parameters
 * @tparam ResponsesT The type of the Property Responses
 * @tparam T The underlying type of the Property (only necessary for the correct
 * signature of the Property Wrapper)
 * @tparam RG Signature of the `RequestGet_<Property>` member function pointer
 */
template<typename GetServiceT, typename ParametersT, typename ResponsesT,
         typename T, GetServiceTemplate>
class CGetRPC :
    public CAsyncRPC<GetServiceT, grpc::ServerAsyncResponseWriter, ResponsesT>,
    public CPropertyObserver<ResponsesT>
{
    using AsyncRPCBase =
        CAsyncRPC<GetServiceT, grpc::ServerAsyncResponseWriter, ResponsesT>;
    using PropertyWrapper = IPropertyWrapper<T, ResponsesT>;

public:
    /**
     * @brief C'tor
     *
     * @param Service The gRPC Async Service class that contains
     * the`RequestGet_<PropertyName>` member function pointer
     * @param CompletionQueue The gRPC server completion queue that should be
     * used for this RPC
     * @param Property The Property Wrapper that contains this RPC
     */
    CGetRPC(GetServiceT* Service,
            std::shared_ptr<grpc::ServerCompletionQueue> CompletionQueue,
            PropertyWrapper* Property);

    /**
     * @brief D'tor
     *
     * Stop observing Property value changes
     */
    ~CGetRPC() override;

    /**
     * @override
     * @brief Get the name of this RPC (e.g. GetRPC)
     * This is only for nicer debugging.
     *
     * @return The RPC name as a string
     */
    [[nodiscard]] const char* name() const override;

    /**
     * @override
     * @brief What should happen in the @a CREATE state of the RPC.
     */
    void onCreate() override;

    /**
     * @override
     * @brief What should happen in the @a PROCESS state of the RPC.
     *
     * @return A pair of the Status of the RPC execution and its result
     */
    std::pair<const grpc::Status, const ResponsesT> onProcess() override;

    /**
     * @override
     * @brief Create a new CGetRPC instance
     */
    void create() const override;

    /**
     * @brief Set the value that should be sent to the client next
     *
     * @param NewValue The new value to set
     */
    void setValue(const ResponsesT& NewValue) override;

private:
    ParametersT m_Request{};  ///< RPC parameters (always empty in this case)
    ResponsesT m_Response{};  ///< Contains the property value to send to the client
    PropertyWrapper* m_Property;  ///< The Property that this RPC belongs to
};

//=============================================================================
template<typename GetServiceT, typename ParametersT, typename ResponsesT,
         typename T, GetServiceTemplate>
CGetRPC<GetServiceT, ParametersT, ResponsesT, T, RG>::CGetRPC(
    GetServiceT* Service,
    std::shared_ptr<grpc::ServerCompletionQueue> CompletionQueue,
    PropertyWrapper* Property)
    : AsyncRPCBase{Service, CompletionQueue}, m_Property{Property}
{
    m_Property->addObserver(this);
    // Invoke the serving logic right away
    this->serve();
}

//=============================================================================
template<typename GetServiceT, typename ParametersT, typename ResponsesT,
         typename T, GetServiceTemplate>
CGetRPC<GetServiceT, ParametersT, ResponsesT, T, RG>::~CGetRPC()
{
    m_Property->removeObserver(this);
}

//============================================================================
template<typename GetServiceT, typename ParametersT, typename ResponsesT,
         typename T, GetServiceTemplate>
const char* CGetRPC<GetServiceT, ParametersT, ResponsesT, T, RG>::name() const
{
    return "Get RPC";
}

//============================================================================
template<typename GetServiceT, typename ParametersT, typename ResponsesT,
         typename T, GetServiceTemplate>
void CGetRPC<GetServiceT, ParametersT, ResponsesT, T, RG>::CGetRPC::onCreate()
{
    (this->m_Service->*RG)(&this->m_Context, &m_Request, &this->m_Responder,
                           this->m_CompletionQueue.get(),
                           this->m_CompletionQueue.get(),
                           new CAsyncRPCTag{this, TagType::REQUEST});
}

//============================================================================
template<typename GetServiceT, typename ParametersT, typename ResponsesT,
         typename T, GetServiceTemplate>
std::pair<const grpc::Status, const ResponsesT>
CGetRPC<GetServiceT, ParametersT, ResponsesT, T, RG>::onProcess()
{
    qCInfo(sila_cpp)
        << "--- Unobservable Property" << m_Property->name() << "requested";

    return {grpc::Status::OK, m_Response};
}

//============================================================================
template<typename GetServiceT, typename ParametersT, typename ResponsesT,
         typename T, GetServiceTemplate>
void CGetRPC<GetServiceT, ParametersT, ResponsesT, T, RG>::create() const
{
    new CGetRPC{this->m_Service, this->m_CompletionQueue, m_Property};
}

//============================================================================
template<typename GetServiceT, typename ParametersT, typename ResponsesT,
         typename T, GetServiceTemplate>
void CGetRPC<GetServiceT, ParametersT, ResponsesT, T, RG>::setValue(
    const ResponsesT& NewValue)
{
    m_Response = NewValue;
}
}  // namespace SiLA2

#endif  // GETRPC_H

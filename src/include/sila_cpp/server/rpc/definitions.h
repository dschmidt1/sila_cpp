/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   definitions.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   24.06.2020
/// \brief  Common definitions for the RPC class templates
//============================================================================
#ifndef DEFINITIONS_H
#define DEFINITIONS_H

/**
 * @brief Signature of the `Request<CommandName>` member function pointer for
 * Unobservable Commands
 */
#define RequestServiceTemplate                                                   \
    void (RequestServiceT::*RC)(grpc::ServerContext*, ParametersT*,              \
                                grpc::ServerAsyncResponseWriter<ResponsesT>*,    \
                                grpc::CompletionQueue*,                          \
                                grpc::ServerCompletionQueue*, void*)

/**
 * @brief Signature of the `Request<CommandName>` member function pointer for
 * Observable Commands
 */
#define InitServiceTemplate                                                      \
    void (InitServiceT::*RC)(                                                    \
        grpc::ServerContext*, ParametersT*,                                      \
        grpc::ServerAsyncResponseWriter<                                         \
            sila2::org::silastandard::CommandConfirmation>*,                     \
        grpc::CompletionQueue*, grpc::ServerCompletionQueue*, void*)

/**
 * @brief Signature of the `Request<CommandName>_Info` member function pointer for
 * Observable Commands
 */
#define InfoServiceTemplate                                                      \
    void (InfoServiceT::*RCI)(                                                   \
        grpc::ServerContext*, sila2::org::silastandard::CommandExecutionUUID*,   \
        grpc::ServerAsyncWriter<sila2::org::silastandard::ExecutionInfo>*,       \
        grpc::CompletionQueue*, grpc::ServerCompletionQueue*, void*)

/**
 * @brief Signature of the `Request<CommandName>_Result` member function pointer
 * for Observable Commands
 */
#define ResultServiceTemplate                                                    \
    void (ResultServiceT::*RCR)(                                                 \
        grpc::ServerContext*, sila2::org::silastandard::CommandExecutionUUID*,   \
        grpc::ServerAsyncResponseWriter<ResponsesT>*, grpc::CompletionQueue*,    \
        grpc::ServerCompletionQueue*, void*)

/**
 * @brief Signature of the `Request<CommandName>_Intermediate` member
 * function pointer for Observable Commands
 */
#define IntermediateResultServiceTemplate                                        \
    void (IntermediateResultServiceT::*RIR)(                                     \
        grpc::ServerContext*, sila2::org::silastandard::CommandExecutionUUID*,   \
        grpc::ServerAsyncWriter<IntermediateResponsesT>*,                        \
        grpc::CompletionQueue*, grpc::ServerCompletionQueue*, void*)

/**
 * @brief Signature of the `RequestGet_<PropertyName>` member function pointer for
 * Unobservable Properties
 */
#define GetServiceTemplate                                                       \
    void (GetServiceT::*RG)(grpc::ServerContext*, ParametersT*,                  \
                            grpc::ServerAsyncResponseWriter<ResponsesT>*,        \
                            grpc::CompletionQueue*,                              \
                            grpc::ServerCompletionQueue*, void*)

/**
 * @brief Signature of the `RequestSubscribe_<PropertyName>` member function
 * pointer for Observable Properties
 */
#define SubscribeServiceTemplate                                                 \
    void (SubscribeServiceT::*RS)(grpc::ServerContext*, ParametersT*,            \
                                  grpc::ServerAsyncWriter<ResponsesT>*,          \
                                  grpc::CompletionQueue*,                        \
                                  grpc::ServerCompletionQueue*, void*)

#endif  // DEFINITIONS_H

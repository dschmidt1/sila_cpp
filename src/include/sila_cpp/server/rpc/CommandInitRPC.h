/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   CommandInitRPC.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   24.06.2020
/// \brief  Definition of the CCommandInitRPC class
//============================================================================
#ifndef COMMANDINITRPC_H
#define COMMANDINITRPC_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/framework/data_types/SiLADuration.h>
#include <sila_cpp/server/command/ObservableCommandManager.h>

#include "AsyncRPC.h"
#include "definitions.h"

#include <grpcpp/grpcpp.h>
#include <grpcpp/impl/codegen/async_stream.h>
#include <grpcpp/impl/codegen/async_unary_call.h>

#include <utility>

namespace SiLA2
{
/**
 * @brief The CCommandInitRPC class encapsulates the logic of the Command
 * Initiation RPC
 *
 * @tparam InitServiceT The gRPC Service that contains the `Request<Command>`
 * member function
 * @tparam ParametersT The type of the Command Parameters
 * @tparam ResponsesT The type of the Command Responses
 * @tparam RC Signature of the `Request<Command>` member function pointer
 * @tparam IntermediateResponsesT The type of the Intermediate Responses (not
 * actually a pack, but rather an optional type parameter, only necessary for the
 * correct signature of the Command Manager class template)
 */
template<typename InitServiceT, typename ParametersT, typename ResponsesT,
         InitServiceTemplate, typename... IntermediateResponsesT>
class CCommandInitRPC :
    public CAsyncRPC<InitServiceT, grpc::ServerAsyncResponseWriter,
                     sila2::org::silastandard::CommandConfirmation>
{
    using AsyncRPCBase = CAsyncRPC<InitServiceT, grpc::ServerAsyncResponseWriter,
                                   sila2::org::silastandard::CommandConfirmation>;
    using ObservableCommandManager =
        IObservableCommandManager<ParametersT, ResponsesT,
                                  IntermediateResponsesT...>;

public:
    /**
     * @brief C'tor
     *
     * @param Service The gRPC Async Service class that contains the
     * `Request<CommandName>` member function
     * @param CompletionQueue The gRPC server completion queue that should be
     * used for this RPC
     * @param CommandManager The Command Manager that this RPC belongs to
     */
    CCommandInitRPC(InitServiceT* Service,
                    std::shared_ptr<grpc::ServerCompletionQueue> CompletionQueue,
                    ObservableCommandManager* CommandManager);

    /**
     * @override
     * @brief Get the name of this RPC (e.g. InitRPC, InfoRPC, or ResultRPC)
     * This is only for nicer debugging.
     *
     * @return The RPC name as a string
     */
    [[nodiscard]] const char* name() const override;

    /**
     * @override
     * @brief What should happen in the @a CREATE state of the RPC.
     */
    void onCreate() override;

    /**
     * @override
     * @brief What should happen in the @a PROCESS state of the RPC.
     *
     * @return A pair of the Status of the RPC execution and its result
     */
    std::pair<const grpc::Status,
              const sila2::org::silastandard::CommandConfirmation>
    onProcess() override;

    /**
     * @override
     * @brief Create a new CCommandInitRPC instance
     */
    void create() const override;

private:
    ObservableCommandManager*
        m_CommandManager;  ///< The Command Manager that this RPC belongs to
    ParametersT m_Request{};
    sila2::org::silastandard::CommandConfirmation m_Response{};
};

//=============================================================================
template<typename InitServiceT, typename ParametersT, typename ResponsesT,
         InitServiceTemplate, typename... IntermediateResponsesT>
CCommandInitRPC<InitServiceT, ParametersT, ResponsesT, RC,
                IntermediateResponsesT...>::
    CCommandInitRPC(InitServiceT* Service,
                    std::shared_ptr<grpc::ServerCompletionQueue> CompletionQueue,
                    ObservableCommandManager* CommandManager)
    : AsyncRPCBase{Service, CompletionQueue}, m_CommandManager{CommandManager}
{
    // Invoke the serving logic right away
    this->serve();
}

//============================================================================
template<typename InitServiceT, typename ParametersT, typename ResponsesT,
         InitServiceTemplate, typename... IntermediateResponsesT>
const char* CCommandInitRPC<InitServiceT, ParametersT, ResponsesT, RC,
                            IntermediateResponsesT...>::name() const
{
    return "Init RPC";
}

//============================================================================
template<typename InitServiceT, typename ParametersT, typename ResponsesT,
         InitServiceTemplate, typename... IntermediateResponsesT>
void CCommandInitRPC<InitServiceT, ParametersT, ResponsesT, RC,
                     IntermediateResponsesT...>::onCreate()
{
    (this->m_Service->*RC)(&this->m_Context, &m_Request, &this->m_Responder,
                           this->m_CompletionQueue.get(),
                           this->m_CompletionQueue.get(),
                           new CAsyncRPCTag{this, TagType::REQUEST});
}

//============================================================================
template<typename InitServiceT, typename ParametersT, typename ResponsesT,
         InitServiceTemplate, typename... IntermediateResponsesT>
std::pair<const grpc::Status, const sila2::org::silastandard::CommandConfirmation>
CCommandInitRPC<InitServiceT, ParametersT, ResponsesT, RC,
                IntermediateResponsesT...>::onProcess()
{
    qCInfo(sila_cpp)
        << "--- Observable Command" << m_CommandManager->name() << "called";
    const auto [UUID, Lifetime, Command] = m_CommandManager->addCommand();
    Command->setParameters(m_Request);

    qCInfo(sila_cpp) << "Starting new Command Execution with UUID" << UUID;
    Command->execute();

    QEventLoop EventLoop;
    QObject::connect(Command.get(),
                     &IObservableCommandWrapper::executionInfoReady, &EventLoop,
                     &QEventLoop::quit);
    QObject::connect(Command.get(), &IObservableCommandWrapper::executionFinished,
                     &EventLoop, &QEventLoop::quit);
    EventLoop.exec();

    m_Response.set_allocated_commandexecutionuuid(UUID.toProtoMessagePtr());
    if (!Lifetime.isNull())
    {
        m_Response.set_allocated_lifetimeofexecution(
            Lifetime.toProtoMessagePtr());
    }
    return {grpc::Status{Command->status()}, m_Response};
}

//============================================================================
template<typename InitServiceT, typename ParametersT, typename ResponsesT,
         InitServiceTemplate, typename... IntermediateResponsesT>
void CCommandInitRPC<InitServiceT, ParametersT, ResponsesT, RC,
                     IntermediateResponsesT...>::create() const
{
    new CCommandInitRPC{this->m_Service, this->m_CompletionQueue,
                        m_CommandManager};
}

}  // namespace SiLA2
#endif  // COMMANDINITRPC_H

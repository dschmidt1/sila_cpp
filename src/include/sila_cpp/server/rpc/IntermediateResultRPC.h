/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   IntermediateResultRPC.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   24.06.2020
/// \brief  Definition of the CIntermediateResultRPC class
//============================================================================
#ifndef INTERMEDIATERESULTRPC_H
#define INTERMEDIATERESULTRPC_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/framework/data_types/ExecutionInfo.h>
#include <sila_cpp/framework/error_handling/FrameworkError.h>
#include <sila_cpp/server/command/IObservableCommandManager.h>
#include <sila_cpp/server/command/ObservableCommandWrapper.h>

#include "AsyncRPC.h"
#include "definitions.h"

#include <QObject>
#include <QSemaphore>

#include <grpcpp/grpcpp.h>
#include <grpcpp/impl/codegen/async_stream.h>
#include <grpcpp/impl/codegen/async_unary_call.h>

#include <utility>

namespace SiLA2
{
/**
 * @brief The CIntermediateResultRPC class encapsulates the logic of the
 * Intermediate Result RPC
 *
 * @tparam IntermediateResultServiceT The gRPC Service that contains the
 * `Request<Command>_Intermediate` member function
 * @tparam ParametersT The type of the Command Parameters
 * @tparam ResponsesT The type of the Command Responses
 * @tparam IntermediateResponsesT The type of the Intermediate Responses
 * @tparam RIR Signature of the `Request<Command>_Intermediate` member function
 * pointer
 */
template<typename IntermediateResultServiceT, typename ParametersT,
         typename ResponsesT, typename IntermediateResponsesT,
         IntermediateResultServiceTemplate>
class CIntermediateResultRPC :
    public CAsyncRPC<IntermediateResultServiceT, grpc::ServerAsyncWriter,
                     IntermediateResponsesT>
{
    using AsyncRPCBase =
        CAsyncRPC<IntermediateResultServiceT, grpc::ServerAsyncWriter,
                  IntermediateResponsesT>;
    using ObservableCommandManager =
        IObservableCommandManager<ParametersT, ResponsesT, IntermediateResponsesT>;
    using ObservableCommandWrapper =
        typename ObservableCommandManager::ObservableCommandWrapper;

public:
    /**
     * @brief C'tor
     *
     * @param Service The gRPC Async Service class that contains the
     * `Request<CommandName>_Intermediate` member function pointer
     * @param CompletionQueue The gRPC server completion queue that should be
     * used for this RPC
     * @param CommandManager The Command Manager that this RPC belongs to
     */
    CIntermediateResultRPC(
        IntermediateResultServiceT* Service,
        std::shared_ptr<grpc::ServerCompletionQueue> CompletionQueue,
        ObservableCommandManager* CommandManager);

    /**
     * @brief D'tor
     *
     * Disconnects from all signals
     */
    ~CIntermediateResultRPC() override;

    /**
     * @override
     * @brief Get the name of this RPC (e.g. Intermediate RPC)
     * This is only for nicer debugging.
     *
     * @return The RPC name as a string
     */
    [[nodiscard]] const char* name() const override;

    /**
     * @override
     * @brief What should happen in the @a CREATE state of the RPC.
     */
    void onCreate() override;

    /**
     * @override
     * @brief What should happen in the @a PROCESS state of the RPC.
     *
     * @return A pair of the Status of the RPC execution and its result
     */
    std::pair<const grpc::Status, const IntermediateResponsesT> onProcess()
        override;

    /**
     * @override
     * @brief Create a new CIntermediateResultRPC instance
     */
    void create() const override;

    /**
     * @override
     * @brief What should happen when the RPC has been cancelled by the client.
     *
     * @details Releases the semaphore to unblock this thread and sets the Status
     * for this RPC to @a CLEANUP. This prevents the RPC from trying to write
     * another message to the client, which wouldn't make sense anyway. Instead,
     * this RPC will simply return from `CAsyncRPC::serve()` without doing
     * anything. The RPC that called `onCancelled()` from another thread will then
     * be properly deallocated. Note, that the two RPCs discussed here refer to
     * the same instance that just executes different functions in different
     * threads.
     */
    void onCancelled() override;

private:
    /**
     * @brief Set the Intermediate Result to @a Result. This Result will
     * subsequently be sent to the client.
     *
     * @param Result The new Intermediate Result to send to the client
     */
    void setIntermediateResult(const IntermediateResponsesT& Result);

    /**
     * @brief Set @a m_Command to the Command Wrapper that is identified by @a
     * UUID if this has not already been done.
     * As a side effect this function also connects the Command Wrapper's signals
     * to corresponding slots in this class.
     *
     * @param UUID The Command Execution UUID that identifies the Command
     * Execution for which Info should be streamed.
     *
     * @returns true, if the Command could be set successfully
     * @returns false, otherwise. This indicates that a SilA Framework Error has
     * to be raised to inform the client over the wrong UUID.
     */
    [[nodiscard]] bool setCommandByUUID(const CCommandExecutionUUID& UUID);

    /**
     * @brief Disconnect from all signals
     */
    void disconnectSignals() const;

    ObservableCommandManager*
        m_CommandManager;  ///< The Command Manager that this RPC belongs to
    std::shared_ptr<ObservableCommandWrapper>
        m_Command;  ///< The currently running Command
    sila2::org::silastandard::CommandExecutionUUID m_Request{};
    IntermediateResponsesT m_Response{};
    QSemaphore m_ResultSemaphore{1};  ///< Used to wait for Results to send
                                      ///< Is initially 1, to send the first value
                                      ///< right away after subscription started.
    QMetaObject::Connection
        m_ResultConn;  ///< Connection to the ExecutorThread's intermediateResultReady signal
    QMetaObject::Connection
        m_FinishedConn;  ///< Connection to the ExecutorThread's finished signal
};

//=============================================================================
template<typename IntermediateResultServiceT, typename ParametersT,
         typename ResponsesT, typename IntermediateResponsesT,
         IntermediateResultServiceTemplate>
CIntermediateResultRPC<
    IntermediateResultServiceT, ParametersT, ResponsesT, IntermediateResponsesT,
    RIR>::CIntermediateResultRPC(IntermediateResultServiceT* Service,
                                 std::shared_ptr<grpc::ServerCompletionQueue>
                                     CompletionQueue,
                                 ObservableCommandManager* CommandManager)
    : CAsyncRPC<IntermediateResultServiceT, grpc::ServerAsyncWriter,
                IntermediateResponsesT>{Service, CompletionQueue},
      m_CommandManager{CommandManager}
{
    // Invoke the serving logic right away
    this->serve();
}

//=============================================================================
template<typename IntermediateResultServiceT, typename ParametersT,
         typename ResponsesT, typename IntermediateResponsesT,
         IntermediateResultServiceTemplate>
CIntermediateResultRPC<IntermediateResultServiceT, ParametersT, ResponsesT,
                       IntermediateResponsesT, RIR>::~CIntermediateResultRPC()
{
    disconnectSignals();
}

//============================================================================
template<typename IntermediateResultServiceT, typename ParametersT,
         typename ResponsesT, typename IntermediateResponsesT,
         IntermediateResultServiceTemplate>
const char*
CIntermediateResultRPC<IntermediateResultServiceT, ParametersT, ResponsesT,
                       IntermediateResponsesT, RIR>::name() const
{
    return "Intermediate RPC";
}

//============================================================================
template<typename IntermediateResultServiceT, typename ParametersT,
         typename ResponsesT, typename IntermediateResponsesT,
         IntermediateResultServiceTemplate>
void CIntermediateResultRPC<IntermediateResultServiceT, ParametersT, ResponsesT,
                            IntermediateResponsesT,
                            RIR>::CIntermediateResultRPC::onCreate()
{
    (this->m_Service->*RIR)(&this->m_Context, &m_Request, &this->m_Responder,
                            this->m_CompletionQueue.get(),
                            this->m_CompletionQueue.get(),
                            new CAsyncRPCTag{this, TagType::REQUEST});
}

//============================================================================
template<typename IntermediateResultServiceT, typename ParametersT,
         typename ResponsesT, typename IntermediateResponsesT,
         IntermediateResultServiceTemplate>
std::pair<const grpc::Status, const IntermediateResponsesT>
CIntermediateResultRPC<IntermediateResultServiceT, ParametersT, ResponsesT,
                       IntermediateResponsesT, RIR>::onProcess()
{
    qCInfo(sila_cpp) << "--- Intermediate Result for Observable Command"
                     << m_CommandManager->name() << "requested";
    qCDebug(sila_cpp) << "UUID of requested Command:" << m_Request.value();

    if (!setCommandByUUID(m_Request))
    {
        this->finish();
        const auto Error = CFrameworkError{
            CFrameworkError::INVALID_COMMAND_EXECUTION_UUID,
            "There is no Command for this Command Execution UUID."};
        return {Error.raise(), m_Response};
    }

    // We only have one value to send, so we always want to decrement the
    // semaphore to zero (i.e. we acquire all available resources or at least one
    // resource if none are available). That way, a client that starts
    // subscription later, does not get the same value multiple times.
    m_ResultSemaphore.acquire(std::max(1, m_ResultSemaphore.available()));

    return {grpc::Status::OK, m_Response};
}

//============================================================================
template<typename IntermediateResultServiceT, typename ParametersT,
         typename ResponsesT, typename IntermediateResponsesT,
         IntermediateResultServiceTemplate>
void CIntermediateResultRPC<IntermediateResultServiceT, ParametersT, ResponsesT,
                            IntermediateResponsesT, RIR>::create() const
{
    new CIntermediateResultRPC{this->m_Service, this->m_CompletionQueue,
                               m_CommandManager};
}

//============================================================================
template<typename IntermediateResultServiceT, typename ParametersT,
         typename ResponsesT, typename IntermediateResponsesT,
         IntermediateResultServiceTemplate>
void CIntermediateResultRPC<IntermediateResultServiceT, ParametersT, ResponsesT,
                            IntermediateResponsesT, RIR>::onCancelled()
{
    this->setStatus(AsyncRPCBase::CallStatus::CLEANUP);
    m_ResultSemaphore.release();
}

//============================================================================
template<typename IntermediateResultServiceT, typename ParametersT,
         typename ResponsesT, typename IntermediateResponsesT,
         IntermediateResultServiceTemplate>
void CIntermediateResultRPC<
    IntermediateResultServiceT, ParametersT, ResponsesT, IntermediateResponsesT,
    RIR>::setIntermediateResult(const IntermediateResponsesT& Result)
{
    m_Response = Result;
    m_ResultSemaphore.release();
}

//============================================================================
template<typename IntermediateResultServiceT, typename ParametersT,
         typename ResponsesT, typename IntermediateResponsesT,
         IntermediateResultServiceTemplate>
bool CIntermediateResultRPC<
    IntermediateResultServiceT, ParametersT, ResponsesT, IntermediateResponsesT,
    RIR>::setCommandByUUID(const CCommandExecutionUUID& UUID)
{
    if (m_Command)
    {
        // we already have the current Command Wrapper instance
        // no need to connect the signals again
        return true;
    }

    m_Command = m_CommandManager->getCommandByUUID(UUID);
    if (!m_Command)
    {
        // there is no such Command; caller is required to raise an error for this
        return false;
    }

    m_ResultConn = QObject::connect(
        m_Command.get(), &ObservableCommandWrapper::intermediateResultReady,
        [this]() { setIntermediateResult(m_Command->intermediateResult()); });
    m_FinishedConn = QObject::connect(
        m_Command.get(), &ObservableCommandWrapper::executionFinished,
        [this](const auto CommandStatus) {
            m_ResultSemaphore.release();
            this->finish();
            m_Command.reset();
            disconnectSignals();
        });
    return true;
}

//============================================================================
template<typename IntermediateResultServiceT, typename ParametersT,
         typename ResponsesT, typename IntermediateResponsesT,
         IntermediateResultServiceTemplate>
void CIntermediateResultRPC<IntermediateResultServiceT, ParametersT, ResponsesT,
                            IntermediateResponsesT, RIR>::disconnectSignals() const
{
    QObject::disconnect(m_ResultConn);
    QObject::disconnect(m_FinishedConn);
}

}  // namespace SiLA2
#endif  // INTERMEDIATERESULTRPC_H

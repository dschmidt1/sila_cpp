/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SubscribeRPC.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   02.07.2020
/// \brief  Declaration of the CSubscribeRPC class
//============================================================================
#ifndef SUBSCRIBERPC_H
#define SUBSCRIBERPC_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/framework/data_types/ExecutionInfo.h>
#include <sila_cpp/framework/error_handling/FrameworkError.h>
#include <sila_cpp/server/property/IPropertyWrapper.h>
#include <sila_cpp/server/property/PropertyObserver.h>

#include "AsyncRPC.h"
#include "definitions.h"

#include <QSemaphore>

#include <grpcpp/grpcpp.h>
#include <grpcpp/impl/codegen/async_stream.h>
#include <grpcpp/impl/codegen/async_unary_call.h>

#include <utility>

namespace SiLA2
{
/**
 * @brief The CSubscribeRPC class encapsulates the logic of the Subscribe Property
 * RPC
 *
 * @tparam SubscribeServiceT The gRPC Service that contains the
 * `RequestSubscribe_<Property>` member function
 * @tparam ParametersT The type of the Property Parameters
 * @tparam ResponsesT The type of the Property Responses
 * @tparam T The underlying type of the Property (only necessary for the correct
 * signature of the Property Wrapper)
 * @tparam RS Signature of the `RequestSubscribe_<Property>` member function
 * pointer
 */
template<typename SubscribeServiceT, typename ParametersT, typename ResponsesT,
         typename T, SubscribeServiceTemplate>
class CSubscribeRPC :
    public CAsyncRPC<SubscribeServiceT, grpc::ServerAsyncWriter, ResponsesT>,
    public CPropertyObserver<ResponsesT>
{
    using AsyncRPCBase =
        CAsyncRPC<SubscribeServiceT, grpc::ServerAsyncWriter, ResponsesT>;
    using PropertyWrapper = IPropertyWrapper<T, ResponsesT>;

public:
    /**
     * @brief C'tor
     *
     * @param Service The gRPC Async Service class that contains the
     * `RequestSubscribe_<PropertyName>` member function pointer
     * @param CompletionQueue The gRPC server completion queue that should be
     * used for this RPC
     * @param Property The Property Wrapper that contains this RPC
     */
    CSubscribeRPC(SubscribeServiceT* Service,
                  std::shared_ptr<grpc::ServerCompletionQueue> CompletionQueue,
                  PropertyWrapper* Property);

    /**
     * @brief D'tor
     *
     * Stop observing Property value changes
     */
    ~CSubscribeRPC() override;

    /**
     * @override
     * @brief Get the name of this RPC (e.g. SubscribeRPC)
     * This is only for nicer debugging.
     *
     * @return The RPC name as a string
     */
    [[nodiscard]] const char* name() const override;

    /**
     * @override
     * @brief What should happen in the @a CREATE state of the RPC.
     */
    void onCreate() override;

    /**
     * @override
     * @brief What should happen in the @a PROCESS state of the RPC.
     *
     * @return A pair of the Status of the RPC execution and its result
     */
    std::pair<const grpc::Status, const ResponsesT> onProcess() override;

    /**
     * @override
     * @brief Create a new CSubscribeRPC instance
     */
    void create() const override;

    /**
     * @override
     * @brief What should happen when the RPC has been cancelled by the client.
     *
     * @details Releases the semaphore to unblock this thread and sets the Status
     * for this RPC to @a CLEANUP. This prevents the RPC from trying to write
     * another message to the client, which wouldn't make sense anyway. Instead,
     * this RPC will simply return from `CAsyncRPC::serve()` without doing
     * anything. The RPC that called `onCancelled()` from another thread will then
     * be properly deallocated. Note, that the two RPCs discussed here refer to
     * the same instance that just executes different functions in different
     * threads.
     */
    void onCancelled() override;

    /**
     * @brief Set the value that should be sent to the client next
     *
     * @param NewValue The new value to set
     */
    void setValue(const ResponsesT& NewValue) override;

private:
    ParametersT m_Request{};  ///< RPC parameters (always empty in this case)
    ResponsesT m_Response{};  ///< Contains the property value to send to the client
    PropertyWrapper* m_Property;     ///< The Property that this RPC belongs to
    QSemaphore m_ValueSemaphore{0};  ///< The number of Property values to send
};

//=============================================================================
template<typename SubscribeServiceT, typename ParametersT, typename ResponsesT,
         typename T, SubscribeServiceTemplate>
CSubscribeRPC<SubscribeServiceT, ParametersT, ResponsesT, T, RS>::CSubscribeRPC(
    SubscribeServiceT* Service,
    std::shared_ptr<grpc::ServerCompletionQueue> CompletionQueue,
    PropertyWrapper* Property)
    : AsyncRPCBase{Service, CompletionQueue}, m_Property{Property}
{
    m_Property->addObserver(this);
    // Invoke the serving logic right away
    this->serve();
}

//=============================================================================
template<typename SubscribeServiceT, typename ParametersT, typename ResponsesT,
         typename T, SubscribeServiceTemplate>
CSubscribeRPC<SubscribeServiceT, ParametersT, ResponsesT, T, RS>::~CSubscribeRPC()
{
    qCInfo(sila_cpp) << "--- Subscription for Observable Property"
                     << m_Property->name() << "ended";
    qCDebug(sila_cpp) << this;

    m_Property->removeObserver(this);
    m_ValueSemaphore.release();
}

//============================================================================
template<typename SubscribeServiceT, typename ParametersT, typename ResponsesT,
         typename T, SubscribeServiceTemplate>
const char*
CSubscribeRPC<SubscribeServiceT, ParametersT, ResponsesT, T, RS>::name() const
{
    return "Subscribe RPC";
}

//============================================================================
template<typename SubscribeServiceT, typename ParametersT, typename ResponsesT,
         typename T, SubscribeServiceTemplate>
void CSubscribeRPC<SubscribeServiceT, ParametersT, ResponsesT, T,
                   RS>::CSubscribeRPC::onCreate()
{
    (this->m_Service->*RS)(&this->m_Context, &m_Request, &this->m_Responder,
                           this->m_CompletionQueue.get(),
                           this->m_CompletionQueue.get(),
                           new CAsyncRPCTag{this, TagType::REQUEST});
}

//============================================================================
template<typename SubscribeServiceT, typename ParametersT, typename ResponsesT,
         typename T, SubscribeServiceTemplate>
std::pair<const grpc::Status, const ResponsesT>
CSubscribeRPC<SubscribeServiceT, ParametersT, ResponsesT, T, RS>::onProcess()
{
    // We only have one value to send, so we always want to decrement the
    // semaphore to zero (i.e. we acquire all available resources or at least one
    // resource if none are available). That way, a client that starts
    // subscription later, does not get the same value multiple times.
    m_ValueSemaphore.acquire(std::max(1, m_ValueSemaphore.available()));
    qCDebug(sila_cpp) << "Sending property value...";
    return {grpc::Status::OK, m_Response};
}

//============================================================================
template<typename SubscribeServiceT, typename ParametersT, typename ResponsesT,
         typename T, SubscribeServiceTemplate>
void CSubscribeRPC<SubscribeServiceT, ParametersT, ResponsesT, T, RS>::create()
    const
{
    qCInfo(sila_cpp) << "--- Subscription for Observable Property"
                     << m_Property->name() << "started";
    qCDebug(sila_cpp) << this;

    new CSubscribeRPC{this->m_Service, this->m_CompletionQueue, m_Property};
}

//============================================================================
template<typename SubscribeServiceT, typename ParametersT, typename ResponsesT,
         typename T, SubscribeServiceTemplate>
void CSubscribeRPC<SubscribeServiceT, ParametersT, ResponsesT, T,
                   RS>::onCancelled()
{
    this->setStatus(AsyncRPCBase::CallStatus::CLEANUP);
    m_ValueSemaphore.release();
}

//============================================================================
template<typename SubscribeServiceT, typename ParametersT, typename ResponsesT,
         typename T, SubscribeServiceTemplate>
void CSubscribeRPC<SubscribeServiceT, ParametersT, ResponsesT, T, RS>::setValue(
    const ResponsesT& NewValue)
{
    m_Response = NewValue;
    m_ValueSemaphore.release();
}
}  // namespace SiLA2
#endif  // SUBSCRIBERPC_H

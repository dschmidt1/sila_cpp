/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   CommandInfoRPC.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   24.06.2020
/// \brief  Definition of the CCommandInfoRPC class
//============================================================================
#ifndef COMMANDINFORPC_H
#define COMMANDINFORPC_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/framework/data_types/ExecutionInfo.h>
#include <sila_cpp/framework/error_handling/FrameworkError.h>
#include <sila_cpp/server/command/IObservableCommandManager.h>
#include <sila_cpp/server/command/ObservableCommandWrapper.h>

#include "AsyncRPC.h"
#include "definitions.h"

#include <QObject>
#include <QSemaphore>

#include <grpcpp/grpcpp.h>
#include <grpcpp/impl/codegen/async_stream.h>
#include <grpcpp/impl/codegen/async_unary_call.h>

#include <utility>

namespace SiLA2
{
/**
 * @brief The CCommandInfoRPC class encapsulates the logic of the Command Info
 * RPC
 *
 * @tparam InfoServiceT The gRPC Service that contains the `Request<Command>_Info`
 * member function
 * @tparam ParametersT The type of the Command Parameters
 * @tparam ResponsesT The type of the Command Responses
 * @tparam RCI Signature of the `Request<Command>_Info` member function pointer
 * @tparam IntermediateResponsesT The type of the Intermediate Responses (not
 * actually a pack, but rather an optional type parameter, only necessary for the
 * correct signature of the Command Manager class template)
 */
template<typename InfoServiceT, typename ParametersT, typename ResponsesT,
         InfoServiceTemplate, typename... IntermediateResponsesT>
class CCommandInfoRPC :
    public CAsyncRPC<InfoServiceT, grpc::ServerAsyncWriter,
                     sila2::org::silastandard::ExecutionInfo>
{
    using AsyncRPCBase = CAsyncRPC<InfoServiceT, grpc::ServerAsyncWriter,
                                   sila2::org::silastandard::ExecutionInfo>;
    using ObservableCommandManager =
        IObservableCommandManager<ParametersT, ResponsesT,
                                  IntermediateResponsesT...>;
    using ObservableCommandWrapper =
        typename ObservableCommandManager::ObservableCommandWrapper;

public:
    /**
     * @brief C'tor
     *
     * @param Service The gRPC Async Service class that contains the
     * `Request<CommandName>_Info` member function pointer
     * @param CompletionQueue The gRPC server completion queue that should be
     * used for this RPC
     * @param CommandManager The Command Manager that this RPC belongs to
     */
    CCommandInfoRPC(InfoServiceT* Service,
                    std::shared_ptr<grpc::ServerCompletionQueue> CompletionQueue,
                    ObservableCommandManager* CommandManager);

    /**
     * @brief D'tor
     *
     * Disconnects from all signals
     */
    ~CCommandInfoRPC() override;

    /**
     * @override
     * @brief Get the name of this RPC (e.g. InitRPC, InfoRPC, or ResultRPC)
     * This is only for nicer debugging.
     *
     * @return The RPC name as a string
     */
    [[nodiscard]] const char* name() const override;

    /**
     * @override
     * @brief What should happen in the @a CREATE state of the RPC.
     */
    void onCreate() override;

    /**
     * @override
     * @brief What should happen in the @a PROCESS state of the RPC.
     *
     * @return A pair of the Status of the RPC execution and its result
     */
    std::pair<const grpc::Status, const sila2::org::silastandard::ExecutionInfo>
    onProcess() override;

    /**
     * @override
     * @brief Create a new CCommandInfoRPC instance
     */
    void create() const override;

    /**
     * @override
     * @brief What should happen when the RPC has been cancelled by the client.
     *
     * @details Releases the semaphore to unblock this thread and sets the Status
     * for this RPC to @a CLEANUP. This prevents the RPC from trying to write
     * another message to the client, which wouldn't make sense anyway. Instead,
     * this RPC will simply return from `CAsyncRPC::serve()` without doing
     * anything. The RPC that called `onCancelled()` from another thread will then
     * be properly deallocated. Note, that the two RPCs discussed here refer to
     * the same instance that just executes different functions in different
     * threads.
     */
    void onCancelled() override;

private:
    /**
     * @brief Set the Execution Info to @a ExecInfo. This Info will subsequently
     * be sent to the client.
     *
     * @param ExecutionInfo The new Execution Info to send to the client
     */
    void setExecutionInfo(const CExecutionInfo& ExecutionInfo);

    /**
     * @brief Set @a m_Command to the Command Wrapper that is identified by @a
     * UUID if this has not already been done.
     * As a side effect this function also connects the Command Wrapper's signals
     * to corresponding slots in this class.
     *
     * @param UUID The Command Execution UUID that identifies the Command
     * Execution for which Info should be streamed.
     *
     * @returns true, if the Command could be set successfully
     * @returns false, otherwise. This indicates that a SilA Framework Error has
     * to be raised to inform the client over the wrong UUID.
     */
    [[nodiscard]] bool setCommandByUUID(const CCommandExecutionUUID& UUID);

    /**
     * @brief Disconnect from all signals
     */
    void disconnectSignals() const;

    ObservableCommandManager*
        m_CommandManager;  ///< The Command Manager that this RPC belongs to
    std::shared_ptr<ObservableCommandWrapper>
        m_Command;  ///< The currently running Command
    sila2::org::silastandard::CommandExecutionUUID m_Request{};
    CExecutionInfo m_Response{CommandStatus::WAITING};
    QSemaphore m_InfoSemaphore{1};  ///< Used to wait for Execution Info to send
                                    ///< Is initially 1, to send the first value
                                    ///< right away after subscription started.
    QMetaObject::Connection
        m_ExecInfoConn;  ///< Connection to the ExecutorThread's executionInfoReady signal
    QMetaObject::Connection
        m_FinishedConn;  ///< Connection to the ExecutorThread's finished signal
};

//=============================================================================
template<typename InfoServiceT, typename ParametersT, typename ResponsesT,
         InfoServiceTemplate, typename... IntermediateResponsesT>
CCommandInfoRPC<InfoServiceT, ParametersT, ResponsesT, RCI,
                IntermediateResponsesT...>::
    CCommandInfoRPC(InfoServiceT* Service,
                    std::shared_ptr<grpc::ServerCompletionQueue> CompletionQueue,
                    ObservableCommandManager* CommandManager)
    : AsyncRPCBase{Service, CompletionQueue}, m_CommandManager{CommandManager}
{
    // Invoke the serving logic right away
    this->serve();
}

//=============================================================================
template<typename InfoServiceT, typename ParametersT, typename ResponsesT,
         InfoServiceTemplate, typename... IntermediateResponsesT>
CCommandInfoRPC<InfoServiceT, ParametersT, ResponsesT, RCI,
                IntermediateResponsesT...>::~CCommandInfoRPC()
{
    disconnectSignals();
}

//============================================================================
template<typename InfoServiceT, typename ParametersT, typename ResponsesT,
         InfoServiceTemplate, typename... IntermediateResponsesT>
const char* CCommandInfoRPC<InfoServiceT, ParametersT, ResponsesT, RCI,
                            IntermediateResponsesT...>::name() const
{
    return "Info RPC";
}

//============================================================================
template<typename InfoServiceT, typename ParametersT, typename ResponsesT,
         InfoServiceTemplate, typename... IntermediateResponsesT>
void CCommandInfoRPC<InfoServiceT, ParametersT, ResponsesT, RCI,
                     IntermediateResponsesT...>::CCommandInfoRPC::onCreate()
{
    (this->m_Service->*RCI)(&this->m_Context, &m_Request, &this->m_Responder,
                            this->m_CompletionQueue.get(),
                            this->m_CompletionQueue.get(),
                            new CAsyncRPCTag{this, TagType::REQUEST});
}

//============================================================================
template<typename InfoServiceT, typename ParametersT, typename ResponsesT,
         InfoServiceTemplate, typename... IntermediateResponsesT>
std::pair<const grpc::Status, const sila2::org::silastandard::ExecutionInfo>
CCommandInfoRPC<InfoServiceT, ParametersT, ResponsesT, RCI,
                IntermediateResponsesT...>::onProcess()
{
    qCInfo(sila_cpp) << "--- Command Execution Info for Observable Command"
                     << m_CommandManager->name() << "requested";
    qCDebug(sila_cpp) << "UUID of requested Command:" << m_Request.value();

    if (!setCommandByUUID(m_Request))
    {
        this->finish();
        const auto Error = CFrameworkError{
            CFrameworkError::INVALID_COMMAND_EXECUTION_UUID,
            "There is no Command for this Command Execution UUID."};
        return {Error.raise(), {}};
    }

    // The very first Execution Info goes to the CommandInit RPC so we might have
    // missed a sudden finish of the Command
    if (m_Command->finished())
    {
        m_Command->requestLastExecutionInfo();
    }

    // We only have one value to send, so we always want to decrement the
    // semaphore to zero (i.e. we acquire all available resources or at least one
    // resource if none are available). That way, a client that starts
    // subscription later, does not get the same value multiple times.
    m_InfoSemaphore.acquire(std::max(1, m_InfoSemaphore.available()));

    return {grpc::Status::OK, m_Response.toProtoMessage()};
}

//============================================================================
template<typename InfoServiceT, typename ParametersT, typename ResponsesT,
         InfoServiceTemplate, typename... IntermediateResponsesT>
void CCommandInfoRPC<InfoServiceT, ParametersT, ResponsesT, RCI,
                     IntermediateResponsesT...>::create() const
{
    new CCommandInfoRPC{this->m_Service, this->m_CompletionQueue,
                        m_CommandManager};
}

//============================================================================
template<typename InfoServiceT, typename ParametersT, typename ResponsesT,
         InfoServiceTemplate, typename... IntermediateResponsesT>
void CCommandInfoRPC<InfoServiceT, ParametersT, ResponsesT, RCI,
                     IntermediateResponsesT...>::onCancelled()
{
    // If we're getting an error the very first time we send Execution Info, this
    // will result in the AsyncRPCTag with TagType::DONE to be delivered next in
    // the CQ. However, the Execution Info we send to the client through
    // `grpc::ServerAsyncWriter::WriteAndFinish()` will put another (regular) Task
    // in the CQ that has to be popped out first before we can clean up. Setting
    // the CallStatus to CLEANUP here, would result in the regular Task being
    // popped out even if the corresponding RPC instance has already been deleted.
    if (m_Response.commandStatus() != CommandStatus::FINISHED_WITH_ERROR)
    {
        this->setStatus(AsyncRPCBase::CallStatus::CLEANUP);
    }
    m_InfoSemaphore.release();
}

//============================================================================
template<typename InfoServiceT, typename ParametersT, typename ResponsesT,
         InfoServiceTemplate, typename... IntermediateResponsesT>
void CCommandInfoRPC<InfoServiceT, ParametersT, ResponsesT, RCI,
                     IntermediateResponsesT...>::
    setExecutionInfo(const CExecutionInfo& ExecutionInfo)
{
    m_Response = ExecutionInfo;
    m_InfoSemaphore.release();
}

//============================================================================
template<typename InfoServiceT, typename ParametersT, typename ResponsesT,
         InfoServiceTemplate, typename... IntermediateResponsesT>
bool CCommandInfoRPC<
    InfoServiceT, ParametersT, ResponsesT, RCI,
    IntermediateResponsesT...>::setCommandByUUID(const CCommandExecutionUUID& UUID)
{
    if (m_Command)
    {
        // we already have the current Command Wrapper instance
        // no need to connect the signals again
        return true;
    }

    m_Command = m_CommandManager->getCommandByUUID(UUID);
    if (!m_Command)
    {
        // there is no such Command; caller is required to raise an error for this
        return false;
    }

    m_ExecInfoConn = QObject::connect(
        m_Command.get(), &ObservableCommandWrapper::executionInfoReady,
        [this](const auto& ExecInfo) { setExecutionInfo(ExecInfo); });
    m_FinishedConn = QObject::connect(
        m_Command.get(), &ObservableCommandWrapper::executionFinished,
        [this](const auto ExecInfo) {
            this->finish();
            setExecutionInfo(ExecInfo);
            m_Command.reset();
            disconnectSignals();
        });
    return true;
}

//============================================================================
template<typename InfoServiceT, typename ParametersT, typename ResponsesT,
         InfoServiceTemplate, typename... IntermediateResponsesT>
void CCommandInfoRPC<InfoServiceT, ParametersT, ResponsesT, RCI,
                     IntermediateResponsesT...>::disconnectSignals() const
{
    QObject::disconnect(m_ExecInfoConn);
    QObject::disconnect(m_FinishedConn);
}

}  // namespace SiLA2
#endif  // COMMANDINFORPC_H

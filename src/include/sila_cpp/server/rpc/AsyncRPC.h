/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   AsyncRPC.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   20.05.2020
/// \brief  Declaration of the CAsyncRPC class
//============================================================================
#ifndef ASYNCRPC_H
#define ASYNCRPC_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/global.h>

#include <grpcpp/grpcpp.h>
#include <grpcpp/impl/codegen/async_unary_call.h>

#include <atomic>
#include <memory>
#include <utility>

namespace SiLA2
{
/**
 * @brief The TagType enum contains the different types of Tags used in gRPC
 * Completion Queue operations.
 */
enum TagType
{
    REQUEST,  ///< used in the `Request<RPC>` function calls
    WRITE,    ///< used in `Write`, `Finish` and `WriteAndFinish` function calls
    DONE      ///< used in `AsyncNotifyWhenDone` function call to identify
              ///< when this RPC is done
};

/**
 * @brief The CAsyncRPCTag class represents the Tag that is used for gRPC
 * Completion Queue operations.
 */
template<typename AsyncRPC>
class CAsyncRPCTag
{
public:
    /**
     * @brief C'tor
     *
     * @param RPC Pointer to the Async RPC associated with this Tag
     * @param Type The Type of this Tag
     */
    CAsyncRPCTag(AsyncRPC* RPC, TagType Type);

    [[nodiscard]] AsyncRPC* rpc() const;

    [[nodiscard]] TagType type() const;

private:
    AsyncRPC* m_RPC;
    TagType m_Type{};
};

//============================================================================
template<typename AsyncRPC>
CAsyncRPCTag<AsyncRPC>::CAsyncRPCTag(AsyncRPC* RPC, TagType Type)
    : m_RPC{RPC}, m_Type{Type}
{}

//============================================================================
template<typename AsyncRPC>
AsyncRPC* CAsyncRPCTag<AsyncRPC>::rpc() const
{
    return m_RPC;
}

//============================================================================
template<typename AsyncRPC>
TagType CAsyncRPCTag<AsyncRPC>::type() const
{
    return m_Type;
}

/**
 * @brief The CAsyncRPC class
 *
 * @tparam ServiceT The gRPC Service that this RPC belongs to
 * @tparam WriterT The type of the gRPC Response Writer (either for unary or
 * streaming RPCs)
 * @tparam ResponsesT The type of the RPC Responses
 */
template<typename ServiceT, template<typename> class WriterT, typename ResponsesT>
class CAsyncRPC
{
public:
    /**
     * @brief The CallStatus enum implements a tiny state machine for the
     * CAsyncRPC class
     */
    enum CallStatus
    {
        CREATE,
        FIRST_PROCESS,
        PROCESS,
        FINISH,
        CLEANUP
    };

    /**
     * @brief C'tor
     *
     * @param Parent The Parent SiLA Feature (resp. gRPC Service) that this RPC
     * belongs to
     * @param CompletionQueue The gRPC server completion queue that should be
     * used for this RPC
     */
    CAsyncRPC(ServiceT* Parent,
              std::shared_ptr<grpc::ServerCompletionQueue> CompletionQueue);

    virtual ~CAsyncRPC() = default;

    /**
     * @brief The serving logic
     */
    void serve();

    /**
     * @brief Whether this RPC is a streaming RPC or not
     *
     * @returns true, if this RPC streams, false otherwise
     */
    [[nodiscard]] bool isStreamingRPC() const;

    /**
     * @brief Whether the RPC is in the @a FIRST_PROCESS or @a PROCESS state
     *
     * @returns true, if @a m_CallStatus is @a FIRST_PROCESS or @a PROCESS
     * @returns false, otherwise
     */
    [[nodiscard]] bool isRunning() const;

    /**
     * @brief Change the call status of this RPC to @a FINISH.
     * Streaming RPCs may call this function to indicate that streaming has
     * finished and want to report that to the gRPC framework by calling
     * the writer's @a Finish method.
     * As a side effect, this function will check if the RCP has been
     * cancelled. If this is the case, the @a onCancelled() handler will be
     * invoked.
     */
    void finish();

    /**
     * @brief Get the name of this RPC. This is only for nicer debugging.
     *
     * @return The RPC name as a string
     */
    [[nodiscard]] virtual const char* name() const = 0;

protected:
    /**
     * @brief Safely set the @a m_CallStatus to the given @a Status.
     *
     * @param Status The new CallStatus to set
     */
    void setStatus(CallStatus Status);

    /**
     * @brief Handler function that is invoked when the CAsyncRPC instance
     * advances to the @a CREATE state. This should invoke the function that
     * 'requests' processing of this RPC.
     */
    virtual void onCreate() = 0;

    /**
     * @brief Handler function that is invoked when the CAsyncRPC instance
     * advances to the @a PROCESS state. This should invoke the function
     * containing the actual RPC logic.
     *
     * @return A pair of the Status of the RPC execution and its result
     */
    virtual std::pair<const grpc::Status, const ResponsesT> onProcess() = 0;

    /**
     * @brief Create a new CAsyncRPC instance. This is used to create new
     * instances when the current instance is in the @a FIRST_PROCESS state. The
     * newly created instance can then take care of new (concurrent) requests,
     * e.g. from other clients.
     */
    virtual void create() const = 0;

    /**
     * @brief Handler function that is invoked when this RPC has been cancelled by
     * the client.
     * The default implementation just sets the @a m_CallStatus to @a FINISH. This
     * can be overridden in subclasses to e.g. stop observing Property value
     * changes or wait for Command Execution Info.
     */
    virtual void onCancelled();

    ServiceT*
        m_Service;  ///< The gRPC Service containing the member functions for this RPC
    std::shared_ptr<grpc::ServerCompletionQueue>
        m_CompletionQueue;  ///< For server notifications (used in derived classes)
    grpc::ServerContext m_Context{};  ///< gRPC server context for e.g. metadata
    WriterT<ResponsesT> m_Responder;  ///< The means to get back to the client

private:
    std::atomic<CallStatus> m_CallStatus{CREATE};  ///< Current state of this RPC
};

//=============================================================================
template<typename Service, template<typename> class Writer, typename Responses>
CAsyncRPC<Service, Writer, Responses>::CAsyncRPC(
    Service* Parent, std::shared_ptr<grpc::ServerCompletionQueue> CompletionQueue)
    : m_Service{Parent},
      m_CompletionQueue{std::move(CompletionQueue)},
      m_Responder{&m_Context}
{}

//=============================================================================
template<typename Service, template<typename> class Writer, typename Responses>
void CAsyncRPC<Service, Writer, Responses>::serve()
{
    switch (m_CallStatus)
    {
    case CREATE:
        m_CallStatus = FIRST_PROCESS;
        qCDebug(sila_cpp) << "Creating new" << this;
        m_Context.AsyncNotifyWhenDone(new CAsyncRPCTag{this, TagType::DONE});

        // As part of the initial CREATE state, we *request* that the system
        // starts processing requests. In this request, "this" acts as the tag
        // uniquely identifying the request (so that different CAsyncRPC
        // instances can serve different requests concurrently), in this case
        // the memory address of this CAsyncRPC instance.
        onCreate();
        break;
    case FIRST_PROCESS:
        // Spawn a new RPC instance to serve new clients while processing this
        // request. The instance will deallocate itself as part of its CLEANUP
        // state.
        // Only do this once so that there won't be a million new instances for
        // streaming RPCs (as they will be in the PROCESS state for multiple
        // cycles)
        m_CallStatus = PROCESS;
        create();
        [[fallthrough]];
    case PROCESS:
    {
        if (m_Context.IsCancelled())
        {
            qCDebug(sila_cpp) << this << "has been cancelled by the client";
        }

        qCDebug(sila_cpp) << "Processing" << this;
        // The actual processing
        const auto [Status, Response] = onProcess();

        if constexpr (std::is_same_v<grpc::ServerAsyncResponseWriter<Responses>,
                                     decltype(m_Responder)>)
        {
            finish();
            // m_Responder is grpc::ServerAsyncResponseWriter that returns the
            // response to the client through the `Finish` call
            m_Responder.Finish(Response, Status,
                               new CAsyncRPCTag{this, TagType::WRITE});
        }
        else
        {
            // m_Responder is grpc::ServerAsyncWriter that returns a message as
            // part of the stream through `Write` and the final Message and Status
            // through `WriteAndFinish`

            // state might change during `onProcess` when `finish` is called
            if (m_CallStatus == PROCESS)
            {
                m_Responder.Write(Response,
                                  new CAsyncRPCTag{this, TagType::WRITE});
            }
            else if (m_CallStatus == FINISH)
            {
                if (Status.ok())
                {
                    m_Responder.WriteAndFinish(
                        Response, grpc::WriteOptions{}.set_last_message(), Status,
                        new CAsyncRPCTag{this, TagType::WRITE});
                }
                else
                {
                    m_Responder.Finish(Status,
                                       new CAsyncRPCTag{this, TagType::WRITE});
                }
            }
        }
        break;
    }
    case FINISH:
        qCDebug(sila_cpp) << this << "is finished";
        m_CallStatus = CLEANUP;
        break;
    case CLEANUP:
        qCDebug(sila_cpp) << "Cleaning up" << this;
        delete this;
        break;
    default:
        qCWarning(sila_cpp) << "Unknown RPC Call State" << m_CallStatus;
        break;
    }
}

//============================================================================
template<typename Service, template<typename> class Writer, typename Responses>
bool CAsyncRPC<Service, Writer, Responses>::isStreamingRPC() const
{
    return !std::is_same_v<grpc::ServerAsyncResponseWriter<Responses>,
                           decltype(m_Responder)>;
}

//============================================================================
template<typename Service, template<typename> class Writer, typename Responses>
bool CAsyncRPC<Service, Writer, Responses>::isRunning() const
{
    return m_CallStatus == PROCESS;
}

//============================================================================
template<typename Service, template<typename> class Writer, typename Responses>
void CAsyncRPC<Service, Writer, Responses>::finish()
{
    if (m_Context.IsCancelled())
    {
        qCDebug(sila_cpp) << this << "has been cancelled by the client";
        this->onCancelled();
        return;
    }
    m_CallStatus = FINISH;
}

//============================================================================
template<typename Service, template<typename> class Writer, typename Responses>
void CAsyncRPC<Service, Writer, Responses>::setStatus(CallStatus Status)
{
    m_CallStatus = Status;
}

//============================================================================
template<typename Service, template<typename> class Writer, typename Responses>
void CAsyncRPC<Service, Writer, Responses>::onCancelled()
{
    qCDebug(sila_cpp) << "Cancelling" << this;
    if (m_CallStatus == CLEANUP)
    {
        // Even though this shouldn't happen, be sure to not reset the status
        // to FINISH if we're already in CLEANUP
        qCDebug(sila_cpp) << this << "is already CLEANUP state";
        return;
    }
    m_CallStatus = FINISH;
}

//============================================================================
template<typename Service, template<typename> class Writer, typename Responses>
QDebug& operator<<(QDebug dbg, CAsyncRPC<Service, Writer, Responses>* rhs)
{
    return dbg << rhs->name() << static_cast<void*>(rhs);
}
}  // namespace SiLA2

#endif  // ASYNCRPC_H

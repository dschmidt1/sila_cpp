/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   CommandResultRPC.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   24.06.2020
/// \brief  Definition of the CCommandResultRPC class
//============================================================================
#ifndef COMMANDRESULTRPC_H
#define COMMANDRESULTRPC_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/framework/error_handling/FrameworkError.h>
#include <sila_cpp/server/command/IObservableCommandManager.h>

#include "AsyncRPC.h"
#include "definitions.h"

#include <grpcpp/grpcpp.h>
#include <grpcpp/impl/codegen/async_stream.h>
#include <grpcpp/impl/codegen/async_unary_call.h>

#include <utility>

namespace SiLA2
{
/**
 * @brief The CCommandResultRPC class encapsulates the logic of the Command
 * Result RPC
 *
 * @tparam ResultServiceT The gRPC Service that contains the
 * `Request<Command>_Result` member function
 * @tparam ParametersT The type of the Command Parameters
 * @tparam ResponsesT The type of the Command Responses
 * @tparam RCR Signature of the `Request<Command>_Result` member function pointer
 * @tparam IntermediateResponsesT The type of the Intermediate Responses (not
 * actually a pack, but rather an optional type parameter, only necessary for the
 * correct signature of the Command Manager class template)
 */
template<typename ResultServiceT, typename ParametersT, typename ResponsesT,
         ResultServiceTemplate, typename... IntermediateResponsesT>
class CCommandResultRPC :
    public CAsyncRPC<ResultServiceT, grpc::ServerAsyncResponseWriter, ResponsesT>
{
    using AsyncRPCBase =
        CAsyncRPC<ResultServiceT, grpc::ServerAsyncResponseWriter, ResponsesT>;
    using ObservableCommandManager =
        IObservableCommandManager<ParametersT, ResponsesT,
                                  IntermediateResponsesT...>;

public:
    /**
     * @brief C'tor
     *
     * @param Service The gRPC Async Service class that contains the
     * `Request<CommandName>_Result` member function pointer
     * @param CompletionQueue The gRPC server completion queue that should be
     * used for this RPC
     * @param CommandManager The Command Manager that this RPC belongs to
     */
    CCommandResultRPC(ResultServiceT* Service,
                      std::shared_ptr<grpc::ServerCompletionQueue> CompletionQueue,
                      ObservableCommandManager* CommandManager);

    /**
     * @override
     * @brief Get the name of this RPC (e.g. InitRPC, InfoRPC, or ResultRPC)
     * This is only for nicer debugging.
     *
     * @return The RPC name as a string
     */
    [[nodiscard]] const char* name() const override;

    /**
     * @override
     * @brief What should happen in the @a CREATE state of the RPC.
     */
    void onCreate() override;

    /**
     * @override
     * @brief What should happen in the @a PROCESS state of the RPC.
     *
     * @return A pair of the Status of the RPC execution and its result
     */
    std::pair<const grpc::Status, const ResponsesT> onProcess() override;

    /**
     * @override
     * @brief Create a new CCommandResultRPC instance
     */
    void create() const override;

private:
    ObservableCommandManager*
        m_CommandManager;  ///< The Command Manager that this RPC belongs to
    sila2::org::silastandard::CommandExecutionUUID m_Request{};
};

//=============================================================================
template<typename ResultServiceT, typename ParametersT, typename ResponsesT,
         ResultServiceTemplate, typename... IntermediateResponsesT>
CCommandResultRPC<ResultServiceT, ParametersT, ResponsesT, RCR,
                  IntermediateResponsesT...>::
    CCommandResultRPC(ResultServiceT* Service,
                      std::shared_ptr<grpc::ServerCompletionQueue> CompletionQueue,
                      ObservableCommandManager* CommandManager)
    : AsyncRPCBase{Service, CompletionQueue}, m_CommandManager{CommandManager}
{
    // Invoke the serving logic right away
    this->serve();
}

//============================================================================
template<typename ResultServiceT, typename ParametersT, typename ResponsesT,
         ResultServiceTemplate, typename... IntermediateResponsesT>
const char*
CCommandResultRPC<ResultServiceT, ParametersT, ResponsesT, RCR,
                  IntermediateResponsesT...>::CCommandResultRPC::name() const
{
    return "Result RPC";
}

//============================================================================
template<typename ResultServiceT, typename ParametersT, typename ResponsesT,
         ResultServiceTemplate, typename... IntermediateResponsesT>
void CCommandResultRPC<ResultServiceT, ParametersT, ResponsesT, RCR,
                       IntermediateResponsesT...>::CCommandResultRPC::onCreate()
{
    (this->m_Service->*RCR)(&this->m_Context, &m_Request, &this->m_Responder,
                            this->m_CompletionQueue.get(),
                            this->m_CompletionQueue.get(),
                            new CAsyncRPCTag{this, TagType::REQUEST});
}

//============================================================================
template<typename ResultServiceT, typename ParametersT, typename ResponsesT,
         ResultServiceTemplate, typename... IntermediateResponsesT>
std::pair<const grpc::Status, const ResponsesT>
CCommandResultRPC<ResultServiceT, ParametersT, ResponsesT, RCR,
                  IntermediateResponsesT...>::CCommandResultRPC::onProcess()
{
    qCInfo(sila_cpp)
        << "--- Command Execution Result for Observable Command"
        << m_CommandManager->name()
        << "requested\nUUID of requested Command:" << m_Request.value();

    const auto Command = m_CommandManager->getCommandByUUID(m_Request);
    if (!Command)
    {
        const auto Error = CFrameworkError{
            CFrameworkError::INVALID_COMMAND_EXECUTION_UUID,
            "There is no Command for this Command Execution UUID."};
        return {Error.raise(), {}};
    }

    if (!Command->finished())
    {
        const auto Error =
            CFrameworkError{CFrameworkError::COMMAND_EXECUTION_NOT_FINISHED,
                            "The Command Execution has not finished yet."};
        return {Error.raise(), {}};
    }

    const auto Response = Command->result();

    qCDebug(sila_cpp) << "Command result" << Response;
    return {grpc::Status{Command->status()}, Response};
}

//============================================================================
template<typename ResultServiceT, typename ParametersT, typename ResponsesT,
         ResultServiceTemplate, typename... IntermediateResponsesT>
void CCommandResultRPC<ResultServiceT, ParametersT, ResponsesT, RCR,
                       IntermediateResponsesT...>::CCommandResultRPC::create()
    const
{
    new CCommandResultRPC{this->m_Service, this->m_CompletionQueue,
                          m_CommandManager};
}
}  // namespace SiLA2

#endif  // COMMANDRESULTRPC_H

/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAServer.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   06.01.2020
/// \brief  Declaration of the CSiLAServer class
//============================================================================
#ifndef SILASERVER_H
#define SILASERVER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/SSLCredentials.h>
#include <sila_cpp/common/ServerAddress.h>
#include <sila_cpp/global.h>

#include <QObject>

#include <polymorphic_value/polymorphic_value.h>

//=============================================================================
//                            FORWARD DECLARATIONS
//=============================================================================
namespace grpc_impl
{
class ServerCompletionQueue;
}  // namespace grpc_impl

namespace grpc
{
class Service;
typedef grpc_impl::ServerCompletionQueue ServerCompletionQueue;
}  // namespace grpc

class QCommandLineParser;

namespace SiLA2
{
class CServerInformation;
class CFeatureID;

/**
 * @brief The CSiLAServer class provides a base for implementing custom SiLA 2
 * servers
 */
class SILA_CPP_EXPORT CSiLAServer : public QObject
{
    Q_OBJECT

    class PrivateImpl;
    using PrivateImplPtr = jbcoe::polymorphic_value<CSiLAServer::PrivateImpl>;

public:
    /**
     * @brief C'tor
     *
     * @param ServerInformation The server's information such as name, type, ...
     * @param Address The server's address (IP and port number), defaults to
     * 127.0.0.1:50051
     * @param Credentials Optional SSL certificate and key for secure
     * communication; if the credentials are empty the server tries to create a
     * self-signed certificate on the fly. If the credentials are set @em and at
     * the same time credentials are given through command line arguments, then
     * the latter take precedence (see @a CSiLAServer::commandLineParser).
     * @param ForceEncryption Same as the corresponding command line option:
     * Prevents falling back to insecure communication and fails if no secure
     * communication can be used. If the CLI option is specified, it takes
     * precedence over this argument.
     * @param parent The parent object
     */
    explicit CSiLAServer(const CServerInformation& ServerInformation,
                         const CServerAddress& Address = {},
                         const CSSLCredentials& Credentials = {},
                         bool ForceEncryption = false, QObject* parent = nullptr,
                         PrivateImplPtr priv = {});

    /**
     * @brief D'tor
     * Shuts down the server if it is still running
     */
    ~CSiLAServer() override;

    /**
     * @brief Register the given @a Feature to the server.
     * This call does not take ownership of the feature. The feature must exist
     * for the lifetime of the SiLA server.
     *
     * @param Feature The feature to register
     * @param Name The fully qualified feature identifier
     * @param FeatureDefinition The feature definition XML document
     */
    void registerFeature(grpc::Service* Feature, const CFeatureID& Name,
                         const QByteArray& FeatureDefinition);

    /**
     * @brief Add a completion queue to the server that can be used in
     * asynchronous Features. Note that the server shares ownership of the queue
     * with the caller. However the server is responsible for shutting down and
     * draining the queue after the server itself was shut down via @a shutdown.
     *
     * @return std::shared_ptr<grpc::ServerCompletionQueue> The shared completion
     * queue
     */
    std::shared_ptr<grpc::ServerCompletionQueue> addCompletionQueue();

    /**
     * @brief Run the SiLA2 server.
     *
     * @param block Indicates whether the server should block execution until
     * Ctrl-C is pressed on the command line or if this function should
     * immediately return after it started the server. In case of the latter the
     * server has to be shut down by calling the @c shutdown() method.
     * Because this function internally uses @c QCoreApplication::exec it is
     * required that you call @c QCoreApplication::exec yourself when @a block is
     * set to @c false. Otherwise some internal signals can not be processed.
     *
     * @note You can also send @c SIGINT on Unix systems or @c CTRL_C_EVENT on
     * Windows to shutdown the server when @a block was set to true.
     */
    void run(bool block = true);

    /**
     * @brief Get the server's command line parser
     * This can be used to add additional command line options to the parser.
     * Per default, the server command line parser contains the following options:
     * @li -?, -h, --help
     * @li -v, --version
     * @li -r, --root-ca <filename>
     * @li -c, --encryption-cert <filename>
     * @li -k, --encryption-key <filename>
     * @li -e, --force-encryption
     *
     * @note When the filename(s) for the SSL certificate and key are set using
     * the command line, you don't need to parse these options yourself and pass
     * them to @b CSiLAServer via its c'tor. Instead, @b CSiLAServer will scan for
     * these options itself and use the specified files to read in the SSL
     * credentials. If you don't set the credentials via command line arguments
     * you will need to use the c'tor argument in order to use our own
     * certificates for encryption or otherwise a self-signed certificate will be
     * created on the fly. However, if you specify credentials via both the
     * command line @em and the c'tor argument, then the values read from the
     * command line will take precedence!
     *
     * @return The server's command line parser
     *
     * @sa CSiLAServer::CSiLAServer
     */
    static std::shared_ptr<QCommandLineParser> commandLineParser();

public slots:
    /**
     * @brief Shutdown the server. Only valid to call when the @c run() method has
     * been called with @c block set to @a false.
     */
    void shutdown();

signals:
    /**
     * @brief This signal is emitted when the server has been started. All
     * Features that have been registered before calling @a run() can now safely
     * start accepting Command Requests and Property reads.
     */
    void started() const;

protected:
    PrivateImplPtr d_ptr;

private:
    PIMPL_DECLARE_PRIVATE(CSiLAServer)
};
}  // namespace SiLA2

#endif  // SILASERVER_H

/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAFeature.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   20.05.2020
/// \brief  Declaration of the CSiLAFeature class template
//============================================================================
#ifndef SILAFEATURE_H
#define SILAFEATURE_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/FeatureID.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/global.h>
#include <sila_cpp/server/SiLAServer.h>

#include <QCoreApplication>
#include <QFile>

#include <memory>

namespace SiLA2
{
static const auto QRC_PREFIX = QStringLiteral(":");
static const auto FDL_FILE_DIR_PREFIX = QStringLiteral("/meta/");
static const auto FDL_FILE_EXTENSION = QStringLiteral(".sila.xml");

/**
 * @brief The CSiLAFeature class is a wrapper class template that every user
 * defined SiLA Feature implementation can inherit from.
 *
 * @tparam BaseClassT The type of the gRPC::AsyncService base class
 *
 * @details It takes the gRPC generated Feature class as template parameter and
 * derives from its nested 'Service' class. Thus, all classes derived from this
 * class are automatically inheriting from this 'Service' class, as well and can
 * implement the RPC methods. It also provided the @a featureDefinition method
 * that provides a default generic way of reading the Feature's feature defintion
 * from its FDL file.
 */
template<typename BaseClassT>
class CSiLAFeature : public BaseClassT::AsyncService
{
public:
    /**
     * @brief C'tor
     *
     * @param parent The SiLA server instance that contains this Feature
     */
    explicit CSiLAFeature(CSiLAServer* parent);

    /**
     * @brief Get a non-owning reference to the SiLA Server that contains this
     * feature
     *
     * @return A reference to this Feature's server
     */
    [[nodiscard]] SiLA2::CSiLAServer* server() const;

    /**
     * @brief Get the feature definition of this feature
     *
     * @details This tries to load the FDL file from a Qt Resource file first
     * (e.g. ":/meta/MyFeature.sila.xml"). If this fails, it is assumed that the
     * FDL file is located in 'meta/MyFeature.sila.xml' in the folder
     * @a QApplication::applicationDirPath(). If you want to use another
     * directory, you can override this method and implement it yourself
     *
     * @return QByteArray The feature definition
     */
    [[nodiscard]] static QByteArray featureDefinition();

    /**
     * @brief Get the Fully Qualified Feature Identifier of this Feature
     *
     * @return CFeatureID This Feature's Fully Qualified Feature Identifier
     */
    [[nodiscard]] static CFeatureID featureIdentifier();

private:
    CSiLAServer* m_Server{};
};

//=============================================================================
template<typename BaseClassT>
CSiLAFeature<BaseClassT>::CSiLAFeature(CSiLAServer* parent) : m_Server{parent}
{}

//=============================================================================
template<typename BaseClassT>
CSiLAServer* CSiLAFeature<BaseClassT>::server() const
{
    return m_Server;
}

//=============================================================================
template<typename BaseClassT>
QByteArray CSiLAFeature<BaseClassT>::featureDefinition()
{
    const auto FilePath = FDL_FILE_DIR_PREFIX
                          + CSiLAFeature::featureIdentifier().featureName()
                          + FDL_FILE_EXTENSION;
    auto File = QFile{QRC_PREFIX + FilePath};
    if (!File.open(QFile::ReadOnly | QFile::Text))
    {
        // try from normal file name if qrc didn't work
        File.setFileName(QCoreApplication::applicationDirPath().append(FilePath));
        if (!File.open(QFile::ReadOnly | QFile::Text))
        {
            qCWarning(sila_cpp) << "Could not open FDL file (" << File.fileName()
                                << ") for reading of feature definition!";
            return {};
        }
    }

    return File.readAll();
}

//=============================================================================
template<typename BaseClassT>
CFeatureID CSiLAFeature<BaseClassT>::featureIdentifier()
{
    return CFeatureID{BaseClassT::service_full_name()};
}

}  // namespace SiLA2

#endif  // SILAFEATURE_H

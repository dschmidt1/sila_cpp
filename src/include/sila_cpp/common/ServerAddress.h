/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ServerAddress.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   08.01.2020
/// \brief  Declaration of the CServerAddress class
//============================================================================
#ifndef SERVERADDRESS_H
#define SERVERADDRESS_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include <QDebug>

//=============================================================================
//                            FORWARD DECLARATIONS
//=============================================================================
class QString;

namespace SiLA2
{
/**
 * @brief The CServerAddress class represents the address of a SiLA 2 server
 * consisting of an IP address and a port number
 */
class SILA_CPP_EXPORT CServerAddress
{
public:
    /**
     * @brief C'tor
     *
     * @param IPAddress The IP address of the SiLA server (default: localhost)
     * @param Port The port number on which the SiLA server applications runs
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CServerAddress(QString IPAddress = "127.0.0.1", QString Port = "50051");

    /**
     * @brief Convenience c'tor for std::string
     *
     * @sa CServerAddress(QString, QString)
     */
    CServerAddress(const std::string& IPAddress, const std::string& Port);

    /**
     * @brief Convert this CServerAddress object to a string
     *
     * @return QString The server address as "ip_address:port"
     */
    [[nodiscard]] QString toString() const;

    /**
     * @brief Convert this CServerAddress object to a string
     *
     * @return std::string The server address as "ip_address:port"
     */
    [[nodiscard]] std::string toStdString() const;

    /**
     * @brief Get a pretty representation of the address and port that can be used
     * in log messages for example
     *
     * @return QString Pretty representation of the server address
     */
    [[nodiscard]] QString prettyString() const;

    /**
     * @brief Get the IP address of this Server Address
     *
     * @return The IP address as a QString
     */
    [[nodiscard]] QString ip() const;

    /**
     * @brief Set the IP of this Server Address
     *
     * @param IP The new IP
     */
    void setIP(const QString& IP);

    /**
     * @brief Get the port of this Server Address
     *
     * @return The port as a QString
     */
    [[nodiscard]] QString port() const;

    /**
     * @brief Set the port of this Server Address
     *
     * @param Port The new port
     */
    void setPort(const QString& Port);

    /**
     * @brief Checks if the @a m_IPAddress is either "localhost" or "127.0.0.1"
     *
     * @returns true, if @a m_IPAddress represents localhost, false otherwise
     */
    bool isLocalhost() const;

private:
    QString m_IPAddress{};
    QString m_Port{};
};

}  // namespace SiLA2

QDebug SILA_CPP_EXPORT operator<<(QDebug debug, const SiLA2::CServerAddress& rhs);

#endif  // SERVERADDRESS_H

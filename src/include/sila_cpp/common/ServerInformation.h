/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ServerInformation.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   07.01.2020
/// \brief  Declaration of the CServerInformation class
//============================================================================
#ifndef SERVERINFORMATION_H
#define SERVERINFORMATION_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include <QString>
#include <QUuid>

namespace SiLA2
{
/**
 * @brief The CServerInformation class holds information about a SiLA 2 server
 * such as its name, type, description, version etc.
 */
class SILA_CPP_EXPORT CServerInformation
{
public:
    /**
     * @brief C'tor
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CServerInformation(QString ServerName = "SiLAServer",
                       QString ServerType = "UnknownServerType",
                       QString Description = "A SiLA server",
                       QString Version = "0.0.1",
                       QString VendorURL = "www.example.com",
                       const QUuid& ServerUUID = {});

    /**
     * @brief Set the server's name to the given @a Name
     *
     * @param Name The new name of the server
     */
    void setServerName(const QString& Name);

    /**
     * @overload
     *
     * @brief For compatibility with C/C++ strings
     */
    void setServerName(std::string_view Name);

    /**
     * @brief Get the server's name
     *
     * @return The name of the server
     */
    [[nodiscard]] const QString& serverName() const;

    /**
     * @brief Set the server's type to the given @a Type
     *
     * @param Type The new type of the server
     */
    void setServerType(const QString& Type);

    /**
     * @overload
     *
     * @brief For compatibility with C/C++ strings
     */
    void setServerType(std::string_view Type);

    /**
     * @brief Get the server's type
     *
     * @return The type of the server
     */
    [[nodiscard]] const QString& serverType() const;

    /**
     * @brief Set the server's description to the given @a Description
     *
     * @param Description The new description of the server
     */
    void setDescription(const QString& Description);

    /**
     * @overload
     *
     * @brief For compatibility with C/C++ strings
     */
    void setDescription(std::string_view Description);

    /**
     * @brief Get the server's description
     *
     * @return The description of the server
     */
    [[nodiscard]] const QString& description() const;

    /**
     * @brief Set the server's version to the given @a Version
     *
     * @param Version The new version of the server
     */
    void setVersion(const QString& Version);

    /**
     * @overload
     *
     * @brief For compatibility with C/C++ strings
     */
    void setVersion(std::string_view Version);

    /**
     * @brief Get the server's version
     *
     * @return The version of the server
     */
    [[nodiscard]] const QString& version() const;

    /**
     * @brief Set the server's vendor URL to the given @a VendorURL
     *
     * @param VendorURL The new vendor URL of the server
     */
    void setVendorURL(const QString& VendorURL);

    /**
     * @overload
     *
     * @brief For compatibility with C/C++ strings
     */
    void setVendorURL(std::string_view VendorURL);

    /**
     * @brief Get the server's vendor URL
     *
     * @return The vendor URL of the server
     */
    [[nodiscard]] const QString& vendorURL() const;

    /**
     * @brief Set the server's UUID to the given @a UUID
     *
     * @param UUID The new UUID of the server
     */
    void setServerUUID(const QUuid& UUID);

    /**
     * @overload
     *
     * @brief For compatibility with C/C++ strings
     */
    void setServerUUID(std::string_view UUID);

    /**
     * @brief Get the server's UUID
     *
     * @return The UUID of the server
     */
    [[nodiscard]] const QUuid& serverUUID() const;

private:
    QString m_ServerName{};   ///< Human readable name of the SiLA server
    QString m_ServerType{};   ///< Type of this server
    QString m_Description{};  ///< Description of the SiLA server
    QString m_Version{};      ///< Version of the SiLA server
    QString m_VendorURL{};  ///< URL to the website of this server's vendor/product
    QUuid m_ServerUUID{};  ///< UUID of this server (auto-generated on first start of a server)
};
}  // namespace SiLA2

#endif  // SERVERINFORMATION_H

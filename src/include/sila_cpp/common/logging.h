/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   logging.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   08.01.2020
/// \brief  Definition of helper functions for logging
//============================================================================
#ifndef LOGGING_H
#define LOGGING_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include <QLoggingCategory>
#include <QString>
#include <QtDebug>

//=============================================================================
//                            FORWARD DECLARATIONS
//=============================================================================
namespace grpc
{
class Status;
}  // namespace grpc

namespace google::protobuf
{
class Message;
}  // namespace google::protobuf

/**
 * @brief The logging category used for all log messages within the sila_cpp lib
 */
SILA_CPP_EXPORT Q_DECLARE_LOGGING_CATEGORY(sila_cpp);

namespace SiLA2::logging
{
/**
 * @brief Sets up the logging for the sila_cpp library. This includes installing a
 * custom message handler as well as setting the logging level via the
 * @a setLoggingLevel function below.
 */
void SILA_CPP_EXPORT setupLogging();

/**
 * @brief Set the logging level for messages of the 'sila_cpp' logging
 * category (see above) according to the environment variable
 * `SILA_CPP_LOGGING_LEVEL`. If you want to change the logging level for
 * your application, either set the environment variable or call this function at
 * the beginning of your @c main.
 * @note The values are case insensitive
 *
 * @param DefaultLevel The default logging level if not set via the
 * environment variable
 */
void SILA_CPP_EXPORT setLoggingLevel(const QString& DefaultLevel = "INFO");
}  // namespace SiLA2::logging

QDebug SILA_CPP_EXPORT operator<<(QDebug dbg, const std::string& rhs);

QDebug SILA_CPP_EXPORT operator<<(QDebug dbg, const grpc::Status& rhs);

QDebug SILA_CPP_EXPORT operator<<(QDebug dbg,
                                  const google::protobuf::Message& rhs);

#endif  // LOGGING_H

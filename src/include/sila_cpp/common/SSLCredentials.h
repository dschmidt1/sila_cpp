/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SSLCredentials.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   21.09.2020
/// \brief  Declaration of the CSSLCredentials class
//============================================================================
#ifndef SSLCREDENTIALS_H
#define SSLCREDENTIALS_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include <QStringView>

#include <memory>
#include <string>
#include <vector>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace grpc_impl
{
class ServerCredentials;
class ChannelCredentials;
}  // namespace grpc_impl
namespace grpc
{
typedef grpc_impl::ServerCredentials ServerCredentials;
typedef grpc_impl::ChannelCredentials ChannelCredentials;
}  // namespace grpc

namespace SiLA2
{
/**
 * @brief The CSSLCredentials class encompasses a SSL certificate and key used for
 * encrypted communication between server and client
 *
 * You only have to generate a key and certificate for the server-side. The client
 * is capable of retrieving the server's certificate (if it has one) and using
 * that for encrypting the communication. But, if you'd like to, you can also
 * generate a certificate for your client and use that.
 * You can generate the a key and (self-signed) certificate like the following,
 * for example:
 * @code{.console}
 * $ openssl req -x509 -nodes -new -keyout out.key -out out.crt -sha256 \
 *   -days 365 -subj "/C=YOUR_COUNTRY_CODE/O=YOUR_COMPANY_NAME/CN=localhost"
 * @endcode
 * Substitute @c YOUR_COUNTRY_CODE and @c YOUR_COMPANY_NAME with your
 * corresponding values.
 */
class SILA_CPP_EXPORT CSSLCredentials
{
public:
    /**
     * @brief Default c'tor
     *
     * Results in empty root CA, certificate and key
     */
    CSSLCredentials() = default;

    /**
     * @brief Construct new SSL credentials that only have a root CA
     *
     * This will be only used by clients that connect to a server which has a
     * certificate that was signed by @a RootCA. If the server has self-signed
     * certificates that weren't signed by a root CA then use the server's
     * certificate here.
     *
     * @param RootCA The certificate authority that signed the certificate
     */
    explicit CSSLCredentials(std::string RootCA);

    /**
     * @brief Construct new SSL credentials that were either self-signed or signed
     * by @a RootCA
     *
     * @param Certificate The SSL certificate
     * @param Key The SSL private key (for the server it is required to use a
     * private key, for the client this can be empty)
     * @param RootCA The certificate authority that signed the @a Certificate (can
     * be left empty in case of a self-signed certificate that didn't use a CA)
     */
    CSSLCredentials(std::string Certificate, std::string Key,
                    std::string RootCA = "");

    /**
     * @brief Set the root CA to @a RootCA
     *
     * @param RootCA The new root CA
     */
    void setRootCA(const std::string& RootCA);

    /**
     * @brief Convenience method to set the root CA from the contents of the
     * file specified by @a FileName
     *
     * If the @a FileName is invalid or the file cannot be opened, this function
     * does nothing.
     *
     * @param FileName The name of the file that contains the new root CA
     */
    void setRootCAFromFile(QStringView FileName);

    /**
     * @brief Get the root CA
     *
     * @return The root CA as a std::string
     */
    [[nodiscard]] std::string rootCA() const;

    /**
     * @brief Set the certificate to @a Certificate
     *
     * @param Certificate The new certificate
     */
    void setCertificate(const std::string& Certificate);

    /**
     * @brief Convenience method to set the certificate from the contents of the
     * file specified by @a FileName
     *
     * If the @a FileName is invalid or the file cannot be opened, this function
     * does nothing.
     *
     * @param FileName The name of the file that contains the new certificate
     */
    void setCertificateFromFile(QStringView FileName);

    /**
     * @brief Get the certificate
     *
     * @return The certificate as a std::string
     */
    [[nodiscard]] std::string certificate() const;

    /**
     * @brief Set the key to @a Key
     *
     * @param Key The new key
     */
    void setKey(const std::string& Key);

    /**
     * @brief Convenience method to set the key from the contents of the
     * file specified by @a FileName
     *
     * If the @a FileName is invalid or the file cannot be opened, this function
     * does nothing.
     *
     * @param FileName The name of the file that contains the new key
     */
    void setKeyFromFile(QStringView FileName);

    /**
     * @brief Get the key
     *
     * @return The key as a std::string
     */
    [[nodiscard]] std::string key() const;

    /**
     * @brief Check whether @a RootCA, @a m_Certificate @em and @a m_Key are empty
     *
     * @returns true, if root CA, certificate and key are empty
     * @returns false, if any or all of them are set
     */
    [[nodiscard]] bool isEmpty() const;

    /**
     * @brief Convert these credentials to gRPC ServerCredentials
     *
     * @returns grpc::InsecureServerCredentials if @a m_Certificate or @a m_Key
     * members are empty
     * @returns according grpc::SslServerCredentials if the members are set
     */
    [[nodiscard]] std::shared_ptr<grpc::ServerCredentials> toServerCredentials()
        const;

    /**
     * @brief Convert these credentials to gRPC ChannelCredentials used in clients
     *
     * @returns grpc::InsecureChannelCredentials if @a m_RootCA is empty
     * @returns according grpc::SslChannelCredentials if @a m_RootCA is set
     */
    [[nodiscard]] std::shared_ptr<grpc::ChannelCredentials> toChannelCredentials()
        const;

    /**
     * @brief Read the files and construct a @b CSSLCertificate object from the
     * file contents
     *
     * @param CertificateFileName The name of the file that contains the SSL
     * certificate
     * @param KeyFileName The name of the file that contains the SSL key
     * @param RootCAFileName The name of the file that contains the root CA
     * certificate that was used to sign the certificate (can be left empty if the
     * certificate was not signed by a root CA)
     *
     * @return A @b CSSLCredentials object with @a m_Certificate, @a m_Key and
     * optionally @a m_RootCA read from the given files
     */
    [[nodiscard]] static CSSLCredentials fromFile(QStringView CertificateFileName,
                                                  QStringView KeyFileName,
                                                  QStringView RootCAFileName = {});

    /**
     * @brief Read the file and construct a @b CSSLCertificate object from the
     * file contents
     *
     * This will be only used by clients that connect to a server which has a
     * certificate that was signed using the file given by @a RootCAFileName. If
     * the server has self-signed certificates that weren't signed by a root CA
     * then use the server's certificate file name here.
     *
     * @param RootCAFileName The name of the file that contains the root CA
     * certificate that was used to sign the certificate
     *
     * @return A @b CSSLCredentials object with @a m_RootCA read from the given
     * file
     */
    [[nodiscard]] static CSSLCredentials fromFile(QStringView RootCAFileName);

    /**
     * @brief Create a private key and a self-signed certificate on the fly
     *
     * @return A @b CSSLCredentials object with the on the fly created key and
     * certificate
     */
    [[nodiscard]] static CSSLCredentials fromSelfSignedCertificate();

private:
    struct KeyCertPair
    {
        std::string Key;
        std::string Certificate;
    };

    /**
     * @brief Construct new SSL credentials from the given key-certificate pairs
     * and the optional root CA certificate that was used to sign the certificates
     *
     * @param KeyCertPairs The key-certificate pairs that should be used fro
     * encryption
     * @param RootCA The certificate authority that signed the certificates (can
     * be left empty in case of self-signed certificates that didn't use a CA)
     *
     * @note This is an internal method that is only used in the context of the
     * @c fromSelfSignedCertificate method to use multiple certificates with
     * different CNs for the different hostnames of the machine.
     */
    explicit CSSLCredentials(std::vector<KeyCertPair> KeyCertPairs,
                             std::string RootCA = "");

    std::string m_RootCA{};
    std::vector<KeyCertPair> m_KeyCertPairs{{}};
};
}  // namespace SiLA2

#endif  // SSLCREDENTIALS_H

/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   FeatureID.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   23.01.2020
/// \brief  Declaration of the CFeatureID class
//============================================================================
#ifndef FEATUREID_H
#define FEATUREID_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include <QString>

namespace SiLA2
{
/**
 * @brief The CFeatureID class represents a fully qualified feature identifier
 * as specified by the SiLA 2 standard
 */
class SILA_CPP_EXPORT CFeatureID : public QString
{
public:
    /**
     * @brief C'tor
     *
     * @param FeatureName The name of the feature as it can be retrieved by
     * calling @c MyFeature::service_full_name().
     */
    explicit CFeatureID(std::string_view FeatureName);

    /**
     * @brief Construct a CFeatureID from the given string @a from. This assumes
     * that @a from is already in the form of a Fully Qualified Identifier and
     * just needs to be converted to the correct type.
     *
     * @param value A Fully Qualified Feature Identifier as a std::string
     */
    static CFeatureID fromStdString(const std::string& value);

    /**
     * @brief Get the name of the SiLA Feature without any additional information
     * like the originator or the feature version.
     *
     * @return QString The Feature's name
     */
    [[nodiscard]] QString featureName() const;

private:
    /**
     * @internal
     * @brief Converting c'tor only for conversion from QString to CFeatureID
     *
     * @param value A fully qualified feature identifier as a QStringView
     */
    explicit CFeatureID(QStringView value);
};
}  // namespace SiLA2

#endif  // FEATUREID_H

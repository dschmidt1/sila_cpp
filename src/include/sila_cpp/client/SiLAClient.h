/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAClient.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   08.01.2020
/// \brief  Declaration of the CSiLAClient class
//============================================================================
#ifndef SILACLIENT_H
#define SILACLIENT_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/SSLCredentials.h>
#include <sila_cpp/data_types.h>
#include <sila_cpp/global.h>

#include <polymorphic_value/polymorphic_value.h>

//=============================================================================
//                            FORWARD DECLARATIONS
//=============================================================================
namespace grpc_impl
{
class Channel;
}  // namespace grpc_impl

namespace grpc
{
typedef grpc_impl::Channel Channel;
}  // namespace grpc

class QCommandLineParser;

namespace SiLA2
{
class CServerAddress;

/**
 * @brief The CSiLAClient class provides a base for implementing custom SiLA2
 * clients
 */
class SILA_CPP_EXPORT CSiLAClient
{
    class PrivateImpl;
    using PrivateImplPtr = jbcoe::polymorphic_value<CSiLAClient::PrivateImpl>;

public:
    /**
     * @brief C'tor
     *
     * @param Address The IP address and port of the server to connect to
     * These can be overwritten by passing command line arguments to SiLA client
     * applications.
     * @param Credentials Optional SSL certificate and key for secure
     * communication; empty credentials result in insecure communication only if
     * the server's certificate cannot be obtained (which usually means that the
     * server doesn't use encryption). If these are set @em and at the same time
     * credentials are given through command line arguments, then the latter take
     * precedence (see @a CSiLAServer::commandLineParser).
     */
    explicit CSiLAClient(const CServerAddress& Address,
                         const CSSLCredentials& Credentials = {},
                         PrivateImplPtr priv = {});

    /**
     * @brief Get the gRPC channel to the connected server
     *
     * @details This method can be used in a subclass to construct a new stub
     * for a SiLA feature, e.g.:
     * \code{.cpp}
     * auto GreetingProviderStub = GreetingProvider::NewStub(channel());
     * \endcode
     *
     * @return std::shared_ptr<grpc::Channel> The gRPC channel to the server
     */
    [[nodiscard]] std::shared_ptr<grpc::Channel> channel() const;

    /**
     * @brief Get the server's command line parser
     * This can be used to add additional command line options to the parser.
     * Per default, the server command line parser contains the following options:
     * @li -?, -h, --help
     * @li -v, --version
     * @li -a, --server-address <ip-address>
     * @li -n, --server-hostname <hostname>
     * @li -p, --server-port <port>
     * @li -r, --root-ca <filename>
     * @li -c, --encryption-cert <filename>
     * @li -k, --encryption-key <filename>
     *
     * @note CSiLAClient will parse these options automatically and overwrite the
     * options given in the c'tor if necessary. So, if you extend the parser with
     * your custom options you'll only have to parse those and not all of the
     * options.
     * @note If you don't set SSL credentials via command line arguments
     * @b CSiLAClient tries to obtain the server's certificate for secure
     * communication and if that fails, falls back to insecure communication.
     * However, if you specify credentials via both the command line @em and the
     * c'tor argument, then the values read from the command line will take
     * precedence!
     *
     * @return The client's command line parser
     *
     * @sa CSiLAClient::CSiLAClient
     */
    static std::shared_ptr<QCommandLineParser> commandLineParser();

    /**
     * @brief Call the Unobservable Command "Get Feature Definition" on the server
     *
     * @param FeatureIdentifier The fully qualified Feature Identifier for which
     * the FeatureDefinition should be retrieved
     *
     * @return CString The FeatureDefinition in XML format
     */
    CString GetFeatureDefinition(const CString& FeatureIdentifier);

    /**
     * @brief Call the Unobservable Command "Set Server Name" on the server
     *
     * @param ServerName The human readable name to assign to the SiLA Server
     */
    void SetServerName(const CString& ServerName);

    /**
     * @brief Request the Unobservable Property "Server Name" on the server
     */
    CString Get_ServerName();

    /**
     * @brief Request the Unobservable Property "Server Type" on the server
     */
    CString Get_ServerType();

    /**
     * @brief Request the Unobservable Property "Server UUID" on the server
     */
    CString Get_ServerUUID();

    /**
     * @brief Request the Unobservable Property "Server Description" on the server
     */
    CString Get_ServerDescription();

    /**
     * @brief Request the Unobservable Property "Server Version" on the server
     */
    CString Get_ServerVersion();

    /**
     * @brief Request the Unobservable Property "Server Vendor URL" on the server
     */
    CString Get_ServerVendorURL();

    /**
     * @brief Request the Unobservable Property "Implemented Features" on the
     * server
     */
    std::vector<CString> Get_ImplementedFeatures();

protected:
    PrivateImplPtr d_ptr;

private:
    PIMPL_DECLARE_PRIVATE(CSiLAClient)
};
}  // namespace SiLA2

#endif  // SILACLIENT_H

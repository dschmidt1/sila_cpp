/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   HostInfo.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   29.09.2020
/// \brief  Declaration of the CHostInfo class
//============================================================================
#ifndef HOSTINFO_H
#define HOSTINFO_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <QString>

namespace SiLA2::internal
{
/**
 * @brief The CHostInfo class is a utility class to construct the FQDN of the
 * localhost and lookup hosts by their IP address
 */
class CHostInfo
{
public:
    /**
     * @brief Look up the host name associated with IP address @a IP
     *
     * @param IP The IP address to look up
     *
     * @returns The host name that corresponds to the given IP address @a IP or an
     * empty string on error
     */
    static QString lookupHostname(const QString& IP);

    /**
     * @brief Get the FQDN of the local machine's host name
     *
     * @return QString The FQDN of the local machine's host name
     */
    static QString localHostName();
};
}  // namespace SiLA2::internal

#endif  // HOSTINFO_H

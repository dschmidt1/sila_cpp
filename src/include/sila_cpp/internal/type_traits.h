/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   type_utils.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   03.07.2020
/// \brief  Definition of helpful type trait checks that are missing in
/// <type_traits>
//============================================================================
#ifndef TYPE_TRAITS_H
#define TYPE_TRAITS_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <type_traits>

namespace SiLA2::internal
{
/**
 * @brief Helper macro to generate a type trait to check if a type has implemented
 * a particular binary operator overload
 *
 * E.g.:
 * @code
 * DEFINE_HAS_BINARY_OPERATOR(has_assignment_operator, =);
 * static_assert(SiLA2::internal::has_assignment_operator_v<Foo>,
 *               "Foo has assignment op");
 * @endcode
 */
#define DEFINE_HAS_BINARY_OPERATOR(trait_name, op)                               \
    namespace detail                                                             \
    {                                                                            \
        template<typename L, typename R>                                         \
        struct trait_name##_impl                                                 \
        {                                                                        \
            template<typename T = L, typename U = R>                             \
            static auto test(T&& t, U&& u)                                       \
                -> decltype(void(t op u), std::true_type{});                     \
            static auto test(...) -> std::false_type;                            \
            using type = decltype(test(std::declval<L>(), std::declval<R>()));   \
        };                                                                       \
    } /* namespace detail*/                                                      \
    template<typename L, typename R = L>                                         \
    struct trait_name : detail::trait_name##_impl<L, R>::type                    \
    {};                                                                          \
    /* helper template*/                                                         \
    template<typename L, typename R = L>                                         \
    inline constexpr bool trait_name##_v = trait_name<L, R>::value;

/**
 * @brief Helper macro to generate a type trait to check if a type has implemented
 * a particular unary operator overload
 *
 * E.g.:
 * @code
 * DEFINE_HAS_UNARY_OPERATOR(has_negate_operator, -);
 * static_assert(SiLA2::internal::has_negate_operator_v<Foo>,
 *               "Foo has negate op");
 * @endcode
 */
#define DEFINE_HAS_UNARY_OPERATOR(trait_name, op)                                \
    namespace detail                                                             \
    {                                                                            \
        template<typename L>                                                     \
        struct trait_name##_impl                                                 \
        {                                                                        \
            template<typename T = L>                                             \
            static auto test(T&& t) -> decltype(op t, void(), std::true_type{}); \
            static auto test(...) -> std::false_type;                            \
            using type = decltype(test(std::declval<L>()));                      \
        };                                                                       \
    } /* namespace detail*/                                                      \
    template<typename L>                                                         \
    struct trait_name : detail::trait_name##_impl<L>::type                       \
    {};                                                                          \
    /* helper template*/                                                         \
    template<typename L>                                                         \
    inline constexpr bool trait_name##_v = trait_name<L>::value;

DEFINE_HAS_BINARY_OPERATOR(has_assignment_operator, =);
DEFINE_HAS_BINARY_OPERATOR(has_add_operator, +);
DEFINE_HAS_BINARY_OPERATOR(has_subtract_operator, -);
DEFINE_HAS_BINARY_OPERATOR(has_multiply_operator, *);
DEFINE_HAS_BINARY_OPERATOR(has_divide_operator, /);
DEFINE_HAS_BINARY_OPERATOR(has_modulo_operator, %);
DEFINE_HAS_BINARY_OPERATOR(has_add_assignment_operator, +=);
DEFINE_HAS_BINARY_OPERATOR(has_subtract_assignment_operator, -=);
DEFINE_HAS_BINARY_OPERATOR(has_multiply_assignment_operator, *=);
DEFINE_HAS_BINARY_OPERATOR(has_divide_assignment_operator, /=);
DEFINE_HAS_BINARY_OPERATOR(has_modulo_assignment_operator, %=);
DEFINE_HAS_BINARY_OPERATOR(has_equal_operator, ==);
DEFINE_HAS_BINARY_OPERATOR(has_unequal_operator, !=);
DEFINE_HAS_BINARY_OPERATOR(has_less_operator, <);
DEFINE_HAS_BINARY_OPERATOR(has_greater_operator, >);
DEFINE_HAS_BINARY_OPERATOR(has_less_equal_operator, <=);
DEFINE_HAS_BINARY_OPERATOR(has_greater_equal_operator, >=);

#undef DEFINE_HAS_BINARY_OPERATOR
#undef DEFINE_HAS_UNARY_OPERATOR

/**
 * @brief Type trait to check if a type @a T is a container
 */
template<typename T, typename _ = void>
struct is_container : std::false_type
{};

template<typename T>
struct is_container<
    T,
    std::void_t<
        typename T::value_type, typename T::size_type, typename T::allocator_type,
        typename T::iterator, typename T::const_iterator,
        decltype(std::declval<T>().size()), decltype(std::declval<T>().begin()),
        decltype(std::declval<T>().end()), decltype(std::declval<T>().cbegin()),
        decltype(std::declval<T>().cend())>> : public std::true_type
{};

// helper template
template<typename T>
inline constexpr bool is_container_v = is_container<T>::value;

/**
 * @brief Type trait to check if a template parameter pack is empty
 */
template<typename...>
struct is_empty_pack : std::true_type
{};

template<typename T, typename... Ts>
struct is_empty_pack<T, Ts...> : std::false_type
{};

// helper template
template<typename... Ts>
inline constexpr bool is_empty_pack_v = is_empty_pack<Ts...>::value;

/**
 * @brief Type trait to get the first type in a template parameter pack
 */
template<typename...>
struct first_type
{
    using type = void;
};

template<typename T, typename... Ts>
struct first_type<T, Ts...>
{
    using type = T;
};

// helper template
template<typename... Ts>
using first_type_t = typename first_type<Ts...>::type;

}  // namespace SiLA2::internal

#endif  // TYPE_TRAITS_H

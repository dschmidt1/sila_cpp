/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2019 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   global.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.12.2019
/// \brief  Global definitions of the sila_cpp library.
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#ifndef SILA_CPP_GLOBAL_H
#define SILA_CPP_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(SILA_CPP_STATIC)
#  define SILA_CPP_EXPORT
#elif defined(SILA_CPP_LIBRARY)
#  define SILA_CPP_EXPORT Q_DECL_EXPORT
#else
#  define SILA_CPP_EXPORT Q_DECL_IMPORT
#endif

#define PIMPL_DECLARE_PRIVATE(Class)                                                                                   \
    inline Class::PrivateImpl* d_func() { return reinterpret_cast<Class::PrivateImpl*>(&(*d_ptr)); }                   \
    inline const Class::PrivateImpl* d_func() const { return reinterpret_cast<const Class::PrivateImpl*>(&(*d_ptr)); } \
    friend class Class::PrivateImpl;

#define PIMPL_DECLARE_PUBLIC(Class)                                                 \
    inline Class* q_func() { return static_cast<Class*>(q_ptr); }                   \
    inline const Class* q_func() const { return static_cast<const Class*>(q_ptr); } \
    friend class Class;

#define PIMPL_D(Class) Class::PrivateImpl* const d = d_func()
#define PIMPL_Q(Class) Class* const q = q_func()

#endif  // SILA_CPP_GLOBAL_H

/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   FrameworkError.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   29.01.2020
/// \brief  Declaration of the CFrameworkError class
//============================================================================
#ifndef FRAMEWORKERROR_H
#define FRAMEWORKERROR_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>
#include <sila_cpp/global.h>

#include "SiLAError.h"

#include <grpcpp/impl/codegen/status.h>

namespace SiLA2
{
/**
 * @brief The CFrameworkError class represents a SiLA 2 Framework Error.
 *
 * A Framework Error is an error which occurs when a SiLA Client accesses a
 * SiLA Server in a way that violates the SiLA 2 specification.
 */
class SILA_CPP_EXPORT CFrameworkError : public CSiLAError
{
    class PrivateImpl;

public:
    /**
     * @brief The ErrorType enum specifies the different types of SiLA Framework
     * Errors
     */
    enum ErrorType
    {
        COMMAND_EXECUTION_NOT_ACCEPTED = sila2::org::silastandard::
            FrameworkError_ErrorType_COMMAND_EXECUTION_NOT_ACCEPTED,
        INVALID_COMMAND_EXECUTION_UUID = sila2::org::silastandard::
            FrameworkError_ErrorType_INVALID_COMMAND_EXECUTION_UUID,
        COMMAND_EXECUTION_NOT_FINISHED = sila2::org::silastandard::
            FrameworkError_ErrorType_COMMAND_EXECUTION_NOT_FINISHED,
        INVALID_METADATA =
            sila2::org::silastandard::FrameworkError_ErrorType_INVALID_METADATA,
        NO_METADATA_ALLOWED =
            sila2::org::silastandard::FrameworkError_ErrorType_NO_METADATA_ALLOWED,
    };

    /**
     * @brief C'tor
     *
     * @param Type The error type of the Framework Error as defined in the
     * SiLA standard
     * @param Message The error message providing details about the occurred
     * error. If left empty, an extremely generic error message will be used.
     */
    explicit CFrameworkError(ErrorType Type, const std::string& Message = {});

    /**
     * @override
     *
     * @brief Raises the error to the underlying gRPC framework.
     */
    [[nodiscard]] grpc::Status raise() const override;

    /**
     * @brief Get a human readable name of the given Framework Error Type @a Type
     *
     * @param Type The Framework Error Type to convert into a string
     * @return const std::string& The human readable name of the Error Type
     */
    static const std::string& errorTypeName(ErrorType Type);

private:
    PIMPL_DECLARE_PRIVATE(CFrameworkError);
};
}  // namespace SiLA2

#endif  // FRAMEWORKERROR_H

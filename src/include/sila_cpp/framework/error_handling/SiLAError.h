/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAError.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   27.01.2020
/// \brief  Declaration of the CSiLAError class
//============================================================================
#ifndef SILAERROR_H
#define SILAERROR_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include <polymorphic_value/polymorphic_value.h>

#include <exception>
#include <string>

//=============================================================================
//                            FORWARD DECLARATIONS
//=============================================================================
namespace grpc
{
class Status;
}  // namespace grpc

namespace SiLA2
{
/**
 * @brief The CSiLAError class is an abstract base class for all SiLA error types
 */
class SILA_CPP_EXPORT CSiLAError : public std::exception
{
protected:
    class PrivateImpl;
    using PrivateImplPtr = jbcoe::polymorphic_value<CSiLAError::PrivateImpl>;

public:
    /**
     * @brief C'tor
     *
     * @param Message The error message providing details about the occurred
     * error. If left empty, an extremely generic error message will be used.
     */
    CSiLAError(const std::string& Message = {});

    /**
     * @brief Raises the SiLA error to the underlying gRPC framework. This method
     * has to be implemented in each subclass so that the appropriate SiLA Error
     * protobuf message can be constructed and sent along as details for the gRPC
     * error.
     *
     * @return grpc::Status The gRPC status code that corresponds to the
     * particular SiLA error.
     */
    [[nodiscard]] virtual grpc::Status raise() const = 0;

    /**
     * @override
     * @brief Get a string with explanatory information about the error.
     */
    [[nodiscard]] const char* what() const noexcept override;

protected:
    /**
     * @brief C'tor for derived classes
     *
     * @param priv Pointer to the derived private class
     */
    CSiLAError(PrivateImplPtr priv);

    PrivateImplPtr d_ptr;

private:
    PIMPL_DECLARE_PRIVATE(CSiLAError)
};
}  // namespace SiLA2

#endif  // SILAERROR_H

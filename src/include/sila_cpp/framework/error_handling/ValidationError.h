/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ValidationError.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   27.01.2020
/// \brief  Declaration of the CValidationError class
//============================================================================
#ifndef VALIDATIONERROR_H
#define VALIDATIONERROR_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include "SiLAError.h"

#include <grpcpp/impl/codegen/status.h>

namespace SiLA2
{
/**
 * @brief The CValidationError class represents a SiLA 2 Validation Error.
 *
 * A Validation Error is an error that occurs during the validation of Parameters
 * before executing a Command.
 */
class SILA_CPP_EXPORT CValidationError : public CSiLAError
{
    class PrivateImpl;

public:
    /**
     * @brief C'tor
     *
     * @param Parameter The parameter for which validation failed
     * @param Message The error message providing details about the occurred
     * error. If left empty, an extremely generic error message will be used.
     */
    explicit CValidationError(const std::string& Parameter,
                              const std::string& Message = {});

    /**
     * @override
     *
     * @brief Raises the error to the underlying gRPC framework.
     */
    [[nodiscard]] grpc::Status raise() const override;

private:
    PIMPL_DECLARE_PRIVATE(CValidationError);
};
}  // namespace SiLA2

#endif  // VALIDATIONERROR_H

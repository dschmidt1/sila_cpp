/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ExecutionError.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   09.07.2020
/// \brief  Declaration of the CExecutionError, CDefinedExecutionError,
/// CUndefinedExecutionError classes
//============================================================================
#ifndef EXECUTIONERROR_H
#define EXECUTIONERROR_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include "SiLAError.h"

#include <grpcpp/impl/codegen/status.h>

namespace SiLA2
{
/**
 * @brief The CExecutionError class represents a SiLA 2 Execution Error.
 *
 * There are two types of Execution Errors in SiLA2: Defined  and Undefined
 * Execution Errors. See the @a CDefinedExecutionError and @a
 * CUndefinedExecutionError classes for more details.
 */
class SILA_CPP_EXPORT CExecutionError : public CSiLAError
{
    class PrivateImpl;

public:
    /**
     * @brief Construct an @b Undefined Execution Error
     *
     * @param Message The error message providing details about the occurred
     * error.
     */
    explicit CExecutionError(const std::string& Message = "");

    /**
     * @brief Construct a @b Defined Execution Error
     *
     * @param Identifier The error identifier of the Execution Error
     * @param Message The error message providing details about the occurred
     * error.
     */
    explicit CExecutionError(const std::string& Identifier,
                             const std::string& Message);

    /**
     * @override
     *
     * @brief Raises the error to the underlying gRPC framework.
     */
    [[nodiscard]] grpc::Status raise() const override;

private:
    PIMPL_DECLARE_PRIVATE(CExecutionError);
};

/**
 * @brief The CDefinedExecutionError class represents a SiLA 2 Defined Execution
 * Error.
 *
 * A Defined Execution Error is an Execution Error that has been defined by the
 * Feature Designer as part of the Feature. It enables the SiLA Client to react
 * more specifically to an Execution Error, as the nature of the error as well
 * as possible recovery procedures are known in better detail.
 */
class SILA_CPP_EXPORT CDefinedExecutionError : public CExecutionError
{
public:
    /**
     * @brief C'tor
     *
     * @param Identifier The error identifier of the Execution Error
     * @param Message The error message providing details about the occurred
     * error. If left empty, an extremely generic error message will be used.
     */
    explicit CDefinedExecutionError(const std::string& Identifier,
                                    const std::string& Message = "");
};

/**
 * @brief The CUndefinedExecutionError class represents a SiLA 2 Undefined
 * Execution Error.
 *
 * Any Execution Error which is not a Defined Execution Error is an Undefined
 * Execution Error. These types of errors are implementation dependent and occur
 * unexpectedly and cannot be foreseen by the Feature Designer.
 */
class SILA_CPP_EXPORT CUndefinedExecutionError : public CExecutionError
{
public:
    /**
     * @brief C'tor
     *
     * @param Message The error message providing details about the occurred
     * error. If left empty, an extremely generic error message will be used.
     */
    explicit CUndefinedExecutionError(const std::string& Message = "");
};

}  // namespace SiLA2

#endif  // EXECUTIONERROR_H

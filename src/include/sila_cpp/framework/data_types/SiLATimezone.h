/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLATimezone.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.01.2020
/// \brief  Declaration of the CTimezone class
//============================================================================
#ifndef SILATIMEZONE_H
#define SILATIMEZONE_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include "DataType.h"
#include "utils.h"

#include <utility>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace sila2::org::silastandard
{
class Timezone;
}

namespace SiLA2
{
/**
 * @brief The CInvalidTimezone class is used to indicate that a particular SiLA
 * Timezone is invalid.
 *
 * @sa SiLA2::CTimezone
 */
class SILA_CPP_EXPORT CInvalidTimezone : public std::exception
{
public:
    /**
     * @brief The TimezonePart enum defines the different parts of a date that can
     * be invalid
     */
    enum TimezonePart
    {
        HOURS,
        MINUTES
    };

    /**
     * @brief Construct a @a CInvalidTimezone for the date specified by @a Hours
     * and @a Minutes
     */
    CInvalidTimezone(int32_t Hours, uint32_t Minutes);

    /**
     * @brief Construct a @a CInvalidTimezone for the part @a Part that has the
     * invalid value @a Value
     *
     * @param Value The invalid value
     * @param Part The part of the date that is invalid
     */
    CInvalidTimezone(int32_t Value, TimezonePart Part);

    /**
     * @override
     * @brief Get a string with explanatory information about the error.
     */
    [[nodiscard]] const char* what() const noexcept override;

private:
    QString m_Message{};
};

/**
 * @brief The CTimezone class provides a convenience wrapper around
 * SiLAFramework's Timezone class.
 *
 * @details A @b Timezone represents a signed, fixed-length span of time
 * represented as a count of hours and minutes as an offset from UTC.
 */
class SILA_CPP_EXPORT CTimezone : public CDataType<std::pair<int32_t, uint32_t>>
{
    using Base = CDataType<std::pair<int32_t, uint32_t>>;

public:
    /**
     * @brief C'tor
     *
     * @note The maximum offset from UTC is +12:00 and the minimum is -12:00
     *
     * @throws SiLA2::CInvalidTimezone if the timezone specified by @a Hours and
     * @a Minutes is invalid.
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CTimezone(int32_t Hours = 0, uint32_t Minutes = 0);

    SILA_CPP_CREATE_SPECIAL_MEMBER_FUNCTIONS(CTimezone, Base)

    /**
     * @brief Converting copy c'tor from sila2::org::silastandard::Timestamp
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CTimezone(const sila2::org::silastandard::Timezone& rhs);

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     */
    [[nodiscard]] sila2::org::silastandard::Timezone toProtoMessage() const;

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     * pointer
     */
    [[nodiscard]] sila2::org::silastandard::Timezone* toProtoMessagePtr() const;

    /**
     * @brief Get the hours
     *
     * @return The hours
     */
    [[nodiscard]] int32_t hours() const;

    /**
     * @brief Set the hours
     *
     * @param Hours The new hours
     *
     * @throws SiAL2::CInvalidTimezone if the resulting timezone would be invalid
     */
    void setHours(int32_t Hours);

    /**
     * @brief Get the minutes
     *
     * @return The minutes
     */
    [[nodiscard]] uint32_t minutes() const;

    /**
     * @brief Set the minutes
     *
     * @param Minutes The new minutes
     *
     * @throws SiAL2::CInvalidTimezone if the resulting timezone would be invalid
     */
    void setMinutes(uint32_t Minutes);
};
}  // namespace SiLA2

/**
 * @brief Overload for debugging CTimezones
 */
QDebug SILA_CPP_EXPORT operator<<(QDebug dbg, const SiLA2::CTimezone& rhs);

#endif  // SILATIMEZONE_H

/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   CommandExecutionUUID.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   05.05.2020
/// \brief  Declaration of the CCommandExecutionUUID class
//============================================================================
#ifndef COMMANDEXECUTIONUUID_H
#define COMMANDEXECUTIONUUID_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include "DataType.h"
#include "utils.h"

#include <QUuid>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace sila2::org::silastandard
{
class CommandExecutionUUID;
}

namespace SiLA2
{
/**
 * @brief The CCommandExecutionUUID class provides a convenience wrapper around
 * SiLAFramework's CommandExecutionUUID class.
 */
class SILA_CPP_EXPORT CCommandExecutionUUID : public CDataType<QUuid>
{
    using Base = CDataType<QUuid>;

public:
    /**
     * @brief C'tor
     *
     * @param Value UUID in the format "f81d4fae-7dec-11d0-a765-00a0c91e6bf6"
     * If the UUID given in @a from is in the wrong format, a null-UUID will
     * be constructed (i.e with the value "00000000-0000-0000-0000-000000000000").
     * If no value is given a random UUID is constructed
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CCommandExecutionUUID(const QUuid& value = QUuid::createUuid());

    SILA_CPP_CREATE_SPECIAL_MEMBER_FUNCTIONS(CCommandExecutionUUID, Base)

    /**
     * @brief Converting copy c'tor
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CCommandExecutionUUID(
        const sila2::org::silastandard::CommandExecutionUUID& rhs);

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     */
    [[nodiscard]] sila2::org::silastandard::CommandExecutionUUID toProtoMessage()
        const;

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     * pointer
     */
    [[nodiscard]] sila2::org::silastandard::CommandExecutionUUID*
    toProtoMessagePtr() const;

    /**
     * @brief Get the QString representation of this UUID. This will always use
     * @c QUuid::WithoutBraces
     *
     * @return The UUID as a QString
     */
    [[nodiscard]] QString toString() const;

    /**
     * @brief Get the std::string representation of this UUID.
     * Does the same as @a toString().
     *
     * @return The UUID as a std::string
     * @sa toString()
     */
    [[nodiscard]] std::string toStdString() const;
};

/**
 * @brief Hash function for usage of CCommandExecutionUUID as key type in QHash
 *
 * @param key The CCommandExecutionUUID object to hash
 * @param seed A random seed to seed the hashing
 *
 * @return The hash for @a key seeded with @a seed
 */
uint SILA_CPP_EXPORT qHash(const CCommandExecutionUUID& key,
                           uint seed = 0) noexcept;
}  // namespace SiLA2

/**
 * @brief Overload for debugging CCommandExecutionUUIDs
 */
QDebug SILA_CPP_EXPORT operator<<(QDebug dbg,
                                  const SiLA2::CCommandExecutionUUID& rhs);

#endif  // COMMANDEXECUTIONUUID_H

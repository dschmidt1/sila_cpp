/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLADate.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.01.2020
/// \brief  Declaration of the CDate class
//============================================================================
#ifndef SILADATE_H
#define SILADATE_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include "DataType.h"
#include "SiLATimezone.h"
#include "utils.h"

#include <exception>
#include <tuple>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace sila2::org::silastandard
{
class Date;
}

namespace SiLA2
{
/**
 * @brief The CInvalidDate class is used to indicate that a particular SiLA Date
 * is invalid.
 *
 * @sa SiLA2::CDate
 */
class SILA_CPP_EXPORT CInvalidDate : public std::exception
{
public:
    /**
     * @brief The DatePart enum defines the different parts of a date that can be
     * invalid
     */
    enum DatePart
    {
        DAY,
        MONTH,
        YEAR
    };

    /**
     * @brief Construct a @a CInvalidDate for the date specified by @a Day,
     * @a Month and @a Year
     */
    CInvalidDate(uint32_t Day, uint32_t Month, uint32_t Year);

    /**
     * @brief Construct a @a CInvalidDate for the part @a Part that has the
     * invalid value @a Value
     *
     * @param Value The invalid value
     * @param Part The part of the date that is invalid
     */
    CInvalidDate(uint32_t Value, DatePart Part);

    /**
     * @override
     * @brief Get a string with explanatory information about the error.
     */
    [[nodiscard]] const char* what() const noexcept override;

private:
    QString m_Message{};
};

/**
 * @brief The CDate class provides a convenience wrapper around
 * SiLAFramework's Date class.
 *
 * @details The <b>SiLA Date Type</b> represents a ISO 8601 date (year
 * [1-9999], month [1-12], day [1-31], timezone [as an offset from UTC]) in
 * the Gregorian calendar.
 */
class SILA_CPP_EXPORT CDate :
    public CDataType<std::tuple<uint32_t, uint32_t, uint32_t, CTimezone>>
{
    using Base = CDataType<std::tuple<uint32_t, uint32_t, uint32_t, CTimezone>>;

public:
    /**
     * @brief C'tor
     *
     * @throws SiLA2::CInvalidDate if the date specified by @a Day, @a Month and
     * @a Year is invalid.
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CDate(uint32_t Day = 1, uint32_t Month = 1, uint32_t Year = 1970,
          const CTimezone& Timezone = {});

    SILA_CPP_CREATE_SPECIAL_MEMBER_FUNCTIONS(CDate, Base)

    /**
     * @brief Converting copy c'tor from sila2::org::silastandard::Date
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CDate(const sila2::org::silastandard::Date& rhs);

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     */
    [[nodiscard]] sila2::org::silastandard::Date toProtoMessage() const;

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     * pointer
     */
    [[nodiscard]] sila2::org::silastandard::Date* toProtoMessagePtr() const;

    /**
     * @brief Get the day
     *
     * @return The day
     */
    [[nodiscard]] uint32_t day() const;

    /**
     * @brief Set the day
     *
     * @param Day The new day
     *
     * @throws SiAL2::CInvalidDate if the resulting date would be invalid
     */
    void setDay(uint32_t Day);

    /**
     * @brief Get the month
     *
     * @return The month
     */
    [[nodiscard]] uint32_t month() const;

    /**
     * @brief Set the month
     *
     * @param Month The new month
     *
     * @throws SiAL2::CInvalidDate if the resulting date would be invalid
     */
    void setMonth(uint32_t Month);

    /**
     * @brief Get the year
     *
     * @return The year
     */
    [[nodiscard]] uint32_t year() const;

    /**
     * @brief Set the year
     *
     * @param Year The new year
     *
     * @throws SiAL2::CInvalidDate if the resulting date would be invalid
     */
    void setYear(uint32_t Year);

    /**
     * @brief Get the timezone
     *
     * @return The timezone
     */
    [[nodiscard]] CTimezone timezone() const;

    /**
     * @brief Set the timezone
     *
     * @param Timezone The new timezone
     */
    void setTimezone(const CTimezone& Timezone);

    /**
     * @brief Check if the date given by @a Date, @a Month and @a Year is valid
     *
     * @return true, if the date is valid, false otherwise
     */
    static bool isValid(uint32_t Day, uint32_t Month, uint32_t Year);
};
}  // namespace SiLA2

/**
 * @brief Overload for debugging CDates
 */
QDebug SILA_CPP_EXPORT operator<<(QDebug dbg, const SiLA2::CDate& rhs);

#endif  // SILADATE_H

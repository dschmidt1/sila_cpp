/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ExecutionInfo.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   05.05.2020
/// \brief  Declaration of the CExecutionInfo class
//============================================================================
#ifndef EXECUTIONINFO_H
#define EXECUTIONINFO_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>
#include <sila_cpp/global.h>

#include "DataType.h"
#include "SiLADuration.h"
#include "SiLAReal.h"
#include "utils.h"

namespace SiLA2
{
/**
 * @brief The CommandStatus enum defines the different states a Observable
 * Command Execution can be in
 */
enum class CommandStatus
{
    WAITING = sila2::org::silastandard::ExecutionInfo_CommandStatus_waiting,
    RUNNING = sila2::org::silastandard::ExecutionInfo_CommandStatus_running,
    FINISHED_SUCCESSFULLY =
        sila2::org::silastandard::ExecutionInfo_CommandStatus_finishedSuccessfully,
    FINISHED_WITH_ERROR =
        sila2::org::silastandard::ExecutionInfo_CommandStatus_finishedWithError,
};

/**
 * @brief The CExecutionInfo class provides a convenience wrapper around
 * SiLAFramework's ExecutionInfo class.
 */
class SILA_CPP_EXPORT CExecutionInfo :
    public CDataType<std::tuple<CommandStatus, CReal, CDuration, CDuration>>
{
    using Base =
        CDataType<std::tuple<CommandStatus, CReal, CDuration, CDuration>>;

public:
    /**
     * @brief C'tor
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CExecutionInfo(CommandStatus Status = CommandStatus::WAITING,
                   const CReal& Progress = {},
                   const CDuration& RemainingTime = {},
                   const CDuration& UpdatedLifetime = {});

    /**
     * @brief Copy c'tor (for using this type with Qt MetaType)
     */
    CExecutionInfo(const CExecutionInfo& rhs);

    SILA_CPP_CREATE_SPECIAL_MEMBER_FUNCTIONS(CExecutionInfo, Base)

    /**
     * @brief D'tor (for using this type with Qt MetaType)
     */
    ~CExecutionInfo() override;

    /**
     * @brief Converting copy c'tor
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CExecutionInfo(const sila2::org::silastandard::ExecutionInfo& rhs);

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     */
    [[nodiscard]] sila2::org::silastandard::ExecutionInfo toProtoMessage() const;

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     * pointer
     */
    [[nodiscard]] sila2::org::silastandard::ExecutionInfo* toProtoMessagePtr()
        const;

    /**
     * @brief Get the Command Status
     *
     * @return The Command Status
     */
    [[nodiscard]] CommandStatus commandStatus() const;

    /**
     * @brief Set the Command Status
     *
     * @param Status The new Command Status
     */
    void setCommandStatus(CommandStatus Status);

    /**
     * @brief Get the Progress Info
     *
     * @return The Progress Info
     */
    [[nodiscard]] CReal progressInfo() const;

    /**
     * @brief Set the Progress Info
     *
     * @param Progress The new Progress Info
     */
    void setProgressInfo(const CReal& Progress);

    /**
     * @brief Get the Estimated Remaining Time
     *
     * @return The Estimated Remaining Time
     */
    [[nodiscard]] CDuration estimatedRemainingTime() const;

    /**
     * @brief Set the Estimated Remaining Time
     *
     * @param RemainingTime The new Estimated Remaining Time
     */
    void setEstimatedRemainingTime(const CDuration& RemainingTime);

    /**
     * @brief Get the Updated Lifetime of Execution
     *
     * @return The Updated Lifetime of Execution
     */
    [[nodiscard]] CDuration updatedLifetimeOfExecution() const;

    /**
     * @brief Set the Updated Lifetime of Execution
     *
     * @param Lifetime The new Updated Lifetime of Execution
     */
    void setUpdatedLifetimeOfExecution(const CDuration& Lifetime);
};
}  // namespace SiLA2

/**
 * @brief Equality comparison operator for SiLA2::CommandStatus and
 * SiLAFramework's CommandStatus
 *
 * @returns true if @a lhs is equal to @a rhs, false otherwise
 */
[[nodiscard]] bool SILA_CPP_EXPORT
operator==(SiLA2::CommandStatus lhs,
           sila2::org::silastandard::ExecutionInfo_CommandStatus rhs);

/**
 * @brief Inequality comparison operator for SiLA2::CommandStatus and
 * SiLAFramework's CommandStatus
 *
 * @returns true if @a lhs is not equal to @a rhs, false otherwise
 */
[[nodiscard]] bool SILA_CPP_EXPORT
operator!=(SiLA2::CommandStatus lhs,
           sila2::org::silastandard::ExecutionInfo_CommandStatus rhs);

/**
 * @brief Overload for debugging CommandStatus
 */
QDebug SILA_CPP_EXPORT operator<<(QDebug dbg, SiLA2::CommandStatus rhs);

/**
 * @brief Overload for debugging CExecutionInfo
 */
QDebug SILA_CPP_EXPORT operator<<(QDebug dbg, const SiLA2::CExecutionInfo& rhs);

#endif  // EXECUTIONINFO_H

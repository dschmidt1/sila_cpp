/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLABinary.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.01.2020
/// \brief  Declaration of the CBinary class
//============================================================================
#ifndef SILABINARY_H
#define SILABINARY_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include "DataType.h"
#include "utils.h"

#include <string_view>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace sila2::org::silastandard
{
class Binary;
}

namespace SiLA2
{
/**
 * @brief The CBinary class provides a convenience wrapper around SiLAFramework's
 * Binary class.
 *
 * @details The <b>SiLA Binary Type</b> represents arbitrary binary data of any
 * size such as images, office files, etc.
 */
class SILA_CPP_EXPORT CBinary : public CDataType<std::string>
{
    using Base = CDataType<std::string>;

public:
    /**
     * @brief C'tor
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CBinary(const std::string_view& Value = "");

    /**
     * @brief Converting copy c'tor from sila2::org::silastandard::Binary
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CBinary(const sila2::org::silastandard::Binary& rhs);

    SILA_CPP_CREATE_SPECIAL_MEMBER_FUNCTIONS(CBinary, Base)

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     */
    [[nodiscard]] sila2::org::silastandard::Binary toProtoMessage() const;

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     * pointer
     */
    [[nodiscard]] sila2::org::silastandard::Binary* toProtoMessagePtr() const;
};
}  // namespace SiLA2

/**
 * @brief Overload for debugging CBinaries
 */
QDebug SILA_CPP_EXPORT operator<<(QDebug dbg, const SiLA2::CBinary& rhs);

#endif  // SILABINARY_H

/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DataType.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   18.08.2020
/// \brief  Declaration of the CDataType class
//============================================================================
#ifndef DATATYPE_H
#define DATATYPE_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>
#include <sila_cpp/internal/type_traits.h>

namespace SiLA2
{
/**
 * @brief The CDataType class provides the general interface for SiLA Data Types
 */
template<typename T>
class CDataType
{
public:
    /**
     * @brief C'tor
     */
    explicit CDataType(const T& value = T{});

    /**
     * @brief Copy c'tor
     */
    CDataType(const CDataType& rhs);

    /**
     * @brief Move c'tor
     */
    CDataType(CDataType&& rhs) noexcept;

    /**
     * @brief D'tor
     */
    virtual ~CDataType() = default;

    /**
     * @brief Get the value
     *
     * @return T The value
     */
    [[nodiscard]] T value() const;

    /**
     * @brief Set the value
     *
     * @param value The new value
     */
    void setValue(const T& value);

    /**
     * @brief Copy assignment operator
     */
    CDataType& operator=(const CDataType& rhs);

    /**
     * @brief Assignment operator from the underlying type
     */
    CDataType& operator=(const T& rhs);

    /**
     * @brief Move assignment operator
     */
    CDataType& operator=(CDataType&& rhs) noexcept;

    /**
     * @brief Move assignment operator from the underlying type
     */
    CDataType& operator=(T&& rhs) noexcept;

    /**
     * @brief Conversion operator to convert to the underlying type
     * Does the same as @c value().
     *
     * @sa value()
     */
    [[nodiscard]] explicit operator T() const;

    /**
     * @brief Add operator
     *
     * @tparam U The underlying type or something convertible to it
     *
     * @param rhs The value to add
     */
    template<typename U>
    typename std::enable_if_t<internal::has_add_operator_v<T, U>, CDataType>
    operator+(const U& rhs) const;

    /**
     * @brief Subtract operator
     *
     * @tparam U The underlying type or something convertible to it
     *
     * @param rhs The value to subtract
     */
    template<typename U>
    typename std::enable_if_t<internal::has_subtract_operator_v<T, U>, CDataType>
    operator-(const U& rhs) const;

    /**
     * @brief Multiply operator
     *
     * @tparam U The underlying type or something convertible to it
     *
     * @param rhs The value to multiply with
     */
    template<typename U>
    typename std::enable_if_t<internal::has_multiply_operator_v<T, U>, CDataType>
    operator*(const U& rhs) const;

    /**
     * @brief Divide operator
     *
     * @tparam U The underlying type or something convertible to it
     *
     * @param rhs The value to divide by
     */
    template<typename U>
    typename std::enable_if_t<internal::has_divide_operator_v<T, U>, CDataType>
    operator/(const U& rhs) const;

    /**
     * @brief Modulo operator
     *
     * @tparam U The underlying type or something convertible to it
     *
     * @param rhs The value to modulo by
     */
    template<typename U>
    typename std::enable_if_t<internal::has_modulo_operator_v<T, U>, CDataType>
    operator%(const U& rhs) const;

    /**
     * @brief Add and assignment operator
     *
     * @tparam U The underlying type or something convertible to it
     *
     * @param rhs The value to add
     */
    template<typename U>
    typename std::enable_if_t<internal::has_add_operator_v<T, U>, CDataType&>
    operator+=(const U& rhs);

    /**
     * @brief Subtract and assignment operator
     *
     * @tparam U The underlying type or something convertible to it
     *
     * @param rhs The value to subtract
     */
    template<typename U>
    typename std::enable_if_t<internal::has_subtract_operator_v<T, U>, CDataType&>
    operator-=(const U& rhs);

    /**
     * @brief Multiply and assignment operator
     *
     * @tparam U The underlying type or something convertible to it
     *
     * @param rhs The value to multiply with
     */
    template<typename U>
    typename std::enable_if_t<internal::has_multiply_operator_v<T, U>, CDataType&>
    operator*=(const U& rhs);

    /**
     * @brief Divide and assignment operator
     *
     * @tparam U The underlying type or something convertible to it
     *
     * @param rhs The value to divide by
     */
    template<typename U>
    typename std::enable_if_t<internal::has_divide_operator_v<T, U>, CDataType&>
    operator/=(const U& rhs);

    /**
     * @brief Modulo and assignment operator
     *
     * @tparam U The underlying type or something convertible to it
     *
     * @param rhs The value to modulo by
     */
    template<typename U>
    typename std::enable_if_t<internal::has_modulo_operator_v<T, U>, CDataType&>
    operator%=(const U& rhs);

private:
    T m_Value;
};

//============================================================================
template<typename T>
CDataType<T>::CDataType(const T& value) : m_Value{value}
{}

//============================================================================
template<typename T>
CDataType<T>::CDataType(const CDataType<T>& rhs) : m_Value{rhs.m_Value}
{}

//============================================================================
template<typename T>
CDataType<T>::CDataType(CDataType<T>&& rhs) noexcept
    : m_Value{std::move(rhs.m_Value)}
{}

//============================================================================
template<typename T>
T CDataType<T>::value() const
{
    return m_Value;
}

//============================================================================
template<typename T>
void CDataType<T>::setValue(const T& value)
{
    m_Value = value;
}

//============================================================================
template<typename T>
CDataType<T>& CDataType<T>::operator=(const CDataType<T>& rhs)
{
    if (this != &rhs)
    {
        m_Value = rhs.m_Value;
    }
    return *this;
}

//============================================================================
template<typename T>
CDataType<T>& CDataType<T>::operator=(const T& rhs)
{
    m_Value = rhs;
    return *this;
}

//============================================================================
template<typename T>
CDataType<T>& CDataType<T>::operator=(CDataType<T>&& rhs) noexcept
{
    m_Value = std::move(rhs.m_Value);
    return *this;
}

//============================================================================
template<typename T>
CDataType<T>& CDataType<T>::operator=(T&& rhs) noexcept
{
    m_Value = std::move(rhs);
    return *this;
}

//============================================================================
template<typename T>
CDataType<T>::operator T() const
{
    return m_Value;
}

///===========================================================================
/// Operator overloads to make this Data Type behave like its underlying type
///===========================================================================
template<typename T>
template<typename U>
typename std::enable_if_t<internal::has_add_operator_v<T, U>, CDataType<T>>
CDataType<T>::operator+(const U& rhs) const
{
    auto res = *this;
    res.m_Value = m_Value + rhs;
    return res;
}

//============================================================================
template<typename T>
template<typename U>
typename std::enable_if_t<internal::has_subtract_operator_v<T, U>, CDataType<T>>
CDataType<T>::operator-(const U& rhs) const
{
    auto res = *this;
    res.m_Value = m_Value - rhs;
    return res;
}

//============================================================================
template<typename T>
template<typename U>
typename std::enable_if_t<internal::has_multiply_operator_v<T, U>, CDataType<T>>
CDataType<T>::operator*(const U& rhs) const
{
    auto res = *this;
    res.m_Value = m_Value * rhs;
    return res;
}

//============================================================================
template<typename T>
template<typename U>
typename std::enable_if_t<internal::has_divide_operator_v<T, U>, CDataType<T>>
CDataType<T>::operator/(const U& rhs) const
{
    auto res = *this;
    res.m_Value = m_Value / rhs;
    return res;
}

//============================================================================
template<typename T>
template<typename U>
typename std::enable_if_t<internal::has_modulo_operator_v<T, U>, CDataType<T>>
CDataType<T>::operator%(const U& rhs) const
{
    auto res = *this;
    res.m_Value = m_Value % rhs;
    return res;
}

//============================================================================
template<typename T>
template<typename U>
typename std::enable_if_t<internal::has_add_operator_v<T, U>, CDataType<T>&>
CDataType<T>::operator+=(const U& rhs)
{
    setValue(m_Value + rhs);
    return *this;
}

//============================================================================
template<typename T>
template<typename U>
typename std::enable_if_t<internal::has_subtract_operator_v<T, U>, CDataType<T>&>
CDataType<T>::operator-=(const U& rhs)
{
    setValue(m_Value - rhs);
    return *this;
}

//============================================================================
template<typename T>
template<typename U>
typename std::enable_if_t<internal::has_multiply_operator_v<T, U>, CDataType<T>&>
CDataType<T>::operator*=(const U& rhs)
{
    setValue(m_Value * rhs);
    return *this;
}

//============================================================================
template<typename T>
template<typename U>
typename std::enable_if_t<internal::has_divide_operator_v<T, U>, CDataType<T>&>
CDataType<T>::operator/=(const U& rhs)
{
    setValue(m_Value / rhs);
    return *this;
}

//============================================================================
template<typename T>
template<typename U>
typename std::enable_if_t<internal::has_modulo_operator_v<T, U>, CDataType<T>&>
CDataType<T>::operator%=(const U& rhs)
{
    setValue(m_Value % rhs);
    return *this;
}

//============================================================================
template<typename T>
typename std::enable_if_t<internal::has_add_operator_v<T>, T> operator+(
    const T& lhs, const CDataType<T>& rhs)
{
    return lhs + rhs.value();
}

//============================================================================
template<typename T>
typename std::enable_if_t<internal::has_subtract_operator_v<T>, T> operator-(
    const T& lhs, const CDataType<T>& rhs)
{
    return lhs - rhs.value();
}

//============================================================================
template<typename T>
typename std::enable_if_t<internal::has_multiply_operator_v<T>, T> operator*(
    const T& lhs, const CDataType<T>& rhs)
{
    return lhs * rhs.value();
}

//============================================================================
template<typename T>
typename std::enable_if_t<internal::has_divide_operator_v<T>, T> operator/(
    const T& lhs, const CDataType<T>& rhs)
{
    return lhs / rhs.value();
}

//============================================================================
template<typename T>
typename std::enable_if_t<internal::has_modulo_operator_v<T>, T> operator%(
    const T& lhs, const CDataType<T>& rhs)
{
    return lhs % rhs.value();
}

///===========================================================================
///                   Non-member logical operator overloads
///===========================================================================
template<typename T>
[[nodiscard]] typename std::enable_if_t<internal::has_equal_operator_v<T>, bool>
operator==(const CDataType<T>& lhs, const T& rhs)
{
    return lhs.value() == rhs;
}

//============================================================================
template<typename T>
[[nodiscard]] typename std::enable_if_t<internal::has_equal_operator_v<T>, bool>
operator==(const CDataType<T>& lhs, const CDataType<T>& rhs)
{
    return lhs.value() == rhs.value();
}

//============================================================================
template<typename T>
[[nodiscard]] typename std::enable_if_t<internal::has_unequal_operator_v<T>, bool>
operator!=(const CDataType<T>& lhs, const T& rhs)
{
    return lhs.value() != rhs;
}

//============================================================================
template<typename T>
[[nodiscard]] typename std::enable_if_t<internal::has_unequal_operator_v<T>, bool>
operator!=(const CDataType<T>& lhs, const CDataType<T>& rhs)
{
    return lhs.value() != rhs.value();
}

//============================================================================
template<typename T>
[[nodiscard]] typename std::enable_if_t<internal::has_less_operator_v<T>, bool>
operator<(const CDataType<T>& lhs, const T& rhs)
{
    return lhs.value() < rhs;
}

//============================================================================
template<typename T>
[[nodiscard]] typename std::enable_if_t<internal::has_less_operator_v<T>, bool>
operator<(const CDataType<T>& lhs, const CDataType<T>& rhs)
{
    return lhs.value() < rhs.value();
}

//============================================================================
template<typename T>
[[nodiscard]] typename std::enable_if_t<internal::has_greater_operator_v<T>, bool>
operator>(const CDataType<T>& lhs, const T& rhs)
{
    return lhs.value() > rhs;
}

//============================================================================
template<typename T>
[[nodiscard]] typename std::enable_if_t<internal::has_greater_operator_v<T>, bool>
operator>(const CDataType<T>& lhs, const CDataType<T>& rhs)
{
    return lhs.value() > rhs.value();
}

//============================================================================
template<typename T>
[[nodiscard]]
typename std::enable_if_t<internal::has_less_equal_operator_v<T>, bool>
operator<=(const CDataType<T>& lhs, const T& rhs)
{
    return lhs.value() <= rhs;
}

//============================================================================
template<typename T>
[[nodiscard]]
typename std::enable_if_t<internal::has_less_equal_operator_v<T>, bool>
operator<=(const CDataType<T>& lhs, const CDataType<T>& rhs)
{
    return lhs.value() <= rhs.value();
}

//============================================================================
template<typename T>
[[nodiscard]]
typename std::enable_if_t<internal::has_greater_equal_operator_v<T>, bool>
operator>=(const CDataType<T>& lhs, const T& rhs)
{
    return lhs.value() >= rhs;
}

//============================================================================
template<typename T>
[[nodiscard]]
typename std::enable_if_t<internal::has_greater_equal_operator_v<T>, bool>
operator>=(const CDataType<T>& lhs, const CDataType<T>& rhs)
{
    return lhs.value() >= rhs.value();
}

}  // namespace SiLA2

#endif  // DATATYPE_H

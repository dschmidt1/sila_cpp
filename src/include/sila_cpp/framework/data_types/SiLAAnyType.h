/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAAnyType.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.01.2020
/// \brief  Declaration of the CAnyType class
//============================================================================
#ifndef SILAANYTYPE_H
#define SILAANYTYPE_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include "DataType.h"
#include "utils.h"

#include <string>
#include <utility>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace sila2::org::silastandard
{
class Any;
}

namespace SiLA2
{
/**
 * @brief The CAnyType class provides a convenience wrapper around SiLAFramework's
 * AnyType class.
 *
 * @details The <b>SiLA Any Type</b> represents information that can be of any
 * <i>SiLA Data Type</i>, except for a <i>Custom Data Type</i>. The value of a
 * <b>SiLA Any Type</b> contains both the information itself and the <i> SiLA
 * Data Type</i>.
 */
class SILA_CPP_EXPORT CAnyType :
    public CDataType<std::pair<std::string, std::string>>
{
    using Base = CDataType<std::pair<std::string, std::string>>;

public:
    /**
     * @brief C'tor
     */
    CAnyType(const std::string& Type, const std::string& Payload);

    SILA_CPP_CREATE_SPECIAL_MEMBER_FUNCTIONS(CAnyType, Base)
};
}  // namespace SiLA2

/**
 * @brief Overload for debugging CAnyTypes
 */
QDebug SILA_CPP_EXPORT operator<<(QDebug dbg, const SiLA2::CAnyType& rhs);

#endif  // SILAANYTYPE_H

/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLATime.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.01.2020
/// \brief  Declaration of the CTime class
//============================================================================
#ifndef SILATIME_H
#define SILATIME_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include "DataType.h"
#include "SiLATimezone.h"
#include "utils.h"

#include <tuple>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace sila2::org::silastandard
{
class Time;
}

namespace SiLA2
{
/**
 * @brief The CInvalidTime class is used to indicate that a particular SiLA Time
 * is invalid.
 *
 * @sa SiLA2::CTime
 */
class SILA_CPP_EXPORT CInvalidTime : public std::exception
{
public:
    /**
     * @brief The TimePart enum defines the different parts of a time that can be
     * invalid
     */
    enum TimePart
    {
        HOUR,
        MINUTE,
        SECOND
    };

    /**
     * @brief Construct a @a CInvalidTime for the time specified by @a Hour,
     * @a Minute and @a Second
     */
    CInvalidTime(uint32_t Hour, uint32_t Minute, uint32_t Second);

    /**
     * @brief Construct a @a CInvalidTime for the part @a Part that has the
     * invalid value @a Value
     *
     * @param Value The invalid value
     * @param Part The part of the time that is invalid
     */
    CInvalidTime(uint32_t Value, TimePart Part);

    /**
     * @override
     * @brief Get a string with explanatory information about the error.
     */
    [[nodiscard]] const char* what() const noexcept override;

private:
    QString m_Message{};
};

/**
 * @brief The CTime class provides a convenience wrapper around SiLAFramework's
 * Time class.
 *
 * @details The <b>SiLA Time Type</b> represents a ISO 8601 time (hours [0-23},
 * minutes [0-59], seconds [0-59], timezone [as an offset from UTC]).
 */
class SILA_CPP_EXPORT CTime :
    public CDataType<std::tuple<uint32_t, uint32_t, uint32_t, CTimezone>>
{
    using Base = CDataType<std::tuple<uint32_t, uint32_t, uint32_t, CTimezone>>;

public:
    /**
     * @brief C'tor
     *
     * @throws SiLA2::CInvalidTime if the time specified by @a Hour, @a Minute and
     * @a Second is invalid.
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CTime(uint32_t Hour = 0, uint32_t Minute = 0, uint32_t Second = 0,
          const CTimezone& Timezone = {});

    /**
     * @brief Converting copy c'tor from sila2::org::silastandard::Time
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CTime(const sila2::org::silastandard::Time& rhs);

    SILA_CPP_CREATE_SPECIAL_MEMBER_FUNCTIONS(CTime, Base)

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     */
    [[nodiscard]] sila2::org::silastandard::Time toProtoMessage() const;

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     * pointer
     */
    [[nodiscard]] sila2::org::silastandard::Time* toProtoMessagePtr() const;

    /**
     * @brief Get the hour
     *
     * @return The hour
     */
    [[nodiscard]] uint32_t hour() const;

    /**
     * @brief Set the hour
     *
     * @param Hour The new hour
     *
     * @throws SiAL2::CInvalidTime if the resulting time would be invalid
     */
    void setHour(uint32_t Hour);

    /**
     * @brief Get the minute
     *
     * @return The minute
     */
    [[nodiscard]] uint32_t minute() const;

    /**
     * @brief Set the minute
     *
     * @param Minute The new minute
     *
     * @throws SiAL2::CInvalidTime if the resulting time would be invalid
     */
    void setMinute(uint32_t Minute);

    /**
     * @brief Get the second
     *
     * @return The second
     */
    [[nodiscard]] uint32_t second() const;

    /**
     * @brief Set the second
     *
     * @param Second The new second
     *
     * @throws SiAL2::CInvalidTime if the resulting time would be invalid
     */
    void setSecond(uint32_t Second);

    /**
     * @brief Get the timezone
     *
     * @return The timezone
     */
    [[nodiscard]] CTimezone timezone() const;

    /**
     * @brief Set the timezone
     *
     * @param Timezone The new timezone
     */
    void setTimezone(const CTimezone& Timezone);

    /**
     * @brief Check if the time given by @a Hour, @a Minute and @a Second is valid
     *
     * @return true, if the date is valid, false otherwise
     */
    static bool isValid(uint32_t Hour, uint32_t Minute, uint32_t Second);
};
}  // namespace SiLA2

/**
 * @brief Overload for debugging CTimes
 */
QDebug SILA_CPP_EXPORT operator<<(QDebug dbg, const SiLA2::CTime& rhs);

#endif  // SILATIME_H

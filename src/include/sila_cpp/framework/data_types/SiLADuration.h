/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLADuration.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   12.05.2020
/// \brief  Declaration of the CDuration class
//============================================================================
#ifndef SILADURATION_H
#define SILADURATION_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include "DataType.h"
#include "utils.h"

#include <chrono>
#include <utility>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace sila2::org::silastandard
{
class Duration;
}

namespace SiLA2
{
/**
 * @brief The CDuration class provides a convenience wrapper around
 * SiLAFramework's Duration class.
 *
 * @details A @b Duration represents a signed, fixed-length span of time
 * represented as a count of seconds an fractions of a second at nanosecond
 * resolution. It is independent of any calendar and concepts like "day" or
 * "month".
 */
class SILA_CPP_EXPORT CDuration : public CDataType<std::pair<uint64_t, uint32_t>>
{
    using Base = CDataType<std::pair<uint64_t, uint32_t>>;

public:
    /**
     * @brief C'tor
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CDuration(uint64_t Seconds = 0, uint32_t Nanos = 0);

    SILA_CPP_CREATE_SPECIAL_MEMBER_FUNCTIONS(CDuration, Base)

    /**
     * @brief Converting copy c'tor
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CDuration(const sila2::org::silastandard::Duration& rhs);

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     */
    [[nodiscard]] sila2::org::silastandard::Duration toProtoMessage() const;

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     * pointer
     */
    [[nodiscard]] sila2::org::silastandard::Duration* toProtoMessagePtr() const;

    /**
     * @brief Get the seconds
     *
     * @return The seconds
     */
    [[nodiscard]] uint64_t seconds() const;

    /**
     * @brief Set the seconds
     *
     * @param Seconds The new value for the seconds
     */
    void setSeconds(uint64_t Seconds);

    /**
     * @brief Get the nanos
     *
     * @return The nanos
     */
    [[nodiscard]] uint32_t nanos() const;

    /**
     * @brief Set the nanos
     *
     * @param Nanos The new value for the nanos
     */
    void setNanos(uint32_t Nanos);

    /**
     * @brief Checks whether both seconds and nanos are 0
     *
     * @returns true, if seconds and nanos are 0, false otherwise
     */
    [[nodiscard]] bool isNull() const;

    /**
     * @brief Convert this Duration to milliseconds, e.g. for using the value in a
     * timer
     *
     * @return The number of milliseconds equivalent to this Duration
     */
    [[nodiscard]] std::chrono::milliseconds toMilliSeconds() const;
};
}  // namespace SiLA2

/**
 * @brief Overload for debugging CDurations
 */
QDebug SILA_CPP_EXPORT operator<<(QDebug dbg, const SiLA2::CDuration& rhs);

#endif  // SILADURATION_H

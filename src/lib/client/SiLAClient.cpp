/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAClient.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   08.01.2020
/// \brief  Implementation of the CSiLAClient class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/SiLAClient.h>
#include <sila_cpp/common/ServerAddress.h>
#include <sila_cpp/common/ServerInformation.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/error_handling/ClientError.h>
#include <sila_cpp/internal/HostInfo.h>

#include "SiLAService.grpc.pb.h"

#include <QCommandLineParser>
#include <QSslSocket>

#include <grpcpp/grpcpp.h>

using namespace std;
using namespace jbcoe;
using namespace sila2::org::silastandard::core::silaservice::v1;

namespace SiLA2
{
/**
 * @brief Private data of the CSiLAClient class - pimpl
 */
class CSiLAClient::PrivateImpl
{
public:
    /**
     * @brief C'tor
     *
     * Tries to get the server's certificate and connect to the server
     */
    explicit PrivateImpl(CServerAddress Address, CSSLCredentials Creds);

    /**
     * @brief Parses the command line arguments (if possible) for the server's
     * address/port/hostname as well as potential SSL credentials and updates the
     * credentials given in @b CSiLAClient's ctor accordingly. If no root CA was
     * given in neither the c'tor nor on the command line this function tries to
     * obtain the certificate from the server.
     *
     * @sa CSiLAClient::PrivateImpl::getServerCertificate
     */
    void parseCommandLine();

    /**
     * @brief Tries to get the certificate of the server at @a ServerAddress
     *
     * @return CSSLCredentials The credentials containing the server's certificate
     * if it could be obtained, or empty credentials otherwise
     */
    [[nodiscard]] CSSLCredentials getServerCertificate() const;

    /**
     * @brief Connect to the server at @a ServerAddress using @a Credentials (if
     * non-empty) for secure communication
     *
     * If @a Credentials is not empty this function first tries to use secure
     * communication. If that fails or @a Credentials is empty, falls back to
     * insecure communication. Aborts the program if no communication can be
     * established at all.
     */
    void connectToServer();

    CServerInformation ServerInfo{};    ///< server info such as name, type, ...
    CServerAddress ServerAddress{};     ///< The server's address
    CSSLCredentials Credentials{};      ///< for secure communication
    shared_ptr<grpc::Channel> Channel;  ///< channel to connect to the server
    shared_ptr<SiLAService::Stub> SiLAServiceStub;  ///< SiLAService feature stub

    static shared_ptr<QCommandLineParser>
        Parser;  ///< Parser with options that are shared between all SiLA Clients
};

/**
 * @brief Setup the command line parser
 */
shared_ptr<QCommandLineParser> CSiLAClient::PrivateImpl::Parser = []() {
    auto Parser = make_shared<QCommandLineParser>();
    // default description if none is provided by the user
    Parser->setApplicationDescription("A SiLA2 client");
    Parser->addHelpOption();
    Parser->addVersionOption();
    static const auto DefaultAddress = CServerAddress{};
    Parser->addOptions({{{"s", "server-host"},
                         "The IP address or hostname of the SiLA server to "
                         "connect with ",
                         "ip-or-hostname",
                         DefaultAddress.ip()},
                        {{"p", "server-port"},
                         "The port on which the SiLA server runs",
                         "port",
                         DefaultAddress.port()},
                        {{"r", "root-ca"},
                         "Root certificate authority that signed the "
                         "\e[3mserver's\e[0m certificate. If the server has a "
                         "self-signed certificate that wasn't signed by a CA "
                         "then use the server's certificate here.",
                         "filename"},
                        {{"c", "encryption-cert"},
                         "SSL certificate filename, e.g. "
                         "'client-ssl.crt' (Note that the client doesn't "
                         "necessarily require a certificate.)",
                         "filename"},
                        {{"k", "encryption-key"},
                         "SSL key filename, e.g. 'client-ssl.key' (Note that the "
                         "client doesn't necessarily require a key.)",
                         "filename"}});
    return Parser;
}();

//============================================================================
CSiLAClient::PrivateImpl::PrivateImpl(CServerAddress Address,
                                      CSSLCredentials Creds)
    : ServerAddress{move(Address)}, Credentials{move(Creds)}
{
    logging::setupLogging();

    parseCommandLine();

    connectToServer();

    SiLAServiceStub = SiLAService::NewStub(Channel);
}

//============================================================================
void CSiLAClient::PrivateImpl::parseCommandLine()
{
    if (qApp && !QCoreApplication::arguments().empty())
    {
        Parser->process(QCoreApplication::arguments());

        // Server Address / Hostname
        if (Parser->isSet("server-host"))
        {
            ServerAddress.setIP(Parser->value("server-host"));
        }
        if (Parser->isSet("server-port"))
        {
            ServerAddress.setPort(Parser->value("server-port"));
        }

        // SSL Certificate
        if (Parser->isSet("root-ca"))
        {
            Credentials.setRootCAFromFile(Parser->value("root-ca"));
        }
        if (Parser->isSet("encryption-cert"))
        {
            Credentials.setCertificateFromFile(Parser->value("encryption-cert"));
        }
        if (Parser->isSet("encryption-key"))
        {
            Credentials.setKeyFromFile(Parser->value("encryption-key"));
        }
    }
    // couldn't get root CA from c'tor argument nor the command line
    if (Credentials.rootCA().empty())
    {
        Credentials = getServerCertificate();
    }
}

//============================================================================
CSSLCredentials CSiLAClient::PrivateImpl::getServerCertificate() const
{
    qCDebug(sila_cpp) << "Trying to get the server's SSL certificate...";

    QSslSocket Socket;
    QObject::connect(&Socket, &QSslSocket::connected,
                     []() { qCDebug(sila_cpp) << "Connected to SSL socket"; });
    QObject::connect(&Socket, &QSslSocket::encrypted,
                     []() { qCDebug(sila_cpp) << "SSL handshake successful"; });
    QObject::connect(
        &Socket, qOverload<const QList<QSslError>&>(&QSslSocket::sslErrors),
        [this](const auto& Errors) {
            if (Errors.constFirst().error() == QSslError::HostNameMismatch)
            {
                // Hostname mismatch is a common "error" we can fix by using
                // SetSslTargetNameOverride for the grpc::ChannelArgument below
                qCDebug(sila_cpp)
                    << "SSL error: The hostname" << ServerAddress.ip()
                    << "does not match any of the hostnames in the server "
                       "certificate";
            }
            else
            {
                // If there's anything else wrong with the certificate gRPC won't
                // like that. But it would still connect to the server using
                // insecure credentials and don't give us any indication that
                // there is something wrong - other than spitting out all kinds of
                // weird errors on the command line. So, we exit here with the
                // error message from QSslSocket to give the user at least some
                // kind of valuable help.
                qCCritical(sila_cpp) << "SSL error:" << Errors.constFirst();
                exit(1);
            }
        });

    Socket.setPeerVerifyMode(QSslSocket::VerifyNone);
    Socket.connectToHostEncrypted(ServerAddress.ip(),
                                  ServerAddress.port().toInt());
    Socket.waitForEncrypted();
    return CSSLCredentials{Socket.peerCertificate().toPem().toStdString()};
}

//============================================================================
void CSiLAClient::PrivateImpl::connectToServer()
{
    if (!Credentials.rootCA().empty())
    {
        qCDebug(sila_cpp) << "Trying to connect to server using secure "
                             "communication...";
        auto ChannelArgs = grpc::ChannelArguments{};
        // reverse-lookup the hostname for the given IP
        using internal::CHostInfo;
        if (const auto Name = CHostInfo::lookupHostname(ServerAddress.ip());
            !Name.isEmpty())
        {
            ChannelArgs.SetSslTargetNameOverride(Name.toStdString());
        }
        Channel = grpc::CreateCustomChannel(ServerAddress.toStdString(),
                                            Credentials.toChannelCredentials(),
                                            ChannelArgs);

        if (Channel->WaitForConnected(gpr_time_from_seconds(1, GPR_TIMESPAN)))
        {
            qCInfo(sila_cpp) << "Successfully connected to SiLA server at"
                             << ServerAddress << "using secure communication";
            return;
        }
    }

    qCInfo(sila_cpp) << "Could not connect to server with secure communication! "
                        "Falling back to insecure communication...";
    Channel = grpc::CreateChannel(ServerAddress.toStdString(),
                                  grpc::InsecureChannelCredentials());
    if (Channel->WaitForConnected(gpr_time_from_seconds(1, GPR_TIMESPAN)))
    {
        qCInfo(sila_cpp) << "Successfully connected to SiLA server at"
                         << ServerAddress << "using insecure communication";
    }
    else
    {
        qFatal("Could not connect to SiLA server at %s! Is the server running?",
               qPrintable(ServerAddress.toString()));
    }
}

//=============================================================================
CSiLAClient::CSiLAClient(const CServerAddress& Address,
                         const CSSLCredentials& Credentials, PrivateImplPtr priv)
    : d_ptr(priv ? priv :
                   make_polymorphic_value<PrivateImpl>(Address, Credentials))
{
    PIMPL_D(CSiLAClient);
    d->ServerInfo.setServerName(Get_ServerName().value());
    d->ServerInfo.setVersion(Get_ServerVersion().value());
    d->ServerInfo.setDescription(Get_ServerDescription().value());
    d->ServerInfo.setServerUUID(Get_ServerUUID().value());

    qCInfo(sila_cpp) << "Connected to SiLA Server" << d->ServerInfo.serverName()
                     << "(UUID:" << d->ServerInfo.serverUUID().toString() << "\b)"
                     << "running in version" << d->ServerInfo.version() << "\n"
                     << "Service description:" << d->ServerInfo.description();
    Get_ImplementedFeatures();
}

//=============================================================================
shared_ptr<grpc::Channel> CSiLAClient::channel() const
{
    PIMPL_D(const CSiLAClient);
    return d->Channel;
}

//============================================================================
shared_ptr<QCommandLineParser> CSiLAClient::commandLineParser()
{
    return PrivateImpl::Parser;
}

//=============================================================================
CString CSiLAClient::GetFeatureDefinition(const CString& FeatureIdentifier)
{
    PIMPL_D(CSiLAClient);
    GetFeatureDefinition_Parameters Parameter;
    Parameter.set_allocated_featureidentifier(
        FeatureIdentifier.toProtoMessagePtr());
    grpc::ClientContext Context;
    static GetFeatureDefinition_Responses Response;

    qCDebug(sila_cpp) << "--- Calling Unobservable Command "
                         "GetFeatureDefinition";
    const auto Status =
        d->SiLAServiceStub->GetFeatureDefinition(&Context, Parameter, &Response);
    if (!SiLA2::hasError(Status))
    {
        qCDebug(sila_cpp) << "GetFeatureDefinition response:" << Response;
    }

    return Response.featuredefinition();
}

//=============================================================================
void CSiLAClient::SetServerName(const CString& ServerName)
{
    PIMPL_D(CSiLAClient);
    SetServerName_Parameters Parameter;
    Parameter.set_allocated_servername(ServerName.toProtoMessagePtr());
    grpc::ClientContext Context;
    static SetServerName_Responses Response;

    qCDebug(sila_cpp) << "--- Calling Unobservable Command SetServerName";
    const auto Status =
        d->SiLAServiceStub->SetServerName(&Context, Parameter, &Response);
    if (!SiLA2::hasError(Status))
    {
        qCDebug(sila_cpp) << "SetServerName response:" << Response;
    }
}

//=============================================================================
CString CSiLAClient::Get_ServerType()
{
    PIMPL_D(CSiLAClient);
    grpc::ClientContext Context;
    static Get_ServerType_Responses Response;

    qCDebug(sila_cpp) << "--- Requesting Unobservable Property ServerType";
    const auto Status =
        d->SiLAServiceStub->Get_ServerType(&Context, {}, &Response);
    if (!SiLA2::hasError(Status))
    {
        qCDebug(sila_cpp) << "ServerType response:" << Response;
    }

    return Response.servertype();
}

//=============================================================================
CString CSiLAClient::Get_ServerUUID()
{
    PIMPL_D(CSiLAClient);
    grpc::ClientContext Context;
    static Get_ServerUUID_Responses Response;

    qCDebug(sila_cpp) << "--- Requesting Unobservable Property ServerUUID";
    const auto Status =
        d->SiLAServiceStub->Get_ServerUUID(&Context, {}, &Response);
    if (!SiLA2::hasError(Status))
    {
        qCDebug(sila_cpp) << "ServerUUID response:" << Response;
    }

    return Response.serveruuid();
}

//=============================================================================
CString CSiLAClient::Get_ServerDescription()
{
    PIMPL_D(CSiLAClient);
    grpc::ClientContext Context;
    static Get_ServerDescription_Responses Response;

    qCDebug(sila_cpp) << "--- Requesting Unobservable Property "
                         "ServerDescription";
    const auto Status =
        d->SiLAServiceStub->Get_ServerDescription(&Context, {}, &Response);
    if (!SiLA2::hasError(Status))
    {
        qCDebug(sila_cpp) << "ServerDescription response:" << Response;
    }

    return Response.serverdescription();
}

//=============================================================================
CString CSiLAClient::Get_ServerVersion()
{
    PIMPL_D(CSiLAClient);
    grpc::ClientContext Context;
    static Get_ServerVersion_Responses Response;

    qCDebug(sila_cpp) << "--- Requesting Unobservable Property ServerVersion";
    const auto Status =
        d->SiLAServiceStub->Get_ServerVersion(&Context, {}, &Response);
    if (!SiLA2::hasError(Status))
    {
        qCDebug(sila_cpp) << "ServerVersion response:" << Response;
    }

    return Response.serverversion();
}

//=============================================================================
CString CSiLAClient::Get_ServerVendorURL()
{
    PIMPL_D(CSiLAClient);
    grpc::ClientContext Context;
    static Get_ServerVendorURL_Responses Response;

    qCDebug(sila_cpp) << "--- Requesting Unobservable Property "
                         "ServerVendorURL";
    const auto Status =
        d->SiLAServiceStub->Get_ServerVendorURL(&Context, {}, &Response);
    if (!SiLA2::hasError(Status))
    {
        qCDebug(sila_cpp) << "ServerVendorURL response:" << Response;
    }

    return Response.servervendorurl();
}

//=============================================================================
CString CSiLAClient::Get_ServerName()
{
    PIMPL_D(CSiLAClient);
    grpc::ClientContext Context;
    static Get_ServerName_Responses Response;

    qCDebug(sila_cpp) << "--- Requesting Unobservable Property ServerName";
    const auto Status =
        d->SiLAServiceStub->Get_ServerName(&Context, {}, &Response);
    if (!SiLA2::hasError(Status))
    {
        qCDebug(sila_cpp) << "ServerName response:" << Response;
    }

    return Response.servername();
}

//=============================================================================
vector<CString> CSiLAClient::Get_ImplementedFeatures()
{
    PIMPL_D(CSiLAClient);
    grpc::ClientContext Context;
    static Get_ImplementedFeatures_Responses Response;

    qCDebug(sila_cpp) << "--- Requesting Unobservable Property "
                         "ImplementedFeatures";
    const auto Status =
        d->SiLAServiceStub->Get_ImplementedFeatures(&Context, {}, &Response);
    if (!SiLA2::hasError(Status))
    {
        qCDebug(sila_cpp) << "ImplementedFeatures response:" << Response;
    }

    auto Result = vector<CString>{};
    for_each(begin(Response.implementedfeatures()),
             end(Response.implementedfeatures()),
             [&Result](const auto& el) { Result.emplace_back(el); });

    return Result;
}
}  // namespace SiLA2

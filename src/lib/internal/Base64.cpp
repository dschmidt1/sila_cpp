/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   Base64.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   13.07.2020
/// \brief  Definition of helper functions for working with Base64 De-/Encoding
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/internal/Base64.h>

#include <QByteArray>

namespace SiLA2::internal
{
//============================================================================
std::string base64Decode(const std::string& Encoded)
{
    return QByteArray::fromBase64(QByteArray::fromStdString(Encoded))
        .toStdString();
}

//============================================================================
std::string base64Encode(const std::string& Raw)
{
    return QByteArray::fromStdString(Raw).toBase64().toStdString();
}
}  // namespace SiLA2::internal

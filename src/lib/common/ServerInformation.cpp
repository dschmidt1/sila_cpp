/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ServerInformation.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   08.01.2020
/// \brief  Implementation of the CServerInformation class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/ServerInformation.h>

#include <QDebug>

#include <utility>

using namespace std;

namespace SiLA2
{
//============================================================================
CServerInformation::CServerInformation(QString ServerName, QString ServerType,
                                       QString Description, QString Version,
                                       QString VendorURL, const QUuid& ServerUUID)
    : m_ServerName{move(ServerName)},
      m_ServerType{move(ServerType)},
      m_Description{move(Description)},
      m_Version{move(Version)},
      m_VendorURL{move(VendorURL)},
      m_ServerUUID{ServerUUID}
{}

//=============================================================================
void CServerInformation::setServerName(const QString& Name)
{
    m_ServerName = Name;
}

//=============================================================================
void CServerInformation::setServerName(string_view Name)
{
    setServerName(QString{(Name.data())});
}

//=============================================================================
const QString& CServerInformation::serverName() const
{
    return m_ServerName;
}

//=============================================================================
void CServerInformation::setServerType(const QString& Type)
{
    m_ServerType = Type;
}

//=============================================================================
void CServerInformation::setServerType(string_view Type)
{
    setServerType(QString{(Type.data())});
}

//=============================================================================
const QString& CServerInformation::serverType() const
{
    return m_ServerType;
}

//=============================================================================
void CServerInformation::setDescription(const QString& Description)
{
    m_Description = Description;
}

//=============================================================================
void CServerInformation::setDescription(string_view Description)
{
    setDescription(QString{(Description.data())});
}

//=============================================================================
const QString& CServerInformation::description() const
{
    return m_Description;
}

//=============================================================================
void CServerInformation::setVersion(const QString& Version)
{
    m_Version = Version;
}

//=============================================================================
void CServerInformation::setVersion(string_view Version)
{
    setVersion(QString{(Version.data())});
}

//=============================================================================
const QString& CServerInformation::version() const
{
    return m_Version;
}

//=============================================================================
void CServerInformation::setVendorURL(const QString& VendorURL)
{
    m_VendorURL = VendorURL;
}

//=============================================================================
void CServerInformation::setVendorURL(string_view VendorURL)
{
    setVendorURL(QString{(VendorURL.data())});
}

//=============================================================================
const QString& CServerInformation::vendorURL() const
{
    return m_VendorURL;
}

//=============================================================================
void CServerInformation::setServerUUID(const QUuid& UUID)
{
    m_ServerUUID = UUID;
}

//=============================================================================
void CServerInformation::setServerUUID(string_view UUID)
{
    setServerUUID(QUuid::fromString(QLatin1String{(UUID.data())}));
}

//=============================================================================
const QUuid& CServerInformation::serverUUID() const
{
    return m_ServerUUID;
}

}  // namespace SiLA2

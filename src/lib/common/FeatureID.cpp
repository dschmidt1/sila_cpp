/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   FeatureID.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   23.01.2020
/// \brief  Implementation of the CFeatureID class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/FeatureID.h>

#include <QDebug>
#include <QStringList>

using namespace std;

namespace SiLA2
{
static QString fromServiceName(string_view ServiceName)
{
    // Getting:
    // sila2.org.silastandard.examples.greetingprovider.v1.GreetingProvider
    // Goal:
    // org.silastandard/examples/GreetingProvider/v1
    // Current:
    // org.silastandard.examples.GreetingProvider.v1

    auto List = QString{(ServiceName.data())}.split('.');
    // remove leading "sila2"
    List.pop_front();
    // swap service name in all lowercase with service name in CamelCase
    // TODO: replace with swapItemsAt() for Qt 5.13
    List.swap(List.length() - 3, List.length() - 1);
    // remove trailing service name in all lowercase
    List.pop_back();
    // NOTE: This is not exactly a fully qualified feature identifier as specified
    //  in the standard because it only contains dots ('.') as separators. This is
    //  due to the fact that we don't have any way of knowing where the originator
    //  ends and where the category begins and ends. If we knew, we could then use
    //  slashes to join originator, category, feature identifier and version
    //  together. But for now this is the closest we get to a fully qualified
    //  feature identifier.
    return List.join('.');
}

//=============================================================================
CFeatureID::CFeatureID(string_view FeatureName)
    : QString{fromServiceName(FeatureName)}
{}

//=============================================================================
CFeatureID CFeatureID::fromStdString(const std::string& from)
{
    return CFeatureID{QString::fromStdString(from)};
}

//=============================================================================
QString CFeatureID::featureName() const
{
    // Feature name is the second to last element, i.e. the next item when
    // starting at the end (= reverse begin)
    return *(++(split('.').crbegin()));
}

//=============================================================================
CFeatureID::CFeatureID(QStringView value) : QString{value.data()}
{}

}  // namespace SiLA2

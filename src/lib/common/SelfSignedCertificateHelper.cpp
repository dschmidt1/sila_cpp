/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SelfSignedCertificateHelper.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   23.09.2020
/// \brief  Implementation of helper functions for creation of a self-signed
/// certificate and key
/// Most of this was adapted from https://gist.github.com/nathan-osman/5041136 and
/// https://stackoverflow.com/a/57478849/12780516
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include "SelfSignedCertificateHelper.h"

#include <sila_cpp/common/logging.h>

#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/x509.h>

#include <iostream>

using namespace std;

namespace SiLA2
{
using bignum_unique_ptr = unique_ptr<BIGNUM, void (*)(BIGNUM*)>;
using bn_gencb_unique_ptr = unique_ptr<BN_GENCB, void (*)(BN_GENCB*)>;
using bio_unique_ptr = unique_ptr<BIO, int (*)(BIO*)>;

/**
 * @brief Helper function to add a subject entry to an X509 certificate
 *
 * @param Name The subject name of the certificate for which the entry should be
 * added
 * @param FieldID The name of the object that should be added to the subject
 * @param Value The value to set for the object identified by @a Field
 */
void addX509SubjectEntry(X509_NAME* Name, const char* FieldID, const char* Value)
{
    X509_NAME_add_entry_by_txt(Name, FieldID, MBSTRING_ASC,
                               reinterpret_cast<const uchar*>(Value), -1, -1, 0);
}

static int callback(int p, int n, BN_GENCB* cb)
{
    if (sila_cpp().isInfoEnabled())
    {
        if (p == 0)
        {
            cout << '.';
        }
        else if (p == 1)
        {
            cout << '+';
        }
        else if (p == 2)
        {
            cout << '*';
        }
        else if (p == 3)
        {
            cout << '\n';
        }
        else
        {
            cout << 'B';
        }
    }
    return 1;
}

//============================================================================
COpenSSLError::COpenSSLError(const std::string& Description)
    : runtime_error{Description
                    + "\nOpenSSL: " + ERR_error_string(ERR_get_error(), nullptr)}
{}

//============================================================================
evp_pkey_unique_ptr generateKey()
{
    qCDebug(sila_cpp) << "Generating RSA private key";
    // 1. Allocate the key structure
    evp_pkey_unique_ptr Key{EVP_PKEY_new(), EVP_PKEY_free};
    if (!Key)
    {
        throw COpenSSLError{"Could not allocate EVP_PKEY structure"};
    }

    // 2. Create the RSA key
    auto BigNum = bignum_unique_ptr{BN_new(), BN_free};
    BN_set_word(BigNum.get(), RSA_F4);
    auto RSA = RSA_new();
    auto cb = bn_gencb_unique_ptr{BN_GENCB_new(), BN_GENCB_free};
    BN_GENCB_set(cb.get(), callback, nullptr);
    RSA_generate_key_ex(RSA, 4096, BigNum.get(), cb.get());
    cout << endl;

    // 3. Assign the RSA key to the Key
    if (!EVP_PKEY_assign_RSA(Key.get(), RSA))
    {
        throw COpenSSLError{"Could not generate RSA private key"};
    }

    return Key;
}

//============================================================================
x509_unique_ptr generateCertificate(const evp_pkey_unique_ptr& Key,
                                    const string& Hostname)
{
    qCDebug(sila_cpp) << "Generating X509 certificate for host" << Hostname;

    // 1. Allocate the x509 structure
    x509_unique_ptr Cert{X509_new(), X509_free};
    if (!Cert)
    {
        throw COpenSSLError{"Could not allocate X509 structure"};
    }

    // 2. Set necessary attributes
    // 2.1 Serial number
    ASN1_INTEGER_set(X509_get_serialNumber(Cert.get()), 1);
    // 2.2 Valid days
    // beginning now
    X509_time_adj_ex(X509_getm_notBefore(Cert.get()), 0, 0, nullptr);
    // ending a year from now
    X509_time_adj_ex(X509_getm_notAfter(Cert.get()), 365, 0, nullptr);
    // 2.3 the public key
    X509_set_pubkey(Cert.get(), Key.get());
    // 2.4 construct the issuer name from the subject name adding country code,
    //     organization, location and common name
    auto IssuerName = X509_get_subject_name(Cert.get());
    addX509SubjectEntry(IssuerName, "C", "CH");
    addX509SubjectEntry(IssuerName, "O", "SiLA Standard");
    addX509SubjectEntry(IssuerName, "L", "BS");
    addX509SubjectEntry(IssuerName, "ST", "Basel");
    addX509SubjectEntry(IssuerName, "CN", Hostname.c_str());
    X509_set_issuer_name(Cert.get(), IssuerName);

    // 3. sign the certificate with our key
    if (!X509_sign(Cert.get(), Key.get(), EVP_sha256()))
    {
        throw COpenSSLError{"Could not sign the certificate"};
    }

    return Cert;
}

//============================================================================
string keyToString(const evp_pkey_unique_ptr& Key)
{
    auto BIOBuffer = bio_unique_ptr{BIO_new(BIO_s_mem()), BIO_free};
    if (!PEM_write_bio_PrivateKey(BIOBuffer.get(), Key.get(), nullptr, nullptr, 0,
                                  nullptr, nullptr))
    {
        throw COpenSSLError{"Could not convert private key"};
    }

    const char* Buffer;
    BIO_get_mem_data(BIOBuffer.get(), &Buffer);

    return string{Buffer};
}

//============================================================================
string certificateToString(const x509_unique_ptr& Certificate)
{
    auto BIOBuffer = bio_unique_ptr{BIO_new(BIO_s_mem()), BIO_free};
    if (!PEM_write_bio_X509(BIOBuffer.get(), Certificate.get()))
    {
        throw COpenSSLError{"Could not convert certificate"};
    }

    const char* Buffer;
    BIO_get_mem_data(BIOBuffer.get(), &Buffer);

    return string{Buffer};
}
}  // namespace SiLA2

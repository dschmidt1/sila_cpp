/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ServerAddress.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   08.01.2020
/// \brief  Implementation of the CServerAddress class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/ServerAddress.h>

#include <QString>

#include <utility>

using namespace std;

namespace SiLA2
{
//=============================================================================
CServerAddress::CServerAddress(QString IPAddress, QString Port)
    : m_IPAddress{move(IPAddress)}, m_Port{move(Port)}
{}

//============================================================================
CServerAddress::CServerAddress(const std::string& IPAddress,
                               const std::string& Port)
    : m_IPAddress{QString::fromStdString(IPAddress)},
      m_Port{QString::fromStdString(Port)}
{}

//=============================================================================
QString CServerAddress::toString() const
{
    return m_IPAddress + ":" + m_Port;
}

//=============================================================================
string CServerAddress::toStdString() const
{
    return toString().toStdString();
}

//============================================================================
QString CServerAddress::prettyString() const
{
    const auto PrettyAddress = m_IPAddress == "[::]" ? "localhost" : m_IPAddress;
    return QString{"port %1 on %2"}.arg(m_Port).arg(PrettyAddress);
}

//============================================================================
QString CServerAddress::ip() const
{
    return m_IPAddress;
}

//============================================================================
void CServerAddress::setIP(const QString& IP)
{
    m_IPAddress = IP;
}

//============================================================================
QString CServerAddress::port() const
{
    return m_Port;
}

//============================================================================
void CServerAddress::setPort(const QString& Port)
{
    m_Port = Port;
}

//============================================================================
bool CServerAddress::isLocalhost() const
{
    return m_IPAddress == "localhost" || m_IPAddress == "127.0.0.1";
}
}  // namespace SiLA2

//=============================================================================
QDebug operator<<(QDebug debug, const SiLA2::CServerAddress& rhs)
{
    QDebugStateSaver s{debug};
    return debug.nospace().noquote() << rhs.toString();
}

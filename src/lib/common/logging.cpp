/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   logging.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   09.01.2020
/// \brief  Implementation of the Clogging class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>

#include <QProcessEnvironment>
#include <QThread>

#include <iostream>
#ifdef Q_OS_WIN
#    include <windows.h>
#    ifndef ENABLE_VIRTUAL_TERMINAL_PROCESSING
#        define ENABLE_VIRTUAL_TERMINAL_PROCESSING 0x0004
#    endif
#    ifndef DISABLE_NEWLINE_AUTO_RETURN
#        define DISABLE_NEWLINE_AUTO_RETURN 0x0008
#    endif
#endif  // Q_OS_WIN

#include <QMutex>
#include <QTime>

#include <google/protobuf/message.h>
#include <grpcpp/impl/codegen/status.h>

Q_LOGGING_CATEGORY(sila_cpp, "sila_cpp")

namespace SiLA2::logging
{
#ifdef Q_OS_WIN
/**
 * @brief Enable the virtual terminal (VT) output mode of the Windows console
 * window
 * @todo[FM] Better react on errors
 */
void enableWinConsoleVTMode()
{
    HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
    if (hOut == INVALID_HANDLE_VALUE)
    {
        return;
    }

    DWORD dwOriginalOutMode = 0;
    if (!GetConsoleMode(hOut, &dwOriginalOutMode))
    {
        return;
    }

    DWORD dwRequestedOutModes = ENABLE_VIRTUAL_TERMINAL_PROCESSING
                                | DISABLE_NEWLINE_AUTO_RETURN;

    DWORD dwOutMode = dwOriginalOutMode | dwRequestedOutModes;
    if (!SetConsoleMode(hOut, dwOutMode))
    {
        // we failed to set both modes, try to step down mode gracefully.
        dwRequestedOutModes = ENABLE_VIRTUAL_TERMINAL_PROCESSING;
        dwOutMode = dwOriginalOutMode | dwRequestedOutModes;
        if (!SetConsoleMode(hOut, dwOutMode))
        {
            // Failed to set any VT mode, can't do anything here.
            return;
        }
    }
}
#endif  // Q_OS_WIN

/**
 * @brief A custom message handler for formatting console output.
 */
void messageHandler(QtMsgType Type, const QMessageLogContext& Context,
                    const QString& Msg)
{
#ifdef Q_OS_WIN
    enableWinConsoleVTMode();
#endif  // Q_OS_WIN

    static QMutex Mutex;
    QMutexLocker Lock{&Mutex};
    auto Time = QTime::currentTime().toString(Qt::ISODateWithMs);

    switch (Type)
    {
    case QtDebugMsg:
        fprintf(stdout, "\e[90;1m(%#lx) %s: [%s] DEBUG: %s \n\e[2m(%s:%u)\e[0m\n",
                QThread::currentThread(), qPrintable(Time), Context.category,
                qPrintable(Msg), Context.file, Context.line);
        break;

    case QtInfoMsg:
        fprintf(stdout, "\e[1m(%#lx) %s: [%s] INFO: %s \n\e[2m(%s:%u)\e[0m\n",
                QThread::currentThread(), qPrintable(Time), Context.category,
                qPrintable(Msg), Context.file, Context.line);
        break;

    case QtWarningMsg:
        fprintf(stderr,
                "\e[1;33m(%#lx) %s: [%s] WARNING: %s \n\e[2m(%s:%u)\e[0m\n",
                QThread::currentThread(), qPrintable(Time), Context.category,
                qPrintable(Msg), Context.file, Context.line);
        break;

    case QtCriticalMsg:
        fprintf(stderr,
                "\e[1;31m(%#lx) %s: [%s] CRITICAL: %s \n\e[2m(%s:%u)\e[0m\n",
                QThread::currentThread(), qPrintable(Time), Context.category,
                qPrintable(Msg), Context.file, Context.line);
        break;

    case QtFatalMsg:
        fprintf(stderr, "\e[1;31m(%#lx) %s: [%s] FATAL: %s \n\e[2m(%s:%u)\e[0m\n",
                QThread::currentThread(), qPrintable(Time), Context.category,
                qPrintable(Msg), Context.file, Context.line);
        fflush(stderr);
        abort();
    }

    fflush(stderr);
    fflush(stdout);
}

//============================================================================
void setupLogging()
{
    qInstallMessageHandler(messageHandler);
    setLoggingLevel();
}

//============================================================================
void setLoggingLevel(const QString& DefaultLevel)
{
    const auto Level =
        qEnvironmentVariable("SILA_CPP_LOGGING_LEVEL", DefaultLevel).toLower();
    auto Rules = QString{};
    const auto appendRule = [&Rules](const QString& Level,
                                     const QString& Enabled) {
        Rules.append(QString{"%1.%2=%3\n"}
                         .arg(sila_cpp().categoryName())
                         .arg(Level)
                         .arg(Enabled));
    };
    if (sila_cpp().isDebugEnabled()
        && (Level == "info" || Level == "warning" || Level == "critical"
            || Level == "fatal"))
    {
        appendRule("debug", "false");
    }
    if (sila_cpp().isInfoEnabled()
        && (Level == "warning" || Level == "critical" || Level == "fatal"))
    {
        appendRule("info", "false");
    }
    if (sila_cpp().isWarningEnabled()
        && (Level == "critical" || Level == "fatal"))
    {
        appendRule("warning", "false");
    }
    if (sila_cpp().isCriticalEnabled() && Level == "fatal")
    {
        appendRule("critical", "false");
    }
    if (!Rules.isEmpty())
    {
        // prevent clearing the rules in case they've been set before
        QLoggingCategory::setFilterRules(Rules);
    }
}
}  // namespace SiLA2::logging

//=============================================================================
QDebug operator<<(QDebug dbg, const std::string& rhs)
{
    if (rhs.empty())
    {
        return dbg;
    }
    return dbg << rhs.c_str();
}

//=============================================================================
QDebug operator<<(QDebug dbg, const grpc::Status& rhs)
{
    return dbg << "\n\tStatus Code:" << rhs.error_code()
               << "\n\tError Message:" << rhs.error_message();
}

//=============================================================================
QDebug operator<<(QDebug dbg, const google::protobuf::Message& rhs)
{
    auto s = QDebugStateSaver{dbg};
    dbg.nospace() << "\n\t" << rhs.GetDescriptor()->name() << " {\n\t  ";
    const auto MsgString = rhs.DebugString();

    return dbg.noquote()
           << QString::fromStdString(MsgString).replace("\n", "\n\t  ")
           << "\b\b}";
}

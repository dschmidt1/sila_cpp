/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   Status.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   08.07.2020
/// \brief  Implementation of the CStatus class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/Status.h>
#include <sila_cpp/common/logging.h>

#include <QDebugStateSaver>

#include <utility>

namespace SiLA2
{
//============================================================================
const CStatus& CStatus::OK = CStatus{};

//============================================================================
CStatus::CStatus() noexcept : m_Status{grpc::Status::OK}
{}

//============================================================================
CStatus CStatus::fromSiLAError(const CSiLAError& Error)
{
    return CStatus{Error.raise()};
}

//============================================================================
CStatus::CStatus(grpc::Status Status) : m_Status{std::move(Status)}
{}

//============================================================================
bool CStatus::ok() const
{
    return m_Status.ok();
}

//============================================================================
CStatus::operator grpc::Status() const
{
    return m_Status;
}

//============================================================================
QDebug& operator<<(QDebug dbg, const CStatus& rhs)
{
    auto s = QDebugStateSaver{dbg};
    return dbg.nospace()
           << "SiLA2::CStatus(" << (rhs.m_Status.ok() ? "OK" : "ERROR") << ")";
}

}  // namespace SiLA2

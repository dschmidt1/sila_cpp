/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAServer.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   06.01.2020
/// \brief  Implementation of the CSiLAServer class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/FeatureID.h>
#include <sila_cpp/common/ServerInformation.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/data_types.h>
#include <sila_cpp/server/SiLAServer.h>

#include "../framework/features/org/silastandard/core/SiLAService/SiLAService.h"
#include "InterruptSignalHandler.h"

#include <QCommandLineParser>
#include <QCoreApplication>
#include <QSettings>

#include <grpcpp/grpcpp.h>

#include <QtZeroConf/qzeroconf.h>

#include <utility>
#include <vector>

static const auto SERVER_UUID_SETTINGS_KEY = QStringLiteral("ServerUUID");

using namespace std;
using namespace jbcoe;

namespace SiLA2
{
/**
 * @brief Private data of the CSiLAServer class - pimpl
 */
class CSiLAServer::PrivateImpl
{
public:
    /**
     * @brief C'tor
     */
    PrivateImpl(CSiLAServer* parent, CServerInformation Info,
                CServerAddress Address, CSSLCredentials Creds, bool ForceEnc);

    /**
     * @brief Parses the command line for SSL credentials and updates the
     * credentials given in @b CSiLAServer's ctor accordingly. If no credentials
     * were given in neither the c'tor nor on the command line this function tries
     * to create them on the fly from a self-signed certificate.
     */
    void parseSSLCredentials();

    /**
     * @brief Set the @a ServerUUID field of @a ServerInfo to the UUID stored in
     * the @a ServerSettings
     */
    void setUUIDFromSettings();

    /**
     * @brief Create and register the SiLA Service Feature
     */
    void registerSiLAService();

    /**
     * @brief Start publishing a ZeroConf service for this SiLA Server
     */
    void startZeroconfPublish();

    CSiLAServer* q_ptr;

    const CServerAddress ServerAddress{};  ///< The server's address
    CServerInformation ServerInfo{};       ///< The server's info (name, type,...)
    CSSLCredentials Credentials{};         ///< for secure communication
    bool ForceEncryption{false};  ///< don't start server if credentials are empty
    shared_ptr<QSettings>
        ServerSettings{};  ///< The server's settings (not copyable)
    shared_ptr<grpc::ServerBuilder>
        ServerBuilder{};              ///< gRPC server builder (not copyable)
    shared_ptr<grpc::Server> Server;  ///< The actual gRPC server
    vector<shared_ptr<grpc::ServerCompletionQueue>>
        CompletionQueues{};  ///< For async communication (shared with CAsyncRPCHandlers)
    shared_ptr<CSiLAService> SiLAService{};  ///< Mandatory SiLAService feature
    shared_ptr<QZeroConf> ZeroConf{};        ///< ZeroConf for SiLA Discovery

    static shared_ptr<QCommandLineParser>
        Parser;  ///< Parser with options that are shared between all SiLA Servers

    PIMPL_DECLARE_PUBLIC(CSiLAServer);
};

/**
 * @brief Setup the command line parser
 */
shared_ptr<QCommandLineParser> CSiLAServer::PrivateImpl::Parser = []() {
    auto Parser = make_shared<QCommandLineParser>();
    // default description if none is provided by the user
    Parser->setApplicationDescription("A SiLA2 service");
    Parser->addHelpOption();
    Parser->addVersionOption();
    Parser->addOptions({{{"r", "root-ca"},
                         "Root certificate authority that signed the "
                         "certificate. Can be left empty if the certificate "
                         "wasn't signed by a CA.",
                         "filename"},
                        {{"c", "encryption-cert"},
                         "SSL certificate filename, e.g. 'server-ssl.crt'",
                         "filename"},
                        {{"k", "encryption-key"},
                         "SSL key filename, e.g. 'server-ssl.key'",
                         "filename"},
                        {{"e", "force-encryption"},
                         "Prevents falling back to insecure communication and "
                         "fails if no secure communication can be used"}});
    return Parser;
}();

//============================================================================
CSiLAServer::PrivateImpl::PrivateImpl(CSiLAServer* parent,
                                      CServerInformation Info,
                                      CServerAddress Address,
                                      CSSLCredentials Creds, bool ForceEnc)
    : q_ptr{parent},
      ServerAddress{move(Address)},
      ServerInfo{move(Info)},
      Credentials{move(Creds)},
      ForceEncryption{ForceEnc},
      ServerSettings{make_shared<QSettings>("SiLA2", "sila_cpp", parent)},
      ServerBuilder{make_shared<grpc::ServerBuilder>()},
      ZeroConf{make_shared<QZeroConf>(parent)}
{
    logging::setupLogging();

    parseSSLCredentials();

    if (Credentials.isEmpty() && ForceEncryption)
    {
        qFatal(
            "Won't start the server because no SSL credentials have been "
            "provided but you forced the use of encryption!");
    }

    ServerBuilder->AddListeningPort(ServerAddress.toStdString(),
                                    Credentials.toServerCredentials());
}

//============================================================================
void CSiLAServer::PrivateImpl::parseSSLCredentials()
{
    if (qApp && !QCoreApplication::arguments().empty())
    {
        Parser->process(QCoreApplication::arguments());

        ForceEncryption = Parser->isSet("force-encryption");

        const auto RootCA = Parser->value("root-ca");
        const auto Certificate = Parser->value("encryption-cert");
        const auto Key = Parser->value("encryption-key");
        if (!RootCA.isEmpty())
        {
            Credentials.setRootCAFromFile(RootCA);
        }
        if (!Certificate.isEmpty())
        {
            Credentials.setCertificateFromFile(Certificate);
        }
        if (!Key.isEmpty())
        {
            Credentials.setKeyFromFile(Key);
        }
    }
    // couldn't get credentials from c'tor argument nor the command line
    if (Credentials.isEmpty())
    {
        Credentials = CSSLCredentials::fromSelfSignedCertificate();
    }
}

//============================================================================
void CSiLAServer::PrivateImpl::setUUIDFromSettings()
{
    // By prepending the server name to the general SERVER_UUID_SETTINGS_KEY
    // every server gets its own section where the UUID will in fact be unique
    const auto ServerUUIDSettingsKey =
        ServerInfo.serverName().toLower().append("/").append(
            SERVER_UUID_SETTINGS_KEY);
    const auto UUID =
        ServerSettings->value(ServerUUIDSettingsKey, QUuid::createUuid()).toUuid();
    ServerInfo.setServerUUID(UUID);
    ServerSettings->setValue(ServerUUIDSettingsKey, UUID);
    qCDebug(sila_cpp) << "Server UUID:" << UUID;
}

//============================================================================
void CSiLAServer::PrivateImpl::registerSiLAService()
{
    PIMPL_Q(CSiLAServer);
    qCDebug(sila_cpp) << "Registering SiLAService feature";
    SiLAService = make_shared<SiLA2::CSiLAService>(ServerInfo, q);
    ServerBuilder->RegisterService(this->SiLAService.get());
}

//============================================================================
void CSiLAServer::PrivateImpl::startZeroconfPublish()
{
    PIMPL_Q(CSiLAServer);

    const auto ServerName = ServerInfo.serverName();
    const auto ServerUUID =
        ServerInfo.serverUUID().toString(QUuid::WithoutBraces);
    const auto ServerPort = ServerAddress.port().toInt();

    QObject::connect(ZeroConf.get(), &QZeroConf::error, q, [](const auto& Error) {
        auto ErrorMessage = ""s;
        switch (Error)
        {
        case QZeroConf::serviceRegistrationFailed:
            ErrorMessage = "Service Registration Failed";
            break;
        case QZeroConf::serviceNameCollision:
            ErrorMessage = "Service Name Collision";
            break;
        case QZeroConf::browserFailed:
            ErrorMessage = "Browser Failed";
            break;
        default:
            break;
        }
        qCWarning(sila_cpp) << "Could not publish ZeroConf service! The "
                               "following error occurred:"
                            << ErrorMessage;
    });
    QObject::connect(ZeroConf.get(), &QZeroConf::servicePublished, q, []() {
        qCInfo(sila_cpp) << "ZeroConf service for SiLA Server published";
    });

    // Naming convention according to SiLA 2 Part B
    ZeroConf->startServicePublish(qPrintable(ServerUUID), "_sila._tcp", "local.",
                                  ServerPort);
}

//============================================================================
CSiLAServer::CSiLAServer(const CServerInformation& ServerInformation,
                         const CServerAddress& Address,
                         const CSSLCredentials& Credentials, bool ForceEncryption,
                         QObject* parent, PrivateImplPtr priv)
    : QObject{parent},
      d_ptr{priv ? priv :
                   make_polymorphic_value<PrivateImpl>(this, ServerInformation,
                                                       Address, Credentials,
                                                       ForceEncryption)}
{
    PIMPL_D(CSiLAServer);

    d->setUUIDFromSettings();
    d->registerSiLAService();
    qCInfo(sila_cpp)
        << "Starting SiLA2 server with server name" << d->ServerInfo.serverName();

    // setting SILA_CPP_DISABLE_DISCOVERY non-zero disables zeroconf registration
    if (qEnvironmentVariable("SILA_CPP_DISABLE_DISCOVERY", "0") != "0")
    {
        qCWarning(sila_cpp) << "Not publishing zeroconf service for this SiLA "
                               "server";
        return;
    }
    d->startZeroconfPublish();
}

//============================================================================
CSiLAServer::~CSiLAServer()
{
    PIMPL_D(CSiLAServer);
    if (d->Server)
    {
        shutdown();
    }
}

//=============================================================================
void CSiLAServer::registerFeature(grpc::Service* Feature, const CFeatureID& Name,
                                  const QByteArray& FeatureDefinition)
{
    PIMPL_D(CSiLAServer);
    qCInfo(sila_cpp) << "Registering feature" << Name;
    d->ServerBuilder->RegisterService(Feature);
    d->SiLAService->registerFeature(Name, FeatureDefinition);
}

//=============================================================================
shared_ptr<grpc::ServerCompletionQueue> CSiLAServer::addCompletionQueue()
{
    PIMPL_D(CSiLAServer);
    d->CompletionQueues.push_back(d->ServerBuilder->AddCompletionQueue());
    return d->CompletionQueues.back();
}

//=============================================================================
void CSiLAServer::run(bool block)
{
    PIMPL_D(CSiLAServer);
    d->Server = d->ServerBuilder->BuildAndStart();
    if (!d->Server)
    {
        qCCritical(sila_cpp).noquote()
            << "SiLA Server could not be started!"
            << "Maybe the" << d->ServerAddress.prettyString()
            << "is already in use by another process?";
        return;
    }
    emit started();
    qCInfo(sila_cpp) << "Server listening on" << d->ServerAddress;

    if (block)
    {
        auto SignalHandler = CInterruptSignalHandler{this};
        connect(&SignalHandler,
                &CInterruptSignalHandler::interruptSignalTriggered, this,
                &CSiLAServer::shutdown);

        qCInfo(sila_cpp) << "Blocking this thread until server is shutdown with "
                            "Ctrl-C...";
        QCoreApplication::exec();
    }
}

//============================================================================
shared_ptr<QCommandLineParser> CSiLAServer::commandLineParser()
{
    return PrivateImpl::Parser;
}

//============================================================================
void CSiLAServer::shutdown()
{
    PIMPL_D(CSiLAServer);
    qCInfo(sila_cpp) << "Shutting down server";

    d->ZeroConf->stopServicePublish();

    d->Server->Shutdown(gpr_time_from_seconds(1, GPR_TIMESPAN));
    d->Server->Wait();
    d->Server.reset();
    for_each(begin(d->CompletionQueues), end(d->CompletionQueues), [](auto cq) {
        cq->Shutdown();
        // Drain the queue
        void* IgnoredTag;
        bool IgnoredOk;
        while (cq->Next(&IgnoredTag, &IgnoredOk))
        {
        }
    });
    QThread::msleep(500);  // give the RPCs some time to deallocate themselves
    QCoreApplication::quit();
}
}  // namespace SiLA2

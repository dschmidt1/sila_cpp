/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   InterruptSignalHandler.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   21.06.2020
/// \brief  Declaration of the CInterruptSignalHandler class
//============================================================================
#ifndef INTERRUPTSIGNALHANDLER_H
#define INTERRUPTSIGNALHANDLER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include <QObject>

#include <polymorphic_value/polymorphic_value.h>

/**
 * @brief The CInterruptSignalHandler class represents a cross-platform signal
 * handler that converts the native interrupt signal (i.e. CTRL_C_EVENT on windows
 * resp. SIGINT on Unix) to a Qt signal.
 */
class CInterruptSignalHandler : public QObject
{
    Q_OBJECT
    class PrivateImpl;
    using PrivateImplPtr = jbcoe::polymorphic_value<PrivateImpl>;

public:
    /**
     * @brief C'tor
     *
     * @param parent The parent object
     */
    explicit CInterruptSignalHandler(QObject* parent = nullptr,
                                     PrivateImplPtr priv = {});

signals:
    /**
     * @brief This signal is emitted when the interrupt signal (i.e. Ctrl-C) was
     * triggered.
     */
    void interruptSignalTriggered();

private slots:
    /**
     * @brief Handle the native interrupt signal. Emits the
     * @a interruptSignalTriggered signal to notify connected objects.
     */
    void handleInterruptSignal();

protected:
    PrivateImplPtr d_ptr;

private:
    PIMPL_DECLARE_PRIVATE(CInterruptSignalHandler)
};

#endif  // INTERRUPTSIGNALHANDLER_H

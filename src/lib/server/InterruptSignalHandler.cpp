/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SignalHandler.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   21.06.2020
/// \brief  Implementation of the CInterruptSignalHandler class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include "InterruptSignalHandler.h"

#include <sila_cpp/common/logging.h>

#ifdef Q_OS_WIN
#    include <QWinEventNotifier>

#    include <windows.h>
#elif defined(Q_OS_UNIX)
#    include <QSocketNotifier>

#    define _POSIX_C_SOURCE 200809L
#    include <sys/socket.h>
#    include <unistd.h>

#    include <csignal>
#endif

#include <iostream>

using namespace std;
using namespace jbcoe;

class CInterruptSignalHandler::PrivateImpl
{
public:
    PrivateImpl(CInterruptSignalHandler* parent);

    ~PrivateImpl();

    /**
     * @brief Setup the interrupt signal handler
     *
     * @param install Whether to install the signal handler or restore the
     default
     * behaviour. When set to @a true, installs the handler, otherwise restores
     * the default.
     *
     * @returns true, if the operation succeeds
     * @returns false, otherwise
     */
    static bool setupSignalHandler(bool install = true);

#ifdef Q_OS_WIN
    /**
     * @brief Native Windows signal handler
     *
     * @param signal The signal to handle
     */
    static BOOL WINAPI signalHandler(DWORD signal);
#elif defined(Q_OS_UNIX)
    /**
     * @brief Native unix signal handler
     *
     * @param signal The signal to handle
     */
    static void signalHandler(int signal);
#endif

    /**
     * @brief Setup the sockets and the @a Notifier that'll watch one of them
     *
     * @return true, if the operation succeeds
     * @return false, otherwise
     */
    bool setupSockets();

#ifdef Q_OS_WIN
    static HANDLE CtrlCEvent;  ///< will be triggered when CTRL_C_EVENT is received
    QWinEventNotifier* Notifier{};  ///< watches for @a CtrlCEvent signals
#elif defined(Q_OS_UNIX)
    static array<int, 2> SigIntFD;  ///< will be written to when SIGINT is signaled
    QSocketNotifier* Notifier{};    ///< watches @a SigIntFD for writes
#endif

protected:
    CInterruptSignalHandler* q_ptr;

private:
    PIMPL_DECLARE_PUBLIC(CInterruptSignalHandler);
};

#ifdef Q_OS_WIN
HANDLE CInterruptSignalHandler::PrivateImpl::CtrlCEvent;
#elif defined(Q_OS_UNIX)
array<int, 2> CInterruptSignalHandler::PrivateImpl::SigIntFD;
#endif

//============================================================================
CInterruptSignalHandler::PrivateImpl::PrivateImpl(CInterruptSignalHandler* parent)
    : q_ptr{parent}
{
    if (!setupSignalHandler() || !setupSockets())
    {
        qCWarning(sila_cpp) << "Couldn't set interrupt signal handler. Shutting "
                               "down with Ctrl-C will result in uncontrolled "
                               "shutdown!";
    }
}

//============================================================================
CInterruptSignalHandler::PrivateImpl::~PrivateImpl()
{
    setupSignalHandler(false);
}

//============================================================================
bool CInterruptSignalHandler::PrivateImpl::setupSignalHandler(bool install)
{
    qCDebug(sila_cpp) << (install ? "Installing" : "Uninstalling")
                      << "interrupt signal handler";

#ifdef Q_OS_WIN
    return (SetConsoleCtrlHandler(&signalHandler, install ? TRUE : FALSE)
            == TRUE);
#elif defined(Q_OS_UNIX)
    struct sigaction SigAction;
    SigAction.sa_handler = install ? signalHandler : SIG_DFL;
    sigemptyset(&(SigAction.sa_mask));
    SigAction.sa_flags = SA_RESTART;
    return (sigaction(SIGINT, &SigAction, nullptr) == 0);
#endif
}

//============================================================================
bool CInterruptSignalHandler::PrivateImpl::setupSockets()
{
    PIMPL_Q(CInterruptSignalHandler);
#ifdef Q_OS_WIN
    CtrlCEvent = CreateEvent(nullptr,   // default security attributes
                             FALSE,     // auto-reset event
                             FALSE,     // initial state is non-signaled
                             nullptr);  // object w/o name
    if (!CtrlCEvent)
    {
        return false;
    }
    Notifier = new QWinEventNotifier{CtrlCEvent, q};
    connect(Notifier, &QWinEventNotifier::activated, q,
            &CInterruptSignalHandler::handleInterruptSignal);
#elif defined(Q_OS_UNIX)
    if (socketpair(AF_UNIX, SOCK_STREAM, 0, SigIntFD.data()) != 0)
    {
        return false;
    }

    Notifier = new QSocketNotifier(SigIntFD[1], QSocketNotifier::Read, q);
    connect(Notifier, &QSocketNotifier::activated, q,
            &CInterruptSignalHandler::handleInterruptSignal);
#endif

    return true;
}

#ifdef Q_OS_WIN
//============================================================================
BOOL CInterruptSignalHandler::PrivateImpl::signalHandler(DWORD signal)
{
    if (signal != CTRL_C_EVENT)
    {
        return FALSE;
    }

    if (!SetEvent(CtrlCEvent))
    {
        qCDebug(sila_cpp) << "SetEvent failed! Error" << GetLastError();
        return FALSE;
    }
    return TRUE;
}
#elif defined(Q_OS_UNIX)
//============================================================================
void CInterruptSignalHandler::PrivateImpl::signalHandler(int signal)
{
    cout << '\n';
    if (signal != SIGINT)
    {
        return;
    }

    char dummy = 1;
    write(SigIntFD[0], &dummy, sizeof(dummy));
}
#endif

//============================================================================
CInterruptSignalHandler::CInterruptSignalHandler(QObject* parent,
                                                 PrivateImplPtr priv)
    : QObject{parent},
      d_ptr{priv ? priv : make_polymorphic_value<PrivateImpl>(this)}
{}

//============================================================================
void CInterruptSignalHandler::handleInterruptSignal()
{
    PIMPL_D(CInterruptSignalHandler);
    qCDebug(sila_cpp) << "Received interrupt signal";
    d->Notifier->setEnabled(false);
#ifdef Q_OS_UNIX
    char dummy;
    read(d->SigIntFD[1], &dummy, sizeof(dummy));
#endif
    emit interruptSignalTriggered();
    d->Notifier->setEnabled(true);
}

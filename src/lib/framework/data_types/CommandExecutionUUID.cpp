/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   CommandExecutionUUID.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   05.05.2020
/// \brief  Implementation of the CCommandExecutionUUID class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/data_types/CommandExecutionUUID.h>
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>

#include <QHash>

using sila2::org::silastandard::CommandExecutionUUID;

namespace SiLA2
{
//============================================================================
CCommandExecutionUUID::CCommandExecutionUUID(const QUuid& Value)
    : CDataType<QUuid>{Value}
{
    if (Value.isNull())
    {
        qCInfo(sila_cpp)
            << "Constructing a null-CommandExecutionUUID! Maybe the given "
               "UUID was not in the correct format.";
    }
}

//============================================================================
CCommandExecutionUUID::CCommandExecutionUUID(const CommandExecutionUUID& rhs)
    : CDataType<QUuid>{QUuid::fromString(QString::fromStdString(rhs.value()))}
{}

//============================================================================
CommandExecutionUUID CCommandExecutionUUID::toProtoMessage() const
{
    auto Result = CommandExecutionUUID{};
    Result.set_value(toStdString());
    return Result;
}

//============================================================================
CommandExecutionUUID* CCommandExecutionUUID::toProtoMessagePtr() const
{
    auto Result = new CommandExecutionUUID{};
    Result->set_value(toStdString());
    return Result;
}

//============================================================================
QString CCommandExecutionUUID::toString() const
{
    return value().toString(QUuid::WithoutBraces);
}

//============================================================================
std::string CCommandExecutionUUID::toStdString() const
{
    return toString().toStdString();
}

//============================================================================
uint qHash(const CCommandExecutionUUID& key, uint seed) noexcept
{
    return qHash(QString::fromStdString(key.toStdString()), seed);
}
}  // namespace SiLA2

//============================================================================
QDebug operator<<(QDebug dbg, const SiLA2::CCommandExecutionUUID& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace()
           << "SiLA2::CCommandExecutionUUID(" << rhs.toString() << ")";
}

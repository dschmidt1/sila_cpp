/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLABoolean.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.01.2020
/// \brief  Implementation of the CBoolean class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/data_types/SiLABoolean.h>
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>

using namespace std;
using sila2::org::silastandard::Boolean;

namespace SiLA2
{
//============================================================================
CBoolean::CBoolean(bool Value) : CDataType<bool>{Value}
{}

//============================================================================
CBoolean::CBoolean(const Boolean& rhs) : CDataType<bool>{rhs.value()}
{}

//============================================================================
Boolean CBoolean::toProtoMessage() const
{
    auto Result = Boolean{};
    Result.set_value(value());
    return Result;
}

//============================================================================
Boolean* CBoolean::toProtoMessagePtr() const
{
    auto Result = new Boolean{};
    Result->set_value(value());
    return Result;
}
}  // namespace SiLA2

//============================================================================
QDebug operator<<(QDebug dbg, const SiLA2::CBoolean& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace() << "SiLA2::CBoolean(" << rhs.value() << ")";
}

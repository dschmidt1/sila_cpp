/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLADate.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.01.2020
/// \brief  Implementation of the CDate class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/data_types/SiLADate.h>
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>

#include <QDate>

using sila2::org::silastandard::Date;

namespace SiLA2
{
using TypeTuple = std::tuple<uint32_t, uint32_t, uint32_t, CTimezone>;

//============================================================================
CDate::CDate(uint32_t Day, uint32_t Month, uint32_t Year,
             const CTimezone& Timezone)
    : CDataType<TypeTuple>{{Day, Month, Year, Timezone}}
{
    if (!isValid(Day, Month, Year))
    {
        throw CInvalidDate{Day, Month, Year};
    }
}

//============================================================================
CDate::CDate(const Date& rhs)
    : CDataType<TypeTuple>{{rhs.day(), rhs.month(), rhs.year(), rhs.timezone()}}
{}

//============================================================================
sila2::org::silastandard::Date CDate::toProtoMessage() const
{
    auto [Day, Month, Year, Timezone] = value();
    auto Result = Date{};
    Result.set_day(Day);
    Result.set_month(Month);
    Result.set_year(Year);
    Result.set_allocated_timezone(Timezone.toProtoMessagePtr());
    return Result;
}

//============================================================================
Date* CDate::toProtoMessagePtr() const
{
    auto [Day, Month, Year, Timezone] = value();
    auto Result = new Date{};
    Result->set_day(Day);
    Result->set_month(Month);
    Result->set_year(Year);
    Result->set_allocated_timezone(Timezone.toProtoMessagePtr());
    return Result;
}

//============================================================================
uint32_t CDate::day() const
{
    return std::get<0>(value());
}

//============================================================================
void CDate::setDay(uint32_t Day)
{
    if (!isValid(Day, month(), year()))
    {
        throw CInvalidDate{Day, CInvalidDate::DAY};
    }

    auto tmp = value();
    std::get<0>(tmp) = Day;
    setValue(tmp);
}

//============================================================================
uint32_t CDate::month() const
{
    return std::get<1>(value());
}

//============================================================================
void CDate::setMonth(uint32_t Month)
{
    if (!isValid(day(), Month, year()))
    {
        throw CInvalidDate{Month, CInvalidDate::MONTH};
    }

    auto tmp = value();
    std::get<1>(tmp) = Month;
    setValue(tmp);
}

//============================================================================
uint32_t CDate::year() const
{
    return std::get<2>(value());
}

//============================================================================
void CDate::setYear(uint32_t Year)
{
    if (!isValid(day(), month(), Year))
    {
        throw CInvalidDate{Year, CInvalidDate::YEAR};
    }

    auto tmp = value();
    std::get<2>(tmp) = Year;
    setValue(tmp);
}

//============================================================================
CTimezone CDate::timezone() const
{
    return std::get<3>(value());
}

//============================================================================
void CDate::setTimezone(const CTimezone& Timezone)
{
    auto tmp = value();
    std::get<3>(tmp) = Timezone;
    setValue(tmp);
}

//============================================================================
bool CDate::isValid(uint32_t Day, uint32_t Month, uint32_t Year)
{
    return QDate::isValid(Year, Month, Day)
           && static_cast<int32_t>(Year) >= 1;  // Qt accepts years < 0
}

///===========================================================================
///                        CInvalidDate implementation
///===========================================================================
CInvalidDate::CInvalidDate(uint32_t Day, uint32_t Month, uint32_t Year)
{
    auto Stream = QTextStream{&m_Message};
    Stream << "Tried to construct an invalid SiLA2::CDate with day = " << Day
           << ", month = " << Month << ", year = " << Year;
}

//============================================================================
CInvalidDate::CInvalidDate(uint32_t Value, DatePart Part)
{
    const char* PartString;
    switch (Part)
    {
    case DAY:
        PartString = "day";
        break;
    case MONTH:
        PartString = "month";
        break;
    case YEAR:
        PartString = "year";
        break;
    }
    auto Stream = QTextStream{&m_Message};
    Stream << "Invalid " << PartString << " value " << Value
           << " for SiLA2::CDate";
}

//============================================================================
const char* CInvalidDate::what() const noexcept
{
    return qPrintable(m_Message);
}
}  // namespace SiLA2

//============================================================================
QDebug operator<<(QDebug dbg, const SiLA2::CDate& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace() << "SiLA2::CDate(" << rhs.year() << "-" << rhs.month()
                         << "-" << rhs.day() << ", " << rhs.timezone() << ")";
}

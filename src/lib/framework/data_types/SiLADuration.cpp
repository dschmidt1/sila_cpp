/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLADuration.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   12.05.2020
/// \brief  Implementation of the CDuration class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/data_types/SiLADuration.h>
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>

using sila2::org::silastandard::Duration;

using TypePair = std::pair<uint64_t, uint32_t>;

namespace SiLA2
{
//============================================================================
CDuration::CDuration(uint64_t seconds, uint32_t nanos)
    : CDataType<TypePair>{{seconds, nanos}}
{}

//============================================================================
CDuration::CDuration(const Duration& rhs)
    : CDataType<TypePair>{{rhs.seconds(), rhs.nanos()}}
{}

//============================================================================
Duration CDuration::toProtoMessage() const
{
    auto Result = Duration{};
    Result.set_seconds(seconds());
    Result.set_nanos(nanos());
    return Result;
}

//============================================================================
Duration* CDuration::toProtoMessagePtr() const
{
    auto Result = new Duration{};
    Result->set_seconds(seconds());
    Result->set_nanos(nanos());
    return Result;
}

//============================================================================
uint64_t CDuration::seconds() const
{
    return value().first;
}

//============================================================================
void CDuration::setSeconds(uint64_t Seconds)
{
    auto tmp = value();
    tmp.first = Seconds;
    setValue(tmp);
}

//============================================================================
uint32_t CDuration::nanos() const
{
    return value().second;
}

//============================================================================
void CDuration::setNanos(uint32_t Nanos)
{
    auto tmp = value();
    tmp.second = Nanos;
    setValue(tmp);
}

//============================================================================
bool CDuration::isNull() const
{
    return *this == TypePair{0, 0};
}

//============================================================================
std::chrono::milliseconds CDuration::toMilliSeconds() const
{
    return std::chrono::milliseconds{
        seconds() * std::milli::den + nanos() * std::milli::den / std::nano::den};
}
}  // namespace SiLA2

//============================================================================
QDebug operator<<(QDebug dbg, const SiLA2::CDuration& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace() << "SiLA2::CDuration(" << rhs.seconds() << "s, "
                         << rhs.nanos() << "ns)";
}

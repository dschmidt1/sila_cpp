/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ExecutionInfo.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   05.05.2020
/// \brief  Implementation of the CExecutionInfo class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/data_types/ExecutionInfo.h>

using sila2::org::silastandard::Duration;
using sila2::org::silastandard::ExecutionInfo;
using sila2::org::silastandard::ExecutionInfo_CommandStatus;
using sila2::org::silastandard::Real;

namespace SiLA2
{
using TypeTuple = std::tuple<CommandStatus, CReal, CDuration, CDuration>;

//============================================================================
CExecutionInfo::CExecutionInfo(CommandStatus Status, const CReal& Progress,
                               const CDuration& RemainingTime,
                               const CDuration& UpdatedLifetime)
    : CDataType<TypeTuple>{
        {CommandStatus{Status}, Progress, RemainingTime, UpdatedLifetime}}
{}

//============================================================================
CExecutionInfo::CExecutionInfo(const CExecutionInfo& rhs) = default;

//============================================================================
CExecutionInfo::CExecutionInfo(const sila2::org::silastandard::ExecutionInfo& rhs)
    : CDataType<TypeTuple>{{CommandStatus{rhs.commandstatus()},
                            rhs.progressinfo(), rhs.estimatedremainingtime(),
                            rhs.updatedlifetimeofexecution()}}
{}

//============================================================================
CExecutionInfo::~CExecutionInfo() = default;

//============================================================================
ExecutionInfo CExecutionInfo::toProtoMessage() const
{
    auto [Status, Progress, RemainingTime, Lifetime] = value();
    auto Result = ExecutionInfo{};
    Result.set_commandstatus(static_cast<ExecutionInfo_CommandStatus>(Status));
    Result.set_allocated_progressinfo(Progress.toProtoMessagePtr());
    Result.set_allocated_estimatedremainingtime(
        RemainingTime.toProtoMessagePtr());
    Result.set_allocated_updatedlifetimeofexecution(Lifetime.toProtoMessagePtr());
    return Result;  // NOLINT(bugprone-infinite-loop)
}

//============================================================================
ExecutionInfo* CExecutionInfo::toProtoMessagePtr() const
{
    auto [Status, Progress, RemainingTime, Lifetime] = value();
    auto Result = new ExecutionInfo{};
    Result->set_commandstatus(static_cast<ExecutionInfo_CommandStatus>(Status));
    Result->set_allocated_progressinfo(Progress.toProtoMessagePtr());
    Result->set_allocated_estimatedremainingtime(
        RemainingTime.toProtoMessagePtr());
    Result->set_allocated_updatedlifetimeofexecution(
        Lifetime.toProtoMessagePtr());
    return Result;
}

//============================================================================
CommandStatus CExecutionInfo::commandStatus() const
{
    return std::get<0>(value());
}

//============================================================================
void CExecutionInfo::setCommandStatus(CommandStatus Status)
{
    auto tmp = value();
    std::get<0>(tmp) = Status;
    setValue(tmp);
}

//============================================================================
CReal CExecutionInfo::progressInfo() const
{
    return std::get<1>(value());
}

//============================================================================
void CExecutionInfo::setProgressInfo(const CReal& Progress)
{
    auto tmp = value();
    std::get<1>(tmp) = Progress;
    setValue(tmp);
}

//============================================================================
CDuration CExecutionInfo::estimatedRemainingTime() const
{
    return std::get<2>(value());
}

//============================================================================
void CExecutionInfo::setEstimatedRemainingTime(const CDuration& RemainingTime)
{
    auto tmp = value();
    std::get<2>(tmp) = RemainingTime;
    setValue(tmp);
}

//============================================================================
CDuration CExecutionInfo::updatedLifetimeOfExecution() const
{
    return std::get<3>(value());
}

//============================================================================
void CExecutionInfo::setUpdatedLifetimeOfExecution(const CDuration& Lifetime)
{
    auto tmp = value();
    std::get<3>(tmp) = Lifetime;
    setValue(tmp);
}
}  // namespace SiLA2

//============================================================================
bool operator==(SiLA2::CommandStatus lhs,
                sila2::org::silastandard::ExecutionInfo_CommandStatus rhs)
{
    using sila2::org::silastandard::ExecutionInfo_CommandStatus;
    return static_cast<ExecutionInfo_CommandStatus>(lhs) == rhs;
}

//============================================================================
bool operator!=(SiLA2::CommandStatus lhs,
                sila2::org::silastandard::ExecutionInfo_CommandStatus rhs)
{
    return !(lhs == rhs);
}

//============================================================================
QDebug operator<<(QDebug dbg, SiLA2::CommandStatus rhs)
{
    switch (rhs)
    {
    case SiLA2::CommandStatus::WAITING:
        return dbg << "SiLA2::CommandStatus(WAITING)";
    case SiLA2::CommandStatus::RUNNING:
        return dbg << "SiLA2::CommandStatus(RUNNING)";
    case SiLA2::CommandStatus::FINISHED_SUCCESSFULLY:
        return dbg << "SiLA2::CommandStatus(FINISHED_SUCCESSFULLY)";
    case SiLA2::CommandStatus::FINISHED_WITH_ERROR:
        return dbg << "SiLA2::CommandStatus(FINISHED_WITH_ERROR)";
    default:
        qCCritical(sila_cpp)
            << "Cannot handle SiLA2::CommandStatus" << static_cast<uint>(rhs);
        return dbg;
    }
}

//============================================================================
QDebug operator<<(QDebug dbg, const SiLA2::CExecutionInfo& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace()
           << "SiLA2::CExecutionInfo(Status: " << rhs.commandStatus()
           << ", Progress: " << rhs.progressInfo()
           << ", Remaining: " << rhs.estimatedRemainingTime()
           << ", Lifetime: " << rhs.updatedLifetimeOfExecution() << ")";
}

/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLATime.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.01.2020
/// \brief  Implementation of the CTime class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/data_types/SiLATime.h>
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>

#include <QTime>

using sila2::org::silastandard::Time;

namespace SiLA2
{
using TypeTuple = std::tuple<uint32_t, uint32_t, uint32_t, CTimezone>;

//============================================================================
CTime::CTime(uint32_t Hour, uint32_t Minute, uint32_t Second,
             const CTimezone& Timezone)
    : CDataType<TypeTuple>{{Hour, Minute, Second, Timezone}}
{
    if (!isValid(Hour, Minute, Second))
    {
        throw CInvalidTime{Hour, Minute, Second};
    }
}

//============================================================================
CTime::CTime(const Time& rhs)
    : CDataType<TypeTuple>{
        {rhs.hour(), rhs.minute(), rhs.second(), rhs.timezone()}}
{}

//============================================================================
Time CTime::toProtoMessage() const
{
    auto [Hour, Minute, Second, Timezone] = value();
    auto Result = Time{};
    Result.set_hour(Hour);
    Result.set_minute(Minute);
    Result.set_second(Second);
    Result.set_allocated_timezone(Timezone.toProtoMessagePtr());
    return Result;
}

//============================================================================
Time* CTime::toProtoMessagePtr() const
{
    auto [Hour, Minute, Second, Timezone] = value();
    auto Result = new Time{};
    Result->set_hour(Hour);
    Result->set_minute(Minute);
    Result->set_second(Second);
    Result->set_allocated_timezone(Timezone.toProtoMessagePtr());
    return Result;
}

//============================================================================
uint32_t CTime::hour() const
{
    return std::get<0>(value());
}

//============================================================================
void CTime::setHour(uint32_t Hour)
{
    if (!isValid(Hour, minute(), second()))
    {
        throw CInvalidTime{Hour, CInvalidTime::HOUR};
    }

    auto tmp = value();
    std::get<0>(tmp) = Hour;
    setValue(tmp);
}

//============================================================================
uint32_t CTime::minute() const
{
    return std::get<1>(value());
}

//============================================================================
void CTime::setMinute(uint32_t Minute)
{
    if (!isValid(hour(), Minute, second()))
    {
        throw CInvalidTime{Minute, CInvalidTime::MINUTE};
    }

    auto tmp = value();
    std::get<1>(tmp) = Minute;
    setValue(tmp);
}

//============================================================================
uint32_t CTime::second() const
{
    return std::get<2>(value());
}

//============================================================================
void CTime::setSecond(uint32_t Second)
{
    if (!isValid(hour(), minute(), Second))
    {
        throw CInvalidTime{Second, CInvalidTime::SECOND};
    }

    auto tmp = value();
    std::get<2>(tmp) = Second;
    setValue(tmp);
}

//============================================================================
CTimezone CTime::timezone() const
{
    return std::get<3>(value());
}

//============================================================================
void CTime::setTimezone(const CTimezone& Timezone)
{
    auto tmp = value();
    std::get<3>(tmp) = Timezone;
    setValue(tmp);
}

//============================================================================
bool CTime::isValid(uint32_t Hour, uint32_t Minute, uint32_t Second)
{
    return QTime::isValid(Hour, Minute, Second);
}

///===========================================================================
///                        CInvalidTime implementation
///===========================================================================
CInvalidTime::CInvalidTime(uint32_t Hour, uint32_t Minute, uint32_t Second)
{
    auto Stream = QTextStream{&m_Message};
    Stream << "Tried to construct an invalid SiLA2::CTime with hour = " << Hour
           << ", minute = " << Minute << ", second = " << Second;
}

//============================================================================
CInvalidTime::CInvalidTime(uint32_t Value, TimePart Part)
{
    const char* PartString;
    switch (Part)
    {
    case HOUR:
        PartString = "hour";
        break;
    case MINUTE:
        PartString = "minute";
        break;
    case SECOND:
        PartString = "second";
        break;
    }
    auto Stream = QTextStream{&m_Message};
    Stream << "Invalid " << PartString << " value " << Value
           << " for SiLA2::CTime";
}

//============================================================================
const char* CInvalidTime::what() const noexcept
{
    return qPrintable(m_Message);
}
}  // namespace SiLA2

//============================================================================
QDebug operator<<(QDebug dbg, const SiLA2::CTime& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace() << "SiLA2::CTime(" << rhs.hour() << ":" << rhs.minute()
                         << ":" << rhs.second() << ", " << rhs.timezone() << ")";
}

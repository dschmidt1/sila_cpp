
/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAService.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   22.01.2020
/// \brief  Declaration of the CSiLAService class
//============================================================================
#ifndef SILASERVICE_H
#define SILASERVICE_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/framework/data_types/SiLAString.h>
#include <sila_cpp/global.h>
#include <sila_cpp/server/SiLAFeature.h>
#include <sila_cpp/server/command/UnobservableCommand.h>
#include <sila_cpp/server/property/UnobservableProperty.h>

#include "SiLAService.grpc.pb.h"

#include <map>
#include <vector>

//=============================================================================
//                            FORWARD DECLARATIONS
//=============================================================================
namespace SiLA2
{
class CServerInformation;
class CFeatureID;

/**
 * @brief The CSiLAService class implements the SiLAService feature
 */
class SILA_CPP_EXPORT CSiLAService final :
    public CSiLAFeature<
        sila2::org::silastandard::core::silaservice::v1::SiLAService>
{
    using GetFeatureDefinitionCommand =
        CUnobservableCommandManager<&CSiLAService::RequestGetFeatureDefinition>;
    using GetFeatureDefinitionWrapper =
        CUnobservableCommandWrapper<sila2::org::silastandard::core::silaservice::
                                        v1::GetFeatureDefinition_Parameters,
                                    sila2::org::silastandard::core::silaservice::
                                        v1::GetFeatureDefinition_Responses>;
    using SetServerNameCommand =
        CUnobservableCommandManager<&CSiLAService::RequestSetServerName>;
    using SetServerNameWrapper = CUnobservableCommandWrapper<
        sila2::org::silastandard::core::silaservice::v1::SetServerName_Parameters,
        sila2::org::silastandard::core::silaservice::v1::SetServerName_Responses>;
    using ServerNameProperty =
        CUnobservablePropertyWrapper<CString,
                                     &CSiLAService::RequestGet_ServerName>;
    using ServerTypeProperty =
        CUnobservablePropertyWrapper<CString,
                                     &CSiLAService::RequestGet_ServerType>;
    using ServerUUIDProperty =
        CUnobservablePropertyWrapper<CString,
                                     &CSiLAService::RequestGet_ServerUUID>;
    using ServerDescriptionProperty =
        CUnobservablePropertyWrapper<CString,
                                     &CSiLAService::RequestGet_ServerDescription>;
    using ServerVersionProperty =
        CUnobservablePropertyWrapper<CString,
                                     &CSiLAService::RequestGet_ServerVersion>;
    using ServerVendorURLProperty =
        CUnobservablePropertyWrapper<CString,
                                     &CSiLAService::RequestGet_ServerVendorURL>;
    using ImplementedFeaturesProperty = CUnobservablePropertyWrapper<
        std::vector<CString>, &CSiLAService::RequestGet_ImplementedFeatures>;

public:
    /**
     * @brief C'tor
     *
     * @param ServerInfo The server's information such as name, type, ...
     * @param Server The SiLA Server instance that contains this Feature
     */
    CSiLAService(const CServerInformation& ServerInfo, CSiLAServer* Server);

    /**
     * @brief Get Feature Definition Command
     *
     * @details Get the Feature Definition of an implemented Feature by its fully
     * qualified Feature Identifier. This command has no preconditions and no
     * further dependencies and can be called at any time.
     *
     * @param Command The current Get Feature Definition Command Execution Wrapper
     * Contains the @b FeatureIdentifier Command Parameter (The fully qualified
     * Feature identifier for which the Feature definition shall be retrieved.)
     *
     * @return GetFeatureDefinition_Responses The command response containing the
     * FeatureDefinition
     */
    sila2::org::silastandard::core::silaservice::v1::GetFeatureDefinition_Responses
    GetFeatureDefinition(GetFeatureDefinitionWrapper* Command);

    /**
     * @brief Set Server Name Command
     *
     * @details Sets a human readable name to the Server Name Property.Command has
     * no preconditions and no further dependencies and can be called at any time.
     *
     * @param Command The current Set Server Name Command Execution Wrapper
     * Contains the @b ServerName Command Parameter (The human readable name to
     * assign to the SiLA Server.)
     *
     * @return Empty SetServerName_Responses
     */
    sila2::org::silastandard::core::silaservice::v1::SetServerName_Responses
    SetServerName(SetServerNameWrapper* Command);

    /**
     * @brief Register the given feature to the SiLAService so that any clients
     * connecting to the server can retrieve all implemented features.
     *
     * @param FeatureName The fully qualified feature identifier of the feature
     * @param FeatureDefinition The feature definition of the feature
     */
    void registerFeature(const CFeatureID& FeatureName,
                         const QByteArray& FeatureDefinition);

private:
    std::map<CFeatureID, QByteArray>
        m_ImplementedFeatures{};  ///< Maps Fully Qualified Feature IDs to their
                                  ///< feature definitions
    GetFeatureDefinitionCommand m_GetFeatureDefinitionCommand;
    SetServerNameCommand m_SetServerNameCommand;
    ServerNameProperty m_ServerNameProperty;
    ServerTypeProperty m_ServerTypeProperty;
    ServerUUIDProperty m_ServerUUIDProperty;
    ServerDescriptionProperty m_ServerDescriptionProperty;
    ServerVersionProperty m_ServerVersionProperty;
    ServerVendorURLProperty m_ServerVendorURLProperty;
    ImplementedFeaturesProperty m_ImplementedFeaturesProperty;
};
}  // namespace SiLA2

#endif  // SILASERVICE_H

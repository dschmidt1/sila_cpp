/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAService.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   22.01.2020
/// \brief  Implementation of the CSiLAService class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include "SiLAService.h"

#include <sila_cpp/common/ServerInformation.h>
#include <sila_cpp/framework/error_handling/ValidationError.h>

#include <QUuid>

#include <utility>

using namespace std;
using namespace sila2::org::silastandard::core::silaservice::v1;

namespace SiLA2
{
//=============================================================================
CSiLAService::CSiLAService(const CServerInformation& ServerInfo,
                           CSiLAServer* Server)
    : CSiLAFeature{Server},
      m_GetFeatureDefinitionCommand{this, "GetFeatureDefinition"},
      m_SetServerNameCommand{this, "SetServerName"},
      m_ServerNameProperty{this, {ServerInfo.serverName()}, "ServerName"},
      m_ServerTypeProperty{this, {ServerInfo.serverType()}, "ServerType"},
      m_ServerUUIDProperty{
          this,
          {ServerInfo.serverUUID().toString(QUuid::WithoutBraces)},
          "ServerUUID"},
      m_ServerDescriptionProperty{
          this, {ServerInfo.description()}, "ServerDescription"},
      m_ServerVersionProperty{this, {ServerInfo.version()}, "ServerVersion"},
      m_ServerVendorURLProperty{
          this, {ServerInfo.vendorURL()}, "ServerVendorURL"},
      m_ImplementedFeaturesProperty{this, "ImplementedFeatures"}
{
    // register the SiLAService feature to itself so that clients can request
    // its Feature Definition
    registerFeature(CFeatureID{SiLAService::service_full_name()},
                    featureDefinition());

    m_GetFeatureDefinitionCommand.setExecutor(
        this, &CSiLAService::GetFeatureDefinition);
    m_SetServerNameCommand.setExecutor(this, &CSiLAService::SetServerName);
}

//=========================================================================
GetFeatureDefinition_Responses CSiLAService::GetFeatureDefinition(
    GetFeatureDefinitionWrapper* Command)
{
    const auto Request = Command->parameters();
    if (!Request.has_featureidentifier())
    {
        throw CValidationError{"FeatureIdentifier",
                               "No FeatureIdentifier given! Specify a valid "
                               "FeatureIdentifier to get its Feature "
                               "Description. You can get a list of all "
                               "implemented features by reading the property "
                               "'ImplementedFeatures'"};
    }
    const auto RequestedFeature = Request.featureidentifier().value();
    qCInfo(sila_cpp)
        << "Requested feature definition for feature" << RequestedFeature;
    const auto FeatureID = CFeatureID::fromStdString(RequestedFeature);
    auto Response = GetFeatureDefinition_Responses{};
    try
    {
        Response.set_allocated_featuredefinition(
            CString{m_ImplementedFeatures.at(FeatureID).constData()}
                .toProtoMessagePtr());
        return Response;
    }
    catch (const std::out_of_range&)
    {
        throw CDefinedExecutionError{"UnimplementedFeature",
                                     "The Feature specified by the given Feature "
                                     "Identifier is not implemented by the "
                                     "server."};
    }
}

//=========================================================================
SetServerName_Responses CSiLAService::SetServerName(SetServerNameWrapper* Command)
{
    const auto NewServerName = Command->parameters().servername().value();
    if (NewServerName.empty())
    {
        throw CValidationError{"ServerName",
                               "No server name given! Provide a name with at "
                               "least one character."};
    }
    if (NewServerName.length() > 255)  // Parameter Constraint
    {
        throw CValidationError{"ServerName",
                               "The given server name is longer than the "
                               "maximum length of 255 characters! Specify a "
                               "server name with at least one and at most "
                               "255 characters."};
    }
    m_ServerNameProperty = NewServerName;
    return {};
}

//=============================================================================
void CSiLAService::registerFeature(const CFeatureID& FeatureName,
                                   const QByteArray& FeatureDefinition)
{
    m_ImplementedFeatures[FeatureName] = FeatureDefinition;
    m_ImplementedFeaturesProperty.append(CString{FeatureName.toStdString()});
}
}  // namespace SiLA2

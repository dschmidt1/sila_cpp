/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ValidationError.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   27.01.2020
/// \brief  Implementation of the CValidationError class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/error_handling/ValidationError.h>
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>
#include <sila_cpp/internal/Base64.h>

#include "SiLAError_p.h"

#include <string>
#include <utility>

using namespace std;
using namespace jbcoe;
using namespace sila2::org::silastandard;

namespace SiLA2
{
static const string DEFAULT_MESSAGE =
    "A validation error occurred during parameter evaluation.";

/**
 * @brief Private data of the CValidationError class - pimpl
 */
class CValidationError::PrivateImpl : public CSiLAError::PrivateImpl
{
public:
    /**
     * @brief C'tor
     */
    PrivateImpl(std::string param, const std::string& msg)
        : CSiLAError::PrivateImpl{msg.empty() ? DEFAULT_MESSAGE : msg},
          Parameter{move(param)}
    {}

    /**
     * @brief Construct a SiLA Error protobuf message from the internal fields
     * @a Message and @a Parameter. Note that the caller takes ownership
     * of the returned message.
     *
     * @return SiLAError* A pointer to the SiLA Error protobuf message
     */
    [[nodiscard]] SiLAError* makeErrorMessage() const override;

    std::string Parameter{};  ///< The parameter for which the validation failed
};

//=============================================================================
SiLAError* CValidationError::PrivateImpl::makeErrorMessage() const
{
    auto VError = new ValidationError;
    VError->set_parameter(Parameter);
    VError->set_message(Message);
    auto Error = new SiLAError;
    Error->set_allocated_validationerror(VError);
    return Error;
}

//=============================================================================
CValidationError::CValidationError(const string& Parameter, const string& Message)
    : CSiLAError{make_polymorphic_value<CSiLAError::PrivateImpl, PrivateImpl>(
        Parameter, Message)}
{
    if (Message.empty())
    {
        qCWarning(sila_cpp)
            << "Constructing a Validation Error with a generic message. "
               "Note that the SiLA 2 standard requires a description why "
               "the given parameter was invalid and how to resolve the "
               "error!";
    }
}

//=============================================================================
grpc::Status CValidationError::raise() const
{
    PIMPL_D(const CValidationError);
    const auto Error = d->makeErrorMessage();
    qCCritical(sila_cpp) << "A ValidationError occurred:" << *Error;
    return grpc::Status{grpc::StatusCode::ABORTED,
                        internal::base64Encode(Error->SerializeAsString())};
}

}  // namespace SiLA2

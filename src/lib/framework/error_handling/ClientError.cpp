/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ClientError.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   24.01.2020
/// \brief  Implementation of client related error handling methods
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/error_handling/ClientError.h>
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>
#include <sila_cpp/internal/Base64.h>

#include <grpcpp/impl/codegen/status.h>

using namespace std;
using namespace sila2::org::silastandard;

namespace SiLA2
{
//=============================================================================
bool hasError(const grpc::Status& Status)
{
    if (!Status.ok())
    {
        auto Error = SiLAError{};
        Error.ParseFromString(internal::base64Decode(Status.error_message()));
        switch (Error.error_case())
        {
        case SiLAError::kValidationError:
            qCCritical(sila_cpp) << "A Validation Error occurred:" << Error;
            break;
        case SiLAError::kDefinedExecutionError:
            qCCritical(sila_cpp)
                << "A Defined Execution Error occurred:" << Error;
            break;
        case SiLAError::kUndefinedExecutionError:
            qCCritical(sila_cpp)
                << "A Undefined Execution Error occurred:" << Error;
            break;
        case SiLAError::kFrameworkError:
            qCCritical(sila_cpp) << "A Framework Error occurred:" << Error;
            break;
        default:
            qCCritical(sila_cpp) << "Error during gRPC communication." << Status;
            break;
        }
        return true;
    }
    return false;
}
}  // namespace SiLA2

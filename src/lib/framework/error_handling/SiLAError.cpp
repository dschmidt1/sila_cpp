/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAError.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   27.01.2020
/// \brief  Implementation of the CSiLAError class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/error_handling/SiLAError.h>
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>

#include "SiLAError_p.h"

#include <google/protobuf/message.h>
#include <grpcpp/impl/codegen/status.h>

#include <string>
#include <typeinfo>
#include <utility>

using namespace std;
using namespace jbcoe;
using namespace sila2::org::silastandard;

namespace SiLA2
{
//=============================================================================
CSiLAError::PrivateImpl::PrivateImpl(string msg) : Message{move(msg)}
{
    // at least have a most generic error message
    if (Message.empty())
    {
        Message = "An error occurred while performing a SiLA related operation!";
    }
}

//=============================================================================
SiLAError* CSiLAError::PrivateImpl::makeErrorMessage() const
{
    return new SiLAError;
}

//=============================================================================
CSiLAError::CSiLAError(const string& Message)
    : d_ptr{make_polymorphic_value<PrivateImpl>(Message)}
{}

//=============================================================================
CSiLAError::CSiLAError(PrivateImplPtr priv) : d_ptr{std::move(priv)}
{}

//============================================================================
const char* CSiLAError::what() const noexcept
{
    PIMPL_D(const CSiLAError);
    static const auto ErrorString = d->makeErrorMessage()->DebugString();
    return ErrorString.c_str();
}
}  // namespace SiLA2

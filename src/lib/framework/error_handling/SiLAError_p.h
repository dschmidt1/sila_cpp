/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAError_p.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   27.01.2020
/// \brief  Declaration of the CSiLAError::PrivateImpl class
//============================================================================
#ifndef SILAERROR_P_H
#define SILAERROR_P_H

//=============================================================================
//                                  INCLUDES
//=============================================================================
#include <sila_cpp/global.h>

#include <string>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace sila2::org::silastandard
{
class SiLAError;
}

namespace SiLA2
{
/**
 * @brief Private data of the CSiLAError class - pimpl
 */
class CSiLAError::PrivateImpl
{
public:
    explicit PrivateImpl(std::string msg = {});

    /**
     * @brief Construct a SiLA Error protobuf message. Note that the caller takes
     * ownership of the returned message. The default implementation just creates
     * an empty SiLAError.
     *
     * @return SiLAError* A pointer to the SiLA Error protobuf message
     */
    [[nodiscard]] virtual sila2::org::silastandard::SiLAError* makeErrorMessage()
        const;

    std::string Message{};  ///< The error message
};

}  // namespace SiLA2

#endif  // SILAERROR_P_H

/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ExecutionError.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   09.07.2020
/// \brief  Implementation of the CExecutionError, CDefinedExecutionError,
/// CUndefinedExecutionError classes
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/error_handling/ExecutionError.h>
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>
#include <sila_cpp/internal/Base64.h>

#include "SiLAError_p.h"

#include <string>

using namespace std;
using namespace jbcoe;
using namespace sila2::org::silastandard;

namespace SiLA2
{
static const string UNDEFINED_DEFAULT_MESSAGE =
    "An undefined execution error occurred during command execution or property "
    "access.";
static const string DEFINED_DEFAULT_MESSAGE =
    "A defined execution error occurred during command execution or property "
    "access.";

/**
 * @brief Private data of the CExecutionError class - pimpl
 */
class CExecutionError::PrivateImpl : public CSiLAError::PrivateImpl
{
public:
    /**
     * @brief The ErrorType enum defines the different types of Execution Errors
     */
    enum ErrorType
    {
        DEFINED_EXECUTION_ERROR,
        UNDEFINED_EXECUTION_ERROR
    };

    /**
     * @brief C'tor for Defined Execution Error
     */
    PrivateImpl(std::string id, const std::string& msg)
        : CSiLAError::PrivateImpl{msg.empty() ? DEFINED_DEFAULT_MESSAGE : msg},
          Type{DEFINED_EXECUTION_ERROR},
          ErrorIdentifier{move(id)}
    {}

    /**
     * @brief C'tor for Undefined Execution Error
     */
    explicit PrivateImpl(const std::string& msg = {})
        : CSiLAError::PrivateImpl{msg.empty() ? UNDEFINED_DEFAULT_MESSAGE : msg},
          Type{UNDEFINED_EXECUTION_ERROR}
    {}

    /**
     * @brief Construct a SiLA Error protobuf message from the internal fields
     * @a Message and @a Identifier (if set). Note that the caller takes ownership
     * of the returned message.
     *
     * @return SiLAError* A pointer to the SiLA Error protobuf message
     */
    [[nodiscard]] SiLAError* makeErrorMessage() const override;

    ErrorType Type{};
    std::string ErrorIdentifier{};  ///< The error identifier of the Defined Error
};

///============================================================================
///                 CExecutionError::PrivateImpl implementation
///============================================================================
SiLAError* CExecutionError::PrivateImpl::makeErrorMessage() const
{
    auto Error = new SiLAError;
    if (Type == DEFINED_EXECUTION_ERROR)
    {
        auto EError = new DefinedExecutionError;
        EError->set_erroridentifier(ErrorIdentifier);
        EError->set_message(Message);
        Error->set_allocated_definedexecutionerror(EError);
    }
    else
    {
        auto EError = new UndefinedExecutionError;
        EError->set_message(Message);
        Error->set_allocated_undefinedexecutionerror(EError);
    }
    return Error;
}

///============================================================================
///                        CExecutionError implementation
///============================================================================
CExecutionError::CExecutionError(const std::string& Message)
    : CSiLAError{
        make_polymorphic_value<CSiLAError::PrivateImpl, PrivateImpl>(Message)}
{}

//============================================================================
CExecutionError::CExecutionError(const std::string& Identifier,
                                 const std::string& Message)
    : CSiLAError{make_polymorphic_value<CSiLAError::PrivateImpl, PrivateImpl>(
        Identifier, Message)}
{}

//============================================================================
grpc::Status CExecutionError::raise() const
{
    PIMPL_D(const CExecutionError);
    const auto Error = d->makeErrorMessage();
    qCCritical(sila_cpp) << "An ExecutionError occurred:" << *Error;
    return grpc::Status{grpc::StatusCode::ABORTED,
                        internal::base64Encode(Error->SerializeAsString())};
}

///============================================================================
///                    CDefinedExecutionError implementation
///============================================================================
CDefinedExecutionError::CDefinedExecutionError(const std::string& Identifier,
                                               const std::string& Message)
    : CExecutionError{Identifier, Message}
{
    if (Message.empty())
    {
        qCWarning(sila_cpp)
            << "Constructing a Defined Execution Error with a generic "
               "message. Note that the SiLA 2 standard requires a "
               "description about the error and should make proposals on "
               "how to resolve the error!";
    }
}

///============================================================================
///                   CUndefinedExecutionError implementation
///============================================================================
CUndefinedExecutionError::CUndefinedExecutionError(const std::string& Message)
    : CExecutionError{Message}
{
    if (Message.empty())
    {
        qCWarning(sila_cpp)
            << "Constructing an Undefined Execution Error with a generic "
               "message. Note that the SiLA 2 standard requires a "
               "description about the error and should make proposals on "
               "how to resolve the error!";
    }
}
}  // namespace SiLA2

file(READ "${FILE}" filedata)
set(MATCH_STRING "#define GOOGLE_PROTOBUF_INCLUDED_SiLAFramework_2eproto")
string(REPLACE "${MATCH_STRING}"
    "${MATCH_STRING}\n\n#include <sila_cpp/global.h>" filedata "${filedata}")
file(WRITE "${FILE}" "${filedata}")

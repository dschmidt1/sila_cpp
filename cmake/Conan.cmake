#
# Run conan
#
# run_conan_internal (<MODE>)
#
#  MODE - either "src" or "test" to indicate whether we are currently building
#         the actual sila_cpp lib ("src") or just its tests which require
#         different dependencies
#
macro(run_conan_internal MODE)
if(CONAN_EXPORTED) # in conan local cache
    # standard conan installation, deps will be defined in conanfile.py
    # and not necessary to call conan again, conan is already running
    include(${CMAKE_CURRENT_BINARY_DIR}/conanbuildinfo.cmake)
    conan_basic_setup(TARGETS)
else() # in user space (i.e. cmake was called with -DSILA_CPP_USE_CONAN)
    # Download the conan.cmake file automatically
    if(NOT EXISTS "${CMAKE_BINARY_DIR}/conan.cmake")
        message(STATUS "Downloading conan.cmake from https://github.com/conan-io/cmake-conan")
        file(DOWNLOAD "https://raw.githubusercontent.com/conan-io/cmake-conan/v0.15/conan.cmake"
                    "${CMAKE_BINARY_DIR}/conan.cmake")
    endif()

    include(${CMAKE_BINARY_DIR}/conan.cmake)

    set(CONAN_PROFILE $<MINGW:PROFILE ${CMAKE_CURRENT_LIST_DIR}/../.conan/profiles/msys2_mingw>)

    if(${MODE} STREQUAL "src")
        # Add remotes
        conan_add_remote(NAME bincrafters URL https://api.bintray.com/conan/bincrafters/public-conan)
        conan_add_remote(NAME inexorgame URL https://api.bintray.com/conan/inexorgame/inexor-conan)
        # Run conan install so that we find all required dependencies
        conan_cmake_run(
            CONANFILE ../conanfile.py
            ${CONAN_PROFILE}
            BASIC_SETUP CMAKE_TARGETS # individual targets to link to
            BUILD missing
            )
    elseif(${MODE} STREQUAL "test")
        if (NOT CMAKE_BUILD_TYPE)
            set(CMAKE_BUILD_TYPE Release)
        endif()
        # Add remote
        conan_add_remote(NAME catchorg URL https://api.bintray.com/conan/catchorg/Catch2)
        # Run conan install so that we find all required dependencies
        conan_cmake_run(
            ${CONAN_PROFILE}
            REQUIRES Catch2/2.11.1@catchorg/stable
            BASIC_SETUP CMAKE_TARGETS # individual targets to link to
            BUILD missing
        )
    endif()
endif()
endmacro()

macro(run_conan)
run_conan_internal("src")
endmacro()

macro(run_conan_for_test)
run_conan_internal("test")
endmacro()

file(READ "${FILE}" filedata)
# better: use regex replace
string(REPLACE "#include \"SiLAFramework.pb.h\""
    "#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>" filedata "${filedata}")
file(WRITE "${FILE}" "${filedata}")

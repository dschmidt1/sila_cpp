/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2019 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// file   HelloSiLA2Server.cpp
/// author Florian Meinicke (florian.meinicke@cetoni.de)
/// date   20.12.2019
/// brief  Minimal HelloSiLA2 example
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/FeatureID.h>
#include <sila_cpp/common/SSLCredentials.h>
#include <sila_cpp/common/ServerAddress.h>
#include <sila_cpp/common/ServerInformation.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/server/SiLAServer.h>

#include <QCommandLineParser>
#include <QCoreApplication>

// include SiLA features
#include "GreetingProvider/GreetingProviderImpl.h"
#include "TemperatureController/TemperatureControllerImpl.h"

using namespace std;
using SiLA2::CFeatureID;
using SiLA2::CServerAddress;
using SiLA2::CServerInformation;
using SiLA2::CSiLAServer;
using SiLA2::CSSLCredentials;
using sila2::org::silastandard::examples::greetingprovider::v1::GreetingProvider;
using sila2::org::silastandard::examples::temperaturecontroller::v1::
    TemperatureController;

/**
 * @brief This is a sample Hello SiLA 2 service
 */
class HelloSiLA2Server : public CSiLAServer
{
public:
    /**
     * @brief C'tor
     */
    HelloSiLA2Server(const CServerInformation& ServerInfo,
                     const CServerAddress& Address)
        : CSiLAServer{ServerInfo, Address}
    {
        qDebug() << "Registering features...";
        registerFeature(new CGreetingProviderImpl(this),
                        CFeatureID{GreetingProvider::service_full_name()},
                        CGreetingProviderImpl::featureDefinition());
        registerFeature(new CTemperatureControllerImpl(this),
                        CFeatureID{TemperatureController::service_full_name()},
                        CTemperatureControllerImpl::featureDefinition());
    }
};

/**
 * @brief Looking for command line arguments
 *
 * @return pair<CServerInformation, CServerAddress, CSSLCredentials> A pair with
 * the server information (name, type, ...) and the server's address (IP and port)
 */
tuple<CServerInformation, const CServerAddress> parseCommandLine()
{
    auto Parser = HelloSiLA2Server::commandLineParser();
    Parser->setApplicationDescription("A SiLA2 service: HelloSiLA2");
    Parser->addOptions({{{"s", "server-name"},
                         "The name of the SiLA server",
                         "name",
                         "HelloSiLA2"},
                        {{"t", "server-type"},
                         "The type of the SiLA server",
                         "type",
                         "UnknownServerType"},
                        {{"d", "description"},
                         "The description of the SiLA server",
                         "description text",
                         "This is a HelloSiLA2 test service"},
                        {{"p", "server-port"},
                         "The port on which the SiLA server should run",
                         "port",
                         "50051"}});

    Parser->process(QCoreApplication::arguments());

    return {
        {Parser->value("server-name"), Parser->value("server-type"),
         Parser->value("description"), QCoreApplication::applicationVersion()},
        {"[::]", Parser->value("server-port")}};
}

//============================================================================
int main(int argc, char* argv[])
{
    QCoreApplication App{argc, argv};
    QCoreApplication::setApplicationName("HelloSiLA2Server");
    QCoreApplication::setApplicationVersion("0.1.0");

    const auto [ServerInfo, ServerAddress] = parseCommandLine();

    auto SiLAServer = HelloSiLA2Server{ServerInfo, ServerAddress};
    SiLAServer.run();

    return 0;
}

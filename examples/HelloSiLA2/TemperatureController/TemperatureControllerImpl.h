/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   TemperatureControllerImpl.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   07.05.2020
/// \brief  Declaration of the CTemperatureControllerImpl class
//============================================================================
#ifndef TEMPERATURECONTROLLERIMPL_H
#define TEMPERATURECONTROLLERIMPL_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/framework/data_types/SiLAReal.h>
#include <sila_cpp/server/SiLAFeature.h>
#include <sila_cpp/server/command/ObservableCommand.h>
#include <sila_cpp/server/property/ObservableProperty.h>

#include "TemperatureController.grpc.pb.h"

/**
 * @brief The CTemperatureControllerImpl class implements the
 * TemperatureController feature
 */
class CTemperatureControllerImpl final :
    public SiLA2::CSiLAFeature<sila2::org::silastandard::examples::
                                   temperaturecontroller::v1::TemperatureController>
{
    // Using declarations for the Feature's Commands and Properties
    using ControlTemperatureCommand = SiLA2::CObservableCommandManager<
        &CTemperatureControllerImpl::RequestControlTemperature,
        &CTemperatureControllerImpl::RequestControlTemperature_Info,
        &CTemperatureControllerImpl::RequestControlTemperature_Result>;
    using ControlTemperatureWrapper = SiLA2::CObservableCommandWrapper<
        sila2::org::silastandard::examples::temperaturecontroller::v1::
            ControlTemperature_Parameters,
        sila2::org::silastandard::examples::temperaturecontroller::v1::
            ControlTemperature_Responses>;
    using CurrentTemperatureProperty = SiLA2::CObservablePropertyWrapper<
        SiLA2::CReal,
        &CTemperatureControllerImpl::RequestSubscribe_CurrentTemperature>;

public:
    /**
     * @brief C'tor
     *
     * @param parent The SiLA server instance that contains this Feature
     */
    explicit CTemperatureControllerImpl(SiLA2::CSiLAServer* parent);

    /**
     * @brief Control Temperature Command
     *
     * @details Control the temperature gradually to a set target.
     * It is RECOMMENDED to use an oscillation free control system.
     *
     * @param Command The current Control Temperature Command Execution Wrapper
     * Contains the @b TargetTemperature Command Parameter (The target temperature
     * that the server will try to reach. Note that the command might be completed
     * at a temperature that it evaluates to be close enough. If the temperature
     * cannot be reached, a 'Temperature Not Reachable' error will be thrown.)
     *
     * @return Empty ControlTemperature_Responses
     *
     * @throw Validation Error if Temperature is out of range (277.0 K, 363.0 K]
     * @throw 'Temperature Not Reachable' Error if Temperature cannot be reached
     * @throw 'Control Temperature Interrupted' Error on interruption
     */
    sila2::org::silastandard::examples::temperaturecontroller::v1::
        ControlTemperature_Responses
        ControlTemperature(ControlTemperatureWrapper* Command);

private:
    double m_TargetTemp{};  ///< `ControlTemperature` target in Kelvin
    ControlTemperatureCommand
        m_ControlTempCommand;  ///< Manager for the `ControlTemperature` Command
    CurrentTemperatureProperty
        m_CurrentTempProperty;  ///< Current temperature in Kelvin (default: 20°C)
};

#endif  // TEMPERATURECONTROLLERIMPL_H

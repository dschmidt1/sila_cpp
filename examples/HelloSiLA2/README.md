# Hello SiLA 2 example
This example shows how you can implement your own SiLA 2 client/server pair using the SiLA C++ reference implementation.

## Requirements
You first need to install the `sila_cpp` library on your system.
Follow the instructions in the [README.md](../../README.md#building-with-cmake) in the repository's root directory to find out how you can build and install `sila_cpp`.
If you built `sila_cpp` with the `SILA_CPP_BUILD_EXAMPLES` option set to `ON`, you can skip to the [Running](#running) section and start playing around with the server and client.

## Building
To build the server and client executables simply run the following commands from this directory
```cmd
> mkdir build & cd build
> cmake .. -DCMAKE_BUILD_TYPE=Release
> cmake --build .
```

> #### Note:
> Most of the boiler-plate code has been generated from the `service_description.json` file and the `.sila.xml` files using the `silacodegenerator` from the [`sila_python`](https://www.gitlab.com/SiLA2/sila_python) implementation in this directory.
> The code generator is able to produce C++ code, as well, by passing the `--cpp` flag, e.g.:
> ```cmd
> > silacodegenerator --cpp -b .
> ```
> See the dedicated [wiki page](https://gitlab.com/SiLA2/sila_cpp/-/wikis/Tutorial/Code-Generator) for more information about using the `silacodegenerator` for `sila_cpp` projects.

## Running
To run the Hello SiLA 2 server/client pair open a terminal and change into the `bin` directory that contains the application executables.
Start the server with:
```shell
$ ./HelloSiLA2_server
```
or for Windows with
```cmd
> HelloSiLA2_server.exe
```
You should see the following output:
```
[sila_cpp] INFO: Starting SiLA2 server with server name "HelloSiLA2"
[default] DEBUG: Registering features...
[sila_cpp] INFO: Registering feature "org.silastandard.examples.GreetingProvider.v1"
[sila_cpp] INFO: Registering feature "org.silastandard.examples.TemperatureController.v1"
[sila_cpp] INFO: Server listening on [::]:50051
```

Open another terminal and start the client as well.
```shell
$ ./HelloSiLA2_client
```
respectively
```cmd
> HelloSiLA2_client.exe
```

You should see that it connected to the server and starts calling the Commands and Properties:
```
[sila_cpp] INFO: Connected to SiLA Server "HelloSiLA2" (UUID: "...") running in version "0.0.1"
 Service description: "This is a HelloSiLA2 test service"
[default] INFO: --- Calling unobservable command SayHello
...
```

The server should also log these calls:
```
[sila_cpp] INFO: --- Unobservable Property ServerName requested
[sila_cpp] INFO: --- Unobservable Property ServerVersion requested
[sila_cpp] INFO: --- Unobservable Property ServerDescription requested
[sila_cpp] INFO: --- Unobservable Property ServerUUID requested
[sila_cpp] INFO: --- Unobservable Property ImplementedFeatures requested
[sila_cpp] INFO: --- Unobservable Command SayHello called
[sila_cpp] INFO: Executing Unobservable Command
...
```

### SiLA Browser
You can also use the [SiLA Browser](https://unitelabs.ch/technology/plug-and-play/sila-browser/) from UniteLabs to interactively play with the `HelloSiLA2` server.
See the [SiLA Browser QuickStart](https://sila2.gitlab.io/sila_base/docs/sila-browser-quickstart) for more information.

### Command Line Arguments
Both the server and client application accept a handful of command line arguments to e.g. control the port on which the server should run or what name the server should have.
The following arguments are available for the server:
```
Usage: HelloSiLA2_server.exe [options]
A SiLA2 service: HelloSiLA2

Options:
  -?, -h, --help                        Displays this help.
  -v, --version                         Displays version information.
  -s, --server-name <name>              The name of the SiLA server
  -t, --server-type <type>              The type of the SiLA server
  -d, --description <description text>  The description of the SiLA server
  -p, --server-port <port>              The port on which the SiLA server
                                        should run
```
And these arguments are available for the client:
```
Usage: HelloSiLA2_client.exe [options]
A SiLA2 client: HelloSiLA2

Options:
  -?, -h, --help                        Displays this help.
  -v, --version                         Displays version information.
  -a, --server-ip-address <ip-address>  The IP address of the SiLA server to
                                        connect with
  -p, --server-port <port>              The port on which the SiLA server
                                        should run
```

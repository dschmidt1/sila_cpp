/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2019 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// file   HelloSiLA2Client.cpp
/// author Florian Meinicke (florian.meinicke@cetoni.de)
/// date   20.12.2019
/// brief  Minimal HelloSiLA2 example
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/SiLAClient.h>
#include <sila_cpp/common/ServerAddress.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/data_types.h>
#include <sila_cpp/framework/error_handling/ClientError.h>

#include "GreetingProvider.grpc.pb.h"
#include "TemperatureController.grpc.pb.h"

#include <QCommandLineParser>
#include <QCoreApplication>
#include <QThread>

#include <grpcpp/grpcpp.h>

using namespace std;
using SiLA2::CServerAddress;
using SiLA2::CSiLAClient;

using namespace sila2::org::silastandard;
using namespace sila2::org::silastandard::examples::greetingprovider::v1;
using namespace sila2::org::silastandard::examples::temperaturecontroller::v1;

/**
 * @brief This is a sample Hello SiLA 2 service
 */
class HelloSiLA2Client : public CSiLAClient
{
public:
    /**
     * @brief C'tor
     */
    explicit HelloSiLA2Client(const CServerAddress& Address)
        : CSiLAClient{Address},
          m_GreetingProviderStub{GreetingProvider::NewStub(channel())},
          m_TemperatureControllerStub{TemperatureController::NewStub(channel())}
    {}

    /**
     * @brief Call the unobservable command "Say Hello" on the server
     *
     * @param Parameters The command parameters containing the name, SayHello
     * shall use to greet.
     * @return The command response containing the greeting string
     */
    SayHello_Responses SayHello(const SayHello_Parameters& Parameters = {}) const
    {
        grpc::ClientContext Context;
        SayHello_Responses Responses;

        qInfo() << "--- Calling unobservable command SayHello";
        const auto Status =
            m_GreetingProviderStub->SayHello(&Context, Parameters, &Responses);
        if (!SiLA2::hasError(Status))
        {
            qInfo() << "SayHello response:" << Responses;
        }

        return Responses;
    }

    /**
     * @brief Request the unobservable property "Start Year" from the server
     *
     * @return The value of the property StartYear
     */
    Get_StartYear_Responses Get_StartYear() const
    {
        grpc::ClientContext Context;
        Get_StartYear_Responses Responses;

        qInfo() << "--- Requesting unobservable property StartYear";
        const auto Status =
            m_GreetingProviderStub->Get_StartYear(&Context, {}, &Responses);
        if (!SiLA2::hasError(Status))
        {
            qInfo() << "Get_StartYear response:" << Responses;
        }

        return Responses;
    }

    /**
     * @brief Request command execution of the observable command
     * 'ControlTemperature' on the server. This function also automatically calls
     * @a ControlTemperature_Info after the execution has been requested.
     *
     * @param Parameters The command parameters containing the TargetTemperature
     * the server should try to reach
     */
    CommandExecutionUUID ControlTemperature(
        const ControlTemperature_Parameters& Parameters = {}) const
    {
        grpc::ClientContext Context;
        CommandConfirmation Confirmation;

        qInfo() << "--- Calling observable command ControlTemperature";
        const auto Status = m_TemperatureControllerStub->ControlTemperature(
            &Context, Parameters, &Confirmation);
        if (SiLA2::hasError(Status))
        {
            return {};
        }
        qInfo() << Confirmation;
        ControlTemperature_Info(Confirmation.commandexecutionuuid());

        return Confirmation.commandexecutionuuid();
    }

    /**
     * @brief Subscribe to the command execution info for the 'ControlTemperature'
     * command. This function also automatically calls
     * @a ControlTemperature_Result after the execution finished successfully.
     *
     * @param UUID The UUID of the command execution
     */
    void ControlTemperature_Info(const CommandExecutionUUID& UUID) const
    {
        grpc::ClientContext Context;
        ExecutionInfo Info;

        qInfo() << "--- Requesting status info about ControlTemperature command "
                   "execution";

        const auto Reader =
            m_TemperatureControllerStub->ControlTemperature_Info(&Context, UUID);
        while (Reader->Read(&Info))
        {
            qInfo() << Info;
        }

        const auto Status = Reader->Finish();
        if (!SiLA2::hasError(Status))
        {
            ControlTemperature_Result(UUID);
        }
    }

    /**
     * @brief Request the final result for the ControlTemperature command
     *
     * @param UUID The UUID of the command execution
     * @return Empty command response
     */
    ControlTemperature_Responses ControlTemperature_Result(
        const CommandExecutionUUID& UUID) const
    {
        grpc::ClientContext Context;
        ControlTemperature_Responses Responses;

        qInfo() << "--- Requesting final result for ControlTemperature command "
                   "execution";

        const auto Status =
            m_TemperatureControllerStub->ControlTemperature_Result(&Context, UUID,
                                                                   &Responses);
        if (!SiLA2::hasError(Status))
        {
            qInfo() << Responses;
        }

        return Responses;
    }

    void Subscribe_CurrentTemperature() const
    {
        grpc::ClientContext Context;
        Subscribe_CurrentTemperature_Responses Responses;

        qInfo() << "--- Subscribing to observable property CurrentTemperature";
        const auto Reader =
            m_TemperatureControllerStub->Subscribe_CurrentTemperature(&Context,
                                                                      {});

        while (Reader->Read(&Responses))
        {
            qInfo() << Responses;
        }

        SiLA2::hasError(Reader->Finish());
    }

private:
    unique_ptr<GreetingProvider::Stub> m_GreetingProviderStub;
    unique_ptr<TemperatureController::Stub> m_TemperatureControllerStub;
};

//============================================================================
int main(int argc, char* argv[])
{
    QCoreApplication App{argc, argv};
    QCoreApplication::setApplicationName("HelloSiLA2Client");
    QCoreApplication::setApplicationVersion("0.1.0");

    auto SiLAClient = HelloSiLA2Client{{"127.0.0.1"s, "50051"s}};
    // GreetingProvider
    auto SayHelloParam = SayHello_Parameters{};
    // should yield a validation error because of missing Name parameter
    SiLAClient.SayHello();
    // should yield a default validation error because Name == 'error'
    SayHelloParam.set_allocated_name(SiLA2::CString{"error"}.toProtoMessagePtr());
    SiLAClient.SayHello(SayHelloParam);
    // actual valid command call
    SayHelloParam.set_allocated_name(SiLA2::CString{"World"}.toProtoMessagePtr());
    SiLAClient.SayHello(SayHelloParam);
    SiLAClient.Get_StartYear();

    // TemperatureController
    auto Thread =
        QThread::create([&]() { SiLAClient.Subscribe_CurrentTemperature(); });
    Thread->start();
    auto ControlTempParam = ControlTemperature_Parameters{};
    // should yield a validation error because of missing TargetTemp parameter
    SiLAClient.ControlTemperature();
    // should yield a framework error because of wrong UUID
    SiLAClient.ControlTemperature_Info(CommandExecutionUUID());
    // valid command call
    ControlTempParam.set_allocated_targettemperature(
        SiLA2::CReal{300}.toProtoMessagePtr());
    const auto UUID = SiLAClient.ControlTemperature(ControlTempParam);

    qDebug() << "Waiting 20 seconds...";
    QThread::sleep(20);

    // should yield a Framework error because the Lifetime has already expired
    SiLAClient.ControlTemperature_Result(UUID);

    Thread->wait();
    return 0;
}

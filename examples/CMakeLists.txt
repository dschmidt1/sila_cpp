cmake_minimum_required(VERSION 3.13)
project(sila_cpp_examples)

# HelloSiLA2 example
add_subdirectory(HelloSiLA2)
